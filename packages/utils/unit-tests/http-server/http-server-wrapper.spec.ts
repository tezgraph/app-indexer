import http from 'http';
import { anyFunction, anyNumber, anyString, instance, mock, verify, when } from 'ts-mockito';

import { describeMemberFactory } from '../../../../test-utilities/mocks';
import { HttpServerWrapper } from '../../src/http-server/http-server-wrapper';

describe(HttpServerWrapper.name, () => {
    let target: HttpServerWrapper;
    let server: http.Server;

    beforeEach(() => {
        server = mock();
        target = new HttpServerWrapper(instance(server));
    });

    const describeMember = describeMemberFactory<typeof target>();

    describeMember('listen', () => {
        const act = async () => target.listen(66, 'indexer.host');

        it('should listen on given host and port', async () => {
            when(server.listen(anyNumber(), anyString(), anyFunction())).thenCall((_p, _h, f) => f());

            await act();

            verify(server.listen(66, 'indexer.host', anyFunction()));
        });

        it.each(['thrown', 'returned in callback'])('should throw if an error %s', async (desc) => {
            const testError = new Error('wtf');
            if (desc === 'thrown') {
                when(server.listen(anyNumber(), anyString(), anyFunction())).thenThrow(testError);
            } else {
                when(server.once('error', anyFunction())).thenCall((_e, f) => f(testError));
            }

            await expect(act).rejects.toThrow(testError);
        });
    });

    describeMember('close', () => {
        const act = async () => target.close();

        it('should close the server', async () => {
            when(server.close(anyFunction())).thenCall(f => f());

            await act();

            verify(server.close(anyFunction()));
        });

        it('should throw if an error returned in callback', async () => {
            const testError = new Error('wtf');
            when(server.close(anyFunction())).thenCall(f => f(testError));

            await expect(act).rejects.toThrow(testError);
        });
    });

    describeMember('getConnections', () => {
        const act = async () => target.getConnections();

        it('should get connection count', async () => {
            when(server.getConnections(anyFunction())).thenCall(f => f(null, 66));

            const count = await act();

            expect(count).toBe(66);
        });

        it('should throw if an error returned in callback', async () => {
            const testError = new Error('wtf');
            when(server.getConnections(anyFunction())).thenCall(f => f(testError, 66));

            await expect(act).rejects.toThrow(testError);
        });
    });
});
