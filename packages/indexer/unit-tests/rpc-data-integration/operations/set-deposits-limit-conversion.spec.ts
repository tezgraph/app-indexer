import { BigNumber } from 'bignumber.js';

import * as samples from './common-samples';
import { OperationKind, OperationResultStatus, SetDepositsLimitOperation } from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    kind: 'set_deposits_limit',
    source: 'tz1gg5bjopPcr9agjamyu9BbXKLibNc2rbAq',
    fee: '332',
    counter: '1379225',
    gas_limit: '1000',
    storage_limit: '0',
    limit: '29600000000',
    metadata: {
        balance_updates: samples.balanceUpdates.input,
        internal_operation_results: samples.internalOperations.input,
        operation_result: {
            status: 'applied',
            consumed_gas: '1000',
            consumed_milligas: '1000000',
        },
    },
};

const expected: SetDepositsLimitOperation = {
    balanceUpdates: samples.balanceUpdates.getExpected('ops/0[set_deposits_limit]/metadata'),
    counter: new BigNumber(1379225),
    fee: new BigNumber(332),
    gasLimit: new BigNumber(1000),
    internalOperations: samples.internalOperations.getExpected('ops/0[set_deposits_limit]/metadata'),
    isInternal: false,
    kind: OperationKind.SetDepositsLimit,
    limit: new BigNumber(29600000000),
    result: {
        consumedGas: new BigNumber(1000),
        consumedMilligas: new BigNumber(1000000),
        rpcData: input.metadata.operation_result as any,
        status: OperationResultStatus.Applied,
        uid: 'ops/0[set_deposits_limit]/metadata/operation_result',
    },
    rpcData: input as any,
    sourceAddress: 'tz1gg5bjopPcr9agjamyu9BbXKLibNc2rbAq',
    storageLimit: new BigNumber(0),
    uid: 'ops/0[set_deposits_limit]',
};

shouldConvertRpcOperation(expected.kind, { input, expected });
