/* eslint-disable @typescript-eslint/ban-types */
import { getEnumValues } from '../../../../../utils/src/public-api';
import { OperationResultStatus } from '../../../../src/rpc-data/operations/common/operation-result/operation-result-status';
import { isApplied } from '../../../../src/rpc-data/operations/narrowed/applied';
import { OperationWithResult } from '../../../../src/rpc-data/operations/narrowed/operation-with-result';

describe(isApplied.name, () => {
    it.each([
        [OperationResultStatus.Applied, true],
        ...getEnumValues(OperationResultStatus).filter(s => s !== OperationResultStatus.Applied).map(s => [s, false] as const),
    ] as const)('should narrow %s to %s', (status, expectedNarrowing) => {
        const isNarrowed = isApplied({ result: { status } } as OperationWithResult);

        expect(isNarrowed).toBe(expectedNarrowing);
    });
});
