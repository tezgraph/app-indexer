import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { DelegationResult } from './delegation-result';
import { BaseMainOperationWithResult } from '../common/main-operation/base-main-operation-with-result';
import { OperationKind } from '../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcDelegationOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultDelegation>;

/**
 * It allows users to {@link https://tezos.gitlab.io/introduction/howtorun.html#delegating-coins | delegate their stake}
 * to a {@link https://tezos.gitlab.io/mumbai/glossary.html#delegate | delegate} (a *baker*), or to register themselves as delegates.
 * @category RPC Block Data
 */
export interface DelegationOperation extends BaseMainOperationWithResult<TaquitoRpcDelegationOperation> {
    readonly kind: OperationKind.Delegation;

    // Common delegation properties:
    readonly delegateAddress: string | null;
    readonly result: DelegationResult;
}
