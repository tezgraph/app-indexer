import * as taquitoRpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { primitives } from '../../primitives';
import { RpcConvertOptions } from '../../rpc-convert-options';
import { RpcObjectConverter } from '../../rpc-converter';
import { Ballot, BallotOperation, OperationKind, TaquitoRpcBallotOperation } from '../../rpc-data-interfaces';

@singleton()
export class BallotOperationConverter extends RpcObjectConverter<TaquitoRpcBallotOperation, BallotOperation, RpcConvertOptions> {
    protected override convertObject(
        rpcData: TaquitoRpcBallotOperation,
        uid: string,
        _options: RpcConvertOptions,
    ): BallotOperation {
        return Object.freeze<BallotOperation>({
            ballot: primitives.enum.convertProperty(rpcData, 'ballot', uid, ballotMapping),
            kind: OperationKind.Ballot,
            period: primitives.integer.convertProperty(rpcData, 'period', uid),
            proposal: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'proposal', uid),
            rpcData,
            sourceAddress: primitives.address.convertProperty(rpcData, 'source', uid),
            uid,
        });
    }
}

const ballotMapping = Object.freeze<Record<taquitoRpc.BallotVote, Ballot>>({
    nay: Ballot.Nay,
    pass: Ballot.Pass,
    yay: Ballot.Yay,
});
