import { Block, IndexedBlock, IndexingCycleHandler } from '@tezos-dappetizer/indexer';

import { IntContextData } from './int-context-data';
import { IntDbContext, verifyContextDataFlow } from './int-db-context';

export class IntIndexingCycleHandler implements IndexingCycleHandler<IntDbContext, IntContextData> {
    private blockCounter = 0;

    async createContextData(block: Block, dbContext: IntDbContext): Promise<IntContextData> {
        if (dbContext.blockLevel !== undefined || dbContext.contextData !== undefined) {
            throw new Error('Block and context data level should be undefined at this stage.');
        }

        dbContext.blockLevel = block.level;
        dbContext.contextData = { foo: `foo-${block.hash}` };

        dbContext.executedMethods.push({
            method: this.getMethodName('createContextData'),
            blockHash: block.hash,
        });

        this.blockCounter++;
        if (this.blockCounter === 3) {
            throw new Error('Simulated error to get rollback.');
        }

        return Promise.resolve(dbContext.contextData); // To test async.
    }

    beforeIndexersExecute(block: Block, dbContext: IntDbContext, contextData: IntContextData): void {
        verifyContextDataFlow(dbContext, contextData);

        dbContext.executedMethods.push({
            method: this.getMethodName('beforeIndexersExecute'),
            blockHash: block.hash,
            contextData,
        });
    }

    async afterIndexersExecuted(block: Block, dbContext: IntDbContext, contextData: IntContextData): Promise<void> {
        verifyContextDataFlow(dbContext, contextData);

        dbContext.executedMethods.push({
            method: this.getMethodName('afterIndexersExecuted'),
            blockHash: block.hash,
            contextData,
        });
        return Promise.resolve(); // To test async.
    }

    afterBlockIndexed(block: Block, contextData: IntContextData): void {
        // Don't push to executedMethods b/c ouput JSON was already written.
        if (!contextData.foo.endsWith(block.hash)) {
            throw new Error('The context data does not correspond to the block.');
        }
    }

    rollBackOnReorganization(_blockToRollBack: IndexedBlock, _dbContext: IntDbContext): void {
        throw new Error('Should not be executed.');
    }

    private getMethodName(methodName: keyof typeof this & string): string {
        return `${this.constructor.name}.${methodName}`;
    }
}
