/**
 * Configures health reporting.
 * This is used as a part of {@link "@tezos-dappetizer/database".DappetizerConfigUsingDb}.
 * @example Sample usage (all properties specified explicitly) within `dappetizer.config.ts` file:
 * ```typescript
 * import { DappetizerConfigUsingDb } from '@tezos-dappetizer/database';
 *
 * const config: DappetizerConfigUsingDb = {
 *     health: {
 *         healthStatusCacheSeconds: 5,
 *     },
 *     ... // Other config parts.
 * };
 *
 * export default config;
 * ```
 * @category Dappetizer Config
 */
export interface HealthDappetizerConfig {
    /**
     * Configures how long is the status of respective (which support caching) health checks cached.
     * @default `5`
     */
    healthStatusCacheSeconds?: number;
}
