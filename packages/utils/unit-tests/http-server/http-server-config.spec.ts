import { StrictOmit } from 'ts-essentials';

import { describeMember, verifyPropertyNames } from '../../../../test-utilities/mocks';
import { DappetizerConfig } from '../../../indexer/src/config/dappetizer-config';
import { ConfigObjectElement } from '../../src/configuration/public-api';
import { ENABLED_PROPERTY, HttpServerConfig } from '../../src/http-server/http-server-config';
import { HttpServerDappetizerConfig } from '../../src/http-server/http-server-dappetizer-config';

type HttpServerConfigProperties = StrictOmit<HttpServerConfig, 'getDiagnostics'>;

describe(HttpServerConfig.name, () => {
    function act(thisJson: DappetizerConfig['httpServer']) {
        const rootJson = { [HttpServerConfig.ROOT_NAME]: thisJson };
        return new HttpServerConfig(new ConfigObjectElement(rootJson, 'file', '$'));
    }

    it('should be created if all values specified', () => {
        const thisJson = {
            enabled: true,
            host: '6.7.8.9',
            port: 123,
        };

        const config = act(thisJson);

        expect(config).toEqual<HttpServerConfigProperties>({
            value: {
                host: '6.7.8.9',
                port: 123,
            },
        });
    });

    it.each([
        ['no root element', undefined],
        ['empty root element', {}],
    ])('should be created with defaults if %s', (_desc, thisJson) => {
        const config = act(thisJson);

        expect(config).toEqual<HttpServerConfigProperties>({
            value: null,
        });
    });

    it('should expose its properties in DappetizerConfig', () => {
        expect(HttpServerConfig.ROOT_NAME).toBe<keyof DappetizerConfig>('httpServer');

        const config = act({ enabled: true });
        verifyPropertyNames<HttpServerDappetizerConfig>(config.value!, ['host', 'port']);
        expect(ENABLED_PROPERTY).toBe<keyof HttpServerDappetizerConfig>('enabled');
    });

    describeMember<HttpServerConfig>('getDiagnostics', () => {
        it('should return config if enabled', () => {
            const config = act({ enabled: true });

            const [, diagnostics] = config.getDiagnostics();

            expect(diagnostics).toEqual(config.value);
        });

        it(`should return 'disabled' string if disabled`, () => {
            const config = act({ enabled: false });

            const [, diagnostics] = config.getDiagnostics();

            expect(diagnostics).toBe('disabled');
        });
    });
});
