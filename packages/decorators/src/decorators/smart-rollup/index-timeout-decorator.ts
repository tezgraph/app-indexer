import { createDecorator } from '../../helpers/decorator-accessors';

/**
 * The decorator for a method of a contract indexer to index its transactions with the specified entrypoint.
 * @example Demonstrates the method signature:
 * ```typescript
 * import { DbContext } from '@tezos-dappetizer/database';
 * import { indexTimeout } from '@tezos-dappetizer/decorators';
 * import { Applied, SmartRollupTimeoutIndexingContext } from '@tezos-dappetizer/indexer';
 *
 * class YourSmartRollupIndexer {
 *     @indexTimeout()
 *     indexMyRollupTimeouted(
 *         operation: Applied<SmartRollupTimeoutOperation>,
 *         dbContext: TDbContext,
 *         indexingContext: SmartRollupTimeoutIndexingContext<TContextData, TRollupData>,
 *     ): void | PromiseLike<void> {
 *         // Your indexing code.
 *     }
 * }
 * ```
 * @remark This is based on {@link "@tezos-dappetizer/indexer".SmartRollupIndexer.indexTimeout | SmartRollupIndexer.indexTimeout(...)}.
 * @experimental
 */
export function indexTimeout(): MethodDecorator {
    return createDecorator(indexTimeout);
}
