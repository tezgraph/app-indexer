import * as taquitoRpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { DelegationResultConverter } from './delegation-result-converter';
import { primitives } from '../../../primitives';
import { RpcConvertOptions } from '../../../rpc-convert-options';
import { RpcObjectConverter } from '../../../rpc-converter';
import { DelegationInternalOperation, OperationKind, TaquitoRpcInternalOperation } from '../../../rpc-data-interfaces';
import {
    CommonInternalOperationConverter,
    guardKind,
} from '../../common/internal-operation/common-internal-operation-converter';

@singleton()
export class DelegationInternalOperationConverter
    extends RpcObjectConverter<TaquitoRpcInternalOperation, DelegationInternalOperation, RpcConvertOptions> {
    constructor(
        private readonly commonInternalOperationConverter: CommonInternalOperationConverter,
        private readonly resultConverter: DelegationResultConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcInternalOperation,
        uid: string,
        _options: RpcConvertOptions,
    ): DelegationInternalOperation {
        guardKind(rpcData, taquitoRpc.OpKind.DELEGATION, uid);

        const commonProperties = this.commonInternalOperationConverter.convert(rpcData, uid);
        const result = this.resultConverter.convertProperty(rpcData, 'result', uid);

        return Object.freeze<DelegationInternalOperation>({
            ...commonProperties,
            delegateAddress: primitives.address.convertNullableProperty(rpcData, 'delegate', uid),
            kind: OperationKind.Delegation,
            result,
        });
    }
}
