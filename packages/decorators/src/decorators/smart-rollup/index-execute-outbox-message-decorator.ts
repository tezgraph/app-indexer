import { createDecorator } from '../../helpers/decorator-accessors';

/**
 * The decorator for a method of a contract indexer to index its transactions with the specified entrypoint.
 * @example Demonstrates the method signature:
 * ```typescript
 * import { DbContext } from '@tezos-dappetizer/database';
 * import { indexExecuteOutboxMessage } from '@tezos-dappetizer/decorators';
 * import { Applied, SmartRollupExecuteOutboxMessageIndexingContext } from '@tezos-dappetizer/indexer';
 *
 * class YourSmartRollupIndexer {
 *     @indexExecuteOutboxMessage()
 *     indexMyRollupExecuteOutboxMessageed(
 *         operation: Applied<SmartRollupExecuteOutboxMessageOperation>,
 *         dbContext: TDbContext,
 *         indexingContext: SmartRollupExecuteOutboxMessageIndexingContext<TContextData, TRollupData>,
 *     ): void | PromiseLike<void> {
 *         // Your indexing code.
 *     }
 * }
 * ```
 * @remark This is based on {@link "@tezos-dappetizer/indexer".SmartRollupIndexer.indexExecuteOutboxMessage
 * | SmartRollupIndexer.indexExecuteOutboxMessage(...)}.
 * @experimental
 */
export function indexExecuteOutboxMessage(): MethodDecorator {
    return createDecorator(indexExecuteOutboxMessage);
}
