import { EnabledHasuraConfig, HasuraNamingStyle } from '../../src/hasura/config/hasura-config';
import { HasuraNamingHelper } from '../../src/hasura/hasura-naming-helper';

describe(HasuraNamingHelper.name, () => {
    const target = new HasuraNamingHelper();

    it.each([
        ['ohMyGod', 'ohMyGod', HasuraNamingStyle.NoTransformation],
        ['OhMyGod', 'ohMyGod', HasuraNamingStyle.NoTransformation],
        ['OHMYGOD', 'oHMYGOD', HasuraNamingStyle.NoTransformation],
        ['ohMyGod', 'oh_my_god', HasuraNamingStyle.SnakeCase],
        ['OhMyGod', 'oh_my_god', HasuraNamingStyle.SnakeCase],
        ['OHMYGOD', 'o_h_m_y_g_o_d', HasuraNamingStyle.SnakeCase],
    ])('should prepare graph ql root field name for %s', (name, expectedName, namingStyle) => {
        const config = { namingStyle } as EnabledHasuraConfig;

        const currentName = target.applyNamingOnRoot(name, config);

        expect(currentName).toEqual(expectedName);
    });

    it.each([
        ['ohMyGod', 'ohMyGod', HasuraNamingStyle.NoTransformation],
        ['OhMyGod', 'OhMyGod', HasuraNamingStyle.NoTransformation],
        ['OHMYGOD', 'OHMYGOD', HasuraNamingStyle.NoTransformation],
        ['ohMyGod', 'oh_my_god', HasuraNamingStyle.SnakeCase],
        ['OhMyGod', 'oh_my_god', HasuraNamingStyle.SnakeCase],
        ['OHMYGOD', 'o_h_m_y_g_o_d', HasuraNamingStyle.SnakeCase],
    ])('should prepare graph ql relationsip name for %s', (name, expectedName, namingStyle) => {
        const config = { namingStyle } as EnabledHasuraConfig;

        const currentName = target.applyNaming(name, config);

        expect(currentName).toEqual(expectedName);
    });

    it.each([
        ['Aggregate', HasuraNamingStyle.NoTransformation],
        ['_aggregate', HasuraNamingStyle.SnakeCase],
    ])('should get aggregate suffix for %s', (expectedSuffix, namingStyle) => {
        const config = { namingStyle } as EnabledHasuraConfig;

        const currentSuffix = target.getAggregateSuffix(config);

        expect(currentSuffix).toEqual(expectedSuffix);
    });
});
