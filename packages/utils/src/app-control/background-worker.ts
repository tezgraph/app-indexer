import { AsyncOrSync } from 'ts-essentials';
import { DependencyContainer, Provider } from 'tsyringe';

import { BACKGROUND_WORKERS_DI_TOKEN } from './background-worker-di-token';
import { registerSingleton } from '../dependency-injection/public-api';

/** Arbitrary logic which is started and stopped together with the app. */
export interface BackgroundWorker {
    readonly name: string;

    start(): AsyncOrSync<void>;
    stop?(): AsyncOrSync<void>;
}

/**
 * Registers a background worker to be executed together with Dappetizer app.
 * It is registered as **singleton**.
 */
export function registerBackgroundWorker(diContainer: DependencyContainer, backgroundWorkerProvider: Provider<BackgroundWorker>): void {
    registerSingleton(diContainer, BACKGROUND_WORKERS_DI_TOKEN, backgroundWorkerProvider);
}
