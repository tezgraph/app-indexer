import { RecordToDump, StringKeyOfType } from '@tezos-dappetizer/utils';
import { lowerFirst } from 'lodash';
import { AsyncOrSync } from 'ts-essentials';

import { ClientIndexerErrorHandler, ErrorHandlingOptions } from './client-indexer-error-handler';
import { EntityToIndex } from '../../rpc-data/rpc-data-interfaces';
import { BlockDataIndexer } from '../block-data-indexer';
import { IndexerInfo } from '../wrappers/indexer-info';
import { TrustedBlockDataIndexer } from '../wrappers/trusted-indexer-interfaces';

export function createErrorHandlingBlockDataIndexer<TDbContext, TContextData>(
    indexer: BlockDataIndexer<TDbContext, TContextData>,
    info: IndexerInfo,
    errorHandler: ClientIndexerErrorHandler,
): TrustedBlockDataIndexer<TDbContext, TContextData> {
    return {
        indexBalanceUpdate: wrapMethod('indexBalanceUpdate'),
        indexBlock: wrapMethod('indexBlock'),
        indexMainOperation: wrapMethod('indexMainOperation'),
        indexOperationGroup: wrapMethod('indexOperationGroup'),
        indexOperationWithResult: wrapMethod('indexOperationWithResult'),
    };

    function wrapMethod<TEntity extends EntityToIndex, TIndexingContext>(
        methodName: StringKeyOfType<typeof indexer, undefined | ((e: TEntity, d: TDbContext, c: TIndexingContext) => AsyncOrSync<void>)>,
    ): (e: TEntity, d: TDbContext, c: TIndexingContext) => Promise<void> {
        return errorHandler.wrap(indexer, methodName, info, new IndexMethodOptions(methodName));
    }
}

export class IndexMethodOptions<TEntity extends EntityToIndex, TDbContext, TIndexingContext>
implements ErrorHandlingOptions<[TEntity, TDbContext, TIndexingContext]> {
    constructor(readonly methodName: `index${string}`) {}

    getArgsToReport(entity: TEntity, _dbContext: TDbContext, _indexingContext: TIndexingContext): RecordToDump {
        return { uid: entity.uid };
    }

    getArgsToTrace(entity: TEntity, _dbContext: TDbContext, indexingContext: TIndexingContext): object {
        const entityName = lowerFirst(this.methodName.substring('index'.length));
        return { [entityName]: entity, indexingContext };
    }
}
