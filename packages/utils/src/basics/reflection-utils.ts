import { ArgError } from './arg-error';
import { argGuard } from './arg-guard';
import { Constructor, NonNullish, Nullish, ToJSON } from './reflection-types';

/** Helper for strongly typed literal values on return statements e.g. return `typed<Foo>({ ... })`. */
export function typed<T>(value: T): T {
    return value;
}

export function keyof<T>(key: keyof T & string): keyof T & string {
    argGuard.string(key, 'key');

    return key;
}

export function isNullish<TValue>(value: Nullish<TValue>): value is null | undefined {
    return value === undefined || value === null;
}

export function isNotNullish<TValue>(value: Nullish<TValue>): value is TValue {
    return !isNullish(value);
}

export function getEnumValues<T extends string>(enumType: Readonly<Record<string, T>>): T[] {
    argGuard.object(enumType, 'enumType');

    return Object.values(enumType);
}

export function getVariableName(variableWrapper: Record<string, unknown>): string {
    argGuard.object(variableWrapper, 'variableWrapper');

    const properties = Object.keys(variableWrapper);
    if (!properties[0] || properties.length > 1) {
        throw new Error('There must be single variable provided.');
    }
    return properties[0];
}

export function isObject(value: unknown): value is Record<string, unknown> {
    return typeof value === 'object' && value !== null;
}

export function isObjectLiteral(value: unknown): value is Record<string, unknown> {
    return isObject(value) && getConstructorName(value) === Object.name;
}

/** Safely gets a constructor name - returns `Object` if an object is created without a prototype. */
export function getConstructorName(value: NonNullish): string {
    if (isNullish(value)) {
        throw new ArgError({
            argName: 'value',
            specifiedValue: value,
            details: 'The value cannot be nullish.',
        });
    }
    return typeof value.constructor === 'function' ? value.constructor.name : Object.name;
}

/**
 * Checks actual constructor, does NOT honor inheritance chain.
 * Compared to `instanceof`, works if type comes from multiple NPM modules.
 */
export function hasConstructor<T extends NonNullish>(value: unknown, type: Constructor<T>): value is T {
    argGuard.function(type, 'type');

    const ctorName = !isNullish(value) ? getConstructorName(value as NonNullish) : null;
    return ctorName === type.name;
}

export function hasToJSON<T extends object>(obj: T): obj is T & ToJSON {
    argGuard.object(obj, 'obj');

    return 'toJSON' in obj;
}

export function dumpType(value: unknown): string {
    if (typeof value === 'object') {
        return value !== null
            ? `'object' (constructor ${getConstructorName(value)})`
            : `'object' (null)`;
    }
    return `'${typeof value}'`;
}

/** Lists both own and inherited properties including non-enumerable ones. */
export function listAllPropertyNames<T extends object>(obj: T): (keyof T & string)[] {
    argGuard.object(obj, 'obj');

    const properties: string[] = [];
    for (const property of iterateAllPropertyNames(obj)) {
        if (property !== 'constructor' && !properties.includes(property)) {
            properties.push(property);
        }
    }
    return properties.sort() as (keyof T & string)[];
}

function *iterateAllPropertyNames(obj: object): Iterable<string> {
    yield* Object.getOwnPropertyNames(obj);
    yield* iterateAllPropertyNamesFromType(obj.constructor);

    const proto: unknown = Object.getPrototypeOf(obj);
    yield* isObject(proto) && proto !== Object.prototype ? listAllPropertyNames(proto) : [];
}

function *iterateAllPropertyNamesFromType(type: Nullish<Function>): Iterable<string> {
    if (isNullish(type) || type === Object) {
        return;
    }

    yield* !isNullish(type.prototype) ? Object.getOwnPropertyNames(type.prototype) : [];

    const proto: unknown = Object.getPrototypeOf(type);
    yield* typeof proto === 'function' ? iterateAllPropertyNamesFromType(proto) : [];
}

/** An alias of `Object.entries()` with strongly-typed keys. */
export function entriesOf<TKey extends string, TValue>(record: Readonly<Partial<Record<TKey, TValue>>>): [TKey, TValue][] {
    return Object.entries(record) as [TKey, TValue][];
}

export function keysOf<TKey extends string>(record: Readonly<Partial<Record<TKey, unknown>>>): TKey[] {
    return Object.keys(record) as TKey[];
}
