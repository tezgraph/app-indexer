import * as taquitoRpc from '@taquito/rpc';
import { resolveLogger } from '@tezos-dappetizer/utils';
import { StrictExclude } from 'ts-essentials';
import { registry } from 'tsyringe';

import { ActivateAccountOperationConverter } from './activate-account/activate-account-operation-converter';
import { BallotOperationConverter } from './ballot/ballot-operation-converter';
import { RpcByKindConverter } from './common/rpc-by-kind-converter';
import { DelegationOperationConverter } from './delegation/converters/delegation-operation-converter';
import { DoubleBakingEvidenceOperationConverter } from './double-baking-evidence/double-baking-evidence-operation-converter';
import { DrainDelegateOperationConverter } from './drain-delegate/drain-delegate-operation-converter';
import {
    IncreasePaidStorageOperationConverter,
} from './increase-paid-storage/converters/increase-paid-storage-operation-converter';
import { OriginationOperationConverter } from './origination/converters/origination-operation-converter';
import { ProposalsOperationConverter } from './proposals/proposals-operation-converter';
import {
    RegisterGlobalConstantOperationConverter,
} from './register-global-constant/converters/register-global-constant-operation-converter';
import { RevealOperationConverter } from './reveal/converters/reveal-operation-converter';
import { SeedNonceRevelationOperationConverter } from './seed-nonce-revelation/seed-nonce-revelation-operation-converter';
import { SetDepositsLimitOperationConverter } from './set-deposits-limit/converters/set-deposits-limit-operation-converter';
import {
    SmartRollupAddMessagesOperationConverter,
} from './smart-rollup/add-messages/converters/smart-rollup-add-messages-operation-converter';
import {
    SmartRollupCementOperationConverter,
} from './smart-rollup/cement/converters/smart-rollup-cement-operation-converter';
import {
    SmartRollupExecuteOutboxMessageOperationConverter,
} from './smart-rollup/execute-outbox-message/converters/smart-rollup-execute-outbox-message-operation-converter';
import {
    SmartRollupOriginateOperationConverter,
} from './smart-rollup/originate/converters/smart-rollup-originate-operation-converter';
import {
    SmartRollupPublishOperationConverter,
} from './smart-rollup/publish/converters/smart-rollup-publish-operation-converter';
import {
    SmartRollupRecoverBondOperationConverter,
} from './smart-rollup/recover-bond/converters/smart-rollup-recover-bond-operation-converter';
import {
    SmartRollupRefuteOperationConverter,
} from './smart-rollup/refute/converters/smart-rollup-refute-operation-converter';
import {
    SmartRollupTimeoutOperationConverter,
} from './smart-rollup/timeout/converters/smart-rollup-timeout-operation-converter';
import { TransactionOperationConverter } from './transaction/converters/transaction-operation-converter';
import { TransferTicketOperationConverter } from './transfer-ticket/converters/transfer-ticket-operation-converter';
import {
    UpdateConsensusKeyOperationConverter,
} from './update-consensus-key/converters/update-consensus-key-operation-converter';
import { VdfRevelationOperationConverter } from './vdf-revelation/vdf-revelation-operation-converter';
import { RpcConvertOptions } from '../rpc-convert-options';
import { RpcConverter } from '../rpc-converter';
import { MainOperation, TaquitoRpcOperation } from '../rpc-data-interfaces';

export type SupportedRpcOpKind =
    StrictExclude<TaquitoRpcOperation['kind'],
    | taquitoRpc.OpKind.ATTESTATION
    | taquitoRpc.OpKind.ATTESTATION_WITH_DAL
    | taquitoRpc.OpKind.DAL_PUBLISH_COMMITMENT
    | taquitoRpc.OpKind.DOUBLE_ATTESTATION_EVIDENCE
    | taquitoRpc.OpKind.DOUBLE_ENDORSEMENT_EVIDENCE
    | taquitoRpc.OpKind.DOUBLE_PREATTESTATION_EVIDENCE
    | taquitoRpc.OpKind.DOUBLE_PREENDORSEMENT_EVIDENCE
    | taquitoRpc.OpKind.ENDORSEMENT
    | taquitoRpc.OpKind.ENDORSEMENT_WITH_DAL
    | taquitoRpc.OpKind.PREATTESTATION
    | taquitoRpc.OpKind.PREENDORSEMENT>;

@registry([{
    token: MainOperationConverter,
    useFactory: diContainer => {
        const converters: Record<SupportedRpcOpKind, RpcConverter<TaquitoRpcOperation, MainOperation, RpcConvertOptions>> = {
            activate_account: diContainer.resolve(ActivateAccountOperationConverter),
            ballot: diContainer.resolve(BallotOperationConverter),
            delegation: diContainer.resolve(DelegationOperationConverter),
            double_baking_evidence: diContainer.resolve(DoubleBakingEvidenceOperationConverter),
            drain_delegate: diContainer.resolve(DrainDelegateOperationConverter),
            increase_paid_storage: diContainer.resolve(IncreasePaidStorageOperationConverter),
            origination: diContainer.resolve(OriginationOperationConverter),
            proposals: diContainer.resolve(ProposalsOperationConverter),
            register_global_constant: diContainer.resolve(RegisterGlobalConstantOperationConverter),
            reveal: diContainer.resolve(RevealOperationConverter),
            seed_nonce_revelation: diContainer.resolve(SeedNonceRevelationOperationConverter),
            set_deposits_limit: diContainer.resolve(SetDepositsLimitOperationConverter),
            smart_rollup_add_messages: diContainer.resolve(SmartRollupAddMessagesOperationConverter),
            smart_rollup_cement: diContainer.resolve(SmartRollupCementOperationConverter),
            smart_rollup_execute_outbox_message: diContainer.resolve(SmartRollupExecuteOutboxMessageOperationConverter),
            smart_rollup_originate: diContainer.resolve(SmartRollupOriginateOperationConverter),
            smart_rollup_publish: diContainer.resolve(SmartRollupPublishOperationConverter),
            smart_rollup_recover_bond: diContainer.resolve(SmartRollupRecoverBondOperationConverter),
            smart_rollup_refute: diContainer.resolve(SmartRollupRefuteOperationConverter),
            smart_rollup_timeout: diContainer.resolve(SmartRollupTimeoutOperationConverter),
            transaction: diContainer.resolve(TransactionOperationConverter),
            transfer_ticket: diContainer.resolve(TransferTicketOperationConverter),
            update_consensus_key: diContainer.resolve(UpdateConsensusKeyOperationConverter),
            vdf_revelation: diContainer.resolve(VdfRevelationOperationConverter),
        };
        return new MainOperationConverter(converters, resolveLogger(diContainer, MainOperationConverter));
    },
}])
export class MainOperationConverter extends RpcByKindConverter<TaquitoRpcOperation, MainOperation> {}
