import { asyncToArray, asyncWrap } from 'iter-tools';
import { Writable } from 'ts-essentials';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { TestLogger, verifyCalled } from '../../../../../../test-utilities/mocks';
import { asReadonly, AsyncIterableProvider } from '../../../../src/basics/public-api';
import { ComponentHealthState } from '../../../../src/health/checks/component-health-state';
import { HealthStatus } from '../../../../src/health/public-api';
import {
    healthMessages,
    ReconnectOnErrorMonitor,
} from '../../../../src/tezos-rpc/monitor/generic/reconnect-on-error-monitor';
import { RpcMetrics } from '../../../../src/tezos-rpc/rpc-metrics';
import { TezosNodeConfig } from '../../../../src/tezos-rpc/tezos-node-config';
import { SleepHelper } from '../../../../src/time/public-api';

describe(ReconnectOnErrorMonitor, () => {
    let target: AsyncIterableProvider<number>;
    let nextProvider: AsyncIterableProvider<number>;
    let healthState: ComponentHealthState;
    let sleepHelper: SleepHelper;
    let logger: TestLogger;
    let config: Writable<TezosNodeConfig>;

    let infiniteLoopCounter: RpcMetrics['monitorInfiniteLoopCount'];
    let reconnectCounter: RpcMetrics['monitorReconnectCount'];
    let retryCounter: RpcMetrics['monitorRetryCount'];

    const act = async () => asyncToArray(target.iterate());

    beforeEach(() => {
        [infiniteLoopCounter, reconnectCounter, retryCounter] = [mock(), mock(), mock()];
        const metrics = {
            monitorInfiniteLoopCount: instance(infiniteLoopCounter),
            monitorReconnectCount: instance(reconnectCounter),
            monitorRetryCount: instance(retryCounter),
        } as RpcMetrics;

        [nextProvider, healthState, sleepHelper] = [mock(), mock(), mock()];
        logger = new TestLogger();
        config = {} as TezosNodeConfig;
        target = new ReconnectOnErrorMonitor(instance(healthState), logger, instance(nextProvider), config, instance(sleepHelper), metrics);

        config.retryDelaysMillis = asReadonly([11, 22]);
        config.url = 'http://tezos.node/';

        when(healthState.name).thenReturn('abc');
    });

    it('should continuously iterate data', async () => {
        when(nextProvider.iterate()).thenReturn(asyncWrap([42, 43, 44]));

        const items = await act();

        expect(items).toEqual([42, 43, 44]);
        logger.verifyNothingLogged();
        verify(healthState.set(anything(), anything())).times(3);
        verify(healthState.set(HealthStatus.Healthy, healthMessages.healthy)).times(3);
        verify(sleepHelper.sleep(anything())).never();
    });

    it('should reconnect on failure', async () => {
        const [error1, error2, error3] = [new Error('oups 1'), new Error('oups 2'), new Error('oups 3')];
        when(nextProvider.iterate())
            .thenThrow(error1, error2, error3)
            .thenReturn(asyncWrap([42]));

        const items = await act();

        expect(items).toEqual([42]);

        logger.verifyLoggedCount(3);
        logger.logged(0).verify('Debug', { error: error1 });
        logger.logged(1).verify('Debug', { error: error2 });
        logger.logged(2).verify('Error', { error: error3 });

        verifyCalled(healthState.set)
            .with(HealthStatus.Degraded, { error: error1, reason: healthMessages.temporaryFailure })
            .thenWith(HealthStatus.Degraded, { error: error2, reason: healthMessages.temporaryFailure })
            .thenWith(HealthStatus.Unhealthy, { error: error3, reason: healthMessages.persistentFailure })
            .thenWith(HealthStatus.Healthy, healthMessages.healthy)
            .thenNoMore();

        verify(reconnectCounter.inc()).times(2);
        verify(infiniteLoopCounter.inc()).times(1);
        verifyCalled(retryCounter.inc)
            .with({ name: 'abc', host: 'http://tezos.node/', delay: 11, index: 0 })
            .thenWith({ name: 'abc', host: 'http://tezos.node/', delay: 22, index: 1 })
            .thenWith({ name: 'abc', host: 'http://tezos.node/', delay: 22, index: 1 })
            .thenNoMore();

        verifyCalled(sleepHelper.sleep)
            .with(11)
            .thenWith(22)
            .thenWith(22)
            .thenNoMore();
    });

    it('should reconnect on failure if no retries configured', async () => {
        config.retryDelaysMillis = [];
        const error1 = new Error('oups 1');
        when(nextProvider.iterate())
            .thenThrow(error1)
            .thenReturn(asyncWrap([42]));

        const items = await act();

        expect(items).toEqual([42]);

        logger.verifyLoggedCount(1);
        logger.logged(0).verify('Error', { error: error1 });

        verifyCalled(healthState.set)
            .atIndexWith(0, HealthStatus.Unhealthy, { error: error1, reason: healthMessages.persistentFailure })
            .atIndexWith(1, HealthStatus.Healthy, healthMessages.healthy)
            .times(2);

        verify(reconnectCounter.inc()).never();
        verify(infiniteLoopCounter.inc()).times(1);
        verifyCalled(retryCounter.inc).onceWith({ name: 'abc', host: 'http://tezos.node/', delay: 0, index: -1 });

        verifyCalled(sleepHelper.sleep).onceWith(0);
    });
});
