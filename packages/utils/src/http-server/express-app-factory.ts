import express from 'express';
import { injectAll, singleton } from 'tsyringe';

import { HttpHandler } from './http-handler';
import { HTTP_HANDLERS_DI_TOKEN } from './http-handler-di-token';

@singleton()
export class ExpressAppFactory {
    constructor(
        @injectAll(HTTP_HANDLERS_DI_TOKEN) private readonly httpHandlers: readonly HttpHandler[],
    ) {}

    create(): express.Application {
        const expressApp = this.createRaw();

        for (const httpHandler of this.httpHandlers) {
            expressApp.get(httpHandler.urlPath, (request, response) => void httpHandler.handle(request, response));
        }
        return expressApp;
    }

    createRaw(): express.Application {
        return express();
    }
}
