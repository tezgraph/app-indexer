import { inject, injectable } from 'tsyringe';

import { BaseLogger } from './base-logger';
import { LoggingMetrics } from './logging-metrics';
import { injectExplicit } from '../../dependency-injection/public-api';
import { Clock, CLOCK_DI_TOKEN } from '../../time/public-api';
import { LogData, LogLevel } from '../log-entry';
import { LoggerScope } from '../logger-scope';
import { LoggingConfig } from '../logging-config';
import { LoggedPackagesProvider } from '../package-info/logged-packages-provider';
import { ROOT_LOGGER_DI_TOKEN, RootLogger } from '../root-logger';

/** Common internal API for logging. */
@injectable()
export class LoggerImpl extends BaseLogger {
    constructor(
        @injectExplicit() category: string, // eslint-disable-line @typescript-eslint/indent
        @inject(ROOT_LOGGER_DI_TOKEN) private readonly rootLogger: RootLogger,
        @inject(CLOCK_DI_TOKEN) private readonly clock: Clock,
        private readonly metrics: LoggingMetrics,
        private readonly config: LoggingConfig,
        scope: LoggerScope,
        private readonly packagesProvider: LoggedPackagesProvider,
    ) {
        super(category, scope);
    }

    override isEnabled(level: LogLevel): boolean {
        return this.rootLogger.isEnabled(level);
    }

    override writeToLog(level: LogLevel, message: string, data: LogData): void {
        this.rootLogger.log({
            timestamp: this.clock.getNowDate(),
            category: this.category,
            level,
            message,
            data,
            globalData: this.config.globalData,
            packages: this.packagesProvider.packages,
        });
        this.metrics.logCount.inc({ level });
    }
}
