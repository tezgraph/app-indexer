import { BigNumber } from 'bignumber.js';
import { Writable } from 'ts-essentials';
import { instance, mock, when } from 'ts-mockito';

import { expectToThrow } from '../../../../../../../test-utilities/mocks';
import {
    BalanceUpdateConverter,
} from '../../../../../src/rpc-data/common/balance-update/converters/balance-update-converter';
import { BigMapDiffConverter } from '../../../../../src/rpc-data/common/big-map-diff/converters/big-map-diff-converter';
import { LazyStorageChangeConverter } from '../../../../../src/rpc-data/common/storage-change/lazy-storage-change-converter';
import { TicketUpdatesConverter } from '../../../../../src/rpc-data/common/ticket/converters/ticket-updates-converter';
import {
    TransactionResultConverter,
    TransactionResultParts,
} from '../../../../../src/rpc-data/operations/transaction/converters/transaction-result-converter';
import { RpcConversionError } from '../../../../../src/rpc-data/rpc-conversion-error';
import {
    RpcConvertOptionsWithLazyContract,
} from '../../../../../src/rpc-data/rpc-convert-options';
import {
    BalanceUpdate,
    LazyBigMapDiff,
    LazyStorageChange,
    TaquitoRpcTransactionResult,
    TicketUpdate,
} from '../../../../../src/rpc-data/rpc-data-interfaces';
import { convertAppliedResultParts } from '../../../rpc-test-helpers';

describe(TransactionResultConverter, () => {
    let target: TransactionResultConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;
    let bigMapDiffConverter: BigMapDiffConverter;
    let storageChangeConverter: LazyStorageChangeConverter;
    let ticketUpdatesConverter: TicketUpdatesConverter;

    let rpcData: Writable<TaquitoRpcTransactionResult>;
    let options: RpcConvertOptionsWithLazyContract;
    let balanceUpdates: BalanceUpdate[];
    let bigMapDiffs: LazyBigMapDiff[];
    let storageChange: LazyStorageChange;
    let ticketReceipts: TicketUpdate[];
    let ticketUpdates: TicketUpdate[];

    const act = () => convertAppliedResultParts(target, rpcData, 'res/id', options);

    beforeEach(() => {
        [balanceUpdateConverter, bigMapDiffConverter, storageChangeConverter, ticketUpdatesConverter] = [mock(), mock(), mock(), mock()];
        target = new TransactionResultConverter(
            null!,
            instance(balanceUpdateConverter),
            instance(bigMapDiffConverter),
            instance(storageChangeConverter),
            instance(ticketUpdatesConverter),
        );

        rpcData = {
            originated_contracts: [] as readonly string[],
            paid_storage_size_diff: '11',
            storage_size: '22',
            ticket_hash: 'tt-ha',
        } as typeof rpcData;
        options = 'mockedOptions' as any;
        balanceUpdates = 'mockedBalanceUpdates' as any;
        bigMapDiffs = 'mockedBigMapDiffs' as any;
        storageChange = 'mockedStorageChange' as any;
        ticketReceipts = 'mockedTicketReceipts' as any;
        ticketUpdates = 'mockedTicketUpdates' as any;

        when(balanceUpdateConverter.convertArrayProperty(rpcData, 'balance_updates', 'res/id')).thenReturn(balanceUpdates);
        when(bigMapDiffConverter.convertArrayFrom(rpcData, 'res/id', options)).thenReturn(bigMapDiffs);
        when(storageChangeConverter.convertNullableProperty(rpcData, 'storage', 'res/id', options)).thenReturn(storageChange);
        when(ticketUpdatesConverter.convertArrayProperty(rpcData, 'ticket_receipt', 'res/id')).thenReturn(ticketReceipts);
        when(ticketUpdatesConverter.convertArrayProperty(rpcData, 'ticket_updates', 'res/id')).thenReturn(ticketUpdates);
    });

    it('should convert correctly', () => {
        const result = act();

        expect(result).toEqual<TransactionResultParts>({
            allocatedDestinationContract: false,
            balanceUpdates,
            bigMapDiffs,
            paidStorageSizeDiff: new BigNumber(11),
            storageChange,
            storageSize: new BigNumber(22),
            ticketHash: 'tt-ha',
            ticketReceipts,
            ticketUpdates,
        });
    });

    it.each([
        [true, true],
        [false, false],
        [undefined, false],
    ])('should convert allocated_destination_contract %s to %s', (input, expected) => {
        rpcData.allocated_destination_contract = input;

        const result = act();

        expect(result.allocatedDestinationContract).toBe(expected);
    });

    it('should throw if originated_contracts not empty', () => {
        rpcData.originated_contracts = ['KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5'];

        const error = expectToThrow(act, RpcConversionError);

        expect(error.uid).toBe('res/id');
        expect(error.message).toIncludeMultiple(['originated_contracts', 'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5']);
    });
});
