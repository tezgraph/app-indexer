import { InjectionToken } from 'tsyringe';

import { Contract } from './contract';

export interface ContractProvider {
    getContract(address: string): Promise<Contract>;
}

export const CONTRACT_PROVIDER_DI_TOKEN: InjectionToken<ContractProvider> = 'Dappetizer:Indexer:ContractProvider';
