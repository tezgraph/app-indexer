import { HasuraCommand } from './hasura-command';

export class ClearMetadataCommand implements HasuraCommand {
    readonly description = 'clear metadata';

    createPayload(): object {
        return {
            type: 'clear_metadata',
            args: {},
        };
    }
}
