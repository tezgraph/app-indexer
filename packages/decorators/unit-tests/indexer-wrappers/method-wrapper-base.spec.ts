import { anything, instance, mock, spy, verify, when } from 'ts-mockito';

import { DbContext, IndexingContext, expectToThrowAsync } from '../../../../test-utilities/mocks';
import { DecoratedMethod, DecoratedMethodDiscovery } from '../../src/helpers/decorated-method-discovery';
import { createDecorator } from '../../src/helpers/decorator-accessors';
import { MethodWrapperBase } from '../../src/indexer-wrappers/method-wrapper-base';

interface Entity { ent: string }

function testDecorator(options: string): MethodDecorator {
    return createDecorator(testDecorator, options);
}

class TargetWrapper extends MethodWrapperBase<Entity, string> {
    override get decorator() {
        return testDecorator;
    }

    override async executeDecorated<TDb, TIndexing>(_m: DecoratedMethod<Function, string>, _e: Entity, _d: TDb, _i: TIndexing): Promise<void> {
        return Promise.resolve();
    }
}

describe(MethodWrapperBase.name, () => {
    let target: TargetWrapper;
    let targetSpy: TargetWrapper;
    let decoratedMethodDiscovery: DecoratedMethodDiscovery;

    let decoratedIndexer: object;
    let methodFoo: DecoratedMethod<Function, string>;
    let methodBar: DecoratedMethod<Function, string>;

    let entity: Entity;
    let db: DbContext;
    let indexingContext: IndexingContext;

    beforeEach(() => {
        target = new TargetWrapper(instance(decoratedMethodDiscovery = mock()));
        targetSpy = spy(target);

        decoratedIndexer = { mocked: 'decoratedIndexer' };
        methodFoo = { name: 'fooName' } as DecoratedMethod<Function, string>;
        methodBar = { name: 'barName' } as DecoratedMethod<Function, string>;

        entity = { ent: 'eee' };
        db = 'mockedDb' as any;
        indexingContext = 'mockedIndexingContext' as any;

        when(decoratedMethodDiscovery.getMethods(decoratedIndexer, testDecorator)).thenReturn([methodFoo, methodBar]);
    });

    it('should call all decorated methods', async () => {
        const method = target.wrapMethod(decoratedIndexer);
        await method!(entity, db, indexingContext);

        verify(targetSpy.executeDecorated(anything(), anything(), anything(), anything())).twice();
        verify(targetSpy.executeDecorated(methodFoo, entity, db, indexingContext)).once();
        verify(targetSpy.executeDecorated(methodBar, entity, db, indexingContext)).once();
    });

    it('should wrap errors', async () => {
        when(targetSpy.executeDecorated(methodFoo, entity, db, indexingContext)).thenReject(new Error('oups'));

        const method = target.wrapMethod(decoratedIndexer);
        const error = await expectToThrowAsync(async () => method!(entity, db, indexingContext));

        expect(error.message).toIncludeMultiple([
            `'fooName'`,
            `@testDecorator(...)`,
            'oups',
        ]);
    });

    it('should not wrap if no decorated method', () => {
        when(decoratedMethodDiscovery.getMethods(decoratedIndexer, testDecorator)).thenReturn([]);

        const wrapper = target.wrapMethod(decoratedIndexer);

        expect(wrapper).toBeUndefined();
    });
});
