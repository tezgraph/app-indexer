import { argGuard } from '@tezos-dappetizer/utils';
import NodeCache from 'node-cache';
import { singleton } from 'tsyringe';

import { Contract } from './contract';
import { ContractFactory } from './contract-factory';
import { ContractProvider } from './contract-provider';
import { IndexingConfig } from '../config/indexing/indexing-config';

@singleton()
export class ContractProviderImpl implements ContractProvider {
    private readonly cache = new NodeCache({ useClones: false });
    private readonly cacheSeconds: number;

    constructor(
        private readonly contractFactory: ContractFactory,
        config: IndexingConfig,
    ) {
        this.cacheSeconds = config.contractCacheMillis / 1000;
    }

    getContract(address: string): Promise<Contract> { // eslint-disable-line @typescript-eslint/promise-function-async
        argGuard.address(address, 'address', ['KT1']);

        let promise = this.cache.get<Promise<Contract>>(address);

        if (!promise) {
            promise = this.contractFactory.create(address);
            this.cache.set(address, promise, this.cacheSeconds);
        } else {
            this.cache.ttl(address, this.cacheSeconds); // Sliding expiration.
        }
        return promise;
    }
}
