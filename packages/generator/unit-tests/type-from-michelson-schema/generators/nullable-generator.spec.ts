import { anything, instance, mock, when } from 'ts-mockito';

import { verifyCalled } from '../../../../../test-utilities/mocks';
import { Michelson, michelsonDataPrims, MichelsonSchema, michelsonTypePrims } from '../../../../indexer/src/public-api';
import { GeneratedTypeParts, GeneratorRouter } from '../../../src/type-from-michelson-schema/generators/generator-router';
import { NullableGenerator, OptionSchema } from '../../../src/type-from-michelson-schema/generators/nullable-generator';

describe(NullableGenerator.name, () => {
    let target: NullableGenerator;
    let generatorRouter: GeneratorRouter;
    let schema: MichelsonSchema;

    beforeEach(() => {
        generatorRouter = mock();
        target = new NullableGenerator(instance(generatorRouter));

        schema = new MichelsonSchema({
            prim: michelsonTypePrims.OPTION,
            args: [{ prim: michelsonTypePrims.ADDRESS }],
        });
    });

    it('should generate correctly', () => {
        when(generatorRouter.generate(anything(), anything())).thenReturn({
            typeDeclaration: 'MockedType',
            commentLines: 'mockedComment' as any,
            imports: 'mockedImports' as any,
            relatedCodeLines: 'mockedRelatedCodeLines' as any,
        });

        const result = target.generate(schema.generated as any, {
            typeNameToGenerate: 'FooBar',
            bigMapTyping: 'mockedBigMapTyping' as any,
        });

        expect(result).toEqual<GeneratedTypeParts>({
            typeDeclaration: '{ Some: MockedType } | null',
            commentLines: 'mockedComment' as any,
            imports: 'mockedImports' as any,
            relatedCodeLines: 'mockedRelatedCodeLines' as any,
        });
        verifyCalled(generatorRouter.generate).onceWith(
            { __michelsonType: 'address', schema: 'address' },
            { typeNameToGenerate: 'FooBarOption', bigMapTyping: 'mockedBigMapTyping' as any },
        );
    });

    it('should use same generated schema as Taquito', () => {
        expect(schema.generated).toEqual<OptionSchema>({
            __michelsonType: 'option',
            schema: { __michelsonType: 'address', schema: 'address' },
        });
    });

    it.each<[unknown, Michelson]>([
        [{ Some: 'value' }, { prim: michelsonDataPrims.SOME, args: [{ string: 'value' }] }],
        [null, { prim: michelsonDataPrims.NONE }],
    ])('should match %s deserialized by Taquito', (expectedValue, valueMichelson) => {
        const value = schema.execute(valueMichelson);

        expect(value).toEqual(expectedValue);
    });
});
