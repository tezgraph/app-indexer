import { errorToString } from '@tezos-dappetizer/utils';

/**
 * An error in raw data returned from RPC of a Tezos Node.
 * It has a convenient way of locating the error at particular property path.
 */
export class RpcConversionError extends Error {
    constructor(
        readonly uid: string,
        readonly dataDescription: string,
    ) {
        super(`Invalid value from RPC at '${uid}'. ${dataDescription}`);
    }

    static wrap(error: unknown, uid: string): RpcConversionError {
        return error instanceof RpcConversionError
            ? error
            : new RpcConversionError(uid, errorToString(error));
    }
}
