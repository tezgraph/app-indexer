import { createTestBlock, executeCmd, sqlInsert, verifyBlocksInDb } from './helpers';
import { DeleteBlockByHashCommand } from '../../src/commands/delete-block-by-hash-command';
import { TestDatabase, createTestDatabase } from '../test-database';

describe(DeleteBlockByHashCommand.name, () => {
    let db: TestDatabase;

    beforeEach(async () => db = await createTestDatabase());
    afterEach(async () => db.destroy());

    it('should delete particular block', async () => {
        const block = createTestBlock();
        await sqlInsert(db, [block]);

        await executeCmd(db, new DeleteBlockByHashCommand(block.hash));

        await verifyBlocksInDb(db, []);
    });
});
