import { errorToString } from '@tezos-dappetizer/utils';
import { AsyncOrSync } from 'ts-essentials';

import { DecoratedMethod, DecoratedMethodDiscovery } from '../helpers/decorated-method-discovery';
import { MethodDecoratorFactory } from '../helpers/decorator-factories';

export abstract class MethodWrapperBase<TEntity, TDecoratorOptions = undefined> {
    protected readonly abstract decorator: MethodDecoratorFactory<[TDecoratorOptions]>;

    constructor(private readonly decoratedMethodDiscovery = new DecoratedMethodDiscovery()) {}

    wrapMethod<TDbContext, TIndexingContext>(
        decoratedIndexer: object,
    ): ((entity: TEntity, dbContext: TDbContext, indexingContext: TIndexingContext) => AsyncOrSync<void>) | undefined {
        const decoratedMethods = this.decoratedMethodDiscovery.getMethods(decoratedIndexer, this.decorator);

        return decoratedMethods.length > 0
            ? async (entity, dbContext, indexingContext) => {
                for (const decoratedMethod of decoratedMethods) {
                    try {
                        await this.executeDecorated(decoratedMethod, entity, dbContext, indexingContext);
                    } catch (error) {
                        throw new Error(`Failed method '${decoratedMethod.name}' decorated with @${this.decorator.name}(...).`
                            + ` ${errorToString(error)}`);
                    }
                }
            }
            : undefined;
    }

    protected abstract executeDecorated<TDbContext, TIndexingContext>(
        decoratedMethod: DecoratedMethod<Function, TDecoratorOptions>,
        entity: TEntity,
        dbContext: TDbContext,
        indexingContext: TIndexingContext,
    ): Promise<void>;
}
