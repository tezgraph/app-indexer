import { InjectionToken } from 'tsyringe';

export interface IndexerEmitter {
    emitInitialApp(options: EmitInitialAppOptions): Promise<void>;

    updateContractIndexerClass(options: UpdateContractIndexerClassOptions): Promise<void>;
}

export const INDEXER_EMITTER_DI_TOKEN: InjectionToken<IndexerEmitter> = 'Dappetizer:Generator:IndexerEmitter';

export interface CommonEmitOptions {
    readonly outDirPath: string;
    readonly tezosNodeUrl: string;
    readonly isTezosNodeUrlExplicit: boolean;
    readonly tezosNetwork: string;
}

export interface EmitInitialAppOptions extends CommonEmitOptions {
    readonly installNpmDependencies: boolean;
    readonly indexerModuleName: string | null;
    readonly contractAddress: string | null;
    readonly contractName: string | null;
    readonly enableUsageStatistics: boolean;
}

export interface UpdateContractIndexerClassOptions extends CommonEmitOptions {
    readonly contractAddress: string;
    readonly contractName: string;
}
