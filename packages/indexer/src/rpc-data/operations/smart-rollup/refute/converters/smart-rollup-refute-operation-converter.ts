import * as taquitoRpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { SmartRollupRefuteResultConverter } from './smart-rollup-refute-result-converter';
import { primitives } from '../../../../primitives';
import { RpcConvertOptions } from '../../../../rpc-convert-options';
import { RpcObjectConverter } from '../../../../rpc-converter';
import {
    OperationKind,
    SmartRollupRefutation,
    SmartRollupRefutationKind,
    SmartRollupRefuteOperation,
    TaquitoRpcSmartRollupRefuteOperation,
} from '../../../../rpc-data-interfaces';
import { CommonSmartRollupOperationConverter } from '../../common/converters/common-smart-rollup-operation-converter';

@singleton()
export class SmartRollupRefutationConverter extends RpcObjectConverter<TaquitoRpcSmartRollupRefuteOperation['refutation'], SmartRollupRefutation> {
    protected override convertObject(
        rpcData: TaquitoRpcSmartRollupRefuteOperation['refutation'],
        uid: string,
    ): SmartRollupRefutation {
        primitives.enum.convertProperty(rpcData, 'refutation_kind', uid, refutationKindDummyMapping);

        return Object.freeze<SmartRollupRefutation>(rpcData.refutation_kind === taquitoRpc.SmartRollupRefutationOptions.START
            ? {
                choice: null,
                kind: SmartRollupRefutationKind.Start,
                opponentCommitmentHash: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'opponent_commitment_hash', uid),
                playerCommitmentHash: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'player_commitment_hash', uid),
            }
            : {
                choice: primitives.bigInteger.convertProperty(rpcData, 'choice', uid, { min: 0 }),
                kind: SmartRollupRefutationKind.Move,
                opponentCommitmentHash: null,
                playerCommitmentHash: null,
            });
    }
}

const refutationKindDummyMapping: Readonly<Record<taquitoRpc.SmartRollupRefutationOptions, ''>> = {
    move: '',
    start: '',
};

@singleton()
export class SmartRollupRefuteOperationConverter
    extends RpcObjectConverter<TaquitoRpcSmartRollupRefuteOperation, SmartRollupRefuteOperation, RpcConvertOptions> {
    constructor(
        private readonly commonOperationConverter: CommonSmartRollupOperationConverter,
        private readonly resultConverter: SmartRollupRefuteResultConverter,
        private readonly refutationConverter: SmartRollupRefutationConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcSmartRollupRefuteOperation,
        uid: string,
        options: RpcConvertOptions,
    ): SmartRollupRefuteOperation {
        const commonProperties = this.commonOperationConverter.convert(rpcData, uid, options);
        const refutation = this.refutationConverter.convertProperty(rpcData, 'refutation', uid);
        const result = this.resultConverter.convertFromMetadataOperationResult(rpcData, uid);

        return Object.freeze<SmartRollupRefuteOperation>({
            ...commonProperties,
            kind: OperationKind.SmartRollupRefute,
            opponentAddress: primitives.address.convertProperty(rpcData, 'opponent', uid),
            refutation,
            result,
            rpcData,
        });
    }
}
