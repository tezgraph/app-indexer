import { keyof } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { BalanceUpdateConverter } from '../../common/balance-update/converters/balance-update-converter';
import { primitives } from '../../primitives';
import { RpcConvertOptions } from '../../rpc-convert-options';
import { joinUid, RpcObjectConverter } from '../../rpc-converter';
import { DrainDelegateOperation, OperationKind, TaquitoRpcDrainDelegateOperation } from '../../rpc-data-interfaces';

@singleton()
export class DrainDelegateOperationConverter extends RpcObjectConverter<TaquitoRpcDrainDelegateOperation, DrainDelegateOperation, RpcConvertOptions> {
    constructor(
        private readonly balanceUpdateConverter: BalanceUpdateConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcDrainDelegateOperation,
        uid: string,
        _options: RpcConvertOptions,
    ): DrainDelegateOperation {
        const balanceUpdates = this.balanceUpdateConverter.convertFromMetadataProperty(rpcData, uid);

        return Object.freeze<DrainDelegateOperation>({
            allocatedDestinationContract: primitives.boolean.convertNullable(
                rpcData.metadata.allocated_destination_contract,
                joinUid(uid, keyof<typeof rpcData>('metadata'), keyof<typeof rpcData.metadata>('balance_updates')),
            ) ?? false,
            balanceUpdates,
            consensusKey: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'consensus_key', uid),
            delegateAddress: primitives.address.convertNullableProperty(rpcData, 'delegate', uid),
            destinationAddress: primitives.address.convertProperty(rpcData, 'destination', uid),
            kind: OperationKind.DrainDelegate,
            rpcData,
            uid,
        });
    }
}
