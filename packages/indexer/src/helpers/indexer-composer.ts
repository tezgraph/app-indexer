import { hasLength, keysOf } from '@tezos-dappetizer/utils';
import { AsyncOrSync } from 'ts-essentials';
import { singleton } from 'tsyringe';

type ComposableMethod = (...args: any[]) => AsyncOrSync<void>; // eslint-disable-line @typescript-eslint/no-explicit-any

type ComposableIndexer = Readonly<Record<string, ComposableMethod>>;

@singleton()
export class IndexerComposer {
    createComposite<TIndexer extends ComposableIndexer>(
        indexers: readonly TIndexer[],
        blueprint: Readonly<Record<keyof TIndexer, ''>>, // B/c it can be hard to enumerate all indexer methods to delegate.
    ): TIndexer | null {
        if (indexers.length === 0) {
            return null;
        }
        if (hasLength(indexers, 1)) {
            return indexers[0];
        }

        const composite = Object.fromEntries(keysOf(blueprint).map(n => [n, createCompositeMethod(indexers, n)]));
        return composite as TIndexer;
    }
}

function createCompositeMethod(indexers: readonly ComposableIndexer[], methodName: string): ComposableMethod {
    const methods = indexers.map(i => getMethod(i, methodName));

    return async (...args: unknown[]): Promise<void> => {
        // Execute one-by-one b/c these are client indexers.
        for (const method of methods) {
            await method(...args);
        }
    };
}

function getMethod(indexer: ComposableIndexer, methodName: string): ComposableMethod {
    const method = indexer[methodName];
    if (!method) {
        throw new Error(`Missing method ${methodName}(...) in the indexer: ${JSON.stringify(indexer)}`);
    }
    return method.bind(indexer);
}
