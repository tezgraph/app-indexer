import { instance, mock, verify, when } from 'ts-mockito';

import { describeMember, getRandomDate, TestClock } from '../../../../../test-utilities/mocks';
import { keyof } from '../../../src/basics/reflection-utils';
import { CachedHealthCheck } from '../../../src/health/checks/cached-health-check';
import { HealthCheck, HealthCheckResult, HealthStatus } from '../../../src/health/health-check';

describe(CachedHealthCheck.name, () => {
    let target: CachedHealthCheck;
    let checkToCache: HealthCheck;
    let clock: TestClock;

    beforeEach(() => {
        checkToCache = mock();
        clock = new TestClock();
        target = new CachedHealthCheck(instance(checkToCache), 12_000, clock);
    });

    describeMember<typeof target>('name', () => {
        it('should expose value from next check', () => {
            when(checkToCache.name).thenReturn('Foo');

            expect(target.name).toBe('Foo');
        });
    });

    describeMember<typeof target>('checkHealth', () => {
        it('should return fresh result from inner check if nothing cached', async () => {
            const nextResult = mockResult();
            when(checkToCache.checkHealth()).thenReturn(Promise.resolve(nextResult));

            const result = await target.checkHealth();

            expect(result).toEqual(nextResult);
        });

        it('should return cached result if not expired', async () => {
            when(checkToCache.checkHealth()).thenReturn(Promise.resolve(mockResult()));
            const initialResult = await target.checkHealth();
            clock.tick(10_000);

            const result = await target.checkHealth();

            expect(result).toBe(initialResult);
            verify(checkToCache.checkHealth()).once();
        });

        it('should return fresh result from inner check if cached result expired', async () => {
            const secondResult = mockResult();
            when(checkToCache.checkHealth())
                .thenReturn(Promise.resolve(mockResult()))
                .thenReturn(Promise.resolve(secondResult));
            await target.checkHealth();
            clock.tick(13_000);

            const result = await target.checkHealth();

            expect(result).toEqual(secondResult);
            verify(checkToCache.checkHealth()).twice();
        });

        it('should convert error thrown by inner check to unhealthy', async () => {
            const error = new Error('oups');
            when(checkToCache.checkHealth()).thenReturn(Promise.reject(error));

            const result = await target.checkHealth();

            expect(result).toEqual<HealthCheckResult>({
                status: HealthStatus.Unhealthy,
                data: error,
                evaluatedTime: clock.nowDate,
            });
        });

        it(`should replace ${keyof<HealthCheckResult>('evaluatedTime')} if undefined`, async () => {
            const nextResult: HealthCheckResult = { ...mockResult(), evaluatedTime: undefined };
            when(checkToCache.checkHealth()).thenReturn(Promise.resolve(nextResult));

            const result = await target.checkHealth();

            expect(result).toEqual<HealthCheckResult>({
                status: nextResult.status,
                data: nextResult.data,
                evaluatedTime: clock.nowDate,
            });
        });
    });

    function mockResult(): HealthCheckResult {
        return {
            status: HealthStatus.Degraded,
            data: { value: Math.random() },
            evaluatedTime: getRandomDate(),
        };
    }
});
