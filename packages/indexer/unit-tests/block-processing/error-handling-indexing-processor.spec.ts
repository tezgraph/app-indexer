import { Writable } from 'ts-essentials';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { expectToThrowAsync, TestLogger, verifyCalled } from '../../../../test-utilities/mocks';
import { SleepHelper } from '../../../utils/src/public-api';
import { AsyncProcessor } from '../../src/block-processing/async-processor';
import { ErrorHandlingIndexingProcessor } from '../../src/block-processing/error-handling-indexing-processor';
import { IndexingConfig } from '../../src/config/indexing/indexing-config';

describe(ErrorHandlingIndexingProcessor.name, () => {
    let target: ErrorHandlingIndexingProcessor;
    let nextProcessor: AsyncProcessor;
    let config: Writable<IndexingConfig>;
    let sleepHelper: SleepHelper;
    let logger: TestLogger;

    beforeEach(() => {
        [nextProcessor, sleepHelper] = [mock(), mock()];
        config = {} as typeof config;
        logger = new TestLogger();
        target = new ErrorHandlingIndexingProcessor(instance(nextProcessor), config, instance(sleepHelper), logger);

        config.retryDelaysMillis = [11, 22];
    });

    describe('fail -> halt app', () => {
        beforeEach(() => {
            config.retryIndefinitely = false;
        });

        shouldPassIfNoError();
        shouldRetryIfRetriesConfigured();

        it('should throw if failed more than configured retries', async () => {
            when(nextProcessor.process())
                .thenReject(new Error('oups 1'))
                .thenReject(new Error('oups 2'))
                .thenReject(new Error('oups 3'));

            const error = await expectToThrowAsync(async () => target.process());

            expect(error.message).toIncludeMultiple(['2 retries', 'oups 3']);
        });

        it('should throw if no retry configured', async () => {
            config.retryDelaysMillis = [];
            const processError = new Error('oups');
            when(nextProcessor.process()).thenReject(processError);

            const error = await expectToThrowAsync(async () => target.process());

            expect(error).toBe(processError);
            verify(sleepHelper.sleep(anything())).never();
            logger.verifyNothingLogged();
        });
    });

    describe('infinite retry', () => {
        beforeEach(() => {
            config.retryIndefinitely = true;
        });

        shouldPassIfNoError();
        shouldRetryIfRetriesConfigured();

        it('should keep retrying if failed more than configured retries', async () => {
            const processError3 = new Error('oups 3');
            when(nextProcessor.process())
                .thenReject(new Error('oups 1'))
                .thenReject(new Error('oups 2'))
                .thenReject(processError3)
                .thenResolve();

            await target.process();

            verify(nextProcessor.process()).times(4);
            verify(sleepHelper.sleep(anything())).times(3);
            verify(sleepHelper.sleep(22)).times(2);
            logger.verifyLoggedCount(3);
            logger.logged(2).verify('Error', { delayMillis: 22, error: processError3 }).verifyMessage('indefinitely');
        });

        it('should keep retrying if no retry configured', async () => {
            config.retryDelaysMillis = [];
            const processError = new Error('oups');
            when(nextProcessor.process()).thenReject(processError).thenResolve();

            await target.process();

            verify(nextProcessor.process()).times(2);
            verifyCalled(sleepHelper.sleep).onceWith(0);
            logger.verifyLoggedCount(1);
            logger.logged(0).verify('Error', { delayMillis: 0, error: processError }).verifyMessage('indefinitely');
        });
    });

    function shouldPassIfNoError() {
        it('should just pass if no error', async () => {
            await target.process();

            verify(nextProcessor.process()).once();
            verify(sleepHelper.sleep(anything())).never();
            logger.verifyNothingLogged();
        });
    }

    function shouldRetryIfRetriesConfigured() {
        it('should retry on error if retries configured', async () => {
            const processError1 = new Error('oups 1');
            const processError2 = new Error('oups 2');
            when(nextProcessor.process())
                .thenReject(processError1)
                .thenReject(processError2)
                .thenResolve();

            await target.process();

            verify(nextProcessor.process()).times(3);
            verifyCalled(sleepHelper.sleep)
                .with(11)
                .thenWith(22)
                .thenNoMore();

            logger.verifyLoggedCount(2);
            logger.logged(0).verify('Error', { time: 1, delayMillis: 11, maxRetries: 2, error: processError1 });
            logger.logged(1).verify('Error', { time: 2, delayMillis: 22, maxRetries: 2, error: processError2 });
        });
    }
});
