import { DataSource, EntityManager, QueryRunner } from 'typeorm';

/** Database context. */
export interface DbContext {
    /**
     * The transaction for the current block being indexed.
     * TypeORM {@link https://orkhan.gitbook.io/typeorm/docs/entity-manager-api | EntityManager}
     * is more conventient compared to {@link transactionRunner}.
     */
    readonly transaction: EntityManager;

    /**
     * The transaction for the current block being indexed.
     * TypeORM {@link https://orkhan.gitbook.io/typeorm/docs/query-runner | QueryRunner} actually manages the transaction.
     */
    readonly transactionRunner: QueryRunner;

    /** Main global TypeORM data source used by the app. */
    readonly globalDataSource: DataSource;
}
