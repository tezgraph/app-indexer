import path from 'path';
import { instance, mock, when } from 'ts-mockito';

import { npmModules, PackageJson, PackageJsonLoader } from '../../../utils/src/public-api';
import { DatabasePackageJson } from '../../src/helpers/database-package-json';
import { DatabasePackageJsonImpl } from '../../src/helpers/database-package-json-impl';

describe(DatabasePackageJsonImpl.name, () => {
    it('should resolve values correctly', () => {
        const loader = mock<PackageJsonLoader>();
        const packageJson = mock<PackageJson>();
        when(loader.load(path.resolve('package.json'))).thenReturn(instance(packageJson));
        when(packageJson.getDependencyVersion(npmModules.TYPEORM)).thenReturn('^1.2.3');

        const target = new DatabasePackageJsonImpl(instance(loader));

        expect(target).toEqual<DatabasePackageJson>({
            dependencies: { typeorm: '^1.2.3' },
        });
        expect(target.dependencies).toBeFrozen();
    });
});
