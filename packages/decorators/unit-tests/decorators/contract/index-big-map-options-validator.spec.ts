import { expectToThrow } from '../../../../../test-utilities/mocks';
import { ArgError } from '../../../../utils/src/public-api';
import { validateBigMapOptions } from '../../../src/decorators/contract/index-big-map-options-validator';
import { IndexBigMapUpdateOptions } from '../../../src/decorators/contract/index-big-map-update-decorator';

function indexFoo() {}

describe(validateBigMapOptions.name, () => {
    const act = (o: IndexBigMapUpdateOptions) => validateBigMapOptions(o, indexFoo);

    it.each<[string, IndexBigMapUpdateOptions]>([
        ['with name', { name: 'TezosDomains' }],
        ['with path', { path: ['memes', 'lol'] }],
        ['with empty path', { path: [] }],
    ])('should pass if valid options with %s', (_desc, options) => {
        const validated = act(options);

        expect(validated).toEqual(options);
    });

    it.each([
        ['@indexFoo.options', 'undefined', undefined],
        ['@indexFoo.options', 'null', null],
        ['@indexFoo.options', 'not object', 'wtf'],
        ['@indexFoo.options', 'empty', {}],
        ['@indexFoo.options.name', 'null', { name: null }],
        ['@indexFoo.options.name', 'white-space', { name: '  ' }],
        ['@indexFoo.options.name', 'not string', { name: 123 }],
        ['@indexFoo.options.path', 'null', { path: null }],
        ['@indexFoo.options.path', 'not array', { path: 'wtf' }],
        ['@indexFoo.options.path[0]', 'contains null', { path: [null] }],
        ['@indexFoo.options.path[0]', 'contains white-space string', { path: ['  '] }],
    ])('should throw if %s is %s', (expectedArgName, _desc, options: any) => {
        const error = expectToThrow(() => act(options), ArgError);

        expect(error.argName).toBe(expectedArgName);
    });
});
