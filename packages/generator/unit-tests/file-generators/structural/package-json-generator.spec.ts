import { JSONSchemaForNPMPackageJsonFiles } from '@schemastore/package';

import { describeMember, expectToThrow } from '../../../../../test-utilities/mocks';
import { PackageJsonGenerator } from '../../../src/file-generators/structural/package-json-generator';
import { StructuralFileOptions } from '../../../src/file-generators/structural/structural-file-generator';
import { generatedPaths } from '../../../src/helpers/generated-paths';
import { Exposed } from '../exposed-json-file-generator';

describe(PackageJsonGenerator.name, () => {
    const target: Exposed<PackageJsonGenerator> = new PackageJsonGenerator() as any;
    const options: StructuralFileOptions = 'ignored' as any;

    describeMember<typeof target>('getRelPath', () => {
        it('should return static file path', () => {
            expect(target.getRelPath(options)).toBe(generatedPaths.PACKAGE_JSON);
        });
    });

    describeMember<typeof target>('validateExistingContents', () => {
        it('should pass if valid', () => {
            target.validateExistingContents({ scripts: {} });
        });

        it.each([
            ['$', 'wtf' as unknown as JSONSchemaForNPMPackageJsonFiles],
            ['$.scripts', { scripts: 'wtf' as any } as JSONSchemaForNPMPackageJsonFiles],
        ])('should fail if invalid %s', (expectedMessage, config) => {
            const error = expectToThrow(() => target.validateExistingContents(config));

            expect(error.message).toInclude(expectedMessage);
        });
    });

    describeMember<typeof target>('generateContentsObject', () => {
        it.each<[string, JSONSchemaForNPMPackageJsonFiles | null]>([
            ['does not exist', null],
            ['has empty object', {}],
            ['has similar properties', { main: 'FooBar', scripts: { prebuild: 'bbb' } }],
        ])('should overwrite config if previous file %s', (_desc, existingContents) => {
            const contents = target.generateContentsObject(options, existingContents);

            expect(contents).toEqual({
                main: 'dist/index.js',
                scripts: {
                    prebuild: 'rimraf ./dist',
                    build: 'tsc -p ./tsconfig.json',
                },
            });
        });

        it('should merge changes if another already exists', () => {
            const existingContents: JSONSchemaForNPMPackageJsonFiles = {
                name: 'OmgLol',
                main: 'fail.js', // Overwritten.
                scripts: {
                    build: 'wtf lol.json', // Overwritten.
                    test: 'jest *.scpe.ts',
                },
            };

            const contents = target.generateContentsObject(options, existingContents);

            expect(contents).toEqual({
                name: 'OmgLol',
                main: 'dist/index.js',
                scripts: {
                    test: 'jest *.scpe.ts',
                    prebuild: 'rimraf ./dist',
                    build: 'tsc -p ./tsconfig.json',
                },
            });
        });
    });
});
