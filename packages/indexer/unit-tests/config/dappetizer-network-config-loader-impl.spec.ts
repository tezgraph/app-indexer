import nodeFS from 'fs';
import path from 'path';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { expectToThrow } from '../../../../test-utilities/mocks';
import { loadNetworkConfigsImpl } from '../../src/config/dappetizer-network-config-loader-impl';

describe(loadNetworkConfigsImpl.name, () => {
    let fs: typeof nodeFS;
    let nodeJS: { readonly require: NodeJS.Require };

    const act = () => loadNetworkConfigsImpl('dir/path', instance(fs), instance(nodeJS));

    beforeEach(() => {
        fs = mock();
        nodeJS = mock();

        when(fs.readdirSync('dir/path')).thenReturn([
            'node_modules',
            'dist',
            'jest.config.json',
            'dappetizer.config.ts',
            'dappetizer.mainnet.config.json',
            'dappetizer.ghostnet.config.json',
            'dappetizer..config.json',
            'invalid.wtfnet.config.json',
        ]);
        when(nodeJS.require(path.resolve('dir/path/dappetizer.mainnet.config.json'))).thenReturn('mockedMainnet');
        when(nodeJS.require(path.resolve('dir/path/dappetizer.ghostnet.config.json'))).thenReturn('mockedGhostnet');
    });

    it('should load all network configs from directory', () => {
        const config = act();

        expect(config).toEqual({
            mainnet: 'mockedMainnet',
            ghostnet: 'mockedGhostnet',
        });
        verify(nodeJS.require(anything())).times(2);
    });

    it('should throw if dir error', () => {
        when(fs.readdirSync(anything())).thenThrow(new Error('oups'));

        runThrowTest(['oups']);
    });

    it('should throw if no configs', () => {
        when(fs.readdirSync('dir/path')).thenReturn(['dist', 'jest.config.json']);

        runThrowTest([
            `any file`,
            `starting with 'dappetizer.'`,
            `ending with '.config.json'`,
        ]);
        verify(nodeJS.require(anything())).never();
    });

    it('should throw if failed to load config', () => {
        when(nodeJS.require(path.resolve('dir/path/dappetizer.ghostnet.config.json'))).thenThrow(new Error('oups'));

        runThrowTest([`file 'dappetizer.ghostnet.config.json'`]);
    });

    function runThrowTest(expectedError: string[]) {
        const error = expectToThrow(act);

        expect(error.message).toIncludeMultiple([`'${path.resolve('dir/path')}'`, ...expectedError]);
    }
});
