import { singleton } from 'tsyringe';

import { MichelsonSchema } from '../../../michelson/michelson-schema';
import { LazyMichelsonValue, Michelson } from '../../rpc-data-interfaces';

@singleton()
export class LazyMichelsonValueFactory {
    create(michelson: Michelson, schema: MichelsonSchema): LazyMichelsonValue {
        return Object.freeze<LazyMichelsonValue>({
            michelson,
            schema,

            convert: <T>() => {
                return schema.execute<T>(michelson);
            },
        });
    }
}
