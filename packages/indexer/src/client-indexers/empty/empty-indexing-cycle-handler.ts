import { noop } from 'lodash';

import { IndexingCycleHandler } from '../indexing-cycle-handler';

export class EmptyIndexingCycleHandler<TDbContext, TContextData> implements Required<IndexingCycleHandler<TDbContext, TContextData>> {
    readonly beforeIndexersExecute = noop;
    readonly afterIndexersExecuted = noop;
    readonly afterBlockIndexed = noop;
    readonly rollBackOnReorganization = noop;

    createContextData(): TContextData {
        return undefined as TContextData;
    }
}
