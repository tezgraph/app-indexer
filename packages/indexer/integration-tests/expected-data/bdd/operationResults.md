# Story: Indexing of Operation Results

**As** an indexer developer using Dappetizer,  
**When** indexing a block  
**I want** Dappetizer to call my indexer code providing respective operation results,  
**So that** I can index them.

## Scenario: Indexing Block 5771696

**When** Dappetizer indexes block 5771696  
**Then** Dappetizer calls my index method with respective data one-by-one as follows:

| Status | Operation Kind | Operation Group Hash |
| --- | --- | --- |
| Applied | Transaction | ongp9yC7eBYYQxScswxb3uxRg2EDvgtBDZMFUg6P15aM3dLfwzY |
| Applied | SmartRollupExecuteOutboxMessage | ootfQ18AU3K5XWyEC81AMCbxX2QFY3doJw5D3aD53gFPopHJqvW |
| Applied | Transaction | ootfQ18AU3K5XWyEC81AMCbxX2QFY3doJw5D3aD53gFPopHJqvW |
| Applied | Transaction | ootfQ18AU3K5XWyEC81AMCbxX2QFY3doJw5D3aD53gFPopHJqvW |
| Applied | SmartRollupAddMessages | opVEV5j7S2F2qWfSA4324tFc5GhFgJwy6du1hJFKTKvaB3E3Dez |
| Applied | Transaction | onm1EFR1dc4dPLTosv7im2GySVxS6YiMEGR5WwGN7iKp6JXgbfs |

## Scenario: Indexing Block 5771697

**When** Dappetizer indexes block 5771697  
**Then** Dappetizer calls my index method with respective data one-by-one as follows:

| Status | Operation Kind | Operation Group Hash |
| --- | --- | --- |
| Applied | SmartRollupAddMessages | opT9uWRsK5A5j7bDEyA3iCsddTNCg9oERKrpn4DncTdbgGPcyHj |
