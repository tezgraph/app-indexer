import { ArrayValidatorImpl } from './array-validator-impl';
import { typeValidator } from './type-validator';
import { Validator } from './validator';
import { protectAsPublicApi } from './validator-utils';

export type ArrayLimits = 'NotEmpty';

/** Low-level validation of an array. */
export interface ArrayValidator extends Validator<readonly unknown[], ArrayLimits | void> {}

/** Static instance of {@link ArrayValidator}. */
export const arrayValidator: ArrayValidator = protectAsPublicApi(new ArrayValidatorImpl(typeValidator));
