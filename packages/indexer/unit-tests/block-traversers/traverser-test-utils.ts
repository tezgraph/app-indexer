import { Writable } from 'ts-essentials';
import { instance, mock } from 'ts-mockito';

import { ContextData, DbContext } from '../../../../test-utilities/mocks';
import { IndexerModuleWrapper } from '../../src/client-indexers/wrappers/indexer-module-wrapper';

export interface MockedIndexerModule extends Writable<IndexerModuleWrapper<DbContext, ContextData>> {
    readonly mocks: IndexerModuleWrapper<DbContext, ContextData>;
}

export function mockIndexerModule(): MockedIndexerModule {
    const mocks = {
        blockDataIndexer: mock(),
        contractIndexers: mock(),
        smartRollupIndexers: mock(),
    } as IndexerModuleWrapper<DbContext, ContextData>;

    return {
        blockDataIndexer: instance(mocks.blockDataIndexer),
        contractIndexers: instance(mocks.contractIndexers),
        smartRollupIndexers: instance(mocks.smartRollupIndexers),
        mocks,
    } as MockedIndexerModule;
}
