import { spy, when } from 'ts-mockito';

import { expectToThrow } from '../../../../../test-utilities/mocks';
import { PrimitiveConverter } from '../../../src/rpc-data/primitives/primitive-converter';
import { RpcConversionError } from '../../../src/rpc-data/rpc-conversion-error';
import { convertValue } from '../rpc-test-helpers';

interface Options { val: number }

class TargetConverter extends PrimitiveConverter<number, string, Options> {
    override getTypeDescription = (): string => 'Should be mocked.';

    convertPrimitive(_rpcData: unknown, _options: Options): string {
        throw new Error('Should be mocked.');
    }
}

describe(PrimitiveConverter.name, () => {
    let target: TargetConverter;
    let targetSpy: TargetConverter;
    let options: Options;

    const act = (d: unknown) => convertValue(target, d, 'val/id', options);

    beforeEach(() => {
        target = new TargetConverter();
        targetSpy = spy(target);
        options = { val: 66 };
    });

    it('should convert primitive', () => {
        when(targetSpy.convertPrimitive(123, options)).thenReturn('abc');

        const result = act(123);

        expect(result).toBe('abc');
    });

    it('should wrap error including all value details', () => {
        target.getTypeDescription = () => 'foo bar';
        when(targetSpy.convertPrimitive(456, options)).thenThrow(new Error('oups'));

        const error = expectToThrow(() => act(456), RpcConversionError);

        expect(error.dataDescription).toIncludeMultiple(['foo bar', '456', `'number'`, 'oups']);
        expect(error.uid).toBe('val/id');
    });
});
