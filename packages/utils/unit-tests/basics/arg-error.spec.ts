import { ArgError } from '../../src/basics/arg-error';

describe(ArgError.name, () => {
    it('should be constructed correctly', () => {
        const error = new ArgError({
            argName: 'foo',
            specifiedValue: 123,
            details: 'Wtf is this?',
        });

        expect(error.argName).toBe('foo');
        expect(error.specifiedValue).toBe(123);
        expect(error.details).toBe('Wtf is this?');
        expect(error.message).toBe(`Argument 'foo' is invalid. Wtf is this? The specified value: 123.`);
        expect(error).toBeFrozen();
    });
});
