import { LogData, LogLevel } from './log-entry';

/** Main API for logging. */
export interface Logger {
    readonly category: string;

    readonly isCriticalEnabled: boolean;
    readonly isErrorEnabled: boolean;
    readonly isWarningEnabled: boolean;
    readonly isInformationEnabled: boolean;
    readonly isDebugEnabled: boolean;
    readonly isTraceEnabled: boolean;

    isEnabled(level: LogLevel): boolean;

    log(level: LogLevel, message: string, data?: LogData): void;

    runWithData<TResult>(data: LogData, scopeFunc: () => TResult): TResult;

    logCritical(message: string, data?: LogData): void;
    logError(message: string, data?: LogData): void;
    logWarning(message: string, data?: LogData): void;
    logInformation(message: string, data?: LogData): void;
    logDebug(message: string, data?: LogData): void;
    logTrace(message: string, data?: LogData): void;
}
