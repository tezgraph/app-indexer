import path from 'path';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { TestLogger, verifyCalled } from '../../../../test-utilities/mocks';
import { FileSystem } from '../../../utils/src//public-api';
import { OutDirInitializer } from '../../src/helpers/out-dir-initializer';

describe(OutDirInitializer.name, () => {
    let target: OutDirInitializer;
    let fileSystem: FileSystem;
    let logger: TestLogger;

    beforeEach(() => {
        fileSystem = mock();
        logger = new TestLogger();
        target = new OutDirInitializer(instance(fileSystem), logger);
    });

    afterEach(() => {
        logger.loggedSingle()
            .verify('Information', { directory: path.resolve('./out/dir') })
            .verifyMessage('foo bar');
    });

    it('should write package.json if does not exist', async () => {
        await target.init('./out/dir', 'foo bar');

        verifyCalled(fileSystem.existsAsFile).onceWith(path.resolve('./out/dir/package.json'));
        verifyCalled(fileSystem.writeFileText).onceWith(path.resolve('./out/dir/package.json'), '{}');
    });

    it('should do nothing if package.json if exists', async () => {
        when(fileSystem.existsAsFile(path.resolve('./out/dir/package.json'))).thenResolve(true);

        await target.init('./out/dir', 'foo bar');

        verifyCalled(fileSystem.existsAsFile).onceWith(path.resolve('./out/dir/package.json'));
        verify(fileSystem.writeFileText(anything(), anything())).never();
    });
});
