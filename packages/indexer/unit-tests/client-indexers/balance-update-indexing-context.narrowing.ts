import { assert, IsExact } from 'conditional-type-checks';

import { BalanceUpdateIndexingContext } from '../../src/client-indexers/block-data-indexer';
import { OperationGroup } from '../../src/rpc-data/block/operation-group';
import {
    InternalOperationWithBalanceUpdatesInResult,
} from '../../src/rpc-data/operations/narrowed/internal-operation-with-balance-updates-in-result';
import {
    MainOperationWithBalanceUpdatesInMetadata,
} from '../../src/rpc-data/operations/narrowed/main-operation-with-balance-updates-in-metadata';
import {
    MainOperationWithBalanceUpdatesInResult,
} from '../../src/rpc-data/operations/narrowed/main-operation-with-balance-updates-in-result';
import { MainOperationWithResult } from '../../src/rpc-data/operations/narrowed/main-operation-with-result';

const context: BalanceUpdateIndexingContext = {} as any;

assert<IsExact<BalanceUpdateIndexingContext['balanceUpdateSource'], typeof context.balanceUpdateSource>>(true);
assert<IsExact<InternalOperationWithBalanceUpdatesInResult | null, typeof context.internalOperation>>(true);
type ExpectedMainOperation = MainOperationWithResult | MainOperationWithBalanceUpdatesInMetadata | MainOperationWithBalanceUpdatesInResult | null;
assert<IsExact<ExpectedMainOperation, typeof context.mainOperation>>(true);
assert<IsExact<OperationGroup | null, typeof context.operationGroup>>(true);

switch (context.balanceUpdateSource) {
    case 'BlockMetadata':
        assert<IsExact<null, typeof context.internalOperation>>(true);
        assert<IsExact<null, typeof context.mainOperation>>(true);
        assert<IsExact<null, typeof context.operationGroup>>(true);
        break;

    case 'InternalOperationAppliedResult':
        assert<IsExact<InternalOperationWithBalanceUpdatesInResult, typeof context.internalOperation>>(true);
        assert<IsExact<MainOperationWithResult, typeof context.mainOperation>>(true);
        assert<IsExact<OperationGroup, typeof context.operationGroup>>(true);
        break;

    case 'MainOperationMetadata':
        assert<IsExact<null, typeof context.internalOperation>>(true);
        assert<IsExact<MainOperationWithBalanceUpdatesInMetadata, typeof context.mainOperation>>(true);
        assert<IsExact<OperationGroup, typeof context.operationGroup>>(true);
        break;

    case 'MainOperationAppliedResult':
        assert<IsExact<null, typeof context.internalOperation>>(true);
        assert<IsExact<MainOperationWithBalanceUpdatesInResult, typeof context.mainOperation>>(true);
        assert<IsExact<OperationGroup, typeof context.operationGroup>>(true);
        break;
}
