import { dbColumnName, dbTableName } from '../../src/helpers/naming-helper';

class DbFooBar {
    firstName!: string;
}

describe('naming helper', () => {
    describe(dbColumnName.name, () => {
        it('should get column name according to strategy', () => {
            const name = dbColumnName<DbFooBar>('firstName');

            expect(name).toBe('first_name');
        });
    });

    describe(dbTableName.name, () => {
        it('should get correct name according to strategy', () => {
            const name = dbTableName(DbFooBar);

            expect(name).toBe('foo_bar');
        });
    });
});
