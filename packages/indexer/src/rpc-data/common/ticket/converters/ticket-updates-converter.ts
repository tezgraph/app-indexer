import * as taquitoRpc from '@taquito/rpc';
import { keyof, Nullish } from '@tezos-dappetizer/utils';
import { DeepReadonly } from 'ts-essentials';
import { singleton } from 'tsyringe';

import { TicketTokenConverter } from './ticket-token-converter';
import { TicketUpdateConverter } from './ticket-update-converter';
import { joinUid, RpcObjectConverter } from '../../../rpc-converter';
import { TicketUpdate } from '../../../rpc-data-interfaces';

export type TaquitoRpcTicketUpdates = DeepReadonly<taquitoRpc.TicketUpdates>;

@singleton()
export class SingleTicketUpdatesConverter extends RpcObjectConverter<TaquitoRpcTicketUpdates, readonly TicketUpdate[]> {
    constructor(
        private readonly tokenConverter: TicketTokenConverter,
        private readonly updateConverter: TicketUpdateConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcTicketUpdates,
        uid: string,
    ): readonly TicketUpdate[] {
        const tokenUid = joinUid(uid, keyof<typeof rpcData>('ticket_token'));
        const token = this.tokenConverter.convertFromProperties(rpcData.ticket_token, tokenUid, {
            ticketer: 'ticketer',
            content: 'content',
            contentType: 'content_type',
        });

        const updates = this.updateConverter.convertArrayProperty(rpcData, 'updates', uid, token);
        return updates;
    }
}

@singleton()
export class TicketUpdatesConverter {
    constructor(private readonly updatesConverter: SingleTicketUpdatesConverter) {}

    convertArrayProperty<TProperty extends string>(
        rpcParentData: { [P in TProperty]?: Nullish<readonly TaquitoRpcTicketUpdates[]> },
        propertyName: TProperty,
        parentUid: string,
    ): readonly TicketUpdate[] {
        const allUpdates = this.updatesConverter.convertArrayProperty(rpcParentData, propertyName, parentUid);
        return Object.freeze(allUpdates.flatMap(u => u));
    }
}
