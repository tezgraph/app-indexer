import * as taquitoRpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import { TestClock } from '../../../../test-utilities/mocks';
import { HealthCheck, HealthCheckResult, HealthStatus } from '../../src/health/public-api';
import { RpcHealthWatcher } from '../../src/tezos-rpc/rpc-health-watcher';
import { TezosNodeConfig } from '../../src/tezos-rpc/tezos-node-config';

describe(RpcHealthWatcher.name, () => {
    let target: HealthCheck;
    let rpcClient: taquitoRpc.RpcClient;
    let clock: TestClock;
    let config: TezosNodeConfig;

    beforeEach(() => {
        rpcClient = mock();
        clock = new TestClock();
        config = { health: { unhealthyAfterMillis: 3_000 } } as TezosNodeConfig;
        target = new RpcHealthWatcher(instance(rpcClient), clock, config);
    });

    it('should return healthy if received block', async () => {
        setupTestBlock();

        const result = await target.checkHealth();

        expect(result).toEqual<HealthCheckResult>({
            status: HealthStatus.Healthy,
            data: { headBlock: getExpectedBlock() },
        });
    });

    it.each([
        [HealthStatus.Degraded, 0],
        [HealthStatus.Degraded, 3_000],
        [HealthStatus.Unhealthy, 3_001],
    ])('should return %s if an error occurred at %s after healthy', async (expectedStatus, healthyBeforeMillis) => {
        // Fill last healthy block.
        setupTestBlock();
        await target.checkHealth();
        const lastHealthyHeadBlock = getExpectedBlock();

        const error = new Error('Oups');
        when(rpcClient.getBlockHeader()).thenReject(error);
        clock.tick(healthyBeforeMillis);

        const result = await target.checkHealth();

        expect(result).toEqual<HealthCheckResult>({
            status: expectedStatus,
            data: { error, lastHealthyHeadBlock, healthyBeforeMillis },
        });
    });

    function setupTestBlock() {
        when(rpcClient.getBlockHeader()).thenResolve({
            hash: 'hh',
            level: 123,
            chain_id: 'cc',
            timestamp: 'tt',
            protocol: 'not exposed',
        } as taquitoRpc.BlockHeaderResponse);
    }

    function getExpectedBlock() {
        return {
            hash: 'hh',
            level: 123,
            chainId: 'cc',
            timestamp: 'tt',
            receivedFromRpcOn: clock.nowDate,
        };
    }
});
