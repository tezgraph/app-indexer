import { AbortControllerFactory } from '../../src/app-control/abort-controller-factory';

describe(AbortControllerFactory.name, () => {
    let target: AbortControllerFactory;
    let parentCtrl1: AbortController;
    let parentCtrl2: AbortController;

    const act = () => target.createLinked(parentCtrl1.signal, parentCtrl2.signal);

    beforeEach(() => {
        target = new AbortControllerFactory();
        parentCtrl1 = new AbortController();
        parentCtrl2 = new AbortController();
    });

    it('should create not-aborted', () => {
        const linkedCtrl = act();

        expect(linkedCtrl.signal.aborted).toBeFalse();
    });

    it.each([
        [true, false],
        [false, true],
        [true, true],
    ])('should create aborted if some parent (%s %s) is already aborted', (parentAborted1, parentAborted2) => {
        if (parentAborted1) {
            parentCtrl1.abort();
        }
        if (parentAborted2) {
            parentCtrl2.abort();
        }

        const linkedCtrl = act();

        expect(linkedCtrl.signal.aborted).toBeTrue();
    });

    it.each([1, 2 as const])('should create controller that aborts if parent %s gets aborted', parentIndex => {
        const linkedCtrl = target.createLinked(parentCtrl1.signal, parentCtrl2.signal);

        (parentIndex === 1 ? parentCtrl1 : parentCtrl2).abort();
        expect(linkedCtrl.signal.aborted).toBeTrue();
    });
});
