import { Contract, ContractConfig } from '../../../contracts/contract';

/** @category RPC Block Data */
export interface LazyContract {
    /** Usable for narrowing the type of this object. */
    readonly type: 'Contract';

    /** Contract `KT1` address. */
    readonly address: string;

    readonly config: ContractConfig;

    getContract(): Promise<Contract>;
}
