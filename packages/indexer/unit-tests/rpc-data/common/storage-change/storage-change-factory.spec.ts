import * as taquitoRpc from '@taquito/rpc';
import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { Contract } from '../../../../src/contracts/contract';
import { LazyMichelsonValueFactory } from '../../../../src/rpc-data/common/michelson/lazy-michelson-value-factory';
import { StorageChangeFactory } from '../../../../src/rpc-data/common/storage-change/storage-change-factory';
import { LazyContract, LazyMichelsonValue, Michelson, StorageChange } from '../../../../src/rpc-data/rpc-data-interfaces';

describe(StorageChangeFactory.name, () => {
    let target: StorageChangeFactory;
    let lazyValueFactory: LazyMichelsonValueFactory;
    let rpcClient: taquitoRpc.RpcClient;

    let newMichelson: Michelson;
    let lastMichelsonWithinBlock: Michelson | null;
    let predecessorMichelson: taquitoRpc.MichelsonV1Expression;
    let lazyContract: LazyContract;
    let contract: Contract;
    let newValue: LazyMichelsonValue;
    let oldValue: LazyMichelsonValue;

    const act = async () => target.create({
        newMichelson,
        lastMichelsonWithinBlock,
        lazyContract: instance(lazyContract),
        predecessorBlockHash: 'pred-haha',
        uid: 'stor/id',
    });

    beforeEach(() => {
        [lazyValueFactory, rpcClient] = [mock(), mock()];
        target = new StorageChangeFactory(instance(lazyValueFactory), instance(rpcClient));

        newMichelson = 'mockedNewMichelson' as any;
        lastMichelsonWithinBlock = null;
        predecessorMichelson = 'mockedPredecessorMichelson' as any;
        lazyContract = mock();
        contract = { storageSchema: 'mockedSchema' as any } as typeof contract;
        newValue = 'mockedNewValue' as any;
        oldValue = 'mockedOldValue' as any;

        when(lazyContract.address).thenReturn('mockedAddr');
        when(lazyContract.getContract()).thenResolve(contract);
        when(rpcClient.getStorage('mockedAddr', deepEqual({ block: 'pred-haha' }))).thenResolve(predecessorMichelson);
        when(lazyValueFactory.create(newMichelson, contract.storageSchema)).thenReturn(newValue);
    });

    it('should create correctly using predecessor value', async () => {
        when(lazyValueFactory.create(predecessorMichelson, contract.storageSchema)).thenReturn(oldValue);

        const storageChange = await act();

        expect(storageChange).toEqual<StorageChange>({
            newValue,
            oldValue,
            rpcData: newMichelson,
            uid: 'stor/id',
        });
        expect(storageChange).toBeFrozen();
    });

    it('should create correctly if there is last value within block', async () => {
        lastMichelsonWithinBlock = 'mockedLastMichelson' as any;
        when(lazyValueFactory.create(lastMichelsonWithinBlock!, contract.storageSchema)).thenReturn(oldValue);

        const storageChange = await act();

        expect(storageChange.oldValue).toBe(oldValue);
        verify(rpcClient.getStorage(anything(), anything())).never();
    });
});
