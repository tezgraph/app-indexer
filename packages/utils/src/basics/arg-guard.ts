import { AddressPrefix } from './address-prefix';
import { ArgGuardHelper, ArgGuardImpl } from './arg-guard-impl';
import { Constructor, Nullish } from './reflection-types';
import { addressValidator } from './validators/address-validator';
import { arrayValidator } from './validators/array-validator';
import { NumberLimits, numberValidator } from './validators/number-validator';
import { objectValidator } from './validators/object-validator';
import { oneOfSupportedValidator } from './validators/one-of-supported-validator';
import { StringLimits, stringValidator } from './validators/string-validator';
import { typeValidator } from './validators/type-validator';

/**
 * Useful for validation of input arguments on public API.
 * @throws {@link ArgError}
 */
export interface ArgGuard {
    address(value: string, argName: string, prefixes?: readonly AddressPrefix[]): string;
    function<T extends Function>(value: T, argName: string): T;
    object<T extends object>(value: T, argName: string, constructor?: Constructor<T>): T;

    // Array guards:
    array<T extends readonly unknown[]>(value: T, argName: string): T;
    nonEmptyArray<T extends readonly unknown[]>(value: T, argName: string): T;

    // String guards:
    string(value: string, argName: string, limits?: StringLimits): string;
    nonEmptyString(value: string, argName: string): string;
    nonWhiteSpaceString(value: string, argName: string): string;
    number(value: number, argName: string, limits?: NumberLimits): number;

    // One of supported guards:
    oneOf<TSupported extends string, TValue extends TSupported>(
        value: TValue,
        argName: string,
        supportedValues: readonly TSupported[],
    ): TValue;

    enum<TEnum extends string>(
        value: TEnum,
        argName: string,
        enumType: Readonly<Record<string, TEnum>>
    ): TEnum;

    // Helpers:
    ifNotNullish<T>(value: Nullish<T>, validate: (value: T) => T): Nullish<T>;
}

const argGuardImpl: ArgGuard = new ArgGuardImpl(
    new ArgGuardHelper(addressValidator),
    new ArgGuardHelper(arrayValidator),
    new ArgGuardHelper(numberValidator),
    new ArgGuardHelper(objectValidator),
    new ArgGuardHelper(oneOfSupportedValidator),
    new ArgGuardHelper(stringValidator),
    new ArgGuardHelper(typeValidator),
);

/** Static instance of {@link ArgGuard}. */
// Frozen and hidden internal impl b/c it's public API.
export const argGuard: ArgGuard = Object.freeze<ArgGuard>({
    address: argGuardImpl.address.bind(argGuardImpl),
    array: argGuardImpl.array.bind(argGuardImpl),
    enum: argGuardImpl.enum.bind(argGuardImpl),
    function: argGuardImpl.function.bind(argGuardImpl),
    ifNotNullish: argGuardImpl.ifNotNullish.bind(argGuardImpl),
    nonEmptyArray: argGuardImpl.nonEmptyArray.bind(argGuardImpl),
    nonEmptyString: argGuardImpl.nonEmptyString.bind(argGuardImpl),
    nonWhiteSpaceString: argGuardImpl.nonWhiteSpaceString.bind(argGuardImpl),
    number: argGuardImpl.number.bind(argGuardImpl),
    object: argGuardImpl.object.bind(argGuardImpl),
    oneOf: argGuardImpl.oneOf.bind(argGuardImpl),
    string: argGuardImpl.string.bind(argGuardImpl),
});
