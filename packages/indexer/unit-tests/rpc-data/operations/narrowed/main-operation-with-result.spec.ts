import { StrictExclude } from 'ts-essentials';

import { MainOperation } from '../../../../src/rpc-data/operations/main-operation';
import {
    isMainOperationWithResult,
    MainOperationWithResult,
} from '../../../../src/rpc-data/operations/narrowed/main-operation-with-result';

describe(isMainOperationWithResult.name, () => {
    const positiveTestCases: Record<MainOperationWithResult['kind'], true> = {
        Delegation: true,
        IncreasePaidStorage: true,
        Origination: true,
        RegisterGlobalConstant: true,
        Reveal: true,
        SetDepositsLimit: true,
        SmartRollupAddMessages: true,
        SmartRollupCement: true,
        SmartRollupExecuteOutboxMessage: true,
        SmartRollupOriginate: true,
        SmartRollupPublish: true,
        SmartRollupRecoverBond: true,
        SmartRollupRefute: true,
        SmartRollupTimeout: true,
        Transaction: true,
        TransferTicket: true,
        UpdateConsensusKey: true,
    };
    const negativeTestCases: Record<StrictExclude<MainOperation['kind'], MainOperationWithResult['kind']>, false> = {
        ActivateAccount: false,
        Ballot: false,
        DoubleBakingEvidence: false,
        DoubleEndorsementEvidence: false,
        DoublePreendorsementEvidence: false,
        DrainDelegate: false,
        Endorsement: false,
        Preendorsement: false,
        Proposals: false,
        SeedNonceRevelation: false,
        VdfRevelation: false,
    };

    it.each(
        Object.entries({ ...positiveTestCases, ...negativeTestCases }),
    )('should narrow %s to %s', (kind, expectedNarrowing) => {
        const isNarrowed = isMainOperationWithResult({ kind } as MainOperation);

        expect(isNarrowed).toBe(expectedNarrowing);
    });
});
