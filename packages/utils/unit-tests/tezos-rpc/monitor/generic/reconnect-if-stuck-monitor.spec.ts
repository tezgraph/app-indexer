import { asyncToArray, asyncWrap } from 'iter-tools';
import { noop } from 'lodash';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { TestLogger } from '../../../../../../test-utilities/mocks';
import { AbortControllerFactory } from '../../../../src/app-control/abort-controller-factory';
import { AppShutdownManager } from '../../../../src/app-control/public-api';
import { AsyncIterableProvider } from '../../../../src/basics/public-api';
import { DownloadDataMonitor } from '../../../../src/tezos-rpc/monitor/generic/download-data-monitor';
import { ReconnectIfStuckMonitor } from '../../../../src/tezos-rpc/monitor/generic/reconnect-if-stuck-monitor';
import { TezosNodeConfig } from '../../../../src/tezos-rpc/tezos-node-config';
import { Timeout, TimerHelper } from '../../../../src/time/public-api';

describe(ReconnectIfStuckMonitor.name, () => {
    let target: AsyncIterableProvider<unknown>;
    let logger: TestLogger;
    let nextMonitor: DownloadDataMonitor;
    let appShutdownSignal: AbortSignal;
    let abortControllerFactory: AbortControllerFactory;
    let timerHelper: TimerHelper;
    let config: TezosNodeConfig;

    let linkedAbortController: AbortController;

    const act = async () => asyncToArray(target.iterate());

    beforeEach(() => {
        [nextMonitor, appShutdownSignal, abortControllerFactory, timerHelper] = [mock(), mock(), mock(), mock()];
        logger = new TestLogger();
        config = { monitor: { reconnectIfStuckMillis: 666 } } as TezosNodeConfig;
        target = new ReconnectIfStuckMonitor(
            logger,
            instance(nextMonitor),
            { signal: instance(appShutdownSignal) } as AppShutdownManager,
            instance(abortControllerFactory),
            instance(timerHelper),
            config,
        );

        linkedAbortController = mock(AbortController);
        when(linkedAbortController.signal).thenReturn({} as AbortSignal);
        when(abortControllerFactory.createLinked(instance(appShutdownSignal))).thenReturn(instance(linkedAbortController));
    });

    it('should yield item from next monitor until app shutdown', async () => {
        const timeouts = [mock<Timeout>(), mock<Timeout>(), mock<Timeout>()] as const;
        whenSetTimeout().thenReturn(...timeouts.map(instance));

        when(appShutdownSignal.aborted).thenReturn(false, true);
        whenNextMonitorIterate().thenReturn(asyncWrap([1, 2]));

        const items = await act();

        expect(items).toEqual([1, 2]);

        verify(timerHelper.setTimeout(anything(), anything())).times(3);
        verify(timeouts[0].clear()).once();
        verify(timeouts[1].clear()).once();
        verify(timeouts[2].clear()).never();

        verify(abortControllerFactory.createLinked(anything())).once();
        verify(linkedAbortController.abort()).never();
        logger.verifyNothingLogged();
    });

    it.each(['connect', 'iteration'])('should abort iteration if idle too long on %s', async testCase => {
        whenSetTimeout()
            .thenCall((_delay, callback) => {
                (testCase === 'connect' ? callback : noop)();
                return mock();
            })
            .thenCall((_delay, callback) => {
                (testCase === 'iteration' ? callback : noop)();
                return mock();
            });

        when(appShutdownSignal.aborted).thenReturn(false, true);
        whenNextMonitorIterate().thenReturn(asyncWrap([1]));

        const items = await act();

        expect(items).toEqual([1]);
        verify(abortControllerFactory.createLinked(anything())).once();
        verify(linkedAbortController.abort()).once();
        logger.loggedSingle().verify('Warning', { millis: 666 });
    });

    it('should keep iterating if aborted (ended)', async () => {
        whenSetTimeout().thenReturn(mock());
        when(appShutdownSignal.aborted).thenReturn(false, false, true);
        whenNextMonitorIterate().thenReturn(asyncWrap([1, 2]), asyncWrap([3, 4]));

        const items = await act();

        expect(items).toEqual([1, 2, 3, 4]);
        verify(abortControllerFactory.createLinked(anything())).times(2);
        verify(linkedAbortController.abort()).never();
        logger.verifyNothingLogged();
    });

    function whenNextMonitorIterate() {
        return when(nextMonitor.iterate(instance(linkedAbortController).signal));
    }

    function whenSetTimeout() {
        return when(timerHelper.setTimeout(config.monitor.reconnectIfStuckMillis, anything()));
    }
});
