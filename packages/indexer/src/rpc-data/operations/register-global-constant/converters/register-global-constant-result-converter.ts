import { singleton } from 'tsyringe';

import { BalanceUpdateConverter } from '../../../common/balance-update/converters/balance-update-converter';
import { OperationErrorConverter } from '../../../common/operation-error/operation-error-converter';
import { primitives } from '../../../primitives';
import { AppliedRegisterGlobalConstantResult, TaquitoRpcRegisterGlobalConstantResult } from '../../../rpc-data-interfaces';
import { BaseOperationResultConverter, SpecificResultParts } from '../../common/operation-result/base-operation-result-converter';

export type RegisterGlobalConstantResultParts = SpecificResultParts<AppliedRegisterGlobalConstantResult>;

@singleton()
export class RegisterGlobalConstantResultConverter
    extends BaseOperationResultConverter<TaquitoRpcRegisterGlobalConstantResult, RegisterGlobalConstantResultParts> {
    constructor(
        operationErrorConverter: OperationErrorConverter,
        private readonly balanceUpdateConverter: BalanceUpdateConverter,
    ) {
        super(operationErrorConverter);
    }

    protected override convertAppliedResultParts(
        rpcData: TaquitoRpcRegisterGlobalConstantResult,
        uid: string,
    ): RegisterGlobalConstantResultParts {
        const balanceUpdates = this.balanceUpdateConverter.convertArrayProperty(rpcData, 'balance_updates', uid);

        return {
            balanceUpdates,
            globalAddress: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'global_address', uid),
            storageSize: primitives.bigInteger.convertProperty(rpcData, 'storage_size', uid),
        };
    }
}
