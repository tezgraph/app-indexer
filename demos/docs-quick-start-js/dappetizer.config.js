module.exports = {
    modules: [
        { id: './indexer-module.js' },
    ],
    networks: {
        mainnet: {
            indexing: {
                // Only index the registration contract - see https://better-call.dev/mainnet/KT191reDVKrLxU9rjTSxg53wRqj6zh8pnHgr
               contracts: [{
                    addresses: ['KT191reDVKrLxU9rjTSxg53wRqj6zh8pnHgr'],
                    name: 'Registration',
                }],
                fromBlockLevel: 1468130,
            },
            tezosNode: {
                url: 'https://prod.tcinfra.net/rpc/mainnet/',
            },
        },
    },
    database: {
        type: 'postgres',
        host: 'localhost',
        port: 5432,
        username: 'postgres',
        password: 'postgrespassword',
        database: 'postgres',
        schema: 'quick_start_js',
    }
};
