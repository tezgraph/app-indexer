// Separate file from main operation b/c it refers internal operations -> circle.
import { BigNumber } from 'bignumber.js';

import { LazyTransactionParameter } from './transaction-parameter';
import { TransactionResult } from './transaction-result';
import { UserTransactionDestination } from './user-transaction-destination';
import { SmartRollup } from '../../../smart-rollups/smart-rollup';
import { LazyContract } from '../../common/lazy-contract/lazy-contract';
import { BaseInternalOperation } from '../common/internal-operation/base-internal-operation';
import { OperationKind } from '../operation-kind';

/**
 * {@link TransactionOperation} executed as an internal operation.
 * @category RPC Block Data
 */
export interface TransactionInternalOperation extends BaseInternalOperation {
    /** Usable for narrowing the type of this object from {@link InternalOperation}. */
    readonly kind: OperationKind.Transaction;

    // Common transaction operation properties:

    /** It is mutez - greater than or equal to `0`. */
    readonly amount: BigNumber;

    /** @deprecated Use `destination.address` instead. */
    readonly destinationAddress: string;

    /** @deprecated Use `destination` instead. */
    readonly destinationContract: LazyContract | null;

    readonly destination: UserTransactionDestination | LazyContract | SmartRollup;
    readonly transactionParameter: LazyTransactionParameter | null;
    readonly result: TransactionResult;
}
