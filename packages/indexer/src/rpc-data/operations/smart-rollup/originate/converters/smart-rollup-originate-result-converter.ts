import { singleton } from 'tsyringe';

import { BalanceUpdateConverter } from '../../../../common/balance-update/converters/balance-update-converter';
import { OperationErrorConverter } from '../../../../common/operation-error/operation-error-converter';
import { primitives } from '../../../../primitives';
import { AppliedSmartRollupOriginateResult, TaquitoRpcSmartRollupOriginateResult } from '../../../../rpc-data-interfaces';
import {
    BaseOperationResultConverter,
    SpecificResultParts,
} from '../../../common/operation-result/base-operation-result-converter';
import { SmartRollupConverter } from '../../common/converters/smart-rollup-converter';

export type SmartRollupOriginateResultParts = SpecificResultParts<AppliedSmartRollupOriginateResult>;

@singleton()
export class SmartRollupOriginateResultConverter
    extends BaseOperationResultConverter<TaquitoRpcSmartRollupOriginateResult, SmartRollupOriginateResultParts> {
    constructor(
        operationErrorConverter: OperationErrorConverter,
        private readonly balanceUpdateConverter: BalanceUpdateConverter,
        private readonly rollupConverter: SmartRollupConverter,
    ) {
        super(operationErrorConverter);
    }

    protected override convertAppliedResultParts(
        rpcData: TaquitoRpcSmartRollupOriginateResult,
        uid: string,
    ): SmartRollupOriginateResultParts {
        const balanceUpdates = this.balanceUpdateConverter.convertArrayProperty(rpcData, 'balance_updates', uid);
        const originatedRollup = this.rollupConverter.convertProperty(rpcData, 'address', uid);

        return {
            balanceUpdates,
            genesisCommitmentHash: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'genesis_commitment_hash', uid),
            originatedRollup,
            size: primitives.bigInteger.convertProperty(rpcData, 'size', uid, { min: 0 }),
        };
    }
}
