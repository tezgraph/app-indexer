/* eslint-disable @typescript-eslint/no-explicit-any */

export type MethodDecoratorFactory<TArgs extends any[] = any[]> = (...args: TArgs) => MethodDecorator;

export type ClassDecoratorFactory<TArgs extends any[] = any[]> = (...args: TArgs) => ClassDecorator;
