import { expectToThrow } from '../../../../../test-utilities/mocks';
import { indexBigMapUpdate } from '../../../src/decorators/contract/index-big-map-update-decorator';

describe(indexBigMapUpdate.name, () => {
    it('should pass if correctly declared', () => {
        indexBigMapUpdate({ name: 'lol' });
    });

    it('should throw if incorrectly declared', () => {
        expectToThrow(() => indexBigMapUpdate('wtf' as any));
    });
});
