import { singleton } from 'tsyringe';

import { TaquitoRpcEventResult } from '../../../rpc-data-interfaces';
import { SimpleOperationResultConverter } from '../../common/operation-result/simple-operation-result-converter';

@singleton()
export class EventResultConverter extends SimpleOperationResultConverter<TaquitoRpcEventResult> {}
