import { LogData } from '../../src/logging/log-entry';
import { LoggerScope } from '../../src/logging/logger-scope';

describe(LoggerScope.name, () => {
    const target = new LoggerScope();

    it('should be created correctly', () => {
        expect(target).toBeFrozen();
    });

    it('should enrich data with scoped data', async () => {
        const topData: LogData = {
            topConflict: 'top',
            top: 0,
        };
        const enrichedDatas: LogData[] = [];

        await target.runWithData(
            { topConflict: 'scope1', scopeConflict: 'scope1', scope1: 11 },
            async () => {
                await Promise.resolve();
                enrichedDatas.push(target.enrichData(topData));

                target.runWithData(
                    { topConflict: 'scope2', scopeConflict: 'scope2', scope2: 22 },
                    () => {
                        enrichedDatas.push(target.enrichData(topData));
                    },
                );
                enrichedDatas.push(target.enrichData(topData));
            },
        );
        enrichedDatas.push(target.enrichData(topData));

        expect(enrichedDatas).toEqual<LogData[]>([
            { topConflict: 'top', top: 0, scopeConflict: 'scope1', scope1: 11 },
            { topConflict: 'top', top: 0, scopeConflict: 'scope2', scope1: 11, scope2: 22 },
            { topConflict: 'top', top: 0, scopeConflict: 'scope1', scope1: 11 },
            { topConflict: 'top', top: 0 },
        ]);
    });

    it('should not enrich with anything if no scoped data', () => {
        const inputData: LogData = { a: 123 };

        const resultData = target.enrichData(inputData);

        expect(resultData).toBe(inputData);
    });
});
