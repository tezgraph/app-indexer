// Separate file from main operation b/c it refers internal operations -> circle.
import { EventResult } from './event-result';
import { MichelsonSchema } from '../../../michelson/michelson-schema';
import { LazyMichelsonValue } from '../../common/michelson/lazy-michelson-value';
import { BaseInternalOperation } from '../common/internal-operation/base-internal-operation';
import { OperationKind } from '../operation-kind';

/**
 * It enables sending event-like information to external applications from Tezos smart contracts – see
 * {@link https://tezos.gitlab.io/alpha/event.html | Contract Events} for further detail.
 * @category RPC Block Data
 */
export interface EventInternalOperation extends BaseInternalOperation {
    // Narrowing properties:

    /** Usable for narrowing the type of this object from {@link InternalOperation}. */
    readonly kind: OperationKind.Event;

    // Specific properties:

    readonly payload: LazyMichelsonValue | null;
    readonly result: EventResult;
    readonly tag: string | null;
    readonly typeSchema: MichelsonSchema;
}
