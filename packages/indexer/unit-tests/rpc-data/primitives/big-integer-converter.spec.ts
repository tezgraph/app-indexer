import { BigNumber } from 'bignumber.js';

import { expectToThrow } from '../../../../../test-utilities/mocks';
import { BigIntegerConverter, BigIntegerConvertOptions } from '../../../src/rpc-data/primitives/big-integer-converter';
import { convertPrimitive } from '../rpc-test-helpers';

describe(BigIntegerConverter.name, () => {
    const target = new BigIntegerConverter();

    const act = (d: unknown, o: BigIntegerConvertOptions | void) => convertPrimitive(target, d, o);

    it.each<[string, string, BigIntegerConvertOptions | undefined]>([
        ['positive', '123', undefined],
        ['negative', '-456', undefined],
        ['over max JS integer', '9007199254740991125', undefined],
        ['with limits', '15', { min: 10, max: 20 }],
        ['at minimum', '10', { min: 10, max: 20 }],
        ['at maximum', '20', { min: 10, max: 20 }],
    ])('should create %s BigNumber', (_desc, value, options) => {
        const result = act(value, options);

        expect(result).toBeInstanceOf(BigNumber);
        expect(result).toEqual(new BigNumber(value));
    });

    it.each([
        ['NOT a valid value for BigNumber', undefined],
        ['NOT a valid value for BigNumber', null],
        ['NOT a valid value for BigNumber', 'abc'],
        ['NOT an integer', '12.34'],
        ['less than minimum 10', '9'],
        ['greater than maximum 20', '21'],
    ])('should throw if %s with value %s', (expectedMsg, rpcData) => {
        const error = expectToThrow(() => act(rpcData, { min: 10, max: 20 }));

        expect(error.message).toContain(expectedMsg);
    });
});
