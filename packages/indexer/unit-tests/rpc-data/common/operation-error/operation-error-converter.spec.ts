import { OperationErrorConverter } from '../../../../src/rpc-data/common/operation-error/operation-error-converter';
import { OperationError, TaquitoRpcOperationError } from '../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../rpc-test-helpers';

describe(OperationErrorConverter.name, () => {
    let target: OperationErrorConverter;
    let rpcData: TaquitoRpcOperationError;

    const act = () => convertObject(target, rpcData, 'err/id');

    beforeEach(() => {
        target = new OperationErrorConverter();

        rpcData = {
            kind: 'temporary',
            id: 'proto.010-PtGRANAD.michelson_v1.runtime_error',
        };
    });

    it('should convert correctly', () => {
        const error = act();

        expect(error).toEqual<OperationError>({
            id: 'proto.010-PtGRANAD.michelson_v1.runtime_error',
            kind: 'temporary',
            rpcData,
            uid: 'err/id',
        });
        expect(error).toBeFrozen();
    });
});
