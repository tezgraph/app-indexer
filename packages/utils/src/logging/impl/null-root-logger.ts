import { LogEntry, LogLevel } from '../log-entry';
import { RootLogger, RootLoggerFactory } from '../root-logger';

export class NullRootLogger implements RootLogger, RootLoggerFactory {
    constructor() {
        Object.freeze(this);
    }

    create(): RootLogger {
        return this;
    }

    isEnabled(_level: LogLevel): boolean {
        return false;
    }

    log(_entry: LogEntry): void {
        /* The log entry is dismissed. */
    }

    async close(): Promise<void> {
        return Promise.resolve();
    }
}
