import { ContractIndexer, TransactionIndexingContext, TransactionParameter } from '@tezos-dappetizer/indexer';

export class RegContractIndexer implements ContractIndexer<{}> {
    indexTransaction(transactionParameter: TransactionParameter, _dbContext: {}, _indexingContext: TransactionIndexingContext): void {
        if (typeof transactionParameter.entrypoint !== 'string') {
            throw new Error('Invalid transaction.');
        }
    }
}
