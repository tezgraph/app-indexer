import * as taquitoRpc from '@taquito/rpc';
import {
    dumpType,
    errorToString,
    hasConstructor,
    injectLogger,
    isObject,
    Logger,
    TAQUITO_RPC_CLIENT_DI_TOKEN,
} from '@tezos-dappetizer/utils';
import { BigNumber } from 'bignumber.js';
import { inject, singleton } from 'tsyringe';

import { BigMapInfo } from './big-map-info';
import { BigMapInfoImpl } from './big-map-info-impl';
import { scopedLogKeys } from '../../../helpers/scoped-log-keys';
import { LazyContract } from '../../../rpc-data/rpc-data-interfaces';
import { Contract } from '../../contract';

@singleton()
export class BigMapInfoResolver {
    constructor(
        @inject(TAQUITO_RPC_CLIENT_DI_TOKEN) private readonly rpcClient: taquitoRpc.RpcClient,
        @injectLogger(BigMapInfoResolver) private readonly logger: Logger,
    ) {}

    async resolve(id: BigNumber, lazyContract: LazyContract, blockHash: string): Promise<BigMapInfo | null> {
        if (id.lt(new BigNumber(0))) {
            return null; // Temporary big map -> can't be resolved -> null asap.
        }

        const contract = await lazyContract.getContract();

        let bigMap = this.findBigMap(contract, id);
        if (!bigMap) {
            await this.reloadBigMapIdsAtBlock(contract, blockHash);
        }

        bigMap = this.findBigMap(contract, id);
        if (!bigMap) {
            this.logger.logWarning(`Big map with {bigMapId} of {contract} was not found`
                + ` at {${scopedLogKeys.INDEXED_BLOCK}} within {${scopedLogKeys.INDEXED_OPERATION_GROUP_HASH}}.`
                + ` The contract has {knownBigMapIds} at the block.`
                + ` Indexing of related big map diffs may be skipped for {${scopedLogKeys.INDEXER_MODULE}}.`, {
                bigMapId: id,
                contract: contract.address,
                knownBigMapIds: contract.bigMaps.map(m => m.lastKnownId),
            });
        }
        return bigMap;
    }

    private findBigMap(contract: Contract, bigMapId: BigNumber): BigMapInfo | null {
        return contract.bigMaps.find(m => m.lastKnownId.isEqualTo(bigMapId)) ?? null;
    }

    private async reloadBigMapIdsAtBlock(contract: Contract, blockHash: string): Promise<void> {
        try {
            const storageMichelson = await this.rpcClient.getStorage(contract.address, { block: blockHash });
            const storage = contract.storageSchema.execute(storageMichelson);

            for (const bigMap of contract.bigMaps) {
                try {
                    (bigMap as BigMapInfoImpl).lastKnownId = extractBigMapId(storage, bigMap.path);
                } catch (error) {
                    throw new Error(`Failed to extract ID of big map '${bigMap.pathStr}' from contract storage: ${JSON.stringify(storage)}`
                        + ` ${errorToString(error)}`);
                }
            }
        } catch (error) {
            throw new Error(`Failed to update big map IDs for contract '${contract.address}' at block '${blockHash}'.`
                + ` ${errorToString(error)}`);
        }
    }
}

function extractBigMapId(storage: unknown, pathToExctract: readonly string[]): BigNumber {
    const [name, ...pathRest] = pathToExctract;

    if (!name) {
        const id = hasConstructor(storage, BigNumber)
            ? storage
            : new BigNumber(storage as BigNumber.Value);

        if (!id.isInteger()) {
            throw new Error(`Big map in a storage must be a BigNumber integer but it is ${JSON.stringify(storage)} of type ${dumpType(storage)}.`);
        }
        return id;
    }

    if (!isObject(storage) || !(name in storage)) {
        throw new Error(`Expected object with '${name}' property but there is: ${JSON.stringify(storage)}`);
    }
    return extractBigMapId(storage[name], pathRest);
}
