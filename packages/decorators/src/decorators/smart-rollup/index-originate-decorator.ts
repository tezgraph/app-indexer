import { createDecorator } from '../../helpers/decorator-accessors';

/**
 * The decorator for a method of a contract indexer to index its transactions with the specified entrypoint.
 * @example Demonstrates the method signature:
 * ```typescript
 * import { DbContext } from '@tezos-dappetizer/database';
 * import { indexOriginate } from '@tezos-dappetizer/decorators';
 * import { Applied, SmartRollupOriginateIndexingContext } from '@tezos-dappetizer/indexer';
 *
 * class YourSmartRollupIndexer {
 *     @indexOriginate()
 *     indexMyRollupOriginateed(
 *         operation: Applied<SmartRollupOriginateOperation>,
 *         dbContext: TDbContext,
 *         indexingContext: SmartRollupOriginateIndexingContext<TContextData, TRollupData>,
 *     ): void | PromiseLike<void> {
 *         // Your indexing code.
 *     }
 * }
 * ```
 * @remark This is based on {@link "@tezos-dappetizer/indexer".SmartRollupIndexer.indexOriginate | SmartRollupIndexer.indexOriginate(...)}.
 * @experimental
 */
export function indexOriginate(): MethodDecorator {
    return createDecorator(indexOriginate);
}
