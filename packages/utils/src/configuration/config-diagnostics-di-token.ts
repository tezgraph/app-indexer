import { InjectionToken } from 'tsyringe';

import { ConfigWithDiagnostics } from './config-diagnostics';

// It should not be part of public API as far as it should be consumed only by Dappetizer.
export const CONFIGS_WITH_DIAGNOSTIC_DI_TOKEN: InjectionToken<ConfigWithDiagnostics> = 'Dappetizer:Utils:ConfigsWithDiagnostics';
