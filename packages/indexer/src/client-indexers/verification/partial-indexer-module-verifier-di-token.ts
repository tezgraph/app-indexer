import { InjectionToken } from 'tsyringe';

import { PartialIndexerModuleVerifier } from './partial-indexer-module-verifier';

export const PARTIAL_INDEXER_MODULE_VERIFIERS_DI_TOKEN: InjectionToken<PartialIndexerModuleVerifier>
    = 'Dappetizer:Indexer:PartialIndexerModuleVerifier';
