import { AsyncIterableProvider, injectExplicit, injectLogger, isNullish, Logger } from '@tezos-dappetizer/utils';
import { reverse } from 'iter-tools';
import { injectable } from 'tsyringe';

import { ExplicitBlockLevelsResolver } from './explicit-blocks/explicit-block-levels-resolver';
import { IndexedBlock } from '../client-indexers/indexed-block';
import { IndexerDatabaseHandlerWrapper } from '../client-indexers/wrappers/indexer-database-handler-wrapper';
import { IndexingConfig } from '../config/indexing/indexing-config';
import { BlockDownloader } from '../helpers/block-downloader';
import { RollBackBlockHandler } from '../helpers/roll-back-block-handler';
import { TaquitoRpcBlock } from '../rpc-data/rpc-data-interfaces';

/** Detects reorgs, then rolls back respective blocks. */
@injectable()
export class ReorgBlockProvider implements AsyncIterableProvider<TaquitoRpcBlock> {
    constructor(
        @injectExplicit() private readonly nextProvider: AsyncIterableProvider<TaquitoRpcBlock>,
        private readonly blockHandler: IndexerDatabaseHandlerWrapper,
        private readonly blockDownloader: BlockDownloader,
        private readonly rollBackBlockHandler: RollBackBlockHandler,
        private readonly config: IndexingConfig,
        private readonly explicitBlocksResolver: ExplicitBlockLevelsResolver,
        @injectLogger(ReorgBlockProvider) private readonly logger: Logger,
    ) {}

    iterate(): AsyncIterableIterator<TaquitoRpcBlock> {
        const enableReorgs = isNullish(this.config.contractBoost)
            && isNullish(this.explicitBlocksResolver.resolveLevels());

        return enableReorgs
            ? this.iterateWithRollbackSupport()
            : this.nextProvider.iterate();
    }

    private async *iterateWithRollbackSupport(): AsyncIterableIterator<TaquitoRpcBlock> {
        let lastIndexedBlock = await this.blockHandler.getLastIndexedBlock();

        for await (let block of this.nextProvider.iterate()) {
            if (!isNullish(lastIndexedBlock) && block.header.level !== lastIndexedBlock.level + 1) {
                throw new Error(`BUG: Unexpected level of block ${JSON.stringify({ level: block.header.level, hash: block.hash })}`
                    + ` after previous ${JSON.stringify(lastIndexedBlock)}'.`);
            }

            const blocksToIndex = [block];
            const finalIndexedBlock = Object.freeze({
                hash: block.hash,
                level: block.header.level,
            });

            while (!isNullish(lastIndexedBlock) && block.header.predecessor !== lastIndexedBlock.hash) {
                await this.rollBackBlock(lastIndexedBlock, block);

                [block, lastIndexedBlock] = await Promise.all([
                    this.blockDownloader.download(lastIndexedBlock.level),
                    this.blockHandler.getLastIndexedBlock(),
                ]);
                blocksToIndex.push(block);
            }

            lastIndexedBlock = finalIndexedBlock;
            yield* reverse(blocksToIndex);
        }
    }

    private async rollBackBlock(rolledBackBlock: IndexedBlock, block: TaquitoRpcBlock): Promise<void> {
        await this.logger.runWithData({ rolledBackBlock }, async () => {
            this.logger.logInformation('Rolling back {rolledBackBlock} (the last indexed block)'
                + ' because its hash does not match predecessor of {newBlock} because of chain reorganization.', {
                newBlock: {
                    predecessor: block.header.predecessor,
                    hash: block.hash,
                    level: block.header.level,
                },
            });

            await this.rollBackBlockHandler.rollBack(rolledBackBlock);
            this.logger.logInformation('Successfully rolled back {rolledBackBlock}.');
        });
    }
}
