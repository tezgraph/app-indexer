import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { EntityToIndex } from '../../entity-to-index';

/** @category RPC Block Data */
export type TaquitoRpcLegacyBigMapDiff = DeepReadonly<taquitoRpc.ContractBigMapDiffItem>;

/** @category RPC Block Data */
export type TaquitoRpcLazyStorageBigMapDiff = DeepReadonly<taquitoRpc.LazyStorageDiffBigMap>;

/** @category RPC Block Data */
export type TaquitoRpcBigMapDiff = TaquitoRpcLegacyBigMapDiff | TaquitoRpcLazyStorageBigMapDiff;

/**
 * Common properties for all {@link BigMapDiff}-s.
 * @category RPC Block Data
 */
export interface BaseBigMapDiff extends EntityToIndex<TaquitoRpcBigMapDiff> {}

/** @category RPC Block Data */
export enum BigMapDiffAction {
    Alloc = 'Alloc',
    Copy = 'Copy',
    Remove = 'Remove',
    Update = 'Update',
}
