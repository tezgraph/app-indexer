import { describeMemberFactory, expectToThrow } from '../../../../../../../test-utilities/mocks';
import {
    TransactionBlockLevels,
    TransactionBlockLevelsQuery,
} from '../../../../../src/block-source/contract-boost/tzkt/queries/transaction-block-levels-query';

describe(TransactionBlockLevelsQuery.name, () => {
    let target: TransactionBlockLevelsQuery;

    beforeEach(() => {
        target = new TransactionBlockLevelsQuery(
            'http://tzkt/api',
            ['KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton', 'KT1WvzYHCNBvDSdwafTHv7nJ1dWmZ8GCYuuC'],
            { fromBlockLevel: 123 },
            5,
        );
    });

    const describeMember = describeMemberFactory<typeof target>();

    describeMember('description', () => {
        it('should include details', () => {
            expect(target.description).toIncludeMultiple(['transaction', '123']);
        });
    });

    describeMember('url', () => {
        it('should be correct', () => {
            expect(target.url).toBe('http://tzkt/api/v1/operations/transactions?level.ge=123&select.values=level&limit=5'
                + '&target.in=KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton,KT1WvzYHCNBvDSdwafTHv7nJ1dWmZ8GCYuuC&sort.asc=level');
        });
    });

    describeMember('transformResponse', () => {
        it.each([
            ['empty', [], false],
            ['below limit', [0, 11, 22], false],
            ['at limit', [1, 2, 3, 4, 5], true],
        ])('should pass if %s levels', (_desc, rawLevels, expectedHasMore) => {
            const levels = target.transformResponse(rawLevels);

            expect(levels).toEqual<TransactionBlockLevels>({
                levels: rawLevels,
                hasMore: expectedHasMore,
            });
        });

        it.each([
            ['undefined', undefined],
            ['null', null],
            ['not array - primitive', 'wtf'],
            ['not array - object', {}],
            ['level is undefined', ['ccc']],
            ['level is null', [null]],
            ['level is not number', [undefined]],
            ['level is below zero', [-1]],
        ])('should throw if %s', (_desc, rawLevels) => {
            expectToThrow(() => target.transformResponse(rawLevels));
        });
    });
});
