import { Nullish } from '@tezos-dappetizer/utils';
import { ValueTransformer } from 'typeorm';

export interface DbValueTransformer<TAppValue, TDbValue> extends ValueTransformer {
    to(appValue: Nullish<TAppValue>): Nullish<TDbValue>;

    from(dbValue: Nullish<TDbValue>): Nullish<TAppValue>;
}
