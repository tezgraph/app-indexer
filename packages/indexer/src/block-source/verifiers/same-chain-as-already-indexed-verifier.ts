import { injectLogger, Logger } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { BlockVerifier } from './block-verifier';
import { IndexerDatabaseHandlerWrapper } from '../../client-indexers/wrappers/indexer-database-handler-wrapper';
import { TaquitoRpcBlock } from '../../rpc-data/rpc-data-interfaces';

@singleton()
export class SameChainAsAlreadyIndexedVerifier implements BlockVerifier {
    constructor(
        private readonly databaseHandler: IndexerDatabaseHandlerWrapper,
        @injectLogger(SameChainAsAlreadyIndexedVerifier) private readonly logger: Logger,
    ) {}

    async verify(block: TaquitoRpcBlock, previousBlock: TaquitoRpcBlock | null): Promise<void> {
        const alreadyIndexedChainId = previousBlock?.chain_id
            ?? await this.databaseHandler.getIndexedChainId();

        if (alreadyIndexedChainId && (block.chain_id !== alreadyIndexedChainId)) {
            throw new Error(`Already indexed data correspond to chain '${alreadyIndexedChainId}' but a new block is from chain '${block.chain_id}'.`
                + ' Have you configured different Tezos Node as it used to be for already indexed data?');
        }

        if (previousBlock === null) {
            this.logger.logInformation('Chain being indexed is {chainId}.', { chainId: block.chain_id });
        }
    }
}
