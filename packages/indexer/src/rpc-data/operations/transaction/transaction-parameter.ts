import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly, NonEmptyArray } from 'ts-essentials';

import { LazyMichelsonValue } from '../../common/michelson/lazy-michelson-value';
import { EntityToIndex } from '../../entity-to-index';

/** @category RPC Block Data */
export interface LazyTransactionParameter extends EntityToIndex<TaquitoRpcTransactionParameter> {
    getTransactionParameter(): Promise<TransactionParameter>;
}

/** @category RPC Block Data */
export type TaquitoRpcTransactionParameter = DeepReadonly<taquitoRpc.TransactionOperationParameter>;

/** @category RPC Block Data */
export interface TransactionParameter extends EntityToIndex<TaquitoRpcTransactionParameter> {
    readonly entrypoint: string;
    readonly entrypointPath: Readonly<NonEmptyArray<string>>;
    readonly value: LazyMichelsonValue;
}
