import { deepEqual, instance, mock, when } from 'ts-mockito';

import {
    CommonMainOperationWithResult,
    CommonMainOperationWithResultConverter,
} from '../../../../../src/rpc-data/operations/common/main-operation/common-main-operation-with-result-converter';
import {
    CommonOriginationOperation,
    CommonOriginationOperationConverter,
} from '../../../../../src/rpc-data/operations/origination/converters/common-origination-operation-converter';
import {
    OriginationOperationConverter,
} from '../../../../../src/rpc-data/operations/origination/converters/origination-operation-converter';
import { RpcConvertOptions } from '../../../../../src/rpc-data/rpc-convert-options';
import { OriginationOperation, TaquitoRpcOriginationOperation } from '../../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../../rpc-test-helpers';
import { setupCommonOpWithResultProperties } from '../../operation-rpc-test-helpers';

describe(OriginationOperationConverter.name, () => {
    let target: OriginationOperationConverter;
    let commonOperationConverter: CommonMainOperationWithResultConverter;
    let commonOriginationOperationConverter: CommonOriginationOperationConverter;

    let rpcData: TaquitoRpcOriginationOperation;
    let options: RpcConvertOptions;
    let commonOpProperties: CommonMainOperationWithResult;
    let commonOriginationProperties: CommonOriginationOperation;

    const act = () => convertObject(target, rpcData, 'op/id', options);

    beforeEach(() => {
        [commonOperationConverter, commonOriginationOperationConverter] = [mock(), mock()];
        target = new OriginationOperationConverter(instance(commonOperationConverter), instance(commonOriginationOperationConverter));

        rpcData = { metadata: { operation_result: {} } } as typeof rpcData;
        options = { blockHash: 'haha' } as typeof options;
        commonOpProperties = { sourceAddress: 'mockedSource' } as typeof commonOpProperties;
        commonOriginationProperties = { delegateAddress: 'mockedDestination' } as typeof commonOriginationProperties;

        commonOpProperties = setupCommonOpWithResultProperties(commonOperationConverter, rpcData, 'op/id', options);
        when(commonOriginationOperationConverter.convert(rpcData, 'op/id', deepEqual({
            ...options,
            rpcResult: rpcData.metadata.operation_result,
            rpcResultProperty: ['metadata', 'operation_result'],
        }))).thenReturn(commonOriginationProperties);
    });

    it('should combine properties', () => {
        const operation = act();

        expect(operation).toEqual<OriginationOperation>({
            ...commonOpProperties,
            ...commonOriginationProperties,
            rpcData,
        });
        expect(operation).toBeFrozen();
    });
});
