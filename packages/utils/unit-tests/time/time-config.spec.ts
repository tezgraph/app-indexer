import { StrictOmit } from 'ts-essentials';

import { verifyPropertyNames } from '../../../../test-utilities/mocks';
import { DappetizerConfig } from '../../../indexer/src/config/dappetizer-config';
import { ConfigObjectElement } from '../../src/configuration/public-api';
import { TimeConfig } from '../../src/time/time-config';
import { TimeDappetizerConfig } from '../../src/time/time-dappetizer-config';

type TimeConfigProperties = StrictOmit<TimeConfig, 'getDiagnostics'>;

describe(TimeConfig.name, () => {
    function act(thisJson: DappetizerConfig['time']) {
        const rootJson = { [TimeConfig.ROOT_NAME]: thisJson };
        return new TimeConfig(new ConfigObjectElement(rootJson, 'file', '$'));
    }

    it('should be created with all values specified', () => {
        const thisJson = {
            longExecutionWarningMillis: 30_000,
        };

        const config = act(thisJson);

        expect(config).toEqual<TimeConfigProperties>(thisJson);
    });

    it.each([
        ['no root element', undefined],
        ['empty root element', {}],
    ])('should be created with defaults if %s', (_desc, thisJson) => {
        const config = act(thisJson);

        expect(config).toEqual<TimeConfigProperties>({
            longExecutionWarningMillis: 3_000,
        });
    });

    it('should expose its properties in DappetizerConfig', () => {
        expect(TimeConfig.ROOT_NAME).toBe<keyof DappetizerConfig>('time');

        const config = act({});
        verifyPropertyNames<TimeDappetizerConfig>(config, ['longExecutionWarningMillis']);
    });
});
