/**
 * Umbrella interface for all RPC data to be indexed.
 * @category RPC Block Data
 */
export interface EntityToIndex<TRpcData = object> {
    /**
     * Universal ID of the entity on the blockchain.
     * It is still human-readable.
     * @example Block hash / operation group hash / operation index [ operation kind ]
     * ```
     * BLvgirK7K43H5tv8f73cxBaLwcU7euFNwmfr2td2sWKpKXQXxaM/opLpKECfrpuF79YLfSYABvxb8Df8Fh71n3Q8Azy1m8vWU8Czfcn/4[transaction]
     * ```
     */
    readonly uid: string;

    /** Raw data as received from Tezos Node RPC. Taquito interface may not be exact. */
    readonly rpcData: TRpcData;
}
