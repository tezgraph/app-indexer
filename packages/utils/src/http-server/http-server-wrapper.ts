import http from 'http';
import { singleton } from 'tsyringe';

/** Promisified HTTP server. */
@singleton()
export class HttpServerWrapper {
    constructor(
        private readonly server: http.Server,
    ) {}

    async listen(port?: number, host?: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            try {
                this.server.listen(port, host, resolve);
                this.server.once('error', reject);
            } catch (error) {
                reject(error);
            }
        });
    }

    async close(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.server.close(error => {
                if (!error) {
                    resolve();
                } else {
                    reject(error);
                }
            });
        });
    }

    async getConnections(): Promise<number> {
        return new Promise<number>((resolve, reject) => {
            this.server.getConnections((error, count) => {
                if (!error) {
                    resolve(count);
                } else {
                    reject(error);
                }
            });
        });
    }
}
