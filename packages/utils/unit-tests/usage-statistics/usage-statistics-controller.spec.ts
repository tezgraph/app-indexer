import { Tracer } from '@opentelemetry/api';
import { SpanExporter } from '@opentelemetry/sdk-trace-base';
import { instance, mock, verify, when } from 'ts-mockito';

import { describeMember, expectToThrowAsync, fixedMock, TestLogger } from '../../../../test-utilities/mocks';
import { UsageStatisticsController } from '../../src/usage-statistics/usage-statistics-controller';
import { UsageStatisticsFactory } from '../../src/usage-statistics/usage-statistics-factory';

describe(UsageStatisticsController.name, () => {
    let target: UsageStatisticsController;
    let statisticsFactory: UsageStatisticsFactory;
    let logger: TestLogger;

    let exporter: SpanExporter;
    let tracer: Tracer;

    beforeEach(() => {
        statisticsFactory = mock();
        logger = new TestLogger();
        target = new UsageStatisticsController(instance(statisticsFactory), logger);

        exporter = fixedMock();
        tracer = 'mockedTracer' as any;
        when(statisticsFactory.create()).thenResolve({
            exporter: instance(exporter),
            tracer,
        });
    });

    describeMember<typeof target>('start', () => {
        const act = async () => target.start();

        it('should create statistics', async () => {
            await act();

            verify(statisticsFactory.create()).once();
            verify(exporter.shutdown()).never();
            logger.verifyNothingLogged();
        });

        it('should be disabled if no statistics created', async () => {
            when(statisticsFactory.create()).thenResolve(null);

            await act();

            verify(statisticsFactory.create()).once();
            logger.verifyNothingLogged();
        });

        it('should be disabled if failed to create statistics', async () => {
            const error = new Error('oups');
            when(statisticsFactory.create()).thenReject(error);

            await act();

            logger.loggedSingle().verify('Error', { error });
        });

        it('should throw if starting', async () => {
            void target.start();

            await runThrowTest({ act, expectedError: 'starting in progress' });
        });

        it('should throw if already started', async () => {
            await target.start();

            await runThrowTest({ act, expectedError: 'already started' });
        });

        it('should throw if stopping', async () => {
            await target.start();
            void target.stop();

            await runThrowTest({ act, expectedError: 'stopping in progress' });
        });

        it('should throw if stopped', async () => {
            await target.start();
            await target.stop();

            await runThrowTest({ act, expectedError: 'already stopped' });
        });
    });

    describeMember<typeof target>('tracer', () => {
        const act = async () => Promise.resolve(target.tracer);

        it('should come from created statistics', async () => {
            await target.start();

            expect(target.tracer).toBe(tracer);
        });

        it('should be null if no statistics created', async () => {
            when(statisticsFactory.create()).thenResolve(null);
            await target.start();

            expect(target.tracer).toBeNull();
        });

        it('should be disabled if failed to create statistics', async () => {
            when(statisticsFactory.create()).thenReject(new Error('oups'));
            await target.start();

            expect(target.tracer).toBeNull();
        });

        it('should throw if not started', async () => {
            await runThrowTest({ act, expectedError: 'not started yet' });
        });

        it('should throw if starting', async () => {
            void target.start();

            await runThrowTest({ act, expectedError: 'starting in progress' });
        });

        it('should throw if stopping', async () => {
            await target.start();
            void target.stop();

            await runThrowTest({ act, expectedError: 'stopping in progress' });
        });

        it('should throw if stopped', async () => {
            await target.start();
            await target.stop();

            await runThrowTest({ act, expectedError: 'already stopped' });
        });
    });

    describeMember<typeof target>('stop', () => {
        const act = async () => target.stop();

        it('should shutdown exporter', async () => {
            await target.start();

            await act();

            verify(exporter.shutdown()).once();
            logger.verifyNothingLogged();
        });

        it('should handle errors', async () => {
            await target.start();
            const error = new Error('oups');
            when(exporter.shutdown()).thenReject(error);

            await act();

            logger.loggedSingle().verify('Error', { error });
        });

        it('should throw if not started', async () => {
            await runThrowTest({ act, expectedError: 'not started yet' });
        });

        it('should throw if starting', async () => {
            void target.start();

            await runThrowTest({ act, expectedError: 'starting in progress' });
        });

        it('should throw if stopping', async () => {
            await target.start();
            void target.stop();

            await runThrowTest({ act, expectedError: 'stopping in progress' });
        });

        it('should throw if stopped', async () => {
            await target.start();
            await target.stop();

            await runThrowTest({ act, expectedError: 'already stopped' });
        });
    });

    async function runThrowTest(testCase: { act: () => Promise<unknown>; expectedError: string }) {
        const error = await expectToThrowAsync(testCase.act);

        expect(error.message).toInclude(testCase.expectedError);
    }
});
