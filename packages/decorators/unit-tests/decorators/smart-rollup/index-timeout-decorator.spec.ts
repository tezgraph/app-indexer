import { indexTimeout } from '../../../src/decorators/smart-rollup/index-timeout-decorator';

describe(indexTimeout.name, () => {
    it('should pass if correctly declared', () => {
        indexTimeout();
    });
});
