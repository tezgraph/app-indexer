import { DbContext, IndexingContext } from '../../../../test-utilities/mocks';
import { DecoratedMethod } from '../../src/helpers/decorated-method-discovery';
import { MethodWrapperBase } from '../../src/indexer-wrappers/method-wrapper-base';

export async function executeDecorated<TEntity, TDecoratorOptions>(
    target: MethodWrapperBase<TEntity, TDecoratorOptions>,
    decoratedMethod: DecoratedMethod<Function, TDecoratorOptions>,
    entity: TEntity,
    dbContext: DbContext,
    indexingContext: IndexingContext,
): Promise<void> {
    await (target as any).executeDecorated(decoratedMethod, entity, dbContext, indexingContext);
}
