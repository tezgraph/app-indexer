import { singleton } from 'tsyringe';

import { BalanceUpdateConverter } from '../../common/balance-update/converters/balance-update-converter';
import { primitives } from '../../primitives';
import { RpcConvertOptions } from '../../rpc-convert-options';
import { RpcObjectConverter } from '../../rpc-converter';
import {
    OperationKind,
    SeedNonceRevelationOperation,
    TaquitoRpcSeedNonceRevelationOperation,
} from '../../rpc-data-interfaces';

@singleton()
export class SeedNonceRevelationOperationConverter
    extends RpcObjectConverter<TaquitoRpcSeedNonceRevelationOperation, SeedNonceRevelationOperation, RpcConvertOptions> {
    constructor(private readonly balanceUpdateConverter: BalanceUpdateConverter) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcSeedNonceRevelationOperation,
        uid: string,
        _options: RpcConvertOptions,
    ): SeedNonceRevelationOperation {
        const balanceUpdates = this.balanceUpdateConverter.convertFromMetadataProperty(rpcData, uid);

        return Object.freeze<SeedNonceRevelationOperation>({
            balanceUpdates,
            kind: OperationKind.SeedNonceRevelation,
            level: primitives.integer.convertProperty(rpcData, 'level', uid),
            nonce: primitives.string.convertProperty(rpcData, 'nonce', uid),
            rpcData,
            uid,
        });
    }
}
