import { argGuard } from '@tezos-dappetizer/utils';
import { EntityManager } from 'typeorm';

import { DbCommand } from './db-command';
import { DbBlock } from '../entities/db-block';

export class DeleteBlockByHashCommand implements DbCommand {
    readonly description: string;

    constructor(readonly hash: string) {
        argGuard.nonWhiteSpaceString(hash, 'hash');

        this.description = `delete block by hash '${hash}'`;
        Object.freeze(this);
    }

    async execute(dbManager: EntityManager): Promise<void> {
        await dbManager.createQueryBuilder()
            .delete()
            .from(DbBlock)
            .whereInIds(this.hash)
            .execute();
    }
}
