import { expectToThrow } from '../../../../../test-utilities/mocks';
import { smartRollupFilter } from '../../../src/decorators/smart-rollup/smart-rollup-filter-decorator';

describe(smartRollupFilter.name, () => {
    it('should pass if correctly declared', () => {
        smartRollupFilter({ name: 'test' });
    });

    it('should throw if incorrectly declared', () => {
        expectToThrow(() => smartRollupFilter('wtf' as any));
    });
});
