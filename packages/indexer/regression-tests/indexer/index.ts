import { IndexerModuleFactory, registerIndexerDatabaseHandler } from '@tezos-dappetizer/indexer';

import { RegContractIndexer } from './reg-contract-indexer';
import { RegIndexerDatabaseHandler } from './reg-indexer-database-handler';

export const indexerModule: IndexerModuleFactory<{}> = options => {
    registerIndexerDatabaseHandler(options.diContainer, { useValue: new RegIndexerDatabaseHandler() });

    return {
        name: 'RegressionTests',
        contractIndexers: [new RegContractIndexer()],
    };
};
