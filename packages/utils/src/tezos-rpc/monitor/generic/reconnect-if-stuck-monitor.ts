import { inject, injectable } from 'tsyringe';

import { DownloadDataMonitor } from './download-data-monitor';
import { AbortControllerFactory } from '../../../app-control/abort-controller-factory';
import { APP_SHUTDOWN_MANAGER_DI_TOKEN, AppShutdownManager } from '../../../app-control/public-api';
import { AsyncIterableProvider } from '../../../basics/public-api';
import { injectExplicit } from '../../../dependency-injection/public-api';
import { Logger } from '../../../logging/public-api';
import { Timeout, TIMER_HELPER_DI_TOKEN, TimerHelper } from '../../../time/public-api';
import { TezosNodeConfig } from '../../tezos-node-config';

/** Reconnects if stuck = no new item for a configured time. */
@injectable()
export class ReconnectIfStuckMonitor implements AsyncIterableProvider<unknown> {
    constructor(
        @injectExplicit(0) private readonly logger: Logger,
        @injectExplicit(1) private readonly nextMonitor: DownloadDataMonitor,
        @inject(APP_SHUTDOWN_MANAGER_DI_TOKEN) private readonly appShutdownManager: AppShutdownManager,
        private readonly abortControllerFactory: AbortControllerFactory,
        @inject(TIMER_HELPER_DI_TOKEN) private readonly timerHelper: TimerHelper,
        private readonly config: TezosNodeConfig,
    ) {}

    async *iterate(): AsyncIterableIterator<unknown> {
        while (!this.appShutdownManager.signal.aborted) {
            const abortController = this.abortControllerFactory.createLinked(this.appShutdownManager.signal);
            let stuckTimeout = this.abortIfStuck(abortController);

            for await (const item of this.nextMonitor.iterate(abortController.signal)) {
                stuckTimeout.clear();
                stuckTimeout = this.abortIfStuck(abortController);

                yield item;
            }
        }
    }

    private abortIfStuck(abortController: AbortController): Timeout {
        const millis = this.config.monitor.reconnectIfStuckMillis;

        return this.timerHelper.setTimeout(millis, () => {
            this.logger.logWarning('Aborting because the connection is stuck - no item returned for {millis}.', { millis });
            abortController.abort();
        });
    }
}
