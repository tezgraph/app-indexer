import { InjectionToken } from 'tsyringe';

import { HealthCheck } from './health-check';

// It should not be part of public API as far as it should be consumed only by Dappetizer.
export const HEALTH_CHECKS_DI_TOKEN: InjectionToken<HealthCheck> = 'Dappetizer:Utils:HealthChecks';
