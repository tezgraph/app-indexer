export function expectToThrowAny(act: () => unknown): any {
    try {
        act();
    } catch (thrownValue) {
        return thrownValue;
    }
    return onNothingThrown();
}

export async function expectToThrowAnyAsync(act: () => Promise<unknown>): Promise<any> {
    try {
        await act();
    } catch (thrownValue) {
        return thrownValue;
    }
    return onNothingThrown();
}

function onNothingThrown(): never {
    throw new Error(`Expected given function to throw but nothing was thrown.`);
}

export function expectToThrow<TError extends Error>(
    act: () => unknown,
    errorType?: new(...args: any[]) => TError,
): TError {
    const thrownValue = expectToThrowAny(act);
    return verifyReceivedError(thrownValue, errorType);
}

export async function expectToThrowAsync<TError extends Error>(
    act: () => Promise<unknown>,
    errorType?: new(...args: any[]) => TError,
): Promise<TError> {
    const thrownValue = await expectToThrowAnyAsync(act);
    return verifyReceivedError(thrownValue, errorType);
}

function verifyReceivedError<TError extends Error>(
    error: any,
    errorType: (new(...args: any[]) => TError) | undefined,
): TError {
    if (errorType) {
        expect(error.constructor.name).toBe(errorType.name);
    } else {
        expect(error).toBeInstanceOf(Error);
    }
    return error;
}

export async function expectNextValues<T>(iterator: AsyncIterator<T>, expectedValues: T[]): Promise<void> {
    for (const value of expectedValues) {
        await expectNext(iterator, { done: false, value });
    }
}

export async function expectNextDone(iterator: AsyncIterator<unknown>): Promise<void> {
    await expectNext(iterator, { done: true, value: undefined });
}

async function expectNext<T>(
    iterator: AsyncIterator<T>,
    expected: { done: boolean; value: T | undefined },
): Promise<void> {
    const result = await iterator.next();
    expect(result).toEqual(expected);
}

export function verifyPropertyNames<TTarget>(obj: object, expectedProps: (keyof NonNullable<TTarget>)[]) {
    expect(Object.keys(obj)).toIncludeSameMembers(expectedProps);
}

export function describeMember<TTarget>(memberName: keyof TTarget & string, runTests: () => void): void {
    describe(memberName, () => {
        runTests();
    });
}

export function describeMemberFactory<TTarget>(): ((memberName: keyof TTarget & string, runTests: () => void) => void) {
    return (memberName, runTests) => describeMember<TTarget>(memberName, runTests);
}
