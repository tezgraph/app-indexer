import { InjectionToken } from 'tsyringe';

/** Abstraction for easier testing. */
export interface Clock {
    getNowDate(): Date;

    getNowTimestamp(): number;
}

export const CLOCK_DI_TOKEN: InjectionToken<Clock> = 'Dappetizer:Utils:Clock';
