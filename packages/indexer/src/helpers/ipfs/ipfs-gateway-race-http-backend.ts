import * as taquitoHttp from '@taquito/http-utils';
import {
    appendUrl,
    dappetizerAssert,
    hasConstructor,
    injectExplicit,
    injectLogger,
    isNullish,
    Logger,
    removeRequiredPrefix,
    TaquitoHttpBackend,
    TaquitoHttpRequest,
} from '@tezos-dappetizer/utils';
import { injectable } from 'tsyringe';

import { IpfsConfig } from './ipfs-config';
import { IpfsMetrics } from './ipfs-metrics';

export const TAQUITO_HARDCODED_IPFS_GATEWAY = 'https://ipfs.io/';

@injectable()
export class IpfsGatewayRaceHttpBackend extends TaquitoHttpBackend {
    constructor(
        @injectExplicit() private readonly nextBackend: TaquitoHttpBackend,
        private readonly config: IpfsConfig,
        private readonly metrics: IpfsMetrics,
        @injectLogger(IpfsGatewayRaceHttpBackend) private readonly logger: Logger,
    ) {
        super();
    }

    override async execute(request: TaquitoHttpRequest): Promise<unknown> {
        return new Promise<unknown>((resolve, reject) => {
            const failedResponses: FailedResponse[] = [];
            const abortController = new AbortController();

            for (const gateway of this.config.gateways) {
                this.executeWithGateway(request, gateway, abortController.signal).then(
                    result => {
                        abortController.abort();
                        this.reportMetrics(gateway, failedResponses);
                        resolve(result);
                    },
                    (error: unknown) => {
                        failedResponses.push({ gateway, error });

                        if (failedResponses.length === this.config.gateways.length) {
                            const response = this.getMostExplicitResponse(failedResponses);
                            reject(response.error);
                        }
                    },
                );
            }
        });
    }

    private async executeWithGateway(originalRequest: TaquitoHttpRequest, gatewayUrl: string, signal: AbortSignal): Promise<unknown> {
        const gatewayRequest: TaquitoHttpRequest = {
            ...originalRequest,
            url: appendUrl(gatewayUrl, removeRequiredPrefix(originalRequest.url, TAQUITO_HARDCODED_IPFS_GATEWAY)),
            timeout: this.config.timeoutMillis,
            signal,
        };
        return this.nextBackend.execute(gatewayRequest);
    }

    private getMostExplicitResponse(failedResponses: FailedResponse[]): FailedResponse {
        const notFoundResponse = failedResponses.find(r => isHttpNotFound(r.error));
        if (notFoundResponse) {
            this.reportMetrics(notFoundResponse.gateway, failedResponses.filter(r => !isHttpNotFound(r.error)));
            return notFoundResponse;
        }

        const httpResponse = failedResponses.find(r => isHttpError(r.error));
        if (httpResponse) {
            this.reportMetrics(httpResponse.gateway, failedResponses.filter(r => !isHttpError(r.error)));
            return httpResponse;
        }

        const fastestResponse = failedResponses[0];
        dappetizerAssert(!isNullish(fastestResponse), 'There must be at least one response because config has at least one gateway.');
        this.reportMetrics(fastestResponse.gateway, []);
        return fastestResponse;
    }

    private reportMetrics(winningGateway: string, failedResponses: FailedResponse[]): void {
        this.metrics.gatewayWinCount.inc({ gateway: winningGateway });

        for (const response of failedResponses.filter(r => r.gateway !== winningGateway)) {
            this.metrics.gatewayFailureCount.inc({ gateway: response.gateway });
            this.logger.logWarning('IPFS {gateway} responsed with {error} despite the other returned a valid response.', response);
        }
    }
}

type FailedResponse = {
    gateway: string;
    error: unknown;
};

function isHttpError(error: unknown): error is taquitoHttp.HttpResponseError {
    return hasConstructor(error, taquitoHttp.HttpResponseError);
}

function isHttpNotFound(error: unknown): error is taquitoHttp.HttpResponseError {
    return isHttpError(error) && error.status === taquitoHttp.STATUS_CODE.NOT_FOUND;
}
