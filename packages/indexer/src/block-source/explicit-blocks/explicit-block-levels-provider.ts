import { AsyncIterableProvider, fullCliOption, injectExplicit, injectLogger, Logger } from '@tezos-dappetizer/utils';
import { injectable } from 'tsyringe';

import { ExplicitBlockLevelsResolver } from './explicit-block-levels-resolver';
import { IndexerDatabaseHandlerWrapper } from '../../client-indexers/wrappers/indexer-database-handler-wrapper';
import { RollBackBlockHandler } from '../../helpers/roll-back-block-handler';

/** Handles explicitly specified block levels - rolls back respective blocks and yields their levels to re-index them. */
@injectable()
export class ExplicitBlockLevelsProvider<TDbContext> implements AsyncIterableProvider<number> {
    constructor(
        @injectExplicit() private readonly nextProvider: AsyncIterableProvider<number>,
        private readonly explicitLevelsResolver: ExplicitBlockLevelsResolver,
        private readonly rollBackBlockHandler: RollBackBlockHandler,
        private readonly databaseHandler: IndexerDatabaseHandlerWrapper<TDbContext>,
        @injectLogger(ExplicitBlockLevelsProvider) private readonly logger: Logger,
    ) {}

    iterate(): AsyncIterableIterator<number> {
        const levels = this.explicitLevelsResolver.resolveLevels();
        return !levels
            ? this.nextProvider.iterate()
            : this.iterateExplicitBlocks(levels);
    }

    private async *iterateExplicitBlocks(levels: readonly number[]): AsyncIterableIterator<number> {
        this.logger.logInformation(`Re-indexing explicitly specified block {levels} from console option ${fullCliOption('BLOCKS')}.`, { levels });

        for (const level of levels) {
            const indexedBlock = await this.databaseHandler.getIndexedBlock(level);

            if (indexedBlock) {
                await this.logger.runWithData({ rolledBackBlock: indexedBlock }, async () => {
                    this.logger.logInformation('Rolling back already indexed {rolledBackBlock} in order to re-index it.');

                    await this.rollBackBlockHandler.rollBack(indexedBlock);

                    this.logger.logInformation('Successfully rolled back {rolledBackBlock}.');
                });
            } else {
                this.logger.logInformation('No need to roll back block {level} (in order to re-index it) because it was not indexed.', { level });
            }

            yield level;
        }
    }
}
