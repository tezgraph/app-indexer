import { Wrappable } from 'iter-tools';
import { NonEmptyArray } from 'ts-essentials';

import { argGuard } from './arg-guard';
import { Nullish } from './reflection-types';
import { isNullish } from './reflection-utils';

export function isReadOnlyArray(value: unknown): value is readonly unknown[] {
    return Array.isArray(value);
}

export function isNonEmpty<T>(array: Nullish<T[]>): array is NonEmptyArray<T>;
export function isNonEmpty<T>(array: Nullish<readonly T[]>): array is Readonly<NonEmptyArray<T>>;
export function isNonEmpty<T>(array: Nullish<readonly T[]>): boolean {
    argGuard.ifNotNullish(array, a => argGuard.array(a, 'array'));

    return !isNullish(array) && array.length > 0;
}

export function last<T>(array: Readonly<NonEmptyArray<T>>): T;
export function last<T>(array: readonly T[]): T | undefined;
export function last<T>(array: readonly T[]): T | undefined {
    argGuard.array(array, 'array');

    return array[array.length - 1];
}

export function *withIndex<T>(iterable: Iterable<T>): Iterable<[T, number]> {
    argGuard.object(iterable, 'iterable');

    let index = 0;
    for (const item of iterable) {
        yield [item, index++];
    }
}

export function hasLength<T>(array: Nullish<T[]>, lengthToCheck: 1): array is [T];
export function hasLength<T>(array: Nullish<T[]>, lengthToCheck: 2): array is [T, T];
export function hasLength<T>(array: Nullish<T[]>, lengthToCheck: 3): array is [T, T, T];
export function hasLength<T>(array: Nullish<readonly T[]>, lengthToCheck: 1): array is readonly [T];
export function hasLength<T>(array: Nullish<readonly T[]>, lengthToCheck: 2): array is readonly [T, T];
export function hasLength<T>(array: Nullish<readonly T[]>, lengthToCheck: 3): array is readonly [T, T, T];
export function hasLength<T>(array: Nullish<readonly T[]>, lengthToCheck: number): boolean {
    argGuard.ifNotNullish(array, a => argGuard.array(a, 'array'));
    argGuard.number(lengthToCheck, 'lengthToCheck', { integer: true, min: 1, max: 3 });

    return array?.length === lengthToCheck;
}

/** Gets a function which deduplicates items of an iterable by specified key. It can be chained using a pipe(). */
export function deduplicate<T>(
    getKey: (value: T) => string,
): (source: Wrappable<T>) => IterableIterator<T>;

/** Deduplicates items of specified source iterable by specified key. */
export function deduplicate<T>(
    getKey: (value: T) => string,
    source: Wrappable<T>,
): IterableIterator<T>;

// Implementation
export function deduplicate<T>(
    getKey: (value: T) => string,
    source?: Wrappable<T>,
): IterableIterator<T> | ((source: Wrappable<T>) => IterableIterator<T>) {
    argGuard.function(getKey, 'getKey');
    argGuard.ifNotNullish(source, s => argGuard.object(s, 'source'));

    return source ? deduplicateInternal(source) : deduplicateInternal;

    function *deduplicateInternal(internalSource: Wrappable<T>): IterableIterator<T> {
        const keys = new Set<string>();
        for (const item of internalSource ?? []) {
            const key = getKey(item);
            if (!keys.has(key)) {
                keys.add(key);
                yield item;
            }
        }
    }
}

/** Returns first mapped item which is not `null` nor `undefined`. So it does not map all items. */
export function findNonNullish<TSource, TResult>(
    sourceItems: Iterable<TSource>,
    mapItem: (item: TSource) => TResult | null | undefined,
): TResult | null {
    for (const sourceItem of sourceItems) {
        const result = mapItem(sourceItem);
        if (!isNullish(result)) {
            return result;
        }
    }
    return null;
}
