import { DappetizerBug } from '@tezos-dappetizer/utils';
import { Writable } from 'ts-essentials';
import { anything, instance, mock, objectContaining, verify, when } from 'ts-mockito';

import { LazyStorageChangeConverter } from '../../../../src/rpc-data/common/storage-change/lazy-storage-change-converter';
import {
    StorageChangeCreateOptions,
    StorageChangeFactory,
} from '../../../../src/rpc-data/common/storage-change/storage-change-factory';
import { RpcConversionError } from '../../../../src/rpc-data/rpc-conversion-error';
import { RpcConvertOptionsWithLazyContract } from '../../../../src/rpc-data/rpc-convert-options';
import { Michelson, StorageChange } from '../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../rpc-test-helpers';

describe(LazyStorageChangeConverter.name, () => {
    let target: LazyStorageChangeConverter;
    let storageChangeFactory: StorageChangeFactory;

    let rpcData: Michelson;
    let options: Writable<RpcConvertOptionsWithLazyContract>;

    const targetConvertObject = () => convertObject(target, rpcData, 'stor/id', options);

    beforeEach(() => {
        storageChangeFactory = mock();
        target = new LazyStorageChangeConverter(instance(storageChangeFactory));

        rpcData = { prim: 'mocked' };
        options = {
            lazyContract: { address: 'mockedAddr' },
            predecessorBlockHash: 'pred-haha',
            lastStorageWithinBlock: new Map([['mockedAddr', 'lastMichelson' as any]]),
        } as typeof options;
    });

    it('should convert correctly', () => {
        const lazyParam = targetConvertObject();

        expect(lazyParam.rpcData).toBe(rpcData);
        expect(lazyParam).toBeFrozen();
        expect(options.lastStorageWithinBlock).toEqual(new Map([['mockedAddr', rpcData]]));
        verify(storageChangeFactory.create(anything())).never();
    });

    it('should get storage change lazily and cache', async () => {
        const lazyParam = targetConvertObject();
        const createdStorageChange: StorageChange = 'mockedStorageChange' as any;
        when(storageChangeFactory.create(anything())).thenResolve(createdStorageChange);

        const change1 = await lazyParam.getStorageChange();
        const change2 = await lazyParam.getStorageChange();

        expect(change1).toBe(createdStorageChange);
        expect(change2).toBe(createdStorageChange);

        const expectedOptions: StorageChangeCreateOptions = {
            lastMichelsonWithinBlock: 'lastMichelson' as any,
            lazyContract: options.lazyContract!,
            newMichelson: rpcData,
            predecessorBlockHash: options.predecessorBlockHash,
            uid: 'stor/id',
        };
        verify(storageChangeFactory.create(anything())).once();
        verify(storageChangeFactory.create(objectContaining(expectedOptions))).once();
    });

    it('should get storage change using null if no last value within block', async () => {
        options.lastStorageWithinBlock.clear();

        await targetConvertObject().getStorageChange();

        const expectedOptions: Partial<StorageChangeCreateOptions> = { lastMichelsonWithinBlock: null };
        verify(storageChangeFactory.create(objectContaining(expectedOptions))).once();
    });

    it('should throw if no contract', () => {
        options.lazyContract = null;

        expect(targetConvertObject).toThrow(DappetizerBug);
    });

    it('should throw if invalid rpcData', () => {
        rpcData = 'wtf' as any;

        expect(targetConvertObject).toThrow(RpcConversionError);
    });
});
