import { Request as ExpressRequest, Response as ExpressResponse } from 'express';
import { AsyncOrSync } from 'ts-essentials';
import { DependencyContainer, Provider } from 'tsyringe';

import { HTTP_HANDLERS_DI_TOKEN } from './http-handler-di-token';
import { registerSingleton } from '../dependency-injection/public-api';

export interface HttpHandler {
    readonly urlPath: string;

    handle(
        request: ExpressRequest,
        response: ExpressResponse,
    ): AsyncOrSync<void>;
}

/**
 * Registers an HTTP handler which then handles HTTP request coming to its `urlPath` if HTTP server is enabled.
 * It is registered as **singleton**.
 */
export function registerHttpHandler(diContainer: DependencyContainer, httpHandlerProvider: Provider<HttpHandler>): void {
    registerSingleton(diContainer, HTTP_HANDLERS_DI_TOKEN, httpHandlerProvider);
}
