import { anything, capture, instance, mock, verify, when } from 'ts-mockito';

import { MockedIndexerModule, mockIndexerModule } from './traverser-test-utils';
import { ContextData, DbContext, expectToThrowAsync, TestLogger } from '../../../../test-utilities/mocks';
import { asReadonly } from '../../../utils/src/public-api';
import { MainOperationTraverser } from '../../src/block-traversers/main-operation-traverser';
import { OperationGroupTraverser } from '../../src/block-traversers/operation-group-traverser';
import { MainOperationIndexingContext, OperationGroupIndexingContext } from '../../src/client-indexers/block-data-indexer';
import { MainOperation, OperationGroup } from '../../src/rpc-data/rpc-data-interfaces';

describe(OperationGroupTraverser.name, () => {
    let target: OperationGroupTraverser<DbContext>;
    let mainOperationTraverser: MainOperationTraverser<DbContext>;
    let logger: TestLogger;

    let indexerModule: MockedIndexerModule;
    let db: DbContext;
    let operationGroup: OperationGroup;
    let operation1: MainOperation;
    let operation2: MainOperation;
    let indexingContext: OperationGroupIndexingContext<ContextData>;

    const act = async () => target.traverse(indexerModule, db, operationGroup, indexingContext);

    beforeEach(() => {
        mainOperationTraverser = mock();
        logger = new TestLogger();
        target = new OperationGroupTraverser(instance(mainOperationTraverser), logger);

        indexerModule = mockIndexerModule();
        db = 'mockedDb' as any;
        operation1 = 'mockedOp1' as any;
        operation2 = 'mockedOp2' as any;
        operationGroup = { hash: 'haha', operations: asReadonly([operation1, operation2]) } as typeof operationGroup;
        indexingContext = { data: { ctxVal: 'abc' } } as typeof indexingContext;
    });

    it('should index operation group and traverse all operations', async () => {
        await act();

        verify(indexerModule.mocks.blockDataIndexer.indexOperationGroup(operationGroup, db, indexingContext)).once();

        const receivedContext = capture<any, any, any, any>(mainOperationTraverser.traverse).first()[3];
        expect(receivedContext).toEqual<MainOperationIndexingContext<ContextData>>({ ...indexingContext, operationGroup });
        expect(receivedContext).toBeFrozen();

        verify(mainOperationTraverser.traverse(anything(), anything(), anything(), anything())).times(2);
        verify(mainOperationTraverser.traverse(indexerModule, db, operation1, receivedContext)).once();
        verify(mainOperationTraverser.traverse(indexerModule, db, operation2, receivedContext)).once();
    });

    it('should put operation group hash to log scope', async () => {
        when(indexerModule.mocks.blockDataIndexer.indexOperationGroup(anything(), anything(), anything())).thenCall(() => {
            logger.logInformation('SCOPED');
        });

        await act();

        logger.loggedSingle().verify('Information', { indexedOperationGroupHash: 'haha' }).verifyMessage('SCOPED');
    });

    it('should wrap errors', async () => {
        when(indexerModule.mocks.blockDataIndexer.indexOperationGroup(anything(), anything(), anything())).thenReject(new Error('oups'));

        const error = await expectToThrowAsync(act);

        expect(error.message).toIncludeMultiple(['operation group', `'haha'`, 'oups']);
    });
});
