import { IndexerModuleUsingDb } from '@tezos-dappetizer/database';
import { Registration } from './entities';

export const indexerModule: IndexerModuleUsingDb = {
    name: 'Registration',
    dbEntities: [Registration],

    contractIndexers: [{
        indexTransaction: async (transactionParameter, dbContext, indexingContext) => {
            if (transactionParameter.entrypoint !== 'buy') {
                return;
            }

            const buyParam = transactionParameter.value.convert<any>();
            const registration = {
                domain: Buffer.from(buyParam.label, 'hex') + '.tez',
                owner: buyParam.owner,
                duration: buyParam.duration.toNumber(),
                block: indexingContext.block.hash,
            };

            await dbContext.transaction.insert(Registration, registration);
        },
    }],
};