import { asyncToArray, asyncWrap } from 'iter-tools';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { expectNextDone, expectNextValues, TestLogger, verifyCalled } from '../../../../../test-utilities/mocks';
import { AsyncIterableProvider } from '../../../../utils/src/public-api';
import { ContractBoostedBlockProvider } from '../../../src/block-source/contract-boost/contract-boosted-block-provider';
import { ContractIndexingBoost } from '../../../src/block-source/contract-boost/contract-indexing-boost';
import { ContractIndexingBoostFactory } from '../../../src/block-source/contract-boost/contract-indexing-boost-factory';

describe(ContractBoostedBlockProvider.name, () => {
    let target: AsyncIterableProvider<number>;
    let nextProvider: AsyncIterableProvider<number>;
    let boostFactory: ContractIndexingBoostFactory;
    let logger: TestLogger;

    let boost: ContractIndexingBoost;

    beforeEach(() => {
        [nextProvider, boostFactory] = [mock(), mock()];
        logger = new TestLogger();
        target = new ContractBoostedBlockProvider(instance(nextProvider), instance(boostFactory), logger);

        boost = mock();
        when(boost.name).thenReturn('Foo');
        when(boostFactory.create()).thenReturn(instance(boost));
    });

    it('should filter to levels according to boost', async () => {
        when(nextProvider.iterate()).thenReturn(asyncWrap([10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22]));
        when(boost.getLevelsWithContracts(anything()))
            .thenResolve({ hasMore: true, lastLoadedLevel: 15, levelsWithContracts: new Set([11, 13]) })
            .thenResolve({ hasMore: false, lastLoadedLevel: 18, levelsWithContracts: new Set([16]) });

        const iterator = target.iterate();
        verify(boost.getLevelsWithContracts(anything())).never();

        await expectNextValues(iterator, [11, 13]);
        verifyCalled(boost.getLevelsWithContracts).onceWith({ fromBlockLevel: 10 });

        await expectNextValues(iterator, [16, 19, 20, 21, 22]);
        await expectNextDone(iterator);
        verifyCalled(boost.getLevelsWithContracts)
            .atIndexWith(1, { fromBlockLevel: 16 })
            .times(2);

        logger.verifyLoggedCount(7);
        logger.logged(0).verify('Information', { boost: 'Foo' }).verifyMessage('Using');
        logger.logged(1).verify('Debug', { boost: 'Foo', level: 10, lastLoadedBoostLevel: -1 }).verifyMessage('Loading more');
        logger.logged(2).verify('Debug', { boost: 'Foo', level: 11 }).verifyMessage('jump');
        logger.logged(3).verify('Debug', { boost: 'Foo', level: 13 }).verifyMessage('jump');
        logger.logged(4).verify('Debug', { boost: 'Foo', level: 16, lastLoadedBoostLevel: 15 }).verifyMessage('Loading more');
        logger.logged(5).verify('Debug', { boost: 'Foo', level: 16 }).verifyMessage('jump');
        logger.logged(6).verify('Information', { boost: 'Foo', level: 19, lastBoostLevel: 18 }).verifyMessage('Indexing all');
    });

    it('should stop boosting on error', async () => {
        const error = new Error('oups');
        when(nextProvider.iterate()).thenReturn(asyncWrap([10, 11, 12]));
        when(boost.getLevelsWithContracts(anything())).thenReject(error);

        const levels = await asyncToArray(target.iterate());

        expect(levels).toEqual([10, 11, 12]);
        logger.verifyLoggedCount(3);
        logger.logged(0).verify('Information', { boost: 'Foo' }).verifyMessage('Using');
        logger.logged(1).verify('Debug', { boost: 'Foo', level: 10, lastLoadedBoostLevel: -1 }).verifyMessage('Loading more');
        logger.logged(2).verify('Warning', { boost: 'Foo', level: 10, error });
    });

    it('should not boost if boost is disabled', () => {
        const nextIterable: AsyncIterableIterator<number> = 'mockedIterable' as any;
        when(nextProvider.iterate()).thenReturn(nextIterable);
        when(boostFactory.create()).thenReturn(null);

        const iterable = target.iterate();

        expect(iterable).toBe(nextIterable);
    });
});
