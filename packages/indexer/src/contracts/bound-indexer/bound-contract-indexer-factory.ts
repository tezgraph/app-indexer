import { singleton } from 'tsyringe';

import { BoundContractIndexer, UnboundContractData } from './bound-contract-indexer';
import { TrustedContractIndexer } from '../../client-indexers/wrappers/trusted-indexer-interfaces';

@singleton()
export class BoundContractIndexerFactory<TDbContext> {
    create<TContextData, TContractData>(
        indexer: TrustedContractIndexer<TDbContext, TContextData, TContractData>,
        contractData: TContractData,
    ): BoundContractIndexer<TDbContext, TContextData> {
        return {
            indexBigMapDiff: adaptContext(indexer.indexBigMapDiff.bind(indexer), contractData),
            indexBigMapUpdate: adaptContext(indexer.indexBigMapUpdate.bind(indexer), contractData),
            indexEvent: adaptContext(indexer.indexEvent.bind(indexer), contractData),
            indexOrigination: adaptContext(indexer.indexOrigination.bind(indexer), contractData),
            indexStorageChange: adaptContext(indexer.indexStorageChange.bind(indexer), contractData),
            indexTransaction: adaptContext(indexer.indexTransaction.bind(indexer), contractData),
        };
    }
}

function adaptContext<TEntity, TDbContext, TIndexingContext extends { readonly contractData: TContractData }, TContractData>(
    method: (entity: TEntity, dbContext: TDbContext, indexingContext: TIndexingContext) => void | PromiseLike<void>,
    contractData: TContractData,
): (entity: TEntity, dbContext: TDbContext, indexingContext: TIndexingContext & { contractData: UnboundContractData }) => Promise<void> {
    return async (entity, dbContext, indexingContext) => {
        // This is the final indexingContext -> it is served to user -> freeze it.
        await method(entity, dbContext, Object.freeze({ ...indexingContext, contractData }));
    };
}
