import { DappetizerConfig } from '@tezos-dappetizer/indexer';
import { trimEnd } from 'lodash';
import request from 'sync-request';

import { tezosNodeUrlForIntegrationTests } from '../../../../test-utilities/int-tezos-node';

interface TestConfigOptions {
    protocol: string;
    firstLevel: number;
    lastLevel: number;
    levelRangeSizeToGenerate: number 
}

export const BLOCK_COUNT = 7_000;

export function getConfig(options: TestConfigOptions): DappetizerConfig {
    const todayCounter = Math.trunc(Date.now() / (24 * 60 * 60 * 1_000));
    const rangeCount = Math.floor((options.lastLevel - options.firstLevel) / options.levelRangeSizeToGenerate);
    const rangeIndex = todayCounter % rangeCount;

    const fromBlockLevel = options.firstLevel + rangeIndex * options.levelRangeSizeToGenerate;
    const toBlockLevel = Math.min(options.lastLevel, fromBlockLevel + options.levelRangeSizeToGenerate);

    console.log('TESTING PROTOCOL', options.protocol, 'BLOCKS BETWEEN LEVELS', fromBlockLevel, toBlockLevel);

    return {
        modules: [
            { id: '../indexer/dist' },
        ],
        networks: {
            mainnet: {
                indexing: {
                    fromBlockLevel,
                    toBlockLevel,
                    retryIndefinitely: false,
                    retryDelaysMillis: [100, 1_000, 5_000, 60_000, 300_000, 300_000, 300_000],
                    blockQueueSize: 20,
                },
                tezosNode: {
                    url: tezosNodeUrlForIntegrationTests,
                },
            },
        },
        logging: {
            console: {
                minLevel: 'warning',
            },
        },
        time: {
            longExecutionWarningMillis: 5_000,
        },
        usageStatistics: {
            enabled: false,
        },
    };
}

export function getHeadBlockLevel(): number {
    const url = `${trimEnd(tezosNodeUrlForIntegrationTests, '/')}/chains/main/blocks/head/header`;
    const response = request('GET', url);
    const body = response.getBody();
    const json = JSON.parse(body.toString());

    console.log('DETERMINED HEAD LEVEL', json.level);
    return json.level;
}
