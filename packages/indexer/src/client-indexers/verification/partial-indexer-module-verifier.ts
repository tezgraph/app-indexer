import { registerSingleton } from '@tezos-dappetizer/utils';
import { DependencyContainer, Provider } from 'tsyringe';

import { PARTIAL_INDEXER_MODULE_VERIFIERS_DI_TOKEN } from './partial-indexer-module-verifier-di-token';
import { IndexerModule } from '../indexer-module';

export interface PartialIndexerModuleVerifier {
    verify(module: IndexerModule<unknown>): void;
}

export function registerPartialIndexerModuleVerifier(
    diContainer: DependencyContainer,
    partialIndexerModuleVerifier: Provider<PartialIndexerModuleVerifier>,
): void {
    registerSingleton(diContainer, PARTIAL_INDEXER_MODULE_VERIFIERS_DI_TOKEN, partialIndexerModuleVerifier);
}
