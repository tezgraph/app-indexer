import { InjectionToken } from 'tsyringe';

export interface LongExecutionOperation<TResult = unknown> {
    readonly description: string;
    execute(): Promise<TResult>;
}

export interface LongExecutionHelper {
    warn<TResult>(operation: LongExecutionOperation<TResult>): Promise<TResult>;
}

export const LONG_EXECUTION_HELPER_DI_TOKEN: InjectionToken<LongExecutionHelper> = 'Dappetizer:Utils:LongExecutionHelper';
