import { StrictOmit } from 'ts-essentials';

import { describeMember } from '../../../../test-utilities/mocks';
import { Contract, ContractAbstraction, ContractConfig } from '../../src/contracts/contract';
import { ContractImpl } from '../../src/contracts/contract-impl';
import { BigMapInfo } from '../../src/contracts/data/big-map-info/big-map-info';
import { ContractEventInfo } from '../../src/contracts/data/contract-event/contract-event-info';
import { ContractParameterSchemas } from '../../src/contracts/data/schemas/contract-parameter-schemas';
import { MichelsonSchema } from '../../src/michelson/michelson-schema';

describe(ContractImpl.name, () => {
    let target: ContractImpl;
    let abstraction: ContractAbstraction;
    let config: ContractConfig;
    let bigMaps: readonly BigMapInfo[];
    let parameterSchemas: ContractParameterSchemas;
    let storageSchema: MichelsonSchema;
    let events: readonly ContractEventInfo[];

    beforeEach(() => {
        abstraction = { address: 'KT1MTFjUeqBeZoFeW1NLSrzJdcS5apFiUXoB' } as ContractAbstraction;
        config = 'mockedConfig' as any;
        bigMaps = 'mockedBigMaps' as any;
        parameterSchemas = 'mockedParameters' as any;
        storageSchema = 'mockedStorage' as any;
        events = 'mockedEvents' as any;

        target = new ContractImpl(abstraction, config, bigMaps, parameterSchemas, storageSchema, events);
    });

    it('should be frozen', () => {
        expect(target).toBeFrozen();
    });

    describeMember<typeof target>('address', () => {
        it('should get it from abstraction', () => {
            expect(target.address).toBe('KT1MTFjUeqBeZoFeW1NLSrzJdcS5apFiUXoB');
        });
    });

    describeMember<typeof target>('toJSON', () => {
        it('should execlude abstraction', () => {
            const json = target.toJSON();

            expect(json).toBeInstanceOf(Object);
            expect(json).toEqual<StrictOmit<Contract, 'abstraction'>>({
                address: 'KT1MTFjUeqBeZoFeW1NLSrzJdcS5apFiUXoB',
                config,
                bigMaps,
                events,
                parameterSchemas,
                storageSchema,
            });
        });
    });
});
