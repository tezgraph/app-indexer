import { describeMember } from '../../../../../test-utilities/mocks';
import { StructuralFileOptions } from '../../../src/file-generators/structural/structural-file-generator';
import { TsconfigGenerator } from '../../../src/file-generators/structural/tsconfig-generator';
import { generatedPaths } from '../../../src/helpers/generated-paths';
import { Exposed } from '../exposed-json-file-generator';

describe(TsconfigGenerator.name, () => {
    let target: Exposed<TsconfigGenerator>;
    let options: StructuralFileOptions;

    beforeEach(() => {
        target = new TsconfigGenerator() as any;
        options = {} as StructuralFileOptions;
    });

    describeMember<typeof target>('getRelPath', () => {
        it('should return static file', () => {
            expect(target.getRelPath(options)).toBe(generatedPaths.TSCONFIG_JSON);
        });
    });

    describeMember<typeof target>('generateContentsObject', () => {
        it('should generate basic config', () => {
            const contents = target.generateContentsObject(options, 'ignored' as any);

            expect(contents).toEqual({
                include: ['src/**/*'],
                compilerOptions: {
                    outDir: 'dist',
                    target: 'ES2021',
                    lib: ['ES2021'],
                    module: 'CommonJS',
                    moduleResolution: 'Node',
                    experimentalDecorators: true,
                    emitDecoratorMetadata: true,
                    strict: true,
                    typeRoots: ['./node_modules/@types'],
                },
            });
        });
    });
});
