import { argGuard, Constructor, isReadOnlyArray } from '@tezos-dappetizer/utils';
import { ReadonlyArrayOrSingle } from 'ts-essentials';
import { DeepPartial, EntityManager, InsertResult } from 'typeorm';

import { DbCommand } from './db-command';

export class InsertCommand<TEntity extends object> implements DbCommand<InsertResult> {
    readonly description: string;
    readonly entities: readonly DeepPartial<TEntity>[];
    readonly ignoreIfExists: boolean;

    constructor(
        readonly entityType: Constructor<TEntity>,
        entities: ReadonlyArrayOrSingle<DeepPartial<TEntity>>,
        options?: { readonly ignoreIfExists?: boolean },
    ) {
        argGuard.function(entityType, 'entityType');

        this.entities = Object.freeze(isReadOnlyArray(entities) ? entities.slice() : [entities]);
        this.ignoreIfExists = options?.ignoreIfExists === true;
        this.description = `insert ${this.entities.length} of '${entityType.name}' with ignoreIfExists ${this.ignoreIfExists}`;

        Object.freeze(this);
    }

    async execute(dbManager: EntityManager): Promise<InsertResult> {
        if (!this.entities.length) {
            return new InsertResult();
        }
        const builder = dbManager.createQueryBuilder()
            .insert()
            .into(this.entityType)
            .values(this.entities.slice());

        if (this.ignoreIfExists) {
            builder.orIgnore();
        }
        return builder.execute();
    }
}
