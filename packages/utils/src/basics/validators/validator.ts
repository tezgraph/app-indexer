/** Low-level validation of `TResult` value. */
export interface Validator<TValue, TLimits> {
    /**
     * Validates `TResult` value according to the specified limits.
     * @return The specified value if it is valid.
     * @throws `Error` with explanatory message without actual value if it is invalid.
     */
    validate(value: unknown, limits: TLimits): TValue;

    /** Gets the description of expected value according to the specified limits. */
    getExpectedValueDescription(limits: TLimits): string;
}
