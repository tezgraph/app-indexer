import { ArgError } from '@tezos-dappetizer/utils';

import { expectToThrow } from '../../../../../test-utilities/mocks';
import { indexEvent } from '../../../src/decorators/contract/index-event-decorator';

describe(indexEvent.name, () => {
    it.each(['correct', null])('should pass if %s tag', tag => {
        indexEvent(tag);
    });

    it.each([undefined, '', '  \t', 123])('should throw if entrypoint is %s', entrypoint => {
        expectToThrow(() => indexEvent(entrypoint as string), ArgError);
    });
});
