import { injectLogger, Logger, keyof } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { IndexerDatabaseHandlerWrapper } from '../client-indexers/wrappers/indexer-database-handler-wrapper';
import { IndexingConfig } from '../config/indexing/indexing-config';

/** Resolves block level from which the indexing should start. */
@singleton()
export class StartIndexingBlockLevel {
    constructor(
        private readonly config: IndexingConfig,
        private readonly databaseHandler: IndexerDatabaseHandlerWrapper,
        @injectLogger(StartIndexingBlockLevel) private readonly logger: Logger,
    ) {}

    async resolveLevel(): Promise<number> {
        const configProperty = `${this.config.configPropertyPath}.${keyof<typeof this.config>('fromBlockLevel')}`;
        const lastIndexedBlock = await this.databaseHandler.getLastIndexedBlock();

        if (!lastIndexedBlock) {
            this.logger.logInformation(`Starting to index blocks from configured {${configProperty}}`
                + ` because there is no last indexed block (provided by ${this.databaseHandler.name}).`, {
                [configProperty]: this.config.fromBlockLevel,
            });
            return this.config.fromBlockLevel;
        }

        if (this.config.fromBlockLevel > lastIndexedBlock.level + 1) {
            throw new Error(`Configured '${configProperty}' is '${this.config.fromBlockLevel}'`
                + ` which is far from the last indexed block level '${lastIndexedBlock.level}' (provided by ${this.databaseHandler.name})`
                + ` so the indexer cannot start from it because indexed blocks must be continuous to produce consistent data.`
                + ` To start indexing from the configured block level, you need to delete already indexed data.`
                + ` To continue indexing from the last indexed block, configure '${configProperty}' to '${lastIndexedBlock.level + 1}' at most.`);
        }

        this.logger.logInformation(`Continuing indexing blocks from {lastIndexedLevel} + 1 (provided by ${this.databaseHandler.name})`
            + ` because configured {${configProperty}} is lower hence already indexed.`, {
            lastIndexedLevel: lastIndexedBlock.level,
            [configProperty]: this.config.fromBlockLevel,
        });
        return lastIndexedBlock.level + 1;
    }
}
