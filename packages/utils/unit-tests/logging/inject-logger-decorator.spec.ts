import { instance, mock, when } from 'ts-mockito';
import { container, singleton } from 'tsyringe';

import { injectLogger, Logger, LOGGER_FACTORY_DI_TOKEN, LoggerFactory } from '../../src/logging/public-api';

@singleton()
class Foo {
    constructor(@injectLogger(Foo) readonly logger: Logger) {}
}

describe(injectLogger.name, () => {
    it('should inject correct logger', () => {
        const factory = mock<LoggerFactory>();
        const logger = {} as Logger;
        container.registerInstance(LOGGER_FACTORY_DI_TOKEN, instance(factory));
        when(factory.getLogger(Foo)).thenReturn(logger);

        const foo = container.resolve(Foo);

        expect(foo.logger).toBe(logger);
    });
});
