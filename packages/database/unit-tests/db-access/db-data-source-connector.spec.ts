import { DeepWritable } from 'ts-essentials';
import { anything, instance, mock, verify, when } from 'ts-mockito';
import { DataSource, DataSourceOptions } from 'typeorm';

import { expectToThrowAsync, TestLogger } from '../../../../test-utilities/mocks';
import { LongExecutionHelper } from '../../../utils/src/public-api';
import { DbConfig } from '../../src/db-access/db-config';
import { DbDataSourceConnector } from '../../src/db-access/db-data-source-connector';
import { DbEntitiesResolver } from '../../src/db-access/db-entities-resolver';
import { DbInitializerExecutor } from '../../src/db-access/db-initializer-executor';
import { dappetizerNamingStrategy } from '../../src/helpers/naming-helper';
import { TypeormAbstraction } from '../../src/helpers/typeorm-abstraction';

class FooEntity {}

describe(DbDataSourceConnector.name, () => {
    let target: DbDataSourceConnector;
    let dbConfig: DeepWritable<DbConfig>;
    let dbEntitiesResolver: DbEntitiesResolver;
    let longExecutionHelper: LongExecutionHelper;
    let initializerExecutor: DbInitializerExecutor;
    let typeorm: TypeormAbstraction;
    let logger: TestLogger;

    let createdDataSources: DataSource[];
    let calledLog: string[];

    let beforeSynchronizationResult: Promise<void>;
    let afterSynchronizationResult: Promise<void>;
    let dataSourceDestroyResult: Promise<void>;

    beforeEach(() => {
        [dbEntitiesResolver, longExecutionHelper, initializerExecutor, typeorm] = [mock(), mock(), mock(), mock()];
        dbConfig = { dataSourceOptions: { type: 'sqlite', database: 'file.db', synchronize: true } } as DbConfig;
        logger = new TestLogger();
        target = new DbDataSourceConnector(
            dbConfig,
            instance(dbEntitiesResolver),
            instance(longExecutionHelper),
            instance(initializerExecutor),
            instance(typeorm),
            logger,
        );

        createdDataSources = [];
        calledLog = [];
        beforeSynchronizationResult = Promise.resolve();
        afterSynchronizationResult = Promise.resolve();
        dataSourceDestroyResult = Promise.resolve();

        when(typeorm.newDataSource(anything())).thenCall(options => {
            const dataSource = mock(DataSource);
            const index = createdDataSources.length;
            when(dataSource.options).thenReturn(options);
            when(dataSource.metadataTableName).thenReturn(`dataSource-${index}`);
            when(dataSource.initialize()).thenCall(async () => {
                calledLog.push(`dataSource-${index}.initialize()`);
                return Promise.resolve(instance(dataSource));
            });
            when(dataSource.destroy()).thenCall(async () => {
                calledLog.push(`dataSource-${index}.destroy()`);
                return dataSourceDestroyResult;
            });

            calledLog.push(`createDataSource(${index})`);
            createdDataSources.push(instance(dataSource));
            return instance(dataSource);
        });
        when(initializerExecutor.beforeSynchronization(anything())).thenCall(async (src: DataSource) => {
            calledLog.push(`beforeSynchronization(${src.metadataTableName})`);
            return beforeSynchronizationResult;
        });
        when(initializerExecutor.afterSynchronization(anything())).thenCall(async (src: DataSource) => {
            calledLog.push(`afterSynchronization(${src.metadataTableName})`);
            return afterSynchronizationResult;
        });
        when(dbEntitiesResolver.resolve()).thenReturn([FooEntity]);
        when(longExecutionHelper.warn(anything())).thenCall(o => o.execute());
    });

    it('should just connect if no synchronize', async () => {
        dbConfig.dataSourceOptions.synchronize = false;

        const dataSource = await target.connect();

        expect(calledLog).toEqual([
            'createDataSource(0)',
            'dataSource-0.initialize()',
        ]);
        expect(dataSource.options).toEqual<DataSourceOptions>({
            type: 'sqlite',
            database: 'file.db',
            entities: [FooEntity],
            namingStrategy: dappetizerNamingStrategy,
            logging: false,
            synchronize: false,
        });
        expect(dataSource).toBe(createdDataSources[0]);

        verify(longExecutionHelper.warn(anything())).once();
        logger.verifyNothingLogged();
    });

    it('should synchronize on connect', async () => {
        const dataSource = await target.connect();

        expect(calledLog).toEqual([
            'createDataSource(0)',
            'createDataSource(1)',
            'dataSource-1.initialize()',
            'beforeSynchronization(dataSource-1)',
            'dataSource-1.destroy()',
            'dataSource-0.initialize()',
            'afterSynchronization(dataSource-0)',
        ]);
        expect(dataSource).toBe(createdDataSources[0]);
        expect(dataSource.options).toEqual<DataSourceOptions>({
            type: 'sqlite',
            database: 'file.db',
            entities: [FooEntity],
            namingStrategy: dappetizerNamingStrategy,
            logging: false,
            synchronize: true,
        });
        expect(createdDataSources[1]!.options).toEqual<DataSourceOptions>({
            type: 'sqlite',
            database: 'file.db',
            entities: [FooEntity],
            namingStrategy: dappetizerNamingStrategy,
            logging: false,
            synchronize: false,
        });

        verify(longExecutionHelper.warn(anything())).never();
        logger.verifyLoggedCount(2);
        logger.logged(0).verify('Information').verifyMessage('Connecting');
        logger.logged(1).verify('Information').verifyMessage('connected');
    });

    it('should clean up correctly if beforeSynchronization() fails', async () => {
        const initError = new Error('oups');
        beforeSynchronizationResult = Promise.reject(initError);

        const error = await expectToThrowAsync(async () => target.connect());

        expect(error).toBe(initError);
        expect(calledLog.slice(-2)).toEqual([
            'beforeSynchronization(dataSource-1)',
            'dataSource-1.destroy()',
        ]);
        logger.loggedSingle().verify('Information');
    });

    it('should clean up correctly if afterSynchronization() fails', async () => {
        const initError = new Error('oups');
        afterSynchronizationResult = Promise.reject(initError);

        const error = await expectToThrowAsync(async () => target.connect());

        expect(error).toBe(initError);
        expect(calledLog.slice(-2)).toEqual([
            'afterSynchronization(dataSource-0)',
            'dataSource-0.destroy()',
        ]);
        logger.loggedSingle().verify('Information');
    });

    it('should safely destroy data source if previous error', async () => {
        const initError = new Error('oups');
        const destroyError = new Error('wtf');
        beforeSynchronizationResult = Promise.reject(initError);
        dataSourceDestroyResult = Promise.reject(destroyError);

        const error = await expectToThrowAsync(async () => target.connect());

        expect(error).toBe(initError);
        logger.logged(1).verify('Error', { error: destroyError });
        logger.verifyLoggedCount(2);
    });
});
