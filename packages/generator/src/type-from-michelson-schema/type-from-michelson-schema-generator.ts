/* eslint-disable multiline-comment-style */
import { MichelsonSchema } from '@tezos-dappetizer/indexer';
import { errorToString } from '@tezos-dappetizer/utils';
import { inject, singleton } from 'tsyringe';

import { BigMapTyping } from './big-map-typing';
import { addTypeDocComment } from './generator-utils';
import { GeneratorRouter } from './generators/generator-router';
import { GeneratorRouterImpl } from './generators/generator-router-impl';
import { CodeStringBuilder } from '../helpers/code-string-builder';
import { ModuleImport } from '../helpers/module-imports';

export interface GeneratedType {
    readonly codeLines: readonly string[];
    readonly imports: readonly ModuleImport[];
}

@singleton()
export class TypeFromMichelsonSchemaGenerator {
    constructor(
        @inject(GeneratorRouterImpl) private readonly generatorRouter: GeneratorRouter,
    ) {}

    generate(
        schema: MichelsonSchema,
        typeNameToGenerate: string,
        bigMapTyping: BigMapTyping,
    ): GeneratedType {
        try {
            const generated = this.generatorRouter.generate(schema.generated, { typeNameToGenerate, bigMapTyping });
            const codeBuilder = new CodeStringBuilder();

            // Value of `generated.typeDeclaration` may be just a built-int type e.g. `string[]` -> we need to export it.
            // Or an interface named `typeNameToGenerate` which is already generated in `generated.relatedCodeLines` -> no export needed.
            if (generated.typeDeclaration !== typeNameToGenerate) {
                addTypeDocComment(codeBuilder, generated.commentLines)
                    .addLine(`export type ${typeNameToGenerate} = ${generated.typeDeclaration};`)
                    .addEmptyLine();
            }
            codeBuilder.addLines(generated.relatedCodeLines);

            return {
                codeLines: codeBuilder.lines,
                imports: generated.imports,
            };
        } catch (error) {
            throw new Error(`Failed to generate type from Michelson schema.\n`
                + `Schema: ${JSON.stringify(schema.michelson)}\n`
                + `${errorToString(error)}`);
        }
    }
}
