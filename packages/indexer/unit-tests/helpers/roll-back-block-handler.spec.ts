import { Writable } from 'ts-essentials';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { DbContext, expectToThrowAsync, TestLogger } from '../../../../test-utilities/mocks';
import { IndexedBlock } from '../../src/client-indexers/indexed-block';
import { IndexingCycleHandler } from '../../src/client-indexers/indexing-cycle-handler';
import { IndexerDatabaseHandlerWrapper } from '../../src/client-indexers/wrappers/indexer-database-handler-wrapper';
import { IndexerModuleCollection } from '../../src/client-indexers/wrappers/indexer-module-collection';
import { IndexerModuleWrapper } from '../../src/client-indexers/wrappers/indexer-module-wrapper';
import { RollBackBlockHandler } from '../../src/helpers/roll-back-block-handler';
import { scopedLogKeys } from '../../src/helpers/scoped-log-keys';

describe(RollBackBlockHandler.name, () => {
    let target: RollBackBlockHandler;
    let databaseHandler: IndexerDatabaseHandlerWrapper<DbContext>;
    let indexerModules: Writable<IndexerModuleCollection<DbContext>>;
    let logger: TestLogger;

    let cycleHandler1: IndexingCycleHandler<DbContext>;
    let cycleHandler2: IndexingCycleHandler<DbContext>;
    let db: DbContext;
    let blockToRollBack: IndexedBlock;

    beforeEach(() => {
        databaseHandler = mock();
        indexerModules = {} as IndexerModuleCollection<DbContext>;
        logger = new TestLogger();
        target = new RollBackBlockHandler(instance(databaseHandler), indexerModules, logger);

        [cycleHandler1, cycleHandler2] = [mock(), mock()];
        db = 'mockedDb' as any;
        blockToRollBack = { hash: 'haha', level: 123 };

        indexerModules.modules = [
            { name: 'mod1', indexingCycleHandler: instance(cycleHandler1) } as IndexerModuleWrapper<DbContext, unknown>,
            { name: 'mod2', indexingCycleHandler: instance(cycleHandler2) } as IndexerModuleWrapper<DbContext, unknown>,
        ];
        when(databaseHandler.startTransaction()).thenResolve(db);
    });

    it('should roll back by all modules in reversed order', async () => {
        const executed: string[] = [];
        when(cycleHandler1.rollBackOnReorganization!(blockToRollBack, db)).thenCall(() => {
            executed.push('module1RollBack');
        });
        when(cycleHandler2.rollBackOnReorganization!(blockToRollBack, db)).thenCall(() => {
            executed.push('module2RollBack');
        });
        when(databaseHandler.deleteBlock(blockToRollBack, db)).thenCall(() => {
            executed.push('deleteBlock');
        });
        when(databaseHandler.commitTransaction(db)).thenCall(() => {
            executed.push('commit');
        });

        await target.rollBack(blockToRollBack);

        expect(executed).toEqual(['module2RollBack', 'module1RollBack', 'deleteBlock', 'commit']);
        verify(databaseHandler.rollBackTransaction(anything())).never();
        logger.verifyLoggedCount(4);
        logger.logged(0).verify('Debug', { [scopedLogKeys.INDEXER_MODULE]: 'mod2' }).verifyMessage('Rolling back');
        logger.logged(1).verify('Debug', { [scopedLogKeys.INDEXER_MODULE]: 'mod2' }).verifyMessage('rolled back');
        logger.logged(2).verify('Debug', { [scopedLogKeys.INDEXER_MODULE]: 'mod1' }).verifyMessage('Rolling back');
        logger.logged(3).verify('Debug', { [scopedLogKeys.INDEXER_MODULE]: 'mod1' }).verifyMessage('rolled back');
    });

    it('should roll back transaction if block rollback failed', async () => {
        when(cycleHandler2.rollBackOnReorganization!(blockToRollBack, db)).thenReject(new Error('oups'));

        const error = await expectToThrowAsync(async () => target.rollBack(blockToRollBack));

        expect(error.message).toIncludeMultiple([
            `hash: 'haha'`,
            `level: 123`,
            'oups',
        ]);
        verify(databaseHandler.rollBackTransaction(db)).once();
        verify(databaseHandler.commitTransaction(anything())).never();
    });
});
