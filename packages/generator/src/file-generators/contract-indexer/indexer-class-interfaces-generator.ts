import { BigMapInfo, isRawValue, Michelson, MichelsonSchema, michelsonTypePrims } from '@tezos-dappetizer/indexer';
import { isNotNullish, isReadOnlyArray, pascalCase, withIndex } from '@tezos-dappetizer/utils';
import { isEmpty, kebabCase, orderBy, uniq } from 'lodash';
import { singleton } from 'tsyringe';

import { ContractFileGenerator, ContractFileOptions } from './contract-file-generator';
import { ContractDetails } from '../../helpers/contract-details-resolver';
import { TypeScriptBuilder } from '../../helpers/type-script-builder';
import { UniqueNameList } from '../../helpers/unique-name-list';
import { BigMapTyping } from '../../type-from-michelson-schema/big-map-typing';
import { TypeFromMichelsonSchemaGenerator } from '../../type-from-michelson-schema/type-from-michelson-schema-generator';

@singleton()
export class IndexerClassInterfacesGenerator implements ContractFileGenerator {
    constructor(private readonly typeFromMichelsonSchemaGenerator: TypeFromMichelsonSchemaGenerator) {}

    getRelPath(options: ContractFileOptions): string {
        return `src/${getInterfacesFileNameWithoutExt(options.contractDetails)}.ts`;
    }

    generateContents(options: ContractFileOptions): string {
        const builder = new InterfacesBuilder(this.typeFromMichelsonSchemaGenerator, options);

        builder.addPreamble();
        builder.addEntrypointParameterTypes();
        builder.addStorageTypes();
        builder.addBigMapTypes();
        builder.addEventTypes();

        return builder.getFinalCode();
    }
}

class InterfacesBuilder extends TypeScriptBuilder {
    constructor(
        private readonly typeFromMichelsonSchemaGenerator: TypeFromMichelsonSchemaGenerator,
        private readonly options: ContractFileOptions,
    ) {
        super();
    }

    addPreamble(): void {
        this.preambleLines = [
            `/* istanbul ignore next */`,
            `/* eslint-disable */`,
            ``,
            `// This file was generated.`,
            `// It should NOT be modified manually rather it should be regenerated.`,
            `// Contract: ${this.options.contractDetails.contract.address}`,
            ...this.options.tezosNetwork ? [`// Tezos network: ${this.options.tezosNetwork}`] : [],
            ``,
        ];
    }

    addEntrypointParameterTypes(): void {
        const unionName = getEntrypointUnionName(this.options.contractDetails);
        const entrypoints = normalizeEntrypoints(this.options.contractDetails);

        this.code.addLine(`export type ${unionName} =`)
            .indent()
            .addLines(entrypoints.map((entrypoint, index) => {
                const semicolon = index + 1 === entrypoints.length ? ';' : '';
                return `| { entrypoint: '${entrypoint.tezosName}'; value: ${entrypoint.parameterTypeName} }${semicolon}`;
            }))
            .unindent()
            .addEmptyLine();

        for (const entrypoint of entrypoints) {
            this.addFromMichelsonSchema(entrypoint.schema, entrypoint.parameterTypeName, BigMapTyping.InitialValues);
        }
    }

    addStorageTypes(): void {
        const typeNames = getStorageTypeNames(this.options.contractDetails);
        const schema = this.options.contractDetails.contract.storageSchema;

        this.addFromMichelsonSchema(schema, typeNames.current, BigMapTyping.TaquitoAbstraction);

        if (typeNames.changed !== typeNames.current) {
            this.addFromMichelsonSchema(schema, typeNames.changed, BigMapTyping.RawID);
        }
        if (typeNames.initial !== typeNames.current) {
            this.addFromMichelsonSchema(schema, typeNames.initial, BigMapTyping.InitialValues);
        }
    }

    addBigMapTypes(): void {
        for (const bigMap of this.options.contractDetails.contract.bigMaps) {
            const typeNames = getBigMapTypeNames(bigMap, this.options.contractDetails);

            this.addFromMichelsonSchema(bigMap.keySchema, typeNames.key, BigMapTyping.Unexpected);
            this.addFromMichelsonSchema(bigMap.valueSchema, typeNames.value, BigMapTyping.Unexpected);
        }
    }

    addEventTypes(): void {
        for (const event of getEventsByTag(this.options.contractDetails)) {
            const knownSchemas = event.schemas.filter(isNotNullish);
            const payloadTypeNames: string[] = [];

            for (const [schema, index] of withIndex(knownSchemas)) {
                const payloadTypeName = `${event.payloadTypeName}${event.schemas.length > 1 ? index + 1 : ''}`;
                this.addFromMichelsonSchema(schema, payloadTypeName, BigMapTyping.Unexpected);
                payloadTypeNames.push(payloadTypeName);
            }

            if (knownSchemas.length === event.schemas.length && knownSchemas.length > 1) {
                this.code.addLine(`export type ${event.payloadTypeName} = ${payloadTypeNames.join(' | ')};`)
                    .addEmptyLine();
            }
        }
    }

    private addFromMichelsonSchema(schema: MichelsonSchema, typeNameToGenerate: string, bigMapTyping: BigMapTyping): void {
        const generated = this.typeFromMichelsonSchemaGenerator.generate(schema, typeNameToGenerate, bigMapTyping);

        this.code.addLines(generated.codeLines);
        generated.imports.forEach(i => this.imports.add(i));
    }
}

export function getInterfacesFileNameWithoutExt(contractDetails: ContractDetails): string {
    return `${kebabCase(contractDetails.name)}-indexer-interfaces.generated`;
}

export interface NormalizedEntrypoint {
    readonly tezosName: string;
    readonly normalizedName: string;
    readonly schema: MichelsonSchema;
    readonly parameterTypeName: string;
}

export function normalizeEntrypoints(contractDetails: ContractDetails): NormalizedEntrypoint[] {
    const normalizedEntrypoints: NormalizedEntrypoint[] = [];
    const entrypointNames = new UniqueNameList();
    const typePrefix = getContractTypePrefix(contractDetails);
    const entrypoints = !isEmpty(contractDetails.contract.parameterSchemas.entrypoints)
        ? Object.entries(contractDetails.contract.parameterSchemas.entrypoints)
        : [['default', contractDetails.contract.parameterSchemas.default]] as const;

    for (const [tezosName, schema] of entrypoints) {
        const normalizedName = entrypointNames.addAndGetUniqueFor(pascalCase(tezosName));
        const parameterTypeName = `${typePrefix}${normalizedName}Parameter`;
        normalizedEntrypoints.push({ tezosName, normalizedName, schema, parameterTypeName });
    }
    return orderBy(normalizedEntrypoints, e => e.normalizedName);
}

export function getEntrypointUnionName(contractDetails: ContractDetails): string {
    const typePrefix = getContractTypePrefix(contractDetails);
    return `${typePrefix}Parameter`;
}

export interface StorageTypeNames {
    readonly current: string;
    readonly initial: string;
    readonly changed: string;
}

export function getStorageTypeNames(contractDetails: ContractDetails): StorageTypeNames {
    const typePrefix = getContractTypePrefix(contractDetails);

    if (containsBigMap(contractDetails.contract.storageSchema.michelson)) {
        return {
            current: `${typePrefix}CurrentStorage`,
            initial: `${typePrefix}InitialStorage`,
            changed: `${typePrefix}ChangedStorage`,
        };
    }

    const current = `${typePrefix}Storage`;
    return { current, initial: current, changed: current };
}

export interface BigMapTypeNames {
    readonly key: string;
    readonly value: string;
}

export function getBigMapTypeNames(bigMap: BigMapInfo, contractDetails: ContractDetails): BigMapTypeNames {
    const typePrefix = getContractTypePrefix(contractDetails);
    const bigMapInfix = pascalCase(bigMap.pathStr);
    return {
        key: `${typePrefix}${bigMapInfix}Key`,
        value: `${typePrefix}${bigMapInfix}Value`,
    };
}

export function getContractTypePrefix(contractDetails: ContractDetails): string {
    return pascalCase(contractDetails.name);
}

function containsBigMap(schemaMichelson: Michelson): boolean {
    if (isRawValue(schemaMichelson)) {
        return false;
    }
    if (isReadOnlyArray(schemaMichelson)) {
        return schemaMichelson.some(containsBigMap);
    }
    if (schemaMichelson.prim === michelsonTypePrims.BIG_MAP) {
        return true;
    }
    return (schemaMichelson.args ?? []).some(containsBigMap);
}

export interface NormalizedEvent {
    readonly normalizedName: string;
    readonly payloadTypeName: string;
    readonly tezosTag: string | null;
    readonly schemas: readonly (MichelsonSchema | null)[];
}

const UNNAMED = 'Unnamed';

export function getEventsByTag(contractDetails: ContractDetails): readonly NormalizedEvent[] {
    const typePrefix = getContractTypePrefix(contractDetails);
    const uniqueTags = uniq(contractDetails.contract.events.map(e => e.tag));

    if (uniqueTags.includes(null) && uniqueTags.includes(UNNAMED)) {
        throw new Error(`Tag '${UNNAMED}' is reserved by Dappetizer to replace null tag.`
            + ` However, the contract has events with both tags: null and '${UNNAMED}'.`);
    }

    return uniqueTags.map(tag => {
        const normalizedName = tag ? pascalCase(tag) : UNNAMED;
        return {
            normalizedName,
            payloadTypeName: `${typePrefix}${normalizedName}Payload`,
            tezosTag: tag,
            schemas: contractDetails.contract.events.filter(e => e.tag === tag).map(e => e.schema),
        };
    });
}
