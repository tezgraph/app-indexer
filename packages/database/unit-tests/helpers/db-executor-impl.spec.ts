import { anything, deepEqual, instance, mock, objectContaining, spy, verify, when } from 'ts-mockito';
import { DataSource, EntityManager } from 'typeorm';

import { describeMember, expectToThrowAsync, TestClock, TestLogger } from '../../../../test-utilities/mocks';
import { ENABLE_TRACE } from '../../../utils/src/public-api';
import { LongExecutionHelper } from '../../../utils/src/time/public-api';
import { DbCommand } from '../../src/commands/db-command';
import { DbDataSourceProvider } from '../../src/db-access/db-data-source-provider';
import { DbExecutorImpl } from '../../src/helpers/db-executor-impl';
import { DbMetrics } from '../../src/helpers/db-metrics';

describe(DbExecutorImpl.name, () => {
    let target: DbExecutorImpl;
    let longExecutionHelper: LongExecutionHelper;
    let callDurationHistogram: DbMetrics['callDuration'];
    let clock: TestClock;
    let dbConnectionProvider: DbDataSourceProvider;
    let logger: TestLogger;

    beforeEach(() => {
        [longExecutionHelper, dbConnectionProvider, callDurationHistogram] = [mock(), mock(), mock()];
        const metrics = { callDuration: instance(callDurationHistogram) } as DbMetrics;
        clock = new TestClock();
        logger = new TestLogger();
        target = new DbExecutorImpl(instance(longExecutionHelper), metrics, clock, instance(dbConnectionProvider), logger);
    });

    describeMember<typeof target>('executeNoTransaction', () => {
        it('should use global connection', async () => {
            const dbConnection = { manager: 'mockedManager' as any } as DataSource;
            const cmd: DbCommand<string> = 'mockedCmd' as any;
            const targetSpy = spy(target);
            when(dbConnectionProvider.getDataSource()).thenResolve(dbConnection);
            when(targetSpy.execute(dbConnection.manager, cmd)).thenResolve('mockedResult');

            const result = await target.executeNoTransaction(cmd);

            expect(result).toBe('mockedResult');
        });
    });

    describeMember<typeof target>('execute', () => {
        describe.each([true, false])('with logger tracing %s', isLoggerTracing => {
            let dbManager: EntityManager;
            let cmd: DbCommand<string>;

            const act = async () => target.execute(dbManager, instance(cmd));

            class FooCommand {}

            beforeEach(() => {
                dbManager = { mocked: 'DbManager' } as any;
                cmd = mock();
                when(longExecutionHelper.warn(anything())).thenCall(o => o.execute());
                when(cmd.description).thenReturn('Foo desc');
                instance(cmd).constructor = FooCommand;
                (instance(cmd) as any).barArg = 'barVal';
                logger.enableLevel('Trace', isLoggerTracing);
            });

            it('should execute command correctly', async () => {
                when(cmd.execute(dbManager)).thenCall(async () => {
                    clock.tick(66);
                    return Promise.resolve('mockedResult');
                });

                const result = await act();

                expect(result).toBe('mockedResult');
                verify(callDurationHistogram.observe(deepEqual({ command: 'FooCommand' }), 66)).once();
                verify(longExecutionHelper.warn(objectContaining({ description: 'Database command: Foo desc' }))).once();
                logger.logged(0).verifyMessage('Executing').verifyData({
                    command: 'Foo desc',
                    commandArgs: isLoggerTracing ? { barArg: 'barVal' } : ENABLE_TRACE,
                });
                logger.logged(1).verifyMessage('executed').verifyData({
                    command: 'Foo desc',
                    durationMillis: 66,
                    result: isLoggerTracing ? 'mockedResult' : ENABLE_TRACE,
                });
                logger.verifyLoggedCount(2);
            });

            it('should wrap errors', async () => {
                when(cmd.execute(anything())).thenCall(async () => {
                    clock.tick(66);
                    return Promise.reject(new Error('oups'));
                });

                const error = await expectToThrowAsync(act);

                expect(error.message).toIncludeMultiple([`Failed`, `'Foo desc'`, `{"barArg":"barVal"}`, `oups`, '66 millis']);
                logger.logged(0).verifyMessage('Executing');
                logger.logged(1).verifyMessage('Failed').verifyData({
                    command: 'Foo desc',
                    durationMillis: 66,
                    error: new Error('oups'),
                });
                logger.verifyLoggedCount(2);
            });
        });
    });
});
