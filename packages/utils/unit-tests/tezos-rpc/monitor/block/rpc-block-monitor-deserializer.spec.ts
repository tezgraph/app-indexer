import { expectToThrow } from '../../../../../../test-utilities/mocks';
import { RpcBlockMonitorDeserializer } from '../../../../src/tezos-rpc/monitor/block/rpc-block-monitor-deserializer';

describe(RpcBlockMonitorDeserializer.name, () => {
    const target = new RpcBlockMonitorDeserializer();

    it.each([
        ['valid', { hash: 'hash123' }],
    ])('should cast data if %s block header', (_desc, data) => {
        const header = target.deserialize(data);

        expect(header[0]).toBe(data);
    });

    it.each([
        ['undefined', undefined, ['an object']],
        ['null', null, ['an object']],
        ['not array', 'omg', ['an object']],
        ['header with missing hash', { level: 1 }, ['hash', 'Missing non-empty']],
        ['header with missing hash', {}, ['hash', 'Missing non-empty']],
        ['header with mismatched hash', { hash: 123 }, ['hash', 'Missing non-empty']],
        ['header with white-space hash', { hash: '  ' }, ['hash', 'Missing non-empty']],
    ])('should throw if data is %s', (_desc, data, expectedError) => {
        const error = expectToThrow(() => target.deserialize(data));

        expect(error.message).toIncludeMultiple(expectedError);
    });
});
