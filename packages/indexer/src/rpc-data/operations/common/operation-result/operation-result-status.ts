/** @category RPC Block Data */
export enum OperationResultStatus {
    Applied = 'Applied',
    Failed = 'Failed',
    Skipped = 'Skipped',
    Backtracked = 'Backtracked',
}
