import { ObjectValidatorImpl } from './object-validator-impl';
import { typeValidator } from './type-validator';
import { Validator } from './validator';
import { protectAsPublicApi } from './validator-utils';
import { Constructor } from '../reflection-types';

/** Low-level validation of an `object`. */
export interface ObjectValidator extends Validator<object, Constructor | void> {}

/** Static instance of {@link ObjectValidator}. */
export const objectValidator: ObjectValidator = protectAsPublicApi(new ObjectValidatorImpl(typeValidator));
