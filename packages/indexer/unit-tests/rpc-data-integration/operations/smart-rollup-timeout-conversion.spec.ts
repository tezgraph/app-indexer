import { BigNumber } from 'bignumber.js';

import * as samples from './common-samples';
import {
    OperationKind,
    OperationResultStatus,
    SmartRollup,
    SmartRollupGameStatusKind,
    SmartRollupGameStatusLoserReason,
    SmartRollupTimeoutOperation,
} from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    counter: '23077',
    fee: '753',
    gas_limit: '4647',
    kind: 'smart_rollup_timeout',
    rollup: 'sr1QZkk1swognQW3dmiXvga3wVkEgBq7QFjE',
    source: 'tz1TecRhYLVV9bTKRKU9g1Hhpb1Ymw3ynzWS',
    stakers: {
        alice: 'tz1TecRhYLVV9bTKRKU9g1Hhpb1Ymw3ynzWS',
        bob: 'tz1iFnSQ6V2d8piVMPMjtDNdkYNMaUfKwsoy',
    },
    storage_limit: '86',
    metadata: {
        balance_updates: samples.balanceUpdates.input,
        internal_operation_results: samples.internalOperations.input,
        operation_result: {
            balance_updates: samples.balanceUpdates2.input,
            consumed_milligas: '4546724',
            game_status: {
                result: {
                    kind: 'loser',
                    reason: 'timeout',
                    player: 'tz1iFnSQ6V2d8piVMPMjtDNdkYNMaUfKwsoy',
                },
            },
            status: 'applied',
        },
    },
};

const expected: SmartRollupTimeoutOperation = {
    aliceStakerAddress: 'tz1TecRhYLVV9bTKRKU9g1Hhpb1Ymw3ynzWS',
    balanceUpdates: samples.balanceUpdates.getExpected('ops/0[smart_rollup_timeout]/metadata'),
    bobStakerAddress: 'tz1iFnSQ6V2d8piVMPMjtDNdkYNMaUfKwsoy',
    counter: new BigNumber(23077),
    fee: new BigNumber(753),
    gasLimit: new BigNumber(4647),
    internalOperations: samples.internalOperations.getExpected('ops/0[smart_rollup_timeout]/metadata'),
    isInternal: false,
    kind: OperationKind.SmartRollupTimeout,
    result: {
        balanceUpdates: samples.balanceUpdates2.getExpected('ops/0[smart_rollup_timeout]/metadata/operation_result'),
        consumedGas: new BigNumber(4547),
        consumedMilligas: new BigNumber(4546724),
        gameStatus: {
            kind: SmartRollupGameStatusKind.Loser,
            playerAddress: 'tz1iFnSQ6V2d8piVMPMjtDNdkYNMaUfKwsoy',
            reason: SmartRollupGameStatusLoserReason.Timeout,
        },
        rpcData: input.metadata.operation_result as any,
        status: OperationResultStatus.Applied,
        uid: 'ops/0[smart_rollup_timeout]/metadata/operation_result',
    },
    rollup: { address: 'sr1QZkk1swognQW3dmiXvga3wVkEgBq7QFjE' } as SmartRollup,
    rpcData: input as any,
    sourceAddress: 'tz1TecRhYLVV9bTKRKU9g1Hhpb1Ymw3ynzWS',
    storageLimit: new BigNumber(86),
    uid: 'ops/0[smart_rollup_timeout]',
};

shouldConvertRpcOperation(expected.kind, { input, expected });
