import { InjectionToken } from 'tsyringe';

export interface FileSystem {
    /**
     * Determines if the file exists.
     * @returns
     * - `true` if the file system item at the path exists and it is a file.
     * - `false` if there is no the file system item at the path.
     * @throws
     * - `Error` if the file system item at the path exists but is is of different type e.g. a directory.
     * - `Error` including the path and the method name if some other file system error happended.
    */
    existsAsFile(filePath: string): Promise<boolean>;

    /**
     * Reads file text using UTF-8 encoding if the file exists.
     * @returns `null` if the file does not exist.
     */
    readFileTextIfExists(filePath: string): Promise<string | null>;

    /**
     * If the file already exists, then it is overwritten.
     * Also creates directories on the file path if they do not exist.
     */
    writeFileText(filePath: string, contents: string): Promise<void>;
}

export const FILE_SYSTEM_DI_TOKEN: InjectionToken<FileSystem> = 'Dappetizer:Utils:FileSystem';
