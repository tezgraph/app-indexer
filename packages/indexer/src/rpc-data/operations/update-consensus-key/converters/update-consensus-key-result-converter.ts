import { singleton } from 'tsyringe';

import { TaquitoRpcUpdateConsensusKeyResult } from '../../../rpc-data-interfaces';
import { SimpleOperationResultConverter } from '../../common/operation-result/simple-operation-result-converter';

@singleton()
export class UpdateConsensusKeyResultConverter extends SimpleOperationResultConverter<TaquitoRpcUpdateConsensusKeyResult> {}
