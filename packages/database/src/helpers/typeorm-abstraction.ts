import { DataSource, DataSourceOptions } from 'typeorm';

export interface TypeormAbstraction {
    newDataSource(options: DataSourceOptions): DataSource;
}

export const typeormAbstraction: TypeormAbstraction = {
    newDataSource: o => new DataSource(o),
};
