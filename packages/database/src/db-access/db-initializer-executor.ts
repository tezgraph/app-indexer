import {
    errorToString,
    injectLogger,
    Logger,
    LONG_EXECUTION_HELPER_DI_TOKEN,
    LongExecutionHelper,
    StringKeyOfType,
} from '@tezos-dappetizer/utils';
import { orderBy } from 'lodash';
import { AsyncOrSync } from 'ts-essentials';
import { inject, injectAll, singleton } from 'tsyringe';
import { DataSource } from 'typeorm';

import { DbInitializer } from './db-initializer';
import { DB_INITIALIZERS_DI_TOKEN } from './db-initializer-di-token';

@singleton()
export class DbInitializerExecutor {
    private readonly initializers: readonly DbInitializer[];

    constructor(
        @inject(LONG_EXECUTION_HELPER_DI_TOKEN) private readonly longExecutionHelper: LongExecutionHelper,
        @injectLogger(DbInitializerExecutor) private readonly logger: Logger,
        @injectAll(DB_INITIALIZERS_DI_TOKEN) initializers: readonly DbInitializer[],
    ) {
        this.initializers = orderBy(initializers, i => i.order ?? 0);
    }

    async beforeSynchronization(dataSource: DataSource): Promise<void> {
        this.logger.logInformation('Using database {initializers} (in this order).', {
            initializers: this.initializers.map(i => i.name),
        });
        await this.runInitializers('beforeSynchronization', dataSource);
    }

    async afterSynchronization(dataSource: DataSource): Promise<void> {
        await this.runInitializers('afterSynchronization', dataSource);
    }

    private async runInitializers(
        method: StringKeyOfType<DbInitializer, ((s: DataSource) => AsyncOrSync<void>) | undefined>,
        dataSource: DataSource,
    ): Promise<void> {
        for (const initializer of this.initializers) {
            try {
                if (!initializer[method]) {
                    this.logger.logDebug('There is no {method} on {initializer}. So nothing to execute.', { initializer: initializer.name, method });
                    continue;
                }
                this.logger.logDebug('Starting {method} of {initializer}.', { initializer: initializer.name, method });

                await this.longExecutionHelper.warn({
                    description: `${method}(...) of DbInitializer: ${initializer.name}`,
                    execute: async () => initializer[method]?.(dataSource),
                });

                this.logger.logDebug('Successfully finished {method} of {initializer}.', { initializer: initializer.name, method });
            } catch (error) {
                throw new Error(`DbInitializer '${initializer.name}' failed ${method}(...). ${errorToString(error)}`);
            }
        }
    }
}
