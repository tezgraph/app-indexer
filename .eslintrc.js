const config = require('./.eslintrc.base.js');

config.ignorePatterns.push(
    'demos/docs-quick-start-ts',
    'demos/docs-quick-start-js',
    '**/dappetizer*.config.ts',
    '**/.tmp-*/',
    'ts-public-api',
);

config.rules['no-restricted-imports'] = ['error', {
    patterns: [
        '**/*-tests/**',

        // These would create a circle.
        './public-api',
        '../public-api',

        // There cannot be cross-package file imports.
        '**/../cli/src/**',
        '**/../database/src/**',
        '**/../decorators/src/**',
        '**/../generator/src/**',
        '**/../indexer/src/**',
        '**/../utils/src/**',
    ],
}];

module.exports = config;
