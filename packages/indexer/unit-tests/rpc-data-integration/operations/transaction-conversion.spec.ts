import { BigNumber } from 'bignumber.js';

import * as samples from './common-samples';
import {
    BigMapDiffAction,
    LazyBigMapDiff,
    LazyContract,
    LazyStorageChange,
    LazyTransactionParameter,
    OperationKind,
    OperationResultStatus,
    TransactionOperation,
} from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    kind: 'transaction',
    source: 'tz1UQSa7mYNT8BntyxkES1oBvPJr8vRYhmEZ',
    fee: '1086',
    counter: '980443',
    gas_limit: '7820',
    storage_limit: '350',
    amount: '0',
    destination: 'KT1PHubm9HtyQEJ4BBpMTVomq6mhbfNZ9z5w',
    parameters: {
        entrypoint: 'collect',
        value: { int: '101235' },
    },
    metadata: {
        balance_updates: samples.balanceUpdates.input,
        internal_operation_results: samples.internalOperations.input,
        operation_result: {
            balance_updates: samples.balanceUpdates2.input,
            lazy_storage_diff: [{
                kind: 'big_map',
                id: '203375',
                diff: { action: 'remove' },
            }],
            paid_storage_size_diff: '637',
            status: 'applied',
            storage: { int: '101365' },
            consumed_gas: '3260',
            consumed_milligas: '3259860',
            storage_size: '13478187',
            ticket_hash: 'expruhdNPigkTHaU4Kes2fdvuAWH5wot2AELFrzcrq6qJ4veo2J3Uz',
        },
    },
};

const expected: TransactionOperation = {
    amount: new BigNumber(0),
    balanceUpdates: samples.balanceUpdates.getExpected('ops/0[transaction]/metadata'),
    counter: new BigNumber(980443),
    destination: {
        address: 'KT1PHubm9HtyQEJ4BBpMTVomq6mhbfNZ9z5w',
        type: 'Contract',
    } as LazyContract,
    fee: new BigNumber(1086),
    gasLimit: new BigNumber(7820),
    internalOperations: samples.internalOperations.getExpected('ops/0[transaction]/metadata'),
    isInternal: false,
    kind: OperationKind.Transaction,
    result: {
        allocatedDestinationContract: false,
        balanceUpdates: samples.balanceUpdates2.getExpected('ops/0[transaction]/metadata/operation_result'),
        bigMapDiffs: [{
            action: BigMapDiffAction.Remove,
            rpcData: input.metadata.operation_result.lazy_storage_diff[0] as any,
            uid: 'ops/0[transaction]/metadata/operation_result/lazy_storage_diff/0[remove]',
        } as LazyBigMapDiff],
        consumedGas: new BigNumber(3260),
        consumedMilligas: new BigNumber(3259860),
        paidStorageSizeDiff: new BigNumber(637),
        rpcData: input.metadata.operation_result as any,
        status: OperationResultStatus.Applied,
        storageChange: {
            rpcData: { int: '101365' },
            uid: 'ops/0[transaction]/metadata/operation_result/storage',
        } as LazyStorageChange,
        storageSize: new BigNumber(13478187),
        ticketHash: 'expruhdNPigkTHaU4Kes2fdvuAWH5wot2AELFrzcrq6qJ4veo2J3Uz',
        ticketReceipts: [],
        ticketUpdates: [],
        uid: 'ops/0[transaction]/metadata/operation_result',
    },
    rpcData: input as any,
    sourceAddress: 'tz1UQSa7mYNT8BntyxkES1oBvPJr8vRYhmEZ',
    storageLimit: new BigNumber(350),
    transactionParameter: {
        uid: 'ops/0[transaction]/parameters',
    } as LazyTransactionParameter,
    uid: 'ops/0[transaction]',

    // Deprecated:
    destinationAddress: 'KT1PHubm9HtyQEJ4BBpMTVomq6mhbfNZ9z5w',
    destinationContract: { address: 'KT1PHubm9HtyQEJ4BBpMTVomq6mhbfNZ9z5w' } as LazyContract,
};

shouldConvertRpcOperation(expected.kind, { input, expected });
