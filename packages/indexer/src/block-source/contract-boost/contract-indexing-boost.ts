export interface ContractIndexingBoost {
    readonly name: string;

    getLevelsWithContracts(options: BoostLevelsOptions): Promise<BoostedLevels>;

    getChainId(): Promise<string>;
}

export interface BoostLevelsOptions {
    readonly fromBlockLevel: number;
}

export interface BoostedLevels {
    readonly levelsWithContracts: ReadonlySet<number>;
    readonly lastLoadedLevel: number;
    readonly hasMore: boolean;
}
