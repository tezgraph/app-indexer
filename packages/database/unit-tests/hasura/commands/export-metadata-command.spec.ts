import { describeMember } from '../../../../../test-utilities/mocks';
import { ExportMetadataCommand, HasuraMetadata } from '../../../src/hasura/commands/export-metadata-command';

describe(ExportMetadataCommand.name, () => {
    const target = new ExportMetadataCommand();

    describeMember<typeof target>('description', () => {
        it('should be descriptive enough', () => {
            expect(target.description).toBe('export metadata');
        });
    });

    describeMember<typeof target>('createPayload', () => {
        it('should create correct payload', () => {
            const payload = target.createPayload();

            expect(payload).toEqual({
                type: 'export_metadata',
                version: 1,
                args: {},
            });
        });
    });

    describeMember<typeof target>('transformResponse', () => {
        it('should pass response if valid', () => {
            const response: HasuraMetadata = {
                sources: [{
                    name: 'default',
                    tables: [
                        { table: { name: 'foo' } },
                        { table: { name: 'bar' } },
                    ],
                }],
            };

            const transformed = target.transformResponse(response);

            expect(transformed).toBe(response);
        });
    });
});
