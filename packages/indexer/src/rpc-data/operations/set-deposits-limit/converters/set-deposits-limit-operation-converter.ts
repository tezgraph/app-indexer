import { singleton } from 'tsyringe';

import { SetDepositsLimitResultConverter } from './set-deposits-limit-result-converter';
import { primitives } from '../../../primitives';
import { RpcConvertOptions } from '../../../rpc-convert-options';
import { RpcObjectConverter } from '../../../rpc-converter';
import { OperationKind, SetDepositsLimitOperation, TaquitoRpcSetDepositsLimitOperation } from '../../../rpc-data-interfaces';
import {
    CommonMainOperationWithResultConverter,
} from '../../common/main-operation/common-main-operation-with-result-converter';

@singleton()
export class SetDepositsLimitOperationConverter
    extends RpcObjectConverter<TaquitoRpcSetDepositsLimitOperation, SetDepositsLimitOperation, RpcConvertOptions> {
    constructor(
        private readonly commonOperationConverter: CommonMainOperationWithResultConverter,
        private readonly resultConverter: SetDepositsLimitResultConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcSetDepositsLimitOperation,
        uid: string,
        options: RpcConvertOptions,
    ): SetDepositsLimitOperation {
        const commonProperties = this.commonOperationConverter.convert(rpcData, uid, options);
        const result = this.resultConverter.convertFromMetadataOperationResult(rpcData, uid);

        return Object.freeze<SetDepositsLimitOperation>({
            ...commonProperties,
            kind: OperationKind.SetDepositsLimit,
            limit: primitives.bigInteger.convertNullableProperty(rpcData, 'limit', uid, { min: 0 }),
            result,
            rpcData,
        });
    }
}
