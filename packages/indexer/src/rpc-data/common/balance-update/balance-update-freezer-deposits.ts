import { BalanceUpdateFrozenStaker, BalanceUpdateStaker } from './balance-update-stakers';
import { BalanceUpdateCategory, BalanceUpdateKind, BalanceUpdateOrigin, BaseBalanceUpdate } from './base-balance-update';

/** @ignore */
export interface BaseBalanceUpdateFreezerDeposits extends BaseBalanceUpdate {
    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly kind: BalanceUpdateKind.Freezer;

    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly origin: BalanceUpdateOrigin;

    // Null properties:

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly bondRollupAddress: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly committer: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly contractAddress: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly delegateAddress: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly delegatorContractAddress: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly participation: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly revelation: null;
}

/**
 * Corresponds to RPC schema:
 * ```
 * { // Deposits
 *      "kind": "freezer",
 *      "category": "deposits",
 *      "staker": $frozen_staker,
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * || { ...
 *      "origin": "delayed_operation",
 *      "delayed_operation_hash": $Operation_hash }
 * ```
 * @category RPC Block Data
 * @tezosProtocol Oxford
 */
export interface BalanceUpdateFreezerDeposits extends BaseBalanceUpdateFreezerDeposits {
    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly category: BalanceUpdateCategory.Deposits;

    // Specific properties:

    readonly staker: BalanceUpdateFrozenStaker;

    // Null properties:

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly cycle: null;
}

/**
 * Corresponds to RPC schema:
 * ```
 * { // Unstaked_deposits
 *      "kind": "freezer",
 *      "category": "unstaked_deposits",
 *      "staker": $staker,
 *      "cycle": integer ∈ [-2^31-1, 2^31],
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * || { ...
 *      "origin": "delayed_operation",
 *      "delayed_operation_hash": $Operation_hash }
 * ```
 * @category RPC Block Data
 * @tezosProtocol Oxford
 */
export interface BalanceUpdateFreezerUnstakedDeposits extends BaseBalanceUpdateFreezerDeposits {
    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly category: BalanceUpdateCategory.UnstakedDeposits;

    // Specific properties:

    readonly cycle: number;

    readonly staker: BalanceUpdateStaker;
}
