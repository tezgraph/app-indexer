import * as taquitoRpc from '@taquito/rpc';
import { Constructor, freezeResult, injectLogger, keyof, Logger } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { AccumulatorConversion } from './conversions/accumulator-conversion';
import { BaseConversion } from './conversions/base-conversion';
import { BurnedConversion } from './conversions/burned-conversion';
import { CommitmentConversion } from './conversions/commitment-conversion';
import { ContractConversion } from './conversions/contract-conversion';
import { FreezerConversion } from './conversions/freezer-conversion';
import { MintedConversion } from './conversions/minted-conversion';
import { StakingConversion } from './conversions/staking-conversion';
import { primitives } from '../../../primitives';
import { appendUidAttribute, joinUid } from '../../../rpc-converter';
import { BalanceUpdate, BalanceUpdateKind, TaquitoRpcBalanceUpdate } from '../../../rpc-data-interfaces';
import { RpcSkippableArrayConverter } from '../../../rpc-skippable-array-converter';

@singleton()
export class BalanceUpdateConverter extends RpcSkippableArrayConverter<TaquitoRpcBalanceUpdate, BalanceUpdate> {
    constructor(@injectLogger(BalanceUpdateConverter) logger: Logger) {
        super(logger);
    }

    @freezeResult()
    protected override convertObject(rpcData: TaquitoRpcBalanceUpdate, uidPrefix: string): BalanceUpdate {
        const kind = primitives.enum.convertProperty(rpcData, 'kind', uidPrefix, kindMapping);

        const conversionType = allConversionTypes[kind];
        const conversion = new conversionType(rpcData, appendUidAttribute(uidPrefix, rpcData.kind));
        const update = conversion.convert();
        return update;
    }

    convertFromMetadataProperty(
        rpcData: { readonly metadata: { readonly balance_updates?: readonly TaquitoRpcBalanceUpdate[] } },
        uid: string,
    ): readonly BalanceUpdate[] {
        return this.convertArray(
            rpcData.metadata.balance_updates,
            joinUid(uid, keyof<typeof rpcData>('metadata'), keyof<typeof rpcData.metadata>('balance_updates')),
        );
    }
}

const kindMapping = Object.freeze<Record<taquitoRpc.BalanceUpdateKindEnum, BalanceUpdateKind>>({
    accumulator: BalanceUpdateKind.Accumulator,
    burned: BalanceUpdateKind.Burned,
    commitment: BalanceUpdateKind.Commitment,
    contract: BalanceUpdateKind.Contract,
    freezer: BalanceUpdateKind.Freezer,
    minted: BalanceUpdateKind.Minted,
    staking: BalanceUpdateKind.Staking,
});

const allConversionTypes = Object.freeze<Record<BalanceUpdateKind, Constructor<BaseConversion>>>({
    Accumulator: AccumulatorConversion,
    Burned: BurnedConversion,
    Commitment: CommitmentConversion,
    Contract: ContractConversion,
    Freezer: FreezerConversion,
    Minted: MintedConversion,
    Staking: StakingConversion,
});
