// Separate file from main operation b/c it refers internal operations -> circle.
import { DelegationResult } from './delegation-result';
import { BaseInternalOperation } from '../common/internal-operation/base-internal-operation';
import { OperationKind } from '../operation-kind';

/**
 * {@link DelegationOperation} executed as an internal operation.
 * @category RPC Block Data
 */
export interface DelegationInternalOperation extends BaseInternalOperation {
    /** Usable for narrowing the type of this object from {@link InternalOperation}. */
    readonly kind: OperationKind.Delegation;

    // Common delegation properties:
    readonly delegateAddress: string | null;
    readonly result: DelegationResult;
}
