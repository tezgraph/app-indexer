import { BigNumber } from 'bignumber.js';

import * as samples from './common-samples';
import { OperationKind, OperationResultStatus, UpdateConsensusKeyOperation } from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    kind: 'update_consensus_key',
    source: 'tz1fJHFn6sWEd3NnBPngACuw2dggTv6nQZ7g',
    fee: '369',
    counter: '1865327',
    gas_limit: '1100',
    storage_limit: '0',
    pk: 'edpkuGzNDmNbVd87GTHD1n6T6mRNmMuwy5HN1qLwVkrUn5LJcmUkKr',
    metadata: {
        balance_updates: samples.balanceUpdates.input,
        internal_operation_results: samples.internalOperations.input,
        operation_result: {
            status: 'applied',
            consumed_gas: '1000',
            consumed_milligas: '1000000',
        },
    },
};

const expected: UpdateConsensusKeyOperation = {
    balanceUpdates: samples.balanceUpdates.getExpected('ops/0[update_consensus_key]/metadata'),
    counter: new BigNumber(1865327),
    fee: new BigNumber(369),
    gasLimit: new BigNumber(1100),
    internalOperations: samples.internalOperations.getExpected('ops/0[update_consensus_key]/metadata'),
    isInternal: false,
    kind: OperationKind.UpdateConsensusKey,
    pk: 'edpkuGzNDmNbVd87GTHD1n6T6mRNmMuwy5HN1qLwVkrUn5LJcmUkKr',
    result: {
        consumedGas: new BigNumber(1000),
        consumedMilligas: new BigNumber(1000000),
        rpcData: input.metadata.operation_result as any,
        status: OperationResultStatus.Applied,
        uid: 'ops/0[update_consensus_key]/metadata/operation_result',
    },
    rpcData: input as any,
    sourceAddress: 'tz1fJHFn6sWEd3NnBPngACuw2dggTv6nQZ7g',
    storageLimit: new BigNumber(0),
    uid: 'ops/0[update_consensus_key]',
};

shouldConvertRpcOperation(expected.kind, { input, expected });
