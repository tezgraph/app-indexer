import { anything, instance, mock, verify, when } from 'ts-mockito';

import { expectToThrow } from '../../../../test-utilities/mocks';
import { IndexerWrapperFactory, MethodsWrapper, NamedIndexer } from '../../src/indexer-wrappers/indexer-wrapper-factory';

class FooIndexer {
    name?: string = 'ExplicitName';
}

describe(IndexerWrapperFactory.name, () => {
    let target: IndexerWrapperFactory<NamedIndexer>;
    let wrapper: MethodsWrapper<NamedIndexer>;
    let decoratedIndexer: FooIndexer;

    const act = () => target.create(decoratedIndexer, 'test indexer');

    beforeEach(() => {
        wrapper = mock();
        target = new IndexerWrapperFactory(instance(wrapper));
        decoratedIndexer = new FooIndexer();

        when(wrapper.wrapMethods(anything())).thenReturn({ foo: 'wrappedMethod' } as any);
    });

    it('should delegate methods', () => {
        const wrappedIndexer = act();

        expect(wrappedIndexer).toEqual({
            name: 'ExplicitName',
            foo: 'wrappedMethod',
        });
        expect(wrappedIndexer).toBeFrozen();
        verify(wrapper.wrapMethods(decoratedIndexer)).once();
    });

    it.each([null, undefined])(`should use class name if explicit 'name' is %s`, explicitName => {
        decoratedIndexer.name = explicitName!;

        const wrappedIndexer = act();

        expect(wrappedIndexer.name).toBe(FooIndexer.name);
    });

    it(`should throw if unable to resolve 'name' from explicit property nor class name`, () => {
        decoratedIndexer = {} as any;

        runThrowTestExpecting([`specify valid 'name'`, 'from Object class']);
    });

    it('should throw if no decorated methods', () => {
        when(wrapper.wrapMethods(decoratedIndexer)).thenReturn({});

        runThrowTestExpecting(['No decorated index methods', 'from FooIndexer class']);
    });

    it('should wrap error if some wrapper fails', () => {
        when(wrapper.wrapMethods(decoratedIndexer)).thenThrow(new Error('oups'));

        runThrowTestExpecting(['oups', 'from FooIndexer class']);
    });

    function runThrowTestExpecting(expectedError: string[]) {
        const error = expectToThrow(act);

        expect(error.message).toIncludeMultiple([...expectedError, 'create test indexer']);
    }
});
