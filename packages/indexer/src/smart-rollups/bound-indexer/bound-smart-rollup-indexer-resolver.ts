import { getOrCreate } from '@tezos-dappetizer/utils';

import { BoundSmartRollupIndexer } from './bound-smart-rollup-indexer';
import { BoundSmartRollupIndexerBinder } from './bound-smart-rollup-indexer-binder';
import { TrustedSmartRollupIndexer } from '../../client-indexers/wrappers/trusted-indexer-interfaces';
import { SmartRollup } from '../smart-rollup';

export class BoundSmartRollupIndexerResolver<TDbContext, TContextData> {
    private readonly indexerCache = new Map<string, Promise<BoundSmartRollupIndexer<TDbContext, TContextData> | null>>();

    constructor(
        private readonly binder: BoundSmartRollupIndexerBinder<TDbContext>,
        readonly rawIndexers: readonly TrustedSmartRollupIndexer<TDbContext, TContextData, unknown>[],
    ) {}

    async resolve(
        rollup: SmartRollup,
        dbContext: TDbContext,
    ): Promise<BoundSmartRollupIndexer<TDbContext, TContextData> | null> {
        return getOrCreate(this.indexerCache, rollup.address, async () => this.binder.bind(rollup, this.rawIndexers, dbContext));
    }
}
