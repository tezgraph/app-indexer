import { AppShutdownManager, APP_SHUTDOWN_MANAGER_DI_TOKEN, BackgroundWorker, injectExplicit, injectLogger, Logger } from '@tezos-dappetizer/utils';
import { inject, injectable } from 'tsyringe';

import { AsyncProcessor } from './async-processor';

export const BLOCK_INDEXING = 'BlockIndexing';

/** Starts the indexing process on the background. */
export class BlockIndexingBackgroundWorker implements BackgroundWorker {
    readonly name = BLOCK_INDEXING;

    constructor(
        private readonly processor: AsyncProcessor,
    ) {}

    start(): void {
        void this.processor.process();
    }
}

/** Shuts down the app after the indexing is done. */
@injectable()
export class ShutDownAppOnCompletetionProcessor implements AsyncProcessor {
    constructor(
        @injectExplicit() private readonly nextProcessor: AsyncProcessor,
        @inject(APP_SHUTDOWN_MANAGER_DI_TOKEN) private readonly appShutdownManager: AppShutdownManager,
        @injectLogger(ShutDownAppOnCompletetionProcessor) private readonly logger: Logger,
    ) {}

    async process(): Promise<void> {
        await this.nextProcessor.process();

        this.logger.logInformation('Finished block indexing, shutting down the app.');
        await this.appShutdownManager.shutdown();
    }
}
