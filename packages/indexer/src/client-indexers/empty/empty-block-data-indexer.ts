import { noop } from 'lodash';

import { TrustedBlockDataIndexer } from '../wrappers/trusted-indexer-interfaces';

export class EmptyBlockDataIndexer<TDbContext, TContextData> implements TrustedBlockDataIndexer<TDbContext, TContextData> {
    readonly indexBlock = noop;
    readonly indexOperationGroup = noop;
    readonly indexMainOperation = noop;
    readonly indexOperationWithResult = noop;
    readonly indexBalanceUpdate = noop;
}
