import { argGuard, isObject } from '../basics/public-api';

const ABORT_ERROR_NAME = 'AbortError';

export function isAbortError(error: unknown): boolean {
    return isObject(error) && error.name === ABORT_ERROR_NAME;
}

const ABORT_EVENT = 'abort';

export function addAbortListener(signal: AbortSignal, listener: () => void): () => void {
    argGuard.object(signal, 'signal', AbortSignal);
    argGuard.function(listener, 'listener');

    const eventTarget = signal as unknown as EventTarget; // Missing in typings.
    eventTarget.addEventListener(ABORT_EVENT, listener, { once: true });
    return (): void => eventTarget.removeEventListener(ABORT_EVENT, listener);
}

export function throwIfAborted(signal: AbortSignal): void {
    argGuard.object(signal, 'signal', AbortSignal);

    if (signal.aborted) {
        throw new AbortError();
    }
}

/**
 * Indicates that an operation was aborted.
 * That is done usually using {@link https://developer.mozilla.org/en-US/docs/Web/API/AbortController | AbortController}.
 */
export class AbortError extends Error {
    override readonly name = ABORT_ERROR_NAME;

    constructor() {
        super('The operation has been aborted explicitly.');
        Object.freeze(this);
    }
}

interface EventTarget {
    addEventListener(type: string, callback: () => void, options: { once: boolean }): void;
    removeEventListener(type: string, callback: () => void): void;
}
