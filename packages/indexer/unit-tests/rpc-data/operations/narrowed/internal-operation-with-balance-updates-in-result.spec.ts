import { StrictExclude } from 'ts-essentials';

import { OperationResultStatus } from '../../../../src/rpc-data/operations/common/operation-result/operation-result-status';
import { InternalOperation, InternalOperationKind } from '../../../../src/rpc-data/operations/internal-operation';
import {
    InternalOperationWithBalanceUpdatesInResult,
    isInternalOperationWithBalanceUpdatesInResult,
} from '../../../../src/rpc-data/operations/narrowed/internal-operation-with-balance-updates-in-result';

describe(isInternalOperationWithBalanceUpdatesInResult.name, () => {
    const positiveTestCases: Record<InternalOperationWithBalanceUpdatesInResult['kind'], true> = {
        Origination: true,
        Transaction: true,
    };
    const negativeTestCases: Record<StrictExclude<InternalOperationKind, InternalOperationWithBalanceUpdatesInResult['kind']>, false> = {
        Delegation: false,
        Event: false,
    };

    it.each(
        Object.entries({ ...positiveTestCases, ...negativeTestCases }),
    )('should narrow %s to %s', (kind, expectedNarrowing) => {
        const operation = { kind, result: { status: OperationResultStatus.Applied } } as InternalOperation;

        const isNarrowed = isInternalOperationWithBalanceUpdatesInResult(operation);

        expect(isNarrowed).toBe(expectedNarrowing);
    });
});
