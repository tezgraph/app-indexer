import { DependencyContainer } from 'tsyringe';

import { APP_SHUTDOWN_MANAGER_DI_TOKEN } from './app-shutdown-manager';
import { AppShutdownManagerImpl } from './app-shutdown-manager-impl';
import { AppUptimeMetricsUpdater } from './app-uptime-metrics';
import { registerPublicApi } from '../dependency-injection/public-api';
import { registerMetricsUpdater } from '../metrics/public-api';

export function registerAppControl(diContainer: DependencyContainer): void {
    // Public API:
    registerPublicApi(diContainer, APP_SHUTDOWN_MANAGER_DI_TOKEN, {
        useInternalToken: AppShutdownManagerImpl,
        createProxy: internal => ({
            signal: internal.signal,
            shutdown: internal.shutdown.bind(internal),
        }),
    });

    // Inject to Dappetizer features:
    registerMetricsUpdater(diContainer, { useToken: AppUptimeMetricsUpdater });
}
