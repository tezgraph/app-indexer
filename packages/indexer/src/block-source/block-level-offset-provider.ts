import { AsyncIterableProvider, dappetizerAssert, injectExplicit, isNullish } from '@tezos-dappetizer/utils';
import { injectable } from 'tsyringe';

import { IndexingConfig } from '../config/indexing/indexing-config';

/** Offsets returned block levels by configured count of levels in order to avoid rapid reorgs on head esp. since Ithaca protocol. */
@injectable()
export class BlockLevelOffsetProvider implements AsyncIterableProvider<number> {
    constructor(
        @injectExplicit() private readonly nextProvider: AsyncIterableProvider<number>,
        private readonly config: IndexingConfig,
    ) {}

    async *iterate(): AsyncIterableIterator<number> {
        const levelQueue: number[] = [];

        for await (const iteratedLevel of this.nextProvider.iterate()) {
            levelQueue.push(iteratedLevel);

            while (levelQueue.length > this.config.headLevelOffset) {
                const level = levelQueue.shift();
                dappetizerAssert(!isNullish(level), 'Must be defined because of length at least 1.');
                yield level;
            }
        }
    }
}
