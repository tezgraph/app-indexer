import * as taquitoHttp from '@taquito/http-utils';
import { anything, capture, instance, mock, when } from 'ts-mockito';

import { expectToThrowAsync, sleep, TestLogger, verifyCalled } from '../../../../../test-utilities/mocks';
import { asReadonly, TaquitoHttpBackend, TaquitoHttpRequest } from '../../../../utils/src/public-api';
import { IpfsConfig } from '../../../src/helpers/ipfs/ipfs-config';
import {
    IpfsGatewayRaceHttpBackend,
    TAQUITO_HARDCODED_IPFS_GATEWAY,
} from '../../../src/helpers/ipfs/ipfs-gateway-race-http-backend';
import { IpfsMetrics } from '../../../src/helpers/ipfs/ipfs-metrics';

describe(IpfsGatewayRaceHttpBackend.name, () => {
    let target: IpfsGatewayRaceHttpBackend;
    let nextBackend: TaquitoHttpBackend;
    let logger: TestLogger;

    let gatewayWinCounter: IpfsMetrics['gatewayWinCount'];
    let gatewayFailureCounter: IpfsMetrics['gatewayFailureCount'];

    let request: TaquitoHttpRequest;

    const act = async () => target.execute(request);

    beforeEach(() => {
        const config = {
            gateways: asReadonly(['http://first.gate/way', 'http://second.gate/way']),
            timeoutMillis: 123,
        } as IpfsConfig;

        [gatewayWinCounter, gatewayFailureCounter] = [mock(), mock()];
        const metrics = {
            gatewayWinCount: instance(gatewayWinCounter),
            gatewayFailureCount: instance(gatewayFailureCounter),
        } as IpfsMetrics;

        nextBackend = mock();
        logger = new TestLogger();
        target = new IpfsGatewayRaceHttpBackend(instance(nextBackend), config, metrics, logger);

        request = {
            data: 'reqData',
            method: 'POST',
            timeout: 999,
            url: `${TAQUITO_HARDCODED_IPFS_GATEWAY}/hahash?q=1`,
        } as TaquitoHttpRequest;
    });

    it.each([
        ['http://first.gate/way', 1, 100, 'firstResult'],
        ['http://second.gate/way', 100, 1, 'secondResult'],
    ])('should get result from the fastest gateway %s', async (winnerGateway, firstLatency, secondLatency, expectedResult) => {
        const originalRequest = { ...request };
        const abortedOnExec: unknown[] = [];
        setupNextBackend({
            firstGateway: async r => {
                abortedOnExec.push(r.signal?.aborted);
                await sleep(firstLatency);
                return Promise.resolve('firstResult');
            },
            secondGateway: async r => {
                abortedOnExec.push(r.signal?.aborted);
                await sleep(secondLatency);
                return Promise.resolve('secondResult');
            },
        });

        const result = await act();

        expect(result).toBe(expectedResult);
        verifyExecution(0, 'http://first.gate/way/hahash?q=1');
        verifyExecution(1, 'http://second.gate/way/hahash?q=1');
        expect(abortedOnExec).toEqual([false, false]);
        expect(request).toEqual(originalRequest); // Should not be modified.

        verifyCalled(gatewayWinCounter.inc).onceWith({ gateway: winnerGateway });
        verifyCalled(gatewayFailureCounter.inc).never();
        logger.verifyNothingLogged();

        function verifyExecution(callIndex: number, expectedUrl: string) {
            const receivedRequest = capture(nextBackend.execute).byCallIndex(callIndex)[0];
            expect(receivedRequest).toMatchObject<Partial<TaquitoHttpRequest>>({
                data: 'reqData',
                method: 'POST',
                timeout: 123,
                url: expectedUrl,
            });
            expect(receivedRequest.signal?.aborted).toBeTrue(); // Remaining gateways should be aborted after one wins.
        }
    });

    it('should wait for sucessful result if the fastest gateway fails', async () => {
        const fastestError = new Error('oups');
        let abortedOnExec: unknown;
        setupNextBackend({
            firstGateway: async () => Promise.reject(fastestError),
            secondGateway: async r => {
                await sleep(10);
                abortedOnExec = r.signal?.aborted;
                return Promise.resolve('slowResult');
            },
        });

        const result = await act();

        expect(result).toBe('slowResult');
        expect(abortedOnExec).toBeFalse();

        verifyCalled(gatewayWinCounter.inc).onceWith({ gateway: 'http://second.gate/way' });
        verifyCalled(gatewayFailureCounter.inc).onceWith({ gateway: 'http://first.gate/way' });
        logger.loggedSingle().verify('Warning', {
            gateway: 'http://first.gate/way',
            error: fastestError,
        });
    });

    it.each([
        [
            'not-found response over http error',
            new taquitoHttp.HttpResponseError('not-found oups', taquitoHttp.STATUS_CODE.NOT_FOUND, '', '', ''),
            new taquitoHttp.HttpResponseError('http oups', taquitoHttp.STATUS_CODE.INTERNAL_SERVER_ERROR, '', '', ''),
        ],
        [
            'http error over generic error',
            new taquitoHttp.HttpResponseError('http oups', taquitoHttp.STATUS_CODE.INTERNAL_SERVER_ERROR, '', '', ''),
            new Error('oups'),
        ],
    ])('should prefer %s if request failed', async (_desc, preferredError, otherError) => {
        setupNextBackend({
            firstGateway: async () => Promise.reject(preferredError),
            secondGateway: async () => Promise.reject(otherError),
        });

        const error = await expectToThrowAsync(act);

        expect(error).toBe(preferredError);
        verifyCalled(gatewayWinCounter.inc).onceWith({ gateway: 'http://first.gate/way' });
        verifyCalled(gatewayFailureCounter.inc).onceWith({ gateway: 'http://second.gate/way' });
        logger.loggedSingle().verify('Warning', {
            gateway: 'http://second.gate/way',
            error: otherError,
        });
    });

    it('should fallback to the fastest error if request failed with all generic errors', async () => {
        const fastestError = new Error('fast');
        setupNextBackend({
            firstGateway: async () => {
                await sleep(10);
                return Promise.reject(new Error('slow'));
            },
            secondGateway: async () => Promise.reject(fastestError),
        });

        const error = await expectToThrowAsync(act);

        expect(error).toBe(fastestError);
        verifyCalled(gatewayWinCounter.inc).onceWith({ gateway: 'http://second.gate/way' });
        verifyCalled(gatewayFailureCounter.inc).never();
        logger.verifyNothingLogged();
    });

    function setupNextBackend(options: {
        firstGateway: (r: TaquitoHttpRequest) => Promise<unknown>;
        secondGateway: (r: TaquitoHttpRequest) => Promise<unknown>;
    }) {
        when(nextBackend.execute(anything())).thenCall(async (r: TaquitoHttpRequest) => {
            if (r.url.startsWith('http://first.gate/way')) {
                return options.firstGateway(r);
            }
            if (r.url.startsWith('http://second.gate/way')) {
                return options.secondGateway(r);
            }
            return Promise.reject(new Error(`Unexpected URL: ${r.url}`));
        });
    }
});
