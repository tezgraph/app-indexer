import { orderBy } from 'lodash';
import { singleton } from 'tsyringe';

import { ContractEventInfo } from './contract-event-info';
import { MichelsonSchema } from '../../../michelson/michelson-schema';
import { cleanAnnot } from '../../../michelson/michelson-utils';
import { ContractAbstraction } from '../../contract';

@singleton()
export class ContractEventInfosResolver {
    resolve(contractAbstraction: ContractAbstraction): readonly ContractEventInfo[] {
        const events = contractAbstraction.eventSchema.map(e => Object.freeze<ContractEventInfo>({
            tag: e.tag ? cleanAnnot(e.tag) : null,
            schema: e.type ? new MichelsonSchema(e.type) : null,
        }));
        return Object.freeze(orderBy(events, e => e.tag));
    }
}
