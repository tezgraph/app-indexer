import { appendUrl, errorToString, HTTP_FETCHER_DI_TOKEN, HttpFetcher, httpStatusCodes } from '@tezos-dappetizer/utils';
import { inject, singleton } from 'tsyringe';

const healthEndpoint = '/healthz';

@singleton()
export class HasuraHealthApi {
    constructor(
        @inject(HTTP_FETCHER_DI_TOKEN) private readonly httpFetcher: HttpFetcher,
    ) {
    }

    async ensureHealthy(hasuraUrl: string): Promise<void> {
        const healthUrl = appendUrl(hasuraUrl, healthEndpoint);
        try {
            const response = await this.httpFetcher.fetch(healthUrl);

            if (response.status !== httpStatusCodes.OK) {
                const responseBody = await response.text();
                throw new Error(`Response is ${response.status} ${response.statusText} with body: ${responseBody}`);
            }
        } catch (error) {
            throw new Error(`Hasura health check at ${healthUrl} failed. ${errorToString(error)}`);
        }
    }
}
