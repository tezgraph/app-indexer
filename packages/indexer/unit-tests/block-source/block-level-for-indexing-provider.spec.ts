import * as taquitoRpc from '@taquito/rpc';
import { asyncToArray, asyncWrap } from 'iter-tools';
import { instance, mock, verify, when } from 'ts-mockito';

import { expectToThrowAsync } from '../../../../test-utilities/mocks';
import { AsyncIterableProvider, RpcBlockMonitor, RpcMonitorBlockHeader } from '../../../utils/src/public-api';
import { BlockLevelForIndexingProvider } from '../../src/block-source/block-level-for-indexing-provider';
import { StartIndexingBlockLevel } from '../../src/block-source/start-indexing-block-level';

describe(BlockLevelForIndexingProvider.name, () => {
    let target: AsyncIterableProvider<number>;
    let startIndexingBlockLevel: StartIndexingBlockLevel;
    let rpcClient: taquitoRpc.RpcClient;
    let blockMonitor: RpcBlockMonitor;

    const act = async () => asyncToArray(target.iterate());

    beforeEach(() => {
        [startIndexingBlockLevel, rpcClient, blockMonitor] = [mock(), mock(), mock()];
        target = new BlockLevelForIndexingProvider(instance(startIndexingBlockLevel), instance(rpcClient), instance(blockMonitor));
    });

    it('should iterate levels correctly', async () => {
        // Level 15 is some crazy reorg.
        setupLevels({ start: 10, head: 17, monitor: [20, 15, 21, 25] });

        const levels = await act();

        expect(levels).toEqual<number[]>([10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25]);
    });

    it('should fail if start level is in future', async () => {
        setupLevels({ start: 200, head: 17, monitor: [] });

        const error = await expectToThrowAsync(act);

        expect(error.message).toIncludeMultiple(['future', `'200'`, `'17'`]);
        verify(blockMonitor.iterate()).never();
    });

    function setupLevels(options: { start: number; head: number; monitor: number[] }) {
        when(startIndexingBlockLevel.resolveLevel()).thenResolve(options.start);
        when(rpcClient.getBlockHeader()).thenResolve({ level: options.head } as taquitoRpc.BlockHeaderResponse);
        when(blockMonitor.iterate()).thenReturn(asyncWrap(options.monitor.map(l => ({ level: l } as RpcMonitorBlockHeader))));
    }
});
