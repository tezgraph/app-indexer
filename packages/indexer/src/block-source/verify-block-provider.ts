import { AsyncIterableProvider, dumpToString, errorToString, injectExplicit } from '@tezos-dappetizer/utils';
import { injectable, injectAll, registry } from 'tsyringe';

import { BlockVerifier } from './verifiers/block-verifier';
import { MetadataExistenceVerifier } from './verifiers/metadata-existence-verifier';
import { SameChainAsAlreadyIndexedVerifier } from './verifiers/same-chain-as-already-indexed-verifier';
import { SameChainAsContractBoost } from './verifiers/same-chain-as-contract-boost-verifier';
import { SupportedProtocolVerifier } from './verifiers/supported-protocol-verifier';
import { TaquitoRpcBlock } from '../rpc-data/rpc-data-interfaces';

const VERIFIERS_DI_TOKEN = Symbol('BlockVerifiers');

@registry([
    { token: VERIFIERS_DI_TOKEN, useToken: SameChainAsAlreadyIndexedVerifier },
    { token: VERIFIERS_DI_TOKEN, useToken: SameChainAsContractBoost },
    { token: VERIFIERS_DI_TOKEN, useToken: SupportedProtocolVerifier },
    { token: VERIFIERS_DI_TOKEN, useToken: MetadataExistenceVerifier },
])
@injectable()
export class VerifyBlockProvider implements AsyncIterableProvider<TaquitoRpcBlock> {
    constructor(
        @injectExplicit() private readonly nextProvider: AsyncIterableProvider<TaquitoRpcBlock>,
        @injectAll(VERIFIERS_DI_TOKEN) private readonly verifiers: readonly BlockVerifier[],
    ) {}

    async *iterate(): AsyncIterableIterator<TaquitoRpcBlock> {
        let previousBlock: TaquitoRpcBlock | null = null;

        for await (const block of this.nextProvider.iterate()) {
            try {
                const previousBlockConst = previousBlock;
                await Promise.all(this.verifiers.map(async v => v.verify(block, previousBlockConst)));
            } catch (error) {
                const blockInfo = {
                    level: block?.header?.level, // eslint-disable-line @typescript-eslint/no-unnecessary-condition
                    hash: block?.hash, // eslint-disable-line @typescript-eslint/no-unnecessary-condition
                };
                throw new Error(`Invalid block ${dumpToString(blockInfo)} from Tezos node RPC. ${errorToString(error)}`);
            }

            yield block;
            previousBlock = block;
        }
    }
}
