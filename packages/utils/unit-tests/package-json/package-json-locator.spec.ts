import path from 'path';

import { locatePackageJsonInCurrentPackage } from '../../src/package-json/package-json-locator';

describe(locatePackageJsonInCurrentPackage.name, () => {
    it.each([
        ['C:/dev/dappetizer/src/feature/file.ts', 'C:/dev/dappetizer/package.json'],
        ['C:/dev/dappetizer/dist/bundle.js', 'C:/dev/dappetizer/package.json'],
    ])('should resolve %s to %s', (pathWithinPackage, expectedPath) => {
        const packageJsonPath = locatePackageJsonInCurrentPackage(pathWithinPackage);

        expect(packageJsonPath).toBe(path.resolve(expectedPath));
    });
});
