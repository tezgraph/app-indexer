import { Logger } from './logger';
import { argGuard } from '../basics/public-api';

export function runInPerformanceSection<TResult>(
    logger: Logger,
    scopeFunc: () => TResult,
    performanceSection?: string,
): TResult {
    argGuard.object(logger, 'logger');
    argGuard.function(scopeFunc, 'scopeFunc');
    argGuard.ifNotNullish(performanceSection, s => argGuard.nonWhiteSpaceString(s, 'performanceSection'));

    return logger.runWithData({ performanceSection: performanceSection ?? logger.category }, scopeFunc);
}
