import * as taquitoRpc from '@taquito/rpc';
import { keyof, withIndex } from '@tezos-dappetizer/utils';
import { BigNumber } from 'bignumber.js';
import { DeepReadonly } from 'ts-essentials';
import { singleton } from 'tsyringe';

import { actionMapping, BigMapDiffFactory, DiffCreateOptions } from './big-map-diff-factory';
import { primitives } from '../../../primitives';
import {
    getRequiredLazyContract,
    RpcConvertOptionsWithLazyContract,
} from '../../../rpc-convert-options';
import { appendUidAttribute, guardRpcObject, joinUid, RpcObjectConverter } from '../../../rpc-converter';
import { BigMapDiffAction, LazyBigMapDiff } from '../../../rpc-data-interfaces';

export type TaquitoRpcLazyStorageDiff = DeepReadonly<taquitoRpc.LazyStorageDiff>;

@singleton()
export class LazyStorageDiffBigMapConverter
    extends RpcObjectConverter<TaquitoRpcLazyStorageDiff, LazyBigMapDiff[], RpcConvertOptionsWithLazyContract> {
    constructor(
        private readonly diffFactory: BigMapDiffFactory,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcLazyStorageDiff,
        uidPrefix: string,
        options: RpcConvertOptionsWithLazyContract,
    ): LazyBigMapDiff[] {
        if (rpcData.kind !== 'big_map') {
            return [];
        }

        const contract = getRequiredLazyContract(options, { uidPrefix, rpcData });
        guardRpcObject(rpcData.diff, uidPrefix);

        const action = primitives.enum.convertProperty(rpcData.diff, 'action', joinUid(uidPrefix, keyof<typeof rpcData>('diff')), actionMapping);
        const uid = appendUidAttribute(uidPrefix, rpcData.diff.action);

        const diffs = Array.from(this.createDiffs(action, {
            bigMapId: primitives.bigInteger.convertProperty(rpcData, 'id', uid),
            diffCreateOptions: { ...options, contract, rpcData, uid },
            rpcData: rpcData.diff,
            uid: joinUid(uid, keyof<typeof rpcData>('diff')),
        }));
        return diffs;
    }

    private createDiffs(action: BigMapDiffAction, args: DiffCreateArgs): Iterable<LazyBigMapDiff> {
        switch (action) {
            case BigMapDiffAction.Alloc:
                return this.createAllocDiffs(args);
            case BigMapDiffAction.Copy:
                return this.createCopyDiffs(args);
            case BigMapDiffAction.Remove:
                return this.createRemoveDiffs(args);
            case BigMapDiffAction.Update:
                return this.createUpdateDiffs(args);
        }
    }

    private *createAllocDiffs(args: DiffCreateArgs): Iterable<LazyBigMapDiff> {
        yield this.diffFactory.createAlloc({
            ...args.diffCreateOptions,
            bigMapId: args.bigMapId,
            keyType: primitives.michelsonSchema.convertProperty(args.rpcData, 'key_type', args.uid),
            valueType: primitives.michelsonSchema.convertProperty(args.rpcData, 'value_type', args.uid),
        });
        yield* this.createUpdateDiffs(args); // Alloc contains updates too.
    }

    private *createCopyDiffs(args: DiffCreateArgs): Iterable<LazyBigMapDiff> {
        yield this.diffFactory.createCopy({
            ...args.diffCreateOptions,
            sourceBigMapId: primitives.bigInteger.convertProperty(args.rpcData, 'source', args.uid),
            destinationBigMapId: args.bigMapId,
        });
        yield* this.createUpdateDiffs(args); // Copy contains updates too.
    }

    private *createRemoveDiffs(args: DiffCreateArgs): Iterable<LazyBigMapDiff> {
        yield this.diffFactory.createRemove({
            ...args.diffCreateOptions,
            bigMapId: args.bigMapId,
        });
    }

    private *createUpdateDiffs(args: DiffCreateArgs): Iterable<LazyBigMapDiff> {
        for (const [rpcData, index] of withIndex(args.rpcData.updates ?? [])) {
            const uid = joinUid(args.uid, keyof<typeof args.rpcData>('updates'), index);
            guardRpcObject(rpcData, uid);

            yield this.diffFactory.createUpdate({
                ...args.diffCreateOptions,
                bigMapId: args.bigMapId,
                keyHash: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'key_hash', uid),
                keyMichelson: primitives.michelson.convertProperty(rpcData, 'key', uid),
                valueMichelson: primitives.michelson.convertNullableProperty(rpcData, 'value', uid),
                uid,
            });
        }
    }
}

interface DiffCreateArgs {
    readonly bigMapId: BigNumber;
    readonly diffCreateOptions: DiffCreateOptions;
    readonly rpcData: DeepReadonly<taquitoRpc.LazyStorageDiffBigMapItems>;
    readonly uid: string;
}
