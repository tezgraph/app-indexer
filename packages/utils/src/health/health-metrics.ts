import { Gauge } from 'prom-client';
import { singleton } from 'tsyringe';

import { HealthStatus } from './health-check';
import { HealthProvider } from './health-provider';
import { asRequiredLabels, MetricsUpdater } from '../metrics/public-api';

@singleton()
export class HealthMetrics {
    // Intentionally negative name so that the default state (zero value) is healthy.
    readonly unhealthyCount = asRequiredLabels(new Gauge({
        name: 'ai_unhealthy_components',
        help: 'Number of currently unhealthy components.',
        labelNames: ['name'] as const,
    }));
}

export const AGGREGATED_HEALTH_NAME = 'aggregated';

@singleton()
export class HealthMetricsUpdater implements MetricsUpdater {
    constructor(
        private readonly metrics: HealthMetrics,
        private readonly healthProvider: HealthProvider,
    ) {}

    async update(): Promise<void> {
        const health = await this.healthProvider.generateHealthReport();

        this.reportComponent(AGGREGATED_HEALTH_NAME, health.isHealthy);

        for (const [name, result] of Object.entries(health.results)) {
            this.reportComponent(name, result.status !== HealthStatus.Unhealthy);
        }
    }

    private reportComponent(name: string, healthy: boolean): void {
        this.metrics.unhealthyCount.set({ name }, healthy ? 0 : 1);
    }
}
