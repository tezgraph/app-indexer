import { anything, instance, mock, when } from 'ts-mockito';

import { expectToThrowAsync, TestLogger, verifyCalled } from '../../../../test-utilities/mocks';
import { ChildProcessHelper } from '../../src/helpers/child-process-helper';
import { NpmDependenciesInstaller } from '../../src/helpers/npm-dependencies-installer';

describe(NpmDependenciesInstaller.name, () => {
    let target: NpmDependenciesInstaller;
    let childProcess: ChildProcessHelper;
    let logger: TestLogger;

    beforeEach(() => {
        childProcess = mock();
        logger = new TestLogger();
        target = new NpmDependenciesInstaller(instance(childProcess), logger);
    });

    it('should install specified packages', async () => {
        await target.install('./out/dir', [
            { package: 'ts-essentials', isDev: false },
            { package: 'lodash', version: '1.2.3' },
            { package: 'typescript', isDev: true },
        ]);

        verifyCalled(childProcess.exec)
            .with(`npm install "ts-essentials" "lodash@1.2.3" --save`, { cwd: './out/dir' })
            .thenWith(`npm install "typescript" --save-dev`, { cwd: './out/dir' })
            .thenNoMore();
        logger.verifyLoggedCount(4);
        logger.logged(0).verify('Information').verifyMessage('Installing');
        logger.logged(1).verify('Information', { npmPackages: ['ts-essentials', 'lodash@1.2.3'] }).verifyMessage('app');
        logger.logged(2).verify('Information', { npmPackages: ['typescript'] }).verifyMessage('development');
        logger.logged(3).verify('Information').verifyMessage('installed');
    });

    it('should suggest skipping NPM packages', async () => {
        when(childProcess.exec(anything(), anything())).thenReject(new Error('oups'));

        const error = await expectToThrowAsync(async () => target.install('./out/dir', [{ package: 'lodash' }]));

        expect(error.message).toIncludeMultiple([
            `'--npm-install=false'`,
            'oups',
        ]);
    });
});
