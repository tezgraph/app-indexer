import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { SmartRollupCementResult } from './smart-rollup-cement-result';
import { SmartRollup } from '../../../../smart-rollups/smart-rollup';
import { BaseMainOperationWithResult } from '../../common/main-operation/base-main-operation-with-result';
import { OperationKind } from '../../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcSmartRollupCementOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultSmartRollupCement>;

/**
 * It is used to cement a commitment, if the following requirements are met: it has been published for long enough,
 * and there is no concurrent commitment for the same state update. Once a commitment is cemented, it cannot be disputed anymore.
 * @category RPC Block Data
 * @experimental
 */
export interface SmartRollupCementOperation extends BaseMainOperationWithResult<TaquitoRpcSmartRollupCementOperation> {
    // Narrowing properties:

    readonly kind: OperationKind.SmartRollupCement;

    // Specific properties:

    readonly result: SmartRollupCementResult;

    readonly rollup: SmartRollup;
}
