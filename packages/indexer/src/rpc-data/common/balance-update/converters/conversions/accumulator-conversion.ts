import { BaseConversion, createCategoryMapping } from './base-conversion';
import { BalanceUpdateAccumulator, BalanceUpdateCategory, BalanceUpdateKind } from '../../../../rpc-data-interfaces';
import * as RpcCategories from '../rpc-categories';

export class AccumulatorConversion extends BaseConversion {
    override convert(): BalanceUpdateAccumulator {
        const category = this.convertCategory(categoryMapping);

        return {
            ...this.getBlueprint(category),
            ...this.convertOriginProperties(),
            kind: BalanceUpdateKind.Accumulator,
        };
    }
}

const categoryMapping = createCategoryMapping<RpcCategories.Accumulator, BalanceUpdateKind.Accumulator>({
    'block fees': BalanceUpdateCategory.BlockFees,
});
