import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { describeMember, expectToThrowAsync } from '../../../../test-utilities/mocks';
import { DappetizerBug } from '../../../utils/src/public-api';
import { FindAllDappetizerSettingsCommand } from '../../src/commands/find-all-dappetizer-settings-command';
import { InsertCommand } from '../../src/commands/insert-command';
import { DbDappetizerSettings } from '../../src/entities/db-dappetizer-settings';
import { DbDappetizerSettingsManager } from '../../src/helpers/db-dappetizer-settings-manager';
import { DbExecutorImpl } from '../../src/helpers/db-executor-impl';

describe(DbDappetizerSettingsManager.name, () => {
    let target: DbDappetizerSettingsManager;
    let dbExecutor: DbExecutorImpl;

    beforeEach(() => {
        dbExecutor = mock();
        target = new DbDappetizerSettingsManager(instance(dbExecutor));
    });

    describeMember<typeof target>('getSettings', () => {
        it.each([
            ['settings from db', ['mockedSettings' as any], 'mockedSettings' as any],
            ['null if nothing in db', [], null],
        ])('should get %s and cache the result', async (_desc, allSettings, expectedSettings) => {
            setupSettingsInDb(allSettings);

            for (let i = 0; i < 5; i++) {
                expect(await target.getSettings()).toBe(expectedSettings);
            }
            verify(dbExecutor.executeNoTransaction(anything())).once();
        });

        it('should get null if nothing in db', async () => {
            setupSettingsInDb([{ chainId: 'c1' }, { chainId: 'c2' }]);

            const error = await expectToThrowAsync(async () => target.getSettings());

            expect(error.message).toIncludeMultiple(['multiple', '{"chainId":"c1"}', '{"chainId":"c2"}']);
        });
    });

    describeMember<typeof target>('setChainId', () => {
        it('should insert settings if not exist in db', async () => {
            setupSettingsInDb([]);

            await target.setChainId('ccc');

            verify(dbExecutor.executeNoTransaction(deepEqual(new InsertCommand(DbDappetizerSettings, { chainId: 'ccc' })))).once();
            verify(dbExecutor.executeNoTransaction(deepEqual(new FindAllDappetizerSettingsCommand()))).once();
            verify(dbExecutor.executeNoTransaction(anything())).times(2);
        });

        it('should not insert any settings if same chain id exists in db', async () => {
            setupSettingsInDb([{ chainId: 'ccc' }]);

            await target.setChainId('ccc');

            verify(dbExecutor.executeNoTransaction(deepEqual(new FindAllDappetizerSettingsCommand()))).once();
            verify(dbExecutor.executeNoTransaction(anything())).times(1);
        });

        it('should throw if conflicting chain id', async () => {
            setupSettingsInDb([{ chainId: 'ccc' }]);

            await expectToThrowAsync(async () => target.setChainId('bbb'), DappetizerBug);
        });
    });

    function setupSettingsInDb(allSettings: DbDappetizerSettings[]) {
        when(dbExecutor.executeNoTransaction(deepEqual(new FindAllDappetizerSettingsCommand()))).thenResolve(allSettings);
    }
});
