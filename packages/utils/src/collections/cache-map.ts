import { argGuard } from '../basics/public-api';

/**
 * Compared to a Map with a helper method:
 * - Avoids allocation of item factory (+ its closure) on each get.
 * - If TValue is a promise then all callers get the same one -> safe for parallelism.
 * @deprecated Use {@link getOrCreate} instead.
 */
export class CacheMap<TKey, TValue> {
    // JS private fields (using #) b/c the class is exposed as public API.
    readonly #map = new Map<string, { readonly value: TValue }>();
    readonly #stringifyKey: (key: TKey) => string;
    readonly #itemFactory: (key: TKey) => TValue;

    constructor(options: {
        stringifyKey: (key: TKey) => string;
        itemFactory: (key: TKey) => TValue;
    }) {
        argGuard.object(options, 'options');
        argGuard.function(options.stringifyKey, 'options.stringifyKey');
        argGuard.function(options.itemFactory, 'options.itemFactory');

        this.#stringifyKey = options.stringifyKey;
        this.#itemFactory = options.itemFactory;

        Object.freeze(this);
    }

    get(key: TKey): TValue {
        const keyStr = this.#stringifyKey(key);
        let valueWrapper = this.#map.get(keyStr);

        if (!valueWrapper) {
            valueWrapper = { value: this.#itemFactory(key) };
            this.#map.set(keyStr, valueWrapper);
        }
        return valueWrapper.value;
    }
}
