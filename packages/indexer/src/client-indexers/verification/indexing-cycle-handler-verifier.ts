import { IndexerVerifier } from './generic-indexer-verifier';
import { IndexingCycleHandler } from '../indexing-cycle-handler';
import { IndexerInfo } from '../wrappers/indexer-info';

export function verifyIndexingCycleHandler(handler: IndexingCycleHandler<unknown> | undefined, info: IndexerInfo): void {
    if (handler === undefined) {
        return;
    }

    const verifier = new IndexerVerifier(handler, info);

    verifier.verifyOptionalFunctions([
        'createContextData',
        'beforeIndexersExecute',
        'afterIndexersExecuted',
        'afterBlockIndexed',
        'rollBackOnReorganization',
    ]);
}
