import * as taquitoRpc from '@taquito/rpc';
import { AddressPrefix, dappetizerAssert, typed } from '@tezos-dappetizer/utils';
import { StrictOmit } from 'ts-essentials';
import { singleton } from 'tsyringe';

import { BaseInternalOperation } from './base-internal-operation';
import { LazyContractConverter } from '../../../common/lazy-contract/lazy-contract-converter';
import { primitives } from '../../../primitives';
import { TaquitoRpcInternalOperation } from '../../../rpc-data-interfaces';
import { SmartRollupConverter } from '../../smart-rollup/common/converters/smart-rollup-converter';

export function guardKind(rpcData: TaquitoRpcInternalOperation, expectedKind: taquitoRpc.OpKind, uid: string): void {
    dappetizerAssert(rpcData.kind === expectedKind, 'Unexpected operation kind.', { expectedKind, uid, rpcData });
}

export type CommonInternalOperation = StrictOmit<BaseInternalOperation, 'kind'>;

@singleton()
export class CommonInternalOperationConverter {
    constructor(
        private readonly contractConverter: LazyContractConverter,
        private readonly smartRollupConverter: SmartRollupConverter,
    ) {}

    convert(rpcData: TaquitoRpcInternalOperation, uid: string): CommonInternalOperation {
        const sourceAddress = primitives.address.convertProperty(rpcData, 'source', uid, ['KT1', 'sr1']);
        const source = sourceAddress.startsWith(typed<AddressPrefix>('KT1'))
            ? this.contractConverter.convertProperty(rpcData, 'source', uid)
            : this.smartRollupConverter.convertProperty(rpcData, 'source', uid);

        return {
            isInternal: true,
            nonce: primitives.integer.convertProperty(rpcData, 'nonce', uid, { min: 0 }),
            rpcData,
            source,
            sourceAddress,
            sourceContract: source.type === 'Contract' ? source : null,
            uid,
        };
    }
}
