import { AddressValidatorImpl } from './address-validator-impl';
import { stringValidator } from './string-validator';
import { Validator } from './validator';
import { protectAsPublicApi } from './validator-utils';
import { AddressPrefix } from '../address-prefix';

/** Low-level validation of a Tezos address. */
export interface AddressValidator extends Validator<string, readonly AddressPrefix[] | void> {}

/** Static instance of {@link AddressValidator}. */
export const addressValidator: AddressValidator = protectAsPublicApi(new AddressValidatorImpl(stringValidator));
