import { EOL } from 'os';
import { anything, deepEqual, spy, verify, when } from 'ts-mockito';

import { expectToThrow, throwShouldNotBeCalled } from '../../../../test-utilities/mocks';
import { JsonFileGenerator } from '../../src/file-generators/json-file-generator';

interface FooContents { value: string }
interface GeneratorOptions { ooo: number }

class TargetGenerator extends JsonFileGenerator<FooContents, GeneratorOptions> {
    getRelPath(_options: GeneratorOptions): string {
        throwShouldNotBeCalled();
    }

    override validateExistingContents(_contents: FooContents): void {}

    override generateContentsObject(_options: GeneratorOptions, _existingContents: FooContents | null): FooContents {
        throwShouldNotBeCalled();
    }
}

describe(JsonFileGenerator.name, () => {
    let target: TargetGenerator;
    let targetSpy: TargetGenerator;
    let options: GeneratorOptions;

    beforeEach(() => {
        target = new TargetGenerator();
        targetSpy = spy(target);
        options = { ooo: 123 };
    });

    it('should parse existing contents and stringify final contents', () => {
        when(targetSpy.generateContentsObject(anything(), anything())).thenReturn({ value: 'new' });

        const contents = target.generateContents(options, '{ "value": "existing" }');

        expect(contents).toEqual([
            `{`,
            `    "value": "new"`,
            `}`,
            ``,
        ].join(EOL));
        verify(targetSpy.generateContentsObject(options, deepEqual({ value: 'existing' }))).once();
        verify(targetSpy.validateExistingContents(deepEqual({ value: 'existing' }))).once();
    });

    it.each([
        ['does not exist', null],
        ['is empty', ''],
        ['has white-space contents', '  '],
    ])('should provide null existing contents if file %s', (_desc, existingContents) => {
        when(targetSpy.generateContentsObject(anything(), anything())).thenReturn({ value: 'new' });

        target.generateContents(options, existingContents);

        verify(targetSpy.generateContentsObject(options, null)).once();
        verify(targetSpy.validateExistingContents(anything())).never();
    });

    it('should fail if invalid JSON', () => {
        when(targetSpy.getRelPath(options)).thenReturn('dir/file.json');

        const error = expectToThrow(() => target.generateContents(options, 'bullshit'));

        expect(error.message).toIncludeMultiple([`'dir/file.json'`, 'Unexpected token']);
    });

    it('should fail if custom validation fails', () => {
        when(targetSpy.getRelPath(options)).thenReturn('dir/file.json');
        when(targetSpy.validateExistingContents(deepEqual({ value: 'abc' }))).thenThrow(new Error('oups'));

        const error = expectToThrow(() => target.generateContents(options, '{ "value": "abc" }'));

        expect(error.message).toIncludeMultiple([`'dir/file.json'`, 'oups']);
    });
});
