import { BlockDataIndexerUsingDb, DbContext } from '@tezos-dappetizer/database';
import { Block, BlockIndexingContext } from '@tezos-dappetizer/indexer';

import { FooEntity } from './foo-entity';

export class IntBlockDataIndexerUsingDb implements BlockDataIndexerUsingDb {
    private blockCounter = 0;

    async indexBlock(block: Block, dbContext: DbContext, _indexingContext: BlockIndexingContext): Promise<void> {
        await dbContext.transaction.insert(FooEntity, {
            value: `bar-${block.level}`,
            block: { hash: block.hash },
        });

        this.blockCounter++;
        if (this.blockCounter === 3) {
            throw new Error('Simulated error to get rollback.');
        }
    }
}
