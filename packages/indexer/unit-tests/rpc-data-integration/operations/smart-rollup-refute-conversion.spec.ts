import { BigNumber } from 'bignumber.js';

import * as samples from './common-samples';
import {
    OperationKind,
    OperationResultStatus,
    SmartRollup,
    SmartRollupGameStatusKind,
    SmartRollupGameStatusLoserReason,
    SmartRollupRefutationKind,
    SmartRollupRefuteOperation,
} from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    counter: '32553',
    fee: '2096',
    gas_limit: '6299',
    kind: 'smart_rollup_refute',
    metadata: {
        balance_updates: samples.balanceUpdates.input,
        internal_operation_results: samples.internalOperations.input,
        operation_result: {
            balance_updates: samples.balanceUpdates2.input,
            consumed_milligas: '6198354',
            game_status: {
                result: {
                    kind: 'loser',
                    player: 'tz1QD39GBmSzccuDxWMevj2gudiTX1pZL5kC',
                    reason: 'conflict_resolved',
                },
            },
            status: 'applied',
        },
    },
    opponent: 'tz1QD39GBmSzccuDxWMevj2gudiTX1pZL5kC',
    refutation: {
        refutation_kind: 'move',
        choice: '176000000003',
        step: { pvm_step: '03000298e4' },
    },
    rollup: 'sr1LhGA2zC9VcYALSifpRBCgDiQfDSQ6bb4x',
    source: 'tz1ZpuBypK6G754crXDZyoMPaVPoBmBsPda2',
    storage_limit: '7',
};

const expected: SmartRollupRefuteOperation = {
    balanceUpdates: samples.balanceUpdates.getExpected('ops/0[smart_rollup_refute]/metadata'),
    counter: new BigNumber(32553),
    fee: new BigNumber(2096),
    gasLimit: new BigNumber(6299),
    internalOperations: samples.internalOperations.getExpected('ops/0[smart_rollup_refute]/metadata'),
    isInternal: false,
    kind: OperationKind.SmartRollupRefute,
    opponentAddress: 'tz1QD39GBmSzccuDxWMevj2gudiTX1pZL5kC',
    refutation: {
        choice: new BigNumber(176000000003),
        kind: SmartRollupRefutationKind.Move,
        opponentCommitmentHash: null,
        playerCommitmentHash: null,
    },
    result: {
        balanceUpdates: samples.balanceUpdates2.getExpected('ops/0[smart_rollup_refute]/metadata/operation_result'),
        consumedGas: new BigNumber(6199),
        consumedMilligas: new BigNumber(6198354),
        gameStatus: {
            kind: SmartRollupGameStatusKind.Loser,
            playerAddress: 'tz1QD39GBmSzccuDxWMevj2gudiTX1pZL5kC',
            reason: SmartRollupGameStatusLoserReason.ConflictResolved,
        },
        rpcData: input.metadata.operation_result as any,
        status: OperationResultStatus.Applied,
        uid: 'ops/0[smart_rollup_refute]/metadata/operation_result',
    },
    rollup: { address: 'sr1LhGA2zC9VcYALSifpRBCgDiQfDSQ6bb4x' } as SmartRollup,
    rpcData: input as any,
    sourceAddress: 'tz1ZpuBypK6G754crXDZyoMPaVPoBmBsPda2',
    storageLimit: new BigNumber(7),
    uid: 'ops/0[smart_rollup_refute]',
};

shouldConvertRpcOperation(expected.kind, { input, expected });
