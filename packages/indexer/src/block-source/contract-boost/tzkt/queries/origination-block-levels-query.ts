import { dappetizerAssert, JsonFetchCommand } from '@tezos-dappetizer/utils';

import { BoostLevelsOptions } from '../../contract-indexing-boost';
import { buildTzktUrl, MAX_LIMIT } from '../tzkt-url-builder';

export class OriginationBlockLevelsQuery implements JsonFetchCommand<number[]> {
    readonly description: string;
    readonly url: string;

    constructor(
        readonly apiUrl: string,
        readonly contractAddresses: readonly string[],
        readonly options: BoostLevelsOptions,
        readonly limit = MAX_LIMIT,
    ) {
        this.description = `get TzKT origination block levels from ${options.fromBlockLevel}`;
        this.url = buildTzktUrl(apiUrl, '/v1/operations/originations', {
            ['level.ge']: options.fromBlockLevel,
            ['select.values']: 'level',
            ['limit']: limit,
            ['originatedContract.in']: contractAddresses,
        });
    }

    transformResponse(rawLevels: number[]): number[] {
        if (!Array.isArray(rawLevels)) {
            throw new Error('It must be an array.');
        }
        for (const level of rawLevels) {
            if (typeof level !== 'number' || level < 0) {
                throw new Error(`Invalid level: ${JSON.stringify(level)}.`);
            }
        }

        dappetizerAssert(rawLevels.length < this.limit, 'Number of originations should be low -> no paging needed.');
        return rawLevels;
    }
}
