import { BigNumber } from 'bignumber.js';

import * as samples from './common-samples';
import { OperationKind, OperationResultStatus, SmartRollup, SmartRollupPublishOperation } from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    commitment: {
        compressed_state: 'srs12HgNTunZoAEhWa3pTipEGgGRypRzeVxV9ycnrSW6AUCE6fiw6B',
        inbox_level: 104791,
        number_of_ticks: '880000000000',
        predecessor: 'src12W1N4FrCUgrKCAJtScAEPJFcPfAZu3woT6E6wSz3STZvpV7Z4A',
    },
    counter: '31739',
    fee: '964',
    gas_limit: '6418',
    kind: 'smart_rollup_publish',
    metadata: {
        balance_updates: samples.balanceUpdates.input,
        internal_operation_results: samples.internalOperations.input,
        operation_result: {
            balance_updates: samples.balanceUpdates2.input,
            consumed_milligas: '6317837',
            published_at_level: 104794,
            staked_hash: 'src13V3gRSaWDY2UJVTodpY7s8Bb8qCXVj95z4xMecmUsefFQYnMEh',
            status: 'applied',
        },
    },
    rollup: 'sr1AE6U3GNzE8iKzj6sKS5wh1U32ogeULCoN',
    source: 'tz1gCe1sFpppbGGVwCt5jLRqDS9FD1W4Qca4',
    storage_limit: '340',
};

const expected: SmartRollupPublishOperation = {
    balanceUpdates: samples.balanceUpdates.getExpected('ops/0[smart_rollup_publish]/metadata'),
    commitment: {
        compressedState: 'srs12HgNTunZoAEhWa3pTipEGgGRypRzeVxV9ycnrSW6AUCE6fiw6B',
        inboxLevel: 104791,
        numberOfTicks: new BigNumber(880000000000),
        predecessorHash: 'src12W1N4FrCUgrKCAJtScAEPJFcPfAZu3woT6E6wSz3STZvpV7Z4A',
    },
    counter: new BigNumber(31739),
    fee: new BigNumber(964),
    gasLimit: new BigNumber(6418),
    internalOperations: samples.internalOperations.getExpected('ops/0[smart_rollup_publish]/metadata'),
    isInternal: false,
    kind: OperationKind.SmartRollupPublish,
    result: {
        balanceUpdates: samples.balanceUpdates2.getExpected('ops/0[smart_rollup_publish]/metadata/operation_result'),
        consumedGas: new BigNumber(6318),
        consumedMilligas: new BigNumber(6317837),
        publishedAtLevel: 104794,
        rpcData: input.metadata.operation_result as any,
        stakedHash: 'src13V3gRSaWDY2UJVTodpY7s8Bb8qCXVj95z4xMecmUsefFQYnMEh',
        status: OperationResultStatus.Applied,
        uid: 'ops/0[smart_rollup_publish]/metadata/operation_result',
    },
    rollup: { address: 'sr1AE6U3GNzE8iKzj6sKS5wh1U32ogeULCoN' } as SmartRollup,
    rpcData: input as any,
    sourceAddress: 'tz1gCe1sFpppbGGVwCt5jLRqDS9FD1W4Qca4',
    storageLimit: new BigNumber(340),
    uid: 'ops/0[smart_rollup_publish]',
};

shouldConvertRpcOperation(expected.kind, { input, expected });
