import { instance, mock, verify } from 'ts-mockito';

import { TestLogger } from '../../../../test-utilities/mocks';
import { AppShutdownManager, BackgroundWorker } from '../../../utils/src/public-api';
import { AsyncProcessor } from '../../src/block-processing/async-processor';
import {
    BlockIndexingBackgroundWorker,
    ShutDownAppOnCompletetionProcessor,
} from '../../src/block-processing/block-indexing-background-worker';

describe(BlockIndexingBackgroundWorker.name, () => {
    let target: BackgroundWorker;
    let processor: AsyncProcessor;

    beforeEach(() => {
        processor = mock();
        target = new BlockIndexingBackgroundWorker(instance(processor));
    });

    it('should start processing items', async () => {
        await target.start();

        verify(processor.process()).once();
    });
});

describe(ShutDownAppOnCompletetionProcessor.name, () => {
    let target: AsyncProcessor;
    let nextProcessor: AsyncProcessor;
    let appShutdownManager: AppShutdownManager;
    let logger: TestLogger;

    beforeEach(() => {
        [nextProcessor, appShutdownManager] = [mock(), mock()];
        logger = new TestLogger();
        target = new ShutDownAppOnCompletetionProcessor(instance(nextProcessor), instance(appShutdownManager), logger);
    });

    it('should shut down the app after processor is done', async () => {
        await target.process();

        verify(appShutdownManager.shutdown())
            .calledAfter(nextProcessor.process());
        logger.loggedSingle().verify('Information').verifyMessage('Finished');
    });
});
