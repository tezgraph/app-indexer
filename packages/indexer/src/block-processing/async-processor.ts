/** Generic interface for an item processor (e.g. block) or some processing in general (e.g. indexing). */
export interface AsyncProcessor<TItem = void> {
    process(item: TItem): Promise<void>;
}
