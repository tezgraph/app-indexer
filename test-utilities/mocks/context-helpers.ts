export interface DbContext { dbVal: string }
export interface ContextData { ctxVal: string }
export interface ContractData { ctrVal: string }
export interface RollupData { rlpVal: string }
export interface IndexingContext { indx: string }
