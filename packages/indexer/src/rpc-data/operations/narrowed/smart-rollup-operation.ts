import { argGuard } from '@tezos-dappetizer/utils';

import { MainOperation } from '../main-operation';
import { OperationKind } from '../operation-kind';
import { SmartRollupAddMessagesOperation } from '../smart-rollup/add-messages/smart-rollup-add-messages-operation';
import { SmartRollupCementOperation } from '../smart-rollup/cement/smart-rollup-cement-operation';
import {
    SmartRollupExecuteOutboxMessageOperation,
} from '../smart-rollup/execute-outbox-message/smart-rollup-execute-outbox-message-operation';
import { SmartRollupOriginateOperation } from '../smart-rollup/originate/smart-rollup-originate-operation';
import { SmartRollupPublishOperation } from '../smart-rollup/publish/smart-rollup-publish-operation';
import { SmartRollupRecoverBondOperation } from '../smart-rollup/recover-bond/smart-rollup-recover-bond-operation';
import { SmartRollupRefuteOperation } from '../smart-rollup/refute/smart-rollup-refute-operation';
import { SmartRollupTimeoutOperation } from '../smart-rollup/timeout/smart-rollup-timeout-operation';

/**
 * The main (top-level, directly in operation groups) operation releated to some smart rollup.
 * It is the discriminated union which can be narrowed by `kind`.
 * @category RPC Block Data
 */
export type SmartRollupOperation =
    | SmartRollupAddMessagesOperation
    | SmartRollupCementOperation
    | SmartRollupExecuteOutboxMessageOperation
    | SmartRollupOriginateOperation
    | SmartRollupPublishOperation
    | SmartRollupRecoverBondOperation
    | SmartRollupRefuteOperation
    | SmartRollupTimeoutOperation;

const narrowedKinds = new Set([
    OperationKind.SmartRollupAddMessages,
    OperationKind.SmartRollupCement,
    OperationKind.SmartRollupExecuteOutboxMessage,
    OperationKind.SmartRollupOriginate,
    OperationKind.SmartRollupPublish,
    OperationKind.SmartRollupRecoverBond,
    OperationKind.SmartRollupRefute,
    OperationKind.SmartRollupTimeout,
]);

export function isSmartRollupOperation(operation: MainOperation): operation is SmartRollupOperation {
    argGuard.object(operation, 'operation');
    argGuard.enum(operation.kind, 'operation.kind', OperationKind);

    return narrowedKinds.has(operation.kind);
}
