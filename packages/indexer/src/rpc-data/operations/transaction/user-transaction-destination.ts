/** @category RPC Block Data */
export interface UserTransactionDestination {
    /** Usable for narrowing the type of this object. */
    readonly type: 'User';

    /** User `tz` address. */
    readonly address: string;
}
