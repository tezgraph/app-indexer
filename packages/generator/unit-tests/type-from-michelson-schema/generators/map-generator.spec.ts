import { anything, instance, mock, when } from 'ts-mockito';

import { verifyCalled } from '../../../../../test-utilities/mocks';
import { npmModules } from '../../../../utils/src/public-api';
import { GeneratedTypeParts } from '../../../src/type-from-michelson-schema/generators/generator-router';
import { MapGenerator } from '../../../src/type-from-michelson-schema/generators/map-generator';
import { MapGeneratorHelper } from '../../../src/type-from-michelson-schema/generators/map-generator-helper';
import { libTypes } from '../../../src/type-from-michelson-schema/simple-types';

describe(MapGenerator.name, () => {
    let target: MapGenerator;
    let helper: MapGeneratorHelper;

    beforeEach(() => {
        helper = mock(MapGeneratorHelper);
        target = new MapGenerator(instance(helper));
    });

    it('should add MichelsonMap to generated type', () => {
        when(helper.generate(anything(), anything())).thenReturn({
            genericsSuffix: '<Key, Value>',
            commentLines: 'mockedComments' as any,
            imports: ['mockedImport1' as any, 'mockedImport2' as any],
            relatedCodeLines: 'mockedCodeLines' as any,
        });

        const result = target.generate('mockedSchema' as any, 'mockedOptions' as any);

        expect(result).toMatchObject<GeneratedTypeParts>({
            typeDeclaration: 'MichelsonMap<Key, Value>',
            commentLines: 'mockedComments' as any,
            imports: [
                'mockedImport1' as any,
                'mockedImport2' as any,
                { import: libTypes.MICHELSON_MAP, from: npmModules.taquito.TAQUITO },
            ],
            relatedCodeLines: 'mockedCodeLines' as any,
        });
        verifyCalled(helper.generate).onceWith('mockedSchema' as any, 'mockedOptions' as any);
    });
});
