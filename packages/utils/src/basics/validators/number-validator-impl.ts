import { NumberLimits, NumberValidator } from './number-validator';
import { TypeValidator } from './type-validator';

export class NumberValidatorImpl implements NumberValidator {
    constructor(private readonly typeValidator: TypeValidator) {}

    validate(rawValue: unknown, limits?: NumberLimits): number {
        const value = this.typeValidator.validate(rawValue, 'number');

        if (isNaN(value)) {
            throw new Error('It is NaN (not-a-number JavaScript constant).');
        }
        if (limits?.integer === true && !Number.isInteger(value)) {
            throw new Error('It is NOT an integer.');
        }
        if (typeof limits?.min === 'number' && value < limits.min) {
            throw new Error(`It is less than minimum ${limits.min}.`);
        }
        if (typeof limits?.max === 'number' && value > limits.max) {
            throw new Error(`It is greater than maximum ${limits.max}.`);
        }
        return value;
    }

    getExpectedValueDescription(limits?: NumberLimits): string {
        const type = limits?.integer === true ? 'an integer' : 'a number (can be floating point)';
        const min = typeof limits?.min === 'number' ? `greater than or equal to ${limits.min}` : '';
        const max = typeof limits?.max === 'number' ? `less than or equal to ${limits.max}` : '';

        return `${type} ${min}${min && max ? ' and ' : ''}${max}`.trimEnd();
    }
}
