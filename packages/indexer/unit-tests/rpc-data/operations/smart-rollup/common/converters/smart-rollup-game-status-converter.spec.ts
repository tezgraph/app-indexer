import * as taquitoRpc from '@taquito/rpc';

import {
    SmartRollupGameStatusConverter,
    TaquitoRpcGameStatus,
} from '../../../../../../src/rpc-data/operations/smart-rollup/common/converters/smart-rollup-game-status-converter';
import {
    SmartRollupGameStatus,
    SmartRollupGameStatusKind,
    SmartRollupGameStatusLoserReason,
} from '../../../../../../src/rpc-data/rpc-data-interfaces';
import { convertValue } from '../../../../rpc-test-helpers';

describe(SmartRollupGameStatusConverter.name, () => {
    shouldConvertCorrectly({
        rpcData: taquitoRpc.SmartRollupRefuteGameStatusOptions.ONGOING,
        expected: {
            kind: SmartRollupGameStatusKind.Ongoing,
            playerAddress: null,
            reason: null,
        },
    });

    shouldConvertCorrectly({
        rpcData: { result: { kind: taquitoRpc.SmartRollupRefuteGameEndedPlayerOutcomes.DRAW } },
        expected: {
            kind: SmartRollupGameStatusKind.Draw,
            playerAddress: null,
            reason: null,
        },
    });

    shouldConvertCorrectly({
        rpcData: {
            result: {
                kind: taquitoRpc.SmartRollupRefuteGameEndedPlayerOutcomes.LOSER,
                player: 'tz1QD39GBmSzccuDxWMevj2gudiTX1pZL5kC',
                reason: taquitoRpc.SmartRollupRefuteGameEndedReason.CONFLICT_RESOLVED,
            },
        },
        expected: {
            kind: SmartRollupGameStatusKind.Loser,
            playerAddress: 'tz1QD39GBmSzccuDxWMevj2gudiTX1pZL5kC',
            reason: SmartRollupGameStatusLoserReason.ConflictResolved,
        },
    });

    function shouldConvertCorrectly(testCase: {
        rpcData: TaquitoRpcGameStatus;
        expected: SmartRollupGameStatus;
    }) {
        it(`should convert '${testCase.expected.kind}' correctly`, () => {
            const target = new SmartRollupGameStatusConverter();

            const gameStatus = convertValue(target, testCase.rpcData, 'op/id');

            expect(gameStatus).toEqual(testCase.expected);
            expect(gameStatus).toBeFrozen();
        });
    }
});
