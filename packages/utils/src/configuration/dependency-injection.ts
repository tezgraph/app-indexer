import { DependencyContainer } from 'tsyringe';

import { ConfigHttpHandler } from './config-http-handler';
import {
    CONFIG_NETWORK_DI_TOKEN,
    MAINNET,
    NETWORK_SPECIFIC_CONFIG_ELEMENT_DI_TOKEN,
    ROOT_CONFIG_ELEMENT_DI_TOKEN,
} from './di-tokens';
import { NetworkSpecificConfigElementResolver } from './network-specific-config-element-resolver';
import { RootConfigProviderFactory } from './root/root-config-element-factory';
import { registerSingleton } from '../dependency-injection/public-api';
import { registerHttpHandler } from '../http-server/public-api';

export function registerConfiguration(diContainer: DependencyContainer): void {
    // Public API:
    registerSingleton(diContainer, ROOT_CONFIG_ELEMENT_DI_TOKEN, {
        useFactory: c => c.resolve(RootConfigProviderFactory).create(),
    });
    registerSingleton(diContainer, NETWORK_SPECIFIC_CONFIG_ELEMENT_DI_TOKEN, {
        useFactory: c => c.resolve(NetworkSpecificConfigElementResolver).resolve(),
    });

    if (!diContainer.isRegistered(CONFIG_NETWORK_DI_TOKEN)) {
        diContainer.registerInstance(CONFIG_NETWORK_DI_TOKEN, MAINNET);
    }

    // Inject to Dappetizer features:
    registerHttpHandler(diContainer, { useToken: ConfigHttpHandler });
}
