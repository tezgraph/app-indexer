/**
 * Configures Hasura for the indexer app.
 * This is used as a part of {@link "@tezos-dappetizer/database".DappetizerConfigUsingDb}.
 * @example Sample usage (all properties specified explicitly) within `dappetizer.config.ts` file:
 * ```typescript
 * import { DappetizerConfigUsingDb } from '@tezos-dappetizer/database';
 *
 * const config: DappetizerConfigUsingDb = {
 *     hasura: {
 *        dropExistingTracking: false,
 *        autotrackEntities: true,
 *        url: 'http://localhost:8080',
 *        adminSecret: 'secret',
 *        customMappings: [
 *            {
 *                tableName: 'current_balance',   // useful for views tracking
 *                graphQLTypeName: 'CurrentBalance',
 *                graphQLFieldName: 'currentBalance',
 *                columns: [
 *                    { columnName: 'owner_address', graphQLName: 'ownerAddress' },
 *                    { columnName: 'amount', graphQLName: 'amount' },
 *                    { columnName: 'token_id', graphQLName: 'tokenId' },
 *                ],
 *                relationships: [
 *                    {
 *                        name: 'token',
 *                        type: 'manyToOne',
 *                        columns: ['token_id'],
 *                        referencedTableName: 'token',
 *                        referencedColumns: ['id'],
 *                    },
 *                ],
 *            }
 *        ],
 *        autotrackedMappings: [
 *            {
 *                entityName: 'DbAction',
 *                graphQLTypeName: 'Action',
 *                graphQLFieldName: 'action',
 *                columns: [
 *                    { propertyName: 'type', graphQLName: 'actionType' },
 *                ],
 *            },
 *            {
 *                entityName: 'DbToken',
 *                graphQLTypeName: 'Token',
 *                graphQLFieldName: 'token',
 *            },
 *            {
 *                entityName: 'DbContract',
 *                graphQLTypeName: 'Contract',
 *                graphQLFieldName: 'contract',
 *                relationships: [
 *                    {
 *                        name: 'tokens',
 *                        type: 'oneToMany',
 *                        columns: ['address'],
 *                        referencedTableName: 'token',
 *                        referencedColumns: ['contract_address'],
 *                    },
 *                    {
 *                        name: 'contractFeatures',
 *                        type: 'oneToMany',
 *                        columns: ['address'],
 *                        referencedTableName: 'contract_feature',
 *                        referencedColumns: ['contract_address'],
 *                    },
 *                ],
 *            },
 *        ],
 *        selectLimit: 1000,
 *        allowAggregations: true,
 *        namingStyle: 'noTransformation',
 *     },
 *     ... // Other config parts.
 * };
 *
 * export default config;
 * ```
 * @category Dappetizer Config
 */
export interface HasuraDappetizerConfig {
    /**
     * Boolean indicating if Hasura metadata (tracking tables and relationships, and permissions) should be cleared before its recreation.
     * @default `false`
     */
    dropExistingTracking?: boolean;

    /**
     * Boolean indicating if Hasura initializer should automatically track existing entities.
     * @default `false`
     */
    autotrackEntities?: boolean;

    /**
     * The url where Hasura endpoints are available on.
     * @limits An absolute URL string.
     * @default `undefined` (no Hasura is used)
     */
    url?: string;

    /**
     * The admin secret password for Hasura metadata endpoint. Will be used as 'x-hasura-admin-secret' http header.
     * @default `undefined`
     */
    adminSecret?: string;

    /**
     * The Hasura database source.
     * @default `'default'`
     */
    source?: string;

    /**
     * Boolean indicating if aggregation should be added into graphql schema.
     * @default `true`
     */
    allowAggregations?: boolean;

    /**
     * Number indicating limit of rows returned by graphql query.
     * @limits An integer with minimum `1`.
     * @default `100`
     */
    selectLimit?: number;

    /**
     * Naming style for generated graphql schema relationships.
     * @limits A value `'noTransformation'` or `'snakeCase'`.
     * @default `'noTransformation'`
     */
    namingStyle?: 'noTransformation' | 'snakeCase';

    /** The custom mapping of tables, columns and relationships definitions. Useful especially for tracking and configuring views. */
    customMappings?: {
        tableName: string;
        graphQLTypeName: string;
        graphQLFieldName: string;
        graphQLPluralFieldName?: string;
        graphQLAggregateFieldName?: string;

        columns?: {
            columnName: string;
            graphQLName?: string;

            /** @default `false` */
            hidden?: boolean;
        }[];

        relationships?: {
            name: string;
            columns: string[];
            referencedTableName: string;
            referencedColumns: string[];

            /** @default `'manyToOne'` */
            type?: 'oneToMany' | 'manyToOne';
        }[];
    }[];

    /** The entity and column names overrides, additional relationships definitions when autotracking used. */
    autotrackedMappings?: {
        entityName: string;
        graphQLTypeName?: string;
        graphQLFieldName?: string;
        graphQLPluralFieldName?: string;
        graphQLAggregateFieldName?: string;

        /** @default `false` */
        skipped?: boolean;

        columns?: {
            propertyName: string;
            graphQLName?: string;

            /** @default `false` */
            hidden?: boolean;
        }[];

        relationships?: {
            name: string;
            columns: string[];
            referencedTableName: string;
            referencedColumns: string[];

            /** @default `'manyToOne'` */
            type?: 'oneToMany' | 'manyToOne';
        }[];
    }[];
}
