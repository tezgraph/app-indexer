import { AddressPrefix, addressValidator } from '@tezos-dappetizer/utils';

import { PrimitiveConverter } from './primitive-converter';

type AddressConvertOptions = readonly AddressPrefix[];

export class AddressConverter extends PrimitiveConverter<string, string, AddressConvertOptions | void> {
    constructor(
        private readonly validator = addressValidator,
    ) {
        super();
    }

    protected override getTypeDescription(options: AddressConvertOptions | undefined): string {
        return this.validator.getExpectedValueDescription(options);
    }

    protected override convertPrimitive(
        rpcData: string,
        options: AddressConvertOptions | undefined,
    ): string {
        return this.validator.validate(rpcData, options);
    }
}
