import { DappetizerConfigUsingDb } from '@tezos-dappetizer/database';

import { configBase } from './dappetizer.base.config';

const config: DappetizerConfigUsingDb = {
    ...configBase,
    database: {
        type: 'postgres',
        host: process.env.DB_HOST || 'localhost',
        port: 5432,
        username: 'postgres',
        password: 'postgrespassword',
        database: 'postgres',
        schema: 'dappetizer_integration_tests',
        connectTimeoutMS: 1_000,
    },
};

export default config;
