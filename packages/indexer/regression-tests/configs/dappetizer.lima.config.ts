import { BLOCK_COUNT, getConfig, getHeadBlockLevel } from './dappetizer.base.config';

export default getConfig({
    protocol: 'jakarta',
    firstLevel: 2_981_889,
    lastLevel: getHeadBlockLevel(),

    levelRangeSizeToGenerate: 2 * BLOCK_COUNT,
});
