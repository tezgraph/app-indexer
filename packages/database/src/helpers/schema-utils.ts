import { argGuard, isWhiteSpace } from '@tezos-dappetizer/utils';
import { DataSourceOptions } from 'typeorm';
import { CockroachConnectionOptions } from 'typeorm/driver/cockroachdb/CockroachConnectionOptions';
import { OracleConnectionOptions } from 'typeorm/driver/oracle/OracleConnectionOptions';
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';
import { SapConnectionOptions } from 'typeorm/driver/sap/SapConnectionOptions';
import { SpannerConnectionOptions } from 'typeorm/driver/spanner/SpannerConnectionOptions';
import { SqlServerConnectionOptions } from 'typeorm/driver/sqlserver/SqlServerConnectionOptions';

export type DataSourceOptionsWithSchema =
    | CockroachConnectionOptions
    | OracleConnectionOptions
    | PostgresConnectionOptions
    | SapConnectionOptions
    | SpannerConnectionOptions
    | SqlServerConnectionOptions;

const typesWithSchema = new Set<DataSourceOptions['type']>(['cockroachdb', 'mssql', 'oracle', 'postgres', 'sap', 'spanner']);

/**
 * Indicates if the `DataSource` options support `schema` according to the database `type`.
 * The actual value of `schema` may not be specified so {@link resolveSchema} should be used to get the final value.
 */
export function canContainSchema(options: DataSourceOptions): options is DataSourceOptionsWithSchema {
    argGuard.object(options, 'options');
    argGuard.nonWhiteSpaceString(options.type, 'options.type');

    return typesWithSchema.has(options.type);
}

/**
 * Resolves schema as TypeORM would use it from the specified `DataSource` options.
 * @returns
 * - `public` if the options do not contain schema.
 * - `null` if the options cannot contain schema because the database `type` does not support it.
 * - Otherwise `schema` from the options.
 * @throws `Error` if the value of `schema` property is a white-space non-empty string.
 */
export function resolveSchema(options: DataSourceOptionsWithSchema): string;
export function resolveSchema(options: DataSourceOptions): string | null;
export function resolveSchema(options: DataSourceOptions): string | null {
    if (!canContainSchema(options)) {
        return null;
    }

    const schema = options.schema ?? '';
    argGuard.string(schema, 'options.schema');

    if (schema.length === 0) {
        return 'public';
    }
    if (isWhiteSpace(schema)) {
        throw new Error(`Specified schema for database '${options.type}' must be a valid string but it is '${schema}'.`);
    }
    return schema;
}
