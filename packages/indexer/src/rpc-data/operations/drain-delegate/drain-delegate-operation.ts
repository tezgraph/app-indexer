import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { BalanceUpdate } from '../../common/balance-update/balance-update';
import { EntityToIndex } from '../../entity-to-index';
import { OperationKind } from '../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcDrainDelegateOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultDrainDelegate>;

/**
 * It allows an active consensus-key account, i.e., an account to which a baker delegated its consensus-signing responsibility,
 * to *empty* its delegate account. This operation is used as a deterrent to ensure that a delegate secures
 * its consensus key as much as its manager (or main) key.
 * @category RPC Block Data
 */
export interface DrainDelegateOperation extends EntityToIndex<TaquitoRpcDrainDelegateOperation> {
    // Narrowing properties:
    readonly kind: OperationKind.DrainDelegate;

    // Specific properties:
    readonly allocatedDestinationContract: boolean;
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly consensusKey: string;
    readonly delegateAddress: string | null;
    readonly destinationAddress: string | null;
}
