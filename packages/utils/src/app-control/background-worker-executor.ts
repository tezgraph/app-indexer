import { injectAll, singleton } from 'tsyringe';

import { BackgroundWorker } from './background-worker';
import { BACKGROUND_WORKERS_DI_TOKEN } from './background-worker-di-token';
import { errorToString } from '../basics/public-api';
import { injectLogger, Logger } from '../logging/public-api';

enum WorkersStatus {
    Inactive = 'Inactive',
    Starting = 'Starting',
    Running = 'Running',
    Stopping = 'Stopping',
}

@singleton()
export class BackgroundWorkerExecutor {
    private status = WorkersStatus.Inactive;
    private startPromise?: Promise<void>;

    constructor(
        @injectAll(BACKGROUND_WORKERS_DI_TOKEN) private readonly backgroundWorkers: readonly BackgroundWorker[],
        @injectLogger(BackgroundWorkerExecutor) private readonly logger: Logger,
    ) {}

    async start(): Promise<void> {
        this.logWorkers('Starting');
        this.switchStatusFromTo(WorkersStatus.Inactive, WorkersStatus.Starting);

        this.startPromise = this.startInternal();
        return this.startPromise;
    }

    async stop(): Promise<void> {
        if (this.status === WorkersStatus.Starting) {
            await this.startPromise;
        }

        await this.stopInternal();
    }

    private async startInternal(): Promise<void> {
        await Promise.all(this.backgroundWorkers.map(async worker => {
            try {
                this.logWorker('Starting', worker);
                await worker.start();
                this.logWorker('Started', worker);
            } catch (error) {
                this.switchStatusFromTo(WorkersStatus.Starting, WorkersStatus.Running); // To be able to stop().
                throw new Error(`Failed to start background worker '${worker.name}'. ${errorToString(error)}`);
            }
        }));

        this.switchStatusFromTo(WorkersStatus.Starting, WorkersStatus.Running);
        this.logWorkers('Started');
        this.startPromise = undefined;
    }

    private async stopInternal(): Promise<void> {
        this.logWorkers('Stopping');
        this.switchStatusFromTo(WorkersStatus.Running, WorkersStatus.Stopping);

        await Promise.all(this.backgroundWorkers.map(async worker => {
            if (worker.stop) {
                try {
                    this.logWorker('Stopping', worker);
                    await worker.stop();
                    this.logWorker('Stopped', worker);
                } catch (error) {
                    this.logger.logError('Failed to stop {worker}. {error}', {
                        worker: worker.name,
                        error,
                    });
                }
            } else {
                this.logWorker('No need to stop', worker);
            }
        }));

        this.switchStatusFromTo(WorkersStatus.Stopping, WorkersStatus.Inactive);
        this.logWorkers('Stopped');
    }

    private switchStatusFromTo(expectedCurrentStatus: WorkersStatus, targetStatus: WorkersStatus): void {
        if (this.status !== expectedCurrentStatus) {
            throw new Error(`Background workers cannot switch to ${targetStatus} because they are ${this.status}.`);
        }
        this.status = targetStatus;
    }

    private logWorker(verb: string, worker: BackgroundWorker): void {
        this.logger.logInformation(`${verb} {worker}.`, {
            worker: worker.name,
        });
    }

    private logWorkers(verb: string): void {
        this.logger.logInformation(`${verb} all {backgroundWorkers}.`, {
            backgroundWorkers: this.backgroundWorkers.map(w => w.name),
        });
    }
}
