import {
    Clock,
    CLOCK_DI_TOKEN,
    dumpToString,
    ENABLE_TRACE,
    errorToString,
    injectLogger,
    LogData,
    Logger,
    RecordToDump,
    Stopwatch,
    StringKeyOfType,
} from '@tezos-dappetizer/utils';
import { AsyncOrSync } from 'ts-essentials';
import { inject, singleton } from 'tsyringe';

import { IndexerInfo } from '../wrappers/indexer-info';

export interface ErrorHandlingOptions<TArgs extends unknown[], TReturn = void> {
    readonly ifMethodNotDefined?: {
        readonly returnValue: TReturn;
        readonly logMessage: string;
    };
    readonly afterExecuted?: {
        readonly logMessage: string;
        logMessageData?(returnValue: TReturn): LogData;
    };
    getArgsToReport(...args: TArgs): RecordToDump;
    getArgsToTrace(...args: TArgs): object;
}

@singleton()
export class ClientIndexerErrorHandler {
    constructor(
        @inject(CLOCK_DI_TOKEN) private readonly clock: Clock,
        @injectLogger(ClientIndexerErrorHandler) private readonly logger: Logger,
    ) {}

    wrap<TIndexer extends object, TArgs extends unknown[], TReturn>(
        indexer: TIndexer,
        methodName: StringKeyOfType<TIndexer, undefined | ((...args: TArgs) => AsyncOrSync<TReturn>)>,
        info: IndexerInfo,
        options: ErrorHandlingOptions<TArgs, TReturn>,
    ): (...args: TArgs) => Promise<TReturn> {
        const method = (indexer[methodName] as undefined | ((...args: TArgs) => AsyncOrSync<TReturn>))?.bind(indexer);

        return async (...args) => {
            const logData = {
                args: options.getArgsToReport(...args),
                method: methodName,
                indexer: info.indexerName,
                module: info.moduleName,
            };

            if (!method) {
                this.logger.logDebug((`Skipping {method} of {indexer} from {module} with {args} because it is not defined by module developer.`
                    + ` ${options.ifMethodNotDefined?.logMessage ?? ''}`).trimEnd(), logData);
                return options.ifMethodNotDefined?.returnValue ?? undefined as TReturn;
            }

            const stopwatch = new Stopwatch(this.clock);
            try {
                this.logger.logDebug('Executing {method} of {indexer} from {module} with {args}.', {
                    ...logData,
                    argsFull: this.logger.isTraceEnabled ? options.getArgsToTrace(...args) : ENABLE_TRACE,
                });

                const returnValue = await method(...args);

                const isVoid = options.ifMethodNotDefined === undefined;
                this.logger.logDebug((`Successfully executed {method} of {indexer} from {module} with {args} after {durationMillis}.`
                    + ` ${options.afterExecuted?.logMessage ?? ''}`).trimEnd(), {
                    ...logData,
                    ...options.afterExecuted?.logMessageData?.(returnValue),
                    ...!isVoid ? { returnValue: this.logger.isTraceEnabled ? returnValue : ENABLE_TRACE } : {},
                    durationMillis: stopwatch.elapsedMillis,
                });
                return returnValue;
            } catch (error) {
                this.logger.logDebug('Failed executing {method} of {indexer} from {module} with {args} after {durationMillis} because of {error}.', {
                    ...logData,
                    error,
                    durationMillis: stopwatch.elapsedMillis,
                });
                throw new Error(`Module '${info.moduleName}' with its '${info.indexerName}'`
                    + ` failed ${methodName}(${dumpToString(logData.args)}). ${errorToString(error)}`);
            }
        };
    }
}
