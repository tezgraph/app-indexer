import { BigNumber } from 'bignumber.js';

import { BigMapInfoImpl } from '../../../../src/contracts/data/big-map-info/big-map-info-impl';
import { MichelsonSchema } from '../../../../src/michelson/michelson-schema';

describe(BigMapInfoImpl.name, () => {
    let target: BigMapInfoImpl;
    let keySchema: MichelsonSchema;
    let valueSchema: MichelsonSchema;

    beforeEach(() => {
        keySchema = 'key-schema' as any;
        valueSchema = 'value-schema' as any;
        target = new BigMapInfoImpl(['foo', 'bar'], 111, keySchema, valueSchema);
    });

    it('should be created correctly', () => {
        expect(target).toBeFrozen();
        expect(target.path).toEqual(['foo', 'bar']);
        expect(target.path).toBeFrozen();
        expect(target.pathStr).toBe('foo.bar');
        expect(target.name).toBe('bar');
        expect(target.keySchema).toBe(keySchema);
        expect(target.valueSchema).toBe(valueSchema);
        expect(target.lastKnownId).toEqual(new BigNumber(111));
    });

    it('should support changing lastKnownId', () => {
        target.lastKnownId = new BigNumber(222);

        expect(target.lastKnownId).toEqual(new BigNumber(222));
    });

    it('should include all properties in JSON', () => {
        const json = JSON.parse(JSON.stringify(target));

        expect(json).toEqual({
            path: ['foo', 'bar'],
            keySchema,
            valueSchema,
            lastKnownId: '111',
        });
    });
});
