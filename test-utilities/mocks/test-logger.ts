import { BaseLogger, LogData, LogLevel } from '../../packages/utils/src/logging/public-api';

export class LoggedEntry {
    constructor(
        readonly level: LogLevel,
        readonly message: string,
        readonly data: Readonly<Record<string, any>>,
    ) {}

    verify(expectedLevel: keyof typeof LogLevel, expectedData?: LogData): LoggedEntry {
        this.verifyMessagePlaceholdersInData();
        expect(this.level).toBe(LogLevel[expectedLevel]);

        if (expectedData) {
            this.verifyData(expectedData);
        }
        return this;
    }

    verifyData(expectedData: LogData): LoggedEntry {
        this.verifyMessagePlaceholdersInData();
        expect(this.data).toContainAllKeys(Object.keys(expectedData));
        expect(this.data).toMatchObject(expectedData);
        return this;
    }

    verifyMessage(...expectedSubstrs: readonly string[]): LoggedEntry {
        expectedSubstrs.forEach(s => expect(this.message).toContain(s));
        return this;
    }

    verifyMessagePlaceholdersInData(): void {
        const messageKeys = [...(this.message as any).matchAll(/\{[^\\}]+\}/gu)]
            .map(m => m.toString() as string)
            .map(s => s.substring(1, s.length - 1))
            .sort();
        const dataKeys = Object.keys(this.data).sort();

        expect(dataKeys).toEqual(expect.arrayContaining(messageKeys));
    }
}

let loggerCounter = 1;

export class TestLogger extends BaseLogger {
    readonly enabledLevels = new Set(Object.keys(LogLevel) as (keyof typeof LogLevel)[]);
    private readonly loggedEntries: LoggedEntry[] = [];

    constructor() {
        super(`TestCategory-${loggerCounter++}`);
    }

    logged(index: number): LoggedEntry {
        expect(this.loggedEntries.length).toBeGreaterThanOrEqual(index + 1);
        return this.loggedEntries[index]!;
    }

    loggedLast(): LoggedEntry {
        expect(this.loggedEntries).not.toBeEmpty();
        return this.loggedEntries[this.loggedEntries.length - 1]!;
    }

    loggedSingle(level?: keyof typeof LogLevel): LoggedEntry {
        const entries = this.loggedEntries.filter(e => level === undefined || e.level === LogLevel[level]);
        expect(entries).toHaveLength(1);
        return entries[0]!;
    }

    verifyLoggedCount(expectedCount: number): void {
        expect(this.loggedEntries).toHaveLength(expectedCount);
    }

    verifyNothingLogged(): void {
        expect(this.loggedEntries).toEqual([]);
    }

    verifyNotLogged(unexpectedLevel: keyof typeof LogLevel, ...otherUnexpectedLevels: (keyof typeof LogLevel)[]) {
        for (const level of [unexpectedLevel, ...otherUnexpectedLevels]) {
            expect(this.loggedEntries.map((e) => e.level)).not.toContain(LogLevel[level]);
        }
    }

    clear(): void {
        this.loggedEntries.length = 0;
    }

    enableLevel(level: keyof typeof LogLevel, enabled = true): void {
        if (enabled) {
            this.enabledLevels.add(level);
        } else {
            this.enabledLevels.delete(level);
        }
    }

    override isEnabled(level: LogLevel): boolean {
        return Array.from(this.enabledLevels.keys()).some(l => LogLevel[l] === level);
    }

    protected override writeToLog(level: LogLevel, message: string, data: LogData): void {
        const entry = new LoggedEntry(level, message, data);
        entry.verifyMessagePlaceholdersInData();
        this.loggedEntries.push(entry);
    }
}
