import { describeMember } from '../../../../../test-utilities/mocks';
import { ClearMetadataCommand } from '../../../src/hasura/commands/clear-metadata-command';

describe(ClearMetadataCommand.name, () => {
    const target = new ClearMetadataCommand();

    describeMember<typeof target>('description', () => {
        it('should be descriptive enough', () => {
            expect(target.description).toBe('clear metadata');
        });
    });

    describeMember<typeof target>('createPayload', () => {
        it('should create correct payload', () => {
            const payload = target.createPayload();

            expect(payload).toEqual({
                type: 'clear_metadata',
                args: {},
            });
        });
    });
});
