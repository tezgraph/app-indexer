import { errorToString, injectLogger, Logger } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { MainOperationTraverser } from './main-operation-traverser';
import { MainOperationIndexingContext, OperationGroupIndexingContext } from '../client-indexers/block-data-indexer';
import { IndexerModuleWrapper } from '../client-indexers/wrappers/indexer-module-wrapper';
import { scopedLogKeys } from '../helpers/scoped-log-keys';
import { OperationGroup } from '../rpc-data/rpc-data-interfaces';

@singleton()
export class OperationGroupTraverser<TDbContext> {
    constructor(
        private readonly mainOperationTraverser: MainOperationTraverser<TDbContext>,
        @injectLogger(OperationGroupTraverser) private readonly logger: Logger,
    ) {}

    async traverse<TContextData>(
        indexerModule: IndexerModuleWrapper<TDbContext, TContextData>,
        dbContext: TDbContext,
        operationGroup: OperationGroup,
        indexingContext: OperationGroupIndexingContext<TContextData>,
    ): Promise<void> {
        await this.logger.runWithData({ [scopedLogKeys.INDEXED_OPERATION_GROUP_HASH]: operationGroup.hash }, async () => {
            try {
                await indexerModule.blockDataIndexer.indexOperationGroup(operationGroup, dbContext, indexingContext);

                const mainOperationContext = Object.freeze<MainOperationIndexingContext<TContextData>>({
                    ...indexingContext,
                    operationGroup,
                });

                for (const operation of operationGroup.operations) {
                    await this.mainOperationTraverser.traverse(indexerModule, dbContext, operation, mainOperationContext);
                }
            } catch (error) {
                throw new Error(`Failed indexing operation group with hash '${operationGroup.hash}'. ${errorToString(error)}`);
            }
        });
    }
}
