import { cyan, gray, green, red, yellow } from 'colors/safe';
import { Writable } from 'ts-essentials';

import { LogData, LogEntry, LogLevel } from '../../../src/logging/public-api';
import { formatEntryToString, formatMessage } from '../../../src/logging/winston/winston-logger-formatter';

describe('Logger formatter', () => {
    describe(formatMessage.name, () => {
        function act(message: string, data: LogData) {
            return formatMessage({ message, data } as LogEntry, d => `<d>${d}</d>`);
        }

        const noFormattingTestCases = [
            {
                desc: 'empty data',
                message: 'Hello world',
                data: {},
            },
            {
                desc: 'empty message',
                message: '',
                data: { ignored: 123 },
            },
            {
                desc: 'message with no placeholders',
                message: 'Hello world',
                data: { ignored: 123 },
            },
        ];

        for (const testCase of noFormattingTestCases) {
            it(`should return same message if ${testCase.desc}`, () => {
                const formatted = act(testCase.message, testCase.data);

                expect(formatted).toBe(testCase.message);
            });
        }

        it('should format all placedeholers correctly', () => {
            const msg = 'Hello {hero} from {city}.';
            const data = {
                hero: 'Batman',
                city: 'Gotham',
                ignored: 'wtf',
            };

            const formatted = act(msg, data);

            expect(formatted).toBe('Hello hero <d>"Batman"</d> from city <d>"Gotham"</d>.');
        });

        it.each([
            ['undefined', undefined, 'null'],
            ['null', null, 'null'],
            ['empty string', '', '""'],
            ['string', 'omg', '"omg"'],
            ['number', 123, '123'],
            ['JSON', { level: 123 }, '{"level":123}'],
            ['array', [1, 2], '[1,2]'],
        ])('should format %s correctly', (_desc, input, expected) => {
            const msg = 'Testing {value}.';
            const data = { value: input };

            const formatted = act(msg, data);

            expect(formatted).toBe(`Testing value <d>${expected}</d>.`);
        });
    });

    describe(formatEntryToString.name, () => {
        let timeStr: string;
        let entry: Writable<LogEntry>;

        beforeEach(() => {
            timeStr = '2020-11-23T11:51:16.451Z';
            entry = {
                timestamp: new Date(timeStr),
                category: 'Foo',
                level: LogLevel.Information,
                message: 'Hello {name}.',
                data: { name: 'Batman' },
                globalData: 'globalTest',
                packages: [{ packageName: 'name', version: '1.2.3', commitHash: 'hash' }],
            };
        });

        it('should build message correctly', () => {
            const formatted = formatEntryToString(entry);

            expect(formatted).toBe(`${timeStr} information [Foo] Hello name "Batman".`);
        });

        it.each([
            [LogLevel.Critical, red('critical')],
            [LogLevel.Error, red('error')],
            [LogLevel.Warning, yellow('warning')],
            [LogLevel.Information, green('information')],
            [LogLevel.Debug, gray('debug')],
            [LogLevel.Trace, gray('trace')],
        ])('should correctly colorize message with level %s', (level, expectedLevel) => {
            entry.level = level;

            const formatted = formatEntryToString(entry, { colorize: true });

            expect(formatted).toBe(`${gray(timeStr)} ${expectedLevel} [${cyan('Foo')}] Hello name ${gray('"Batman"')}.`);
        });

        it.each([
            [LogLevel.Critical, `${red('critical')} `],
            [LogLevel.Error, `${red('error')} `],
            [LogLevel.Warning, `${yellow('warning')} `],
            [LogLevel.Information, ''],
            [LogLevel.Debug, ''],
            [LogLevel.Trace, ''],
        ])('should build simple message with level %s', (level, expectedMsgPrefix) => {
            entry.level = level;

            const formatted = formatEntryToString(entry, { colorize: true, simplify: true });

            expect(formatted).toBe(`${expectedMsgPrefix}Hello name ${gray('"Batman"')}.`);
        });
    });
});
