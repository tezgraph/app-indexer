import fs from 'fs/promises';
import path from 'path';
import { singleton } from 'tsyringe';

import { FileSystem } from './file-system';
import { isObject } from '../basics/public-api';

@singleton()
export class FileSystemImpl implements FileSystem {
    async existsAsFile(filePath: string): Promise<boolean> {
        return handleErrors({
            func: async () => {
                const stats = await fs.stat(filePath);

                if (!stats.isFile()) {
                    throw new Error('Expected a file but the item at the path is of a different type.');
                }
                return true;
            },
            notFoundValue: false,
            methodName: 'existsAsFile',
            path: filePath,
        });
    }

    async readFileTextIfExists(filePath: string): Promise<string | null> {
        return handleErrors({
            func: async () => {
                const buffer = await fs.readFile(filePath);
                return buffer.toString();
            },
            notFoundValue: null,
            methodName: 'readFileTextIfExists',
            path: filePath,
        });
    }

    async writeFileText(filePath: string, contents: string): Promise<void> {
        await handleErrors({
            func: async () => {
                await ensureDirExists(path.dirname(filePath));
                await fs.writeFile(filePath, contents);
            },
            methodName: 'writeFileText',
            path: filePath,
        });
    }
}

async function ensureDirExists(dirPath: string): Promise<void> {
    await fs.mkdir(dirPath, { recursive: true });
}

async function handleErrors<T>(options: {
    func: () => Promise<T>;
    notFoundValue?: T;
    path: string;
    methodName: keyof FileSystem & string;
}): Promise<T> {
    try {
        return await options.func();
    } catch (error) {
        if (options.notFoundValue !== undefined && isObject(error) && error.code === 'ENOENT') {
            return options.notFoundValue;
        }

        // Let's wrap error b/c Node.js sometimes doesn't include the path or even it isn't Error instance.
        const details = error instanceof Error ? error.message : error as string;
        throw new Error(`Failed ${options.methodName}('${options.path}'). ${details}`);
    }
}
