import { freezeResult } from '../../src/basics/freeze-result-decorator';

class Foo {
    constructor(private readonly title: string) {}

    @freezeResult()
    syncMethod(x: string, y: string) {
        return { message: `Hello ${this.title} ${x} ${y}` };
    }

    @freezeResult()
    async asyncMethod(x: string, y: string) {
        return Promise.resolve({ message: `Welcome ${this.title} ${x} ${y}` });
    }
}

describe(freezeResult.name, () => {
    let foo: Foo;

    beforeEach(() => {
        foo = new Foo('Mr.');
    });

    it('should freeze result of sync method', () => {
        const result = foo.syncMethod('James', 'Bond');

        expect(result).toEqual({ message: 'Hello Mr. James Bond' });
        expect(result).toBeFrozen();
    });

    it('should freeze result of async method', async () => {
        const result = await foo.asyncMethod('James', 'Bond');

        expect(result).toEqual({ message: 'Welcome Mr. James Bond' });
        expect(result).toBeFrozen();
    });
});
