export class PromiseSource<T> {
    readonly promise: Promise<T>;
    resolve: (value: T) => void = undefined!;
    reject: (reason?: any) => void = undefined!;

    constructor() {
        this.promise = new Promise((resolve, reject) => {
            this.resolve = resolve;
            this.reject = reject;
        });
    }
}
