import { argGuard } from '@tezos-dappetizer/utils';

import { ActivateAccountOperation } from '../activate-account/activate-account-operation';
import { DelegationOperation } from '../delegation/delegation-operation';
import { DoubleBakingEvidenceOperation } from '../double-baking-evidence/double-baking-evidence-operation';
import { DoubleEndorsementEvidenceOperation } from '../double-endorsement-evidence/double-endorsement-evidence-operation';
import {
    DoublePreendorsementEvidenceOperation,
} from '../double-preendorsement-evidence/double-preendorsement-evidence-operation';
import { DrainDelegateOperation } from '../drain-delegate/drain-delegate-operation';
import { EndorsementOperation } from '../endorsement/endorsement-operation';
import { IncreasePaidStorageOperation } from '../increase-paid-storage/increase-paid-storage-operation';
import { MainOperation } from '../main-operation';
import { OperationKind } from '../operation-kind';
import { OriginationOperation } from '../origination/origination-operation';
import { PreendorsementOperation } from '../preendorsement/preendorsement-operation';
import { RegisterGlobalConstantOperation } from '../register-global-constant/register-global-constant-operation';
import { RevealOperation } from '../reveal/reveal-operation';
import { SeedNonceRevelationOperation } from '../seed-nonce-revelation/seed-nonce-revelation-operation';
import { SetDepositsLimitOperation } from '../set-deposits-limit/set-deposits-limit-operation';
import { SmartRollupAddMessagesOperation } from '../smart-rollup/add-messages/smart-rollup-add-messages-operation';
import { SmartRollupCementOperation } from '../smart-rollup/cement/smart-rollup-cement-operation';
import {
    SmartRollupExecuteOutboxMessageOperation,
} from '../smart-rollup/execute-outbox-message/smart-rollup-execute-outbox-message-operation';
import { SmartRollupOriginateOperation } from '../smart-rollup/originate/smart-rollup-originate-operation';
import { SmartRollupPublishOperation } from '../smart-rollup/publish/smart-rollup-publish-operation';
import { SmartRollupRecoverBondOperation } from '../smart-rollup/recover-bond/smart-rollup-recover-bond-operation';
import { SmartRollupRefuteOperation } from '../smart-rollup/refute/smart-rollup-refute-operation';
import { SmartRollupTimeoutOperation } from '../smart-rollup/timeout/smart-rollup-timeout-operation';
import { TransactionOperation } from '../transaction/transaction-operation';
import { TransferTicketOperation } from '../transfer-ticket/transfer-ticket-operation';
import { UpdateConsensusKeyOperation } from '../update-consensus-key/update-consensus-key-operation';
import { VdfRevelationOperation } from '../vdf-revelation/vdf-revelation-operation';

/**
 * A main (top-level, directly in operation groups) operation with direct balance upates which correspond to balance updates from RPC metadata.
 * @category RPC Block Data
 */
export type MainOperationWithBalanceUpdatesInMetadata =
    | ActivateAccountOperation
    | DelegationOperation
    | DoubleBakingEvidenceOperation
    // eslint-disable-next-line deprecation/deprecation
    | DoubleEndorsementEvidenceOperation
    // eslint-disable-next-line deprecation/deprecation
    | DoublePreendorsementEvidenceOperation
    | DrainDelegateOperation
    // eslint-disable-next-line deprecation/deprecation
    | EndorsementOperation
    | IncreasePaidStorageOperation
    | PreendorsementOperation
    | OriginationOperation
    | RegisterGlobalConstantOperation
    | RevealOperation
    | SeedNonceRevelationOperation
    | SetDepositsLimitOperation
    | SmartRollupAddMessagesOperation
    | SmartRollupCementOperation
    | SmartRollupExecuteOutboxMessageOperation
    | SmartRollupOriginateOperation
    | SmartRollupPublishOperation
    | SmartRollupRecoverBondOperation
    | SmartRollupRefuteOperation
    | SmartRollupTimeoutOperation
    | TransactionOperation
    | TransferTicketOperation
    | VdfRevelationOperation
    | UpdateConsensusKeyOperation;

const narrowedKinds = new Set([
    OperationKind.ActivateAccount,
    OperationKind.Delegation,
    OperationKind.DoubleBakingEvidence,
    // eslint-disable-next-line deprecation/deprecation
    OperationKind.DoubleEndorsementEvidence,
    // eslint-disable-next-line deprecation/deprecation
    OperationKind.DoublePreendorsementEvidence,
    OperationKind.DrainDelegate,
    // eslint-disable-next-line deprecation/deprecation
    OperationKind.Endorsement,
    OperationKind.IncreasePaidStorage,
    // eslint-disable-next-line deprecation/deprecation
    OperationKind.Preendorsement,
    OperationKind.Origination,
    OperationKind.RegisterGlobalConstant,
    OperationKind.Reveal,
    OperationKind.SeedNonceRevelation,
    OperationKind.SetDepositsLimit,
    OperationKind.SmartRollupAddMessages,
    OperationKind.SmartRollupCement,
    OperationKind.SmartRollupExecuteOutboxMessage,
    OperationKind.SmartRollupOriginate,
    OperationKind.SmartRollupPublish,
    OperationKind.SmartRollupRecoverBond,
    OperationKind.SmartRollupRefute,
    OperationKind.SmartRollupTimeout,
    OperationKind.Transaction,
    OperationKind.TransferTicket,
    OperationKind.VdfRevelation,
    OperationKind.UpdateConsensusKey,
]);

export function isMainOperationWithBalanceUpdatesInMetadata(operation: MainOperation): operation is MainOperationWithBalanceUpdatesInMetadata {
    argGuard.object(operation, 'operation');
    argGuard.enum(operation.kind, 'operation.kind', OperationKind);

    return narrowedKinds.has(operation.kind);
}
