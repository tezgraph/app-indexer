import { instance, mock, when } from 'ts-mockito';

import { NodeJSRequireConfigValueLoader } from '../../../src/configuration/root/nodejs-require-config-value-loader';

describe(NodeJSRequireConfigValueLoader.name, () => {
    let target: NodeJSRequireConfigValueLoader;
    let nodeJSRequire: { require: NodeJS.Require };

    beforeEach(() => {
        nodeJSRequire = mock();
        target = new NodeJSRequireConfigValueLoader(instance(nodeJSRequire));
    });

    it('should load value using require', () => {
        when(nodeJSRequire.require('dir/file.js')).thenReturn('configVal');

        const value = target.loadValue('dir/file.js');

        expect(value).toBe('configVal');
    });
});
