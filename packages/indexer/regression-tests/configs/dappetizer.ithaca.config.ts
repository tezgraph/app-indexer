import { BLOCK_COUNT, getConfig } from './dappetizer.base.config';

export default getConfig({
    protocol: 'ithaca',
    firstLevel: 2_244_609,
    lastLevel: 2_490_368,

    // Aliquot according to 245_759 blocks of this protocol vs other protocols.
    levelRangeSizeToGenerate: 2 * BLOCK_COUNT,
});
