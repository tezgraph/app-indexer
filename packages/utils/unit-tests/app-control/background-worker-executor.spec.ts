import { instance, mock, verify, when } from 'ts-mockito';

import { describeMember, expectToThrowAsync, TestLogger } from '../../../../test-utilities/mocks';
import { BackgroundWorker } from '../../src/app-control/background-worker';
import { BackgroundWorkerExecutor } from '../../src/app-control/background-worker-executor';

describe(BackgroundWorkerExecutor.name, () => {
    let target: BackgroundWorkerExecutor;
    let worker1: BackgroundWorker;
    let worker2: BackgroundWorker;
    let logger: TestLogger;

    beforeEach(() => {
        [worker1, worker2] = [mock(), mock()];
        logger = new TestLogger();
        target = new BackgroundWorkerExecutor([instance(worker1), instance(worker2)], logger);

        when(worker1.name).thenReturn('wrk-1');
        when(worker2.name).thenReturn('wrk-2');
    });

    describeMember<typeof target>('start', () => {
        it('should start all workers', async () => {
            await target.start();

            verify(worker1.start()).once();
            verify(worker2.start()).once();
            logger.verifyLoggedCount(6);
            logger.logged(0).verify('Information', { backgroundWorkers: ['wrk-1', 'wrk-2'] }).verifyMessage('Starting');
            logger.logged(1).verify('Information', { worker: 'wrk-1' }).verifyMessage('Starting');
            logger.logged(2).verify('Information', { worker: 'wrk-2' }).verifyMessage('Starting');
            logger.logged(3).verify('Information', { worker: 'wrk-1' }).verifyMessage('Started');
            logger.logged(4).verify('Information', { worker: 'wrk-2' }).verifyMessage('Started');
            logger.logged(5).verify('Information', { backgroundWorkers: ['wrk-1', 'wrk-2'] }).verifyMessage('Started');
        });

        it('should throw if worker throws', async () => {
            when(worker2.start()).thenReject(new Error('oups'));

            const error = await expectToThrowAsync(async () => target.start());

            expect(error.message).toIncludeMultiple(['Failed to start', `'wrk-2'`, 'oups']);
        });

        it('should throw if already starting', async () => {
            void target.start();

            await runThrowIfInState('Starting');
        });

        it('should throw if already started', async () => {
            await target.start();

            await runThrowIfInState('Running');
        });

        it('should throw if stopping', async () => {
            await target.start();
            void target.stop();

            await runThrowIfInState('Stopping');
        });

        async function runThrowIfInState(expectedCurrentState: string) {
            const error = await expectToThrowAsync(async () => target.start());

            expect(error.message).toIncludeMultiple(['cannot switch to Starting', `they are ${expectedCurrentState}`]);
        }
    });

    describeMember<typeof target>('stop', () => {
        it('should stop all workers', async () => {
            await target.start();
            when(worker1.stop).thenReturn(undefined);
            logger.clear();

            await target.stop();

            verify(worker2.stop!()).once();
            logger.verifyLoggedCount(5);
            logger.logged(0).verify('Information', { backgroundWorkers: ['wrk-1', 'wrk-2'] }).verifyMessage('Stopping');
            logger.logged(1).verify('Information', { worker: 'wrk-1' }).verifyMessage('No need to stop');
            logger.logged(2).verify('Information', { worker: 'wrk-2' }).verifyMessage('Stopping');
            logger.logged(3).verify('Information', { worker: 'wrk-2' }).verifyMessage('Stopped');
            logger.logged(4).verify('Information', { backgroundWorkers: ['wrk-1', 'wrk-2'] }).verifyMessage('Stopped');
        });

        it('should stop all workers if just running', async () => {
            void target.start();
            when(worker1.stop).thenReturn(undefined);
            logger.clear();

            await target.stop();

            verify(worker2.stop!()).once();
        });

        it('should log if worker throws', async () => {
            await target.start();
            const error = new Error('oups');
            when(worker1.stop!()).thenReject(error);

            await target.stop();

            verify(worker2.stop!()).once();
            logger.loggedSingle('Error').verifyData({ worker: 'wrk-1', error });
        });

        it('should throw if inactive', async () => {
            await runThrowIfInState('Inactive');
        });

        it('should throw if already stopping', async () => {
            await target.start();
            void target.stop();

            await runThrowIfInState('Stopping');
        });

        async function runThrowIfInState(expectedCurrentState: string) {
            const error = await expectToThrowAsync(async () => target.stop());

            expect(error.message).toIncludeMultiple(['cannot switch to Stopping', `they are ${expectedCurrentState}`]);
        }
    });
});
