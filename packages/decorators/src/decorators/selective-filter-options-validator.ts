/* eslint-disable @typescript-eslint/no-unnecessary-condition */
import { AddressPrefix, ArgError, argGuard, keyof } from '@tezos-dappetizer/utils';

import { ContractFilterOptions } from './contract/contract-filter-options';
import { SmartRollupFilterOptions } from './smart-rollup/smart-rollup-filter-options';

export type SelectiveIndexingOptions = ContractFilterOptions | SmartRollupFilterOptions;

export function validateSelectiveFilterOptions(
    options: SelectiveIndexingOptions,
    decorator: Function,
    requiredAddressPrefix: AddressPrefix,
): SelectiveIndexingOptions {
    const NAME = keyof<typeof options>('name');
    const ANY_OF_ADDRESSES = keyof<typeof options>('anyOfAddresses');
    const baseArgName = `@${decorator.name}.options`;

    argGuard.object(options, baseArgName);

    if (options.name !== undefined && options.anyOfAddresses !== undefined) {
        throw createOptionsError(`Both '${NAME}' and '${ANY_OF_ADDRESSES}' cannot be specified.`);
    } else if (options.name !== undefined) {
        argGuard.nonWhiteSpaceString(options.name, `${baseArgName}.${NAME}`);

        return { name: options.name }; // Copy to prevent further changes.
    } else if ((options.anyOfAddresses as unknown) !== undefined) {
        argGuard.nonEmptyArray(options.anyOfAddresses, `${baseArgName}.${ANY_OF_ADDRESSES}`)
            .forEach((a, i) => argGuard.address(a, `${baseArgName}.${ANY_OF_ADDRESSES}[${i}]`, [requiredAddressPrefix]));

        return { anyOfAddresses: [...options.anyOfAddresses] }; // Copy to prevent further changes.
    }
    throw createOptionsError(`Either '${NAME}' or '${ANY_OF_ADDRESSES}' must be specified.`);

    function createOptionsError(details: string): Error {
        return new ArgError({ argName: baseArgName, specifiedValue: options, details });
    }
}
