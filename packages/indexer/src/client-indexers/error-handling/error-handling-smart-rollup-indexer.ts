import { KeyOfType, RecordToDump } from '@tezos-dappetizer/utils';
import { lowerFirst } from 'lodash';
import { AsyncOrSync } from 'ts-essentials';

import { ClientIndexerErrorHandler, ErrorHandlingOptions } from './client-indexer-error-handler';
import { shouldIndexBasedOnData } from '../../helpers/should-index-evaluator';
import { Applied, EntityToIndex, SmartRollupAddMessagesOperation } from '../../rpc-data/rpc-data-interfaces';
import { SmartRollup } from '../../smart-rollups/smart-rollup';
import { SmartRollupAddMessagesIndexingContext, SmartRollupIndexer } from '../smart-rollup-indexer';
import { IndexerInfo } from '../wrappers/indexer-info';
import { TrustedSmartRollupIndexer } from '../wrappers/trusted-indexer-interfaces';

export function createErrorHandlingSmartRollupIndexer<TDbContext, TContextData, TRollupData>(
    indexer: SmartRollupIndexer<TDbContext, TContextData, TRollupData>,
    info: IndexerInfo,
    errorHandler: ClientIndexerErrorHandler,
): TrustedSmartRollupIndexer<TDbContext, TContextData, TRollupData> {
    return {
        info,
        shouldIndex: errorHandler.wrap(indexer, 'shouldIndex', info, new ShouldIndexOptions()),
        indexAddMessages: errorHandler.wrap(indexer, 'indexAddMessages', info, new IndexAddMessagesOptions()),
        indexCement: wrapIndexMethod('indexCement'),
        indexExecuteOutboxMessage: wrapIndexMethod('indexExecuteOutboxMessage'),
        indexOriginate: wrapIndexMethod('indexOriginate'),
        indexPublish: wrapIndexMethod('indexPublish'),
        indexRecoverBond: wrapIndexMethod('indexRecoverBond'),
        indexRefute: wrapIndexMethod('indexRefute'),
        indexTimeout: wrapIndexMethod('indexTimeout'),
    };

    type IndexMethod<TOperation, TIndexingContext> = undefined | ((o: TOperation, d: TDbContext, c: TIndexingContext) => AsyncOrSync<void>);

    function wrapIndexMethod<TOperation extends EntityToIndex, TIndexingContext extends { readonly rollup: SmartRollup }>(
        methodName: KeyOfType<typeof indexer, IndexMethod<TOperation, TIndexingContext>> & `index${string}`,
    ): (e: TOperation, d: TDbContext, c: TIndexingContext) => Promise<void> {
        return errorHandler.wrap(indexer, methodName, info, new IndexMethodOptions(methodName));
    }
}

export class IndexMethodOptions<TOperation extends EntityToIndex, TDbContext, TIndexingContext extends { readonly rollup: SmartRollup }>
implements ErrorHandlingOptions<[TOperation, TDbContext, TIndexingContext]> {
    constructor(readonly methodName: `index${string}`) {}

    getArgsToReport(operation: TOperation, _dbContext: TDbContext, indexingContext: TIndexingContext): RecordToDump {
        return {
            rollup: indexingContext.rollup.address,
            uid: operation.uid,
        };
    }

    getArgsToTrace(operation: TOperation, _dbContext: TDbContext, indexingContext: TIndexingContext): object {
        const operationName = `${lowerFirst(this.methodName.substring('index'.length))}Operation`;
        return { [operationName]: operation, indexingContext };
    }
}

export class IndexAddMessagesOptions<TDbContext, TContextData>
implements ErrorHandlingOptions<[Applied<SmartRollupAddMessagesOperation>, TDbContext, SmartRollupAddMessagesIndexingContext<TContextData>]> {
    getArgsToReport(
        operation: Applied<SmartRollupAddMessagesOperation>,
        _dbContext: TDbContext,
        _indexingContext: SmartRollupAddMessagesIndexingContext<TContextData>,
    ): RecordToDump {
        return { uid: operation.uid };
    }

    getArgsToTrace(
        addMessagesOperation: Applied<SmartRollupAddMessagesOperation>,
        _dbContext: TDbContext,
        indexingContext: SmartRollupAddMessagesIndexingContext<TContextData>,
    ): object {
        return { addMessagesOperation, indexingContext };
    }
}

export class ShouldIndexOptions<TDbContext, TRollupData> implements ErrorHandlingOptions<[SmartRollup, TDbContext], TRollupData | false> {
    readonly ifMethodNotDefined = {
        returnValue: undefined as TRollupData,
        logMessage: 'Therefore the rollup will be indexed and its associated data will be undefined for the indexer.',
    } as const;

    readonly afterExecuted = {
        logMessage: 'As a result, the indexer {willIndex} the contract.',
        logMessageArgs: (rollupData: TRollupData | false) => ({ willIndex: shouldIndexBasedOnData(rollupData) }),
    };

    getArgsToReport(rollup: SmartRollup, _dbContext: TDbContext): RecordToDump {
        return { rollup: rollup.address };
    }

    getArgsToTrace(rollup: SmartRollup, _dbContext: TDbContext): object {
        return { rollup };
    }
}
