import { instance, mock, when } from 'ts-mockito';

import { BalanceUpdateConverter } from '../../../../src/rpc-data/common/balance-update/converters/balance-update-converter';
import {
    ActivateAccountOperationConverter,
} from '../../../../src/rpc-data/operations/activate-account/activate-account-operation-converter';
import {
    BalanceUpdate,
    ActivateAccountOperation,
    OperationKind,
    TaquitoRpcActivateAccountOperation,
} from '../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../rpc-test-helpers';

describe(ActivateAccountOperationConverter.name, () => {
    let target: ActivateAccountOperationConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;

    let rpcData: TaquitoRpcActivateAccountOperation;
    let balanceUpdates: BalanceUpdate[];

    const act = () => convertObject(target, rpcData, 'op/id');

    beforeEach(() => {
        balanceUpdateConverter = mock();
        target = new ActivateAccountOperationConverter(instance(balanceUpdateConverter));

        rpcData = {
            pkh: 'tz1LT4eT7Xs7MW3rftqEj4XVfMAdj4wBnVbP',
            secret: '9af9acb082590bce41b3421af299ce941a40fac5',
        } as typeof rpcData;
        balanceUpdates = 'mockedBalanceUpdates' as any;

        when(balanceUpdateConverter.convertFromMetadataProperty(rpcData, 'op/id')).thenReturn(balanceUpdates);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<ActivateAccountOperation>({
            balanceUpdates,
            kind: OperationKind.ActivateAccount,
            pkh: 'tz1LT4eT7Xs7MW3rftqEj4XVfMAdj4wBnVbP',
            rpcData,
            secret: '9af9acb082590bce41b3421af299ce941a40fac5',
            uid: 'op/id',
        });
        expect(operation).toBeFrozen();
    });
});
