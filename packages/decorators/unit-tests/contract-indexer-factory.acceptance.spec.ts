import {
    contractFilter,
    createContractIndexerFromDecorators,
    indexBigMapDiff,
    indexBigMapUpdate,
    indexEntrypoint,
    indexEvent,
    indexOrigination,
    indexStorageChange,
    indexTransaction,
    shouldIndex,
} from '@tezos-dappetizer/decorators';
import {
    BigMapDiff,
    BigMapInfo,
    BigMapUpdate,
    ContractEvent,
    ContractIndexer,
    ContractIndexingContext,
    ContractOrigination,
    LazyContract,
    StorageChange,
    TransactionParameter,
} from '@tezos-dappetizer/indexer';
import { DeepWritable, Writable } from 'ts-essentials';

import { describeMemberFactory } from '../../../test-utilities/mocks';

@contractFilter({ name: 'MyContract' })
class MyIndexer {
    readonly name = 'MyExplicitName';

    constructor(private readonly executed: string[]) {}

    @shouldIndex()
    async myShouldIndex(contract: LazyContract, db: string): Promise<false | string> {
        return Promise.resolve(`contractData[${contract.address}, ${db}]`);
    }

    @indexBigMapDiff({ name: 'ledger' })
    async myIndexBigMapDiff(diff: BigMapDiff, db: string, ctx: ContractIndexingContext<string>): Promise<void> {
        await Promise.resolve();
        this.executed.push(`indexBigMapDiff(${diff.uid}, ${db}, ${ctx.data})`);
    }

    @indexBigMapUpdate({ path: ['assets', 'owners'] })
    myIndexBigMapUpdate(key: string, value: string, db: string, ctx: ContractIndexingContext<string>): void {
        this.executed.push(`indexBigMapUpdate(${key}, ${value}, ${db}, ${ctx.data})`);
    }

    @indexEntrypoint('transfer')
    async myIndexEntrypoint(param: string, db: string, ctx: ContractIndexingContext<string>): Promise<void> {
        await Promise.resolve();
        this.executed.push(`indexEntrypoint(${param}, ${db}, ${ctx.data})`);
    }

    @indexEvent('on_transferred')
    myIndexEvent(payload: string, db: string, ctx: ContractIndexingContext<string>): void {
        this.executed.push(`indexEvent(${payload}, ${db}, ${ctx.data})`);
    }

    @indexOrigination()
    async myIndexOrigination(storage: string, db: string, ctx: ContractIndexingContext<string>): Promise<void> {
        await Promise.resolve();
        this.executed.push(`indexOrigination(${storage}, ${db}, ${ctx.data})`);
    }

    @indexStorageChange()
    async myIndexStorageChange(storage: string, db: string, ctx: ContractIndexingContext<string>): Promise<void> {
        await Promise.resolve();
        this.executed.push(`indexStorageChange(${storage}, ${db}, ${ctx.data})`);
    }

    @indexTransaction()
    myIndexTransaction(param: { entrypoint: string; value: string }, db: string, ctx: ContractIndexingContext<string>): void {
        this.executed.push(`indexTransaction(${param.entrypoint}, ${param.value}, ${db}, ${ctx.data})`);
    }
}

describe('ContractIndexer acceptance tests', () => {
    let wrappedIndexer: ContractIndexer<string, string, string>;
    let executed: string[];
    let context: any;

    beforeEach(() => {
        executed = [];
        const decoratedIndexer = new MyIndexer(executed);
        wrappedIndexer = createContractIndexerFromDecorators(decoratedIndexer);

        context = { data: 'myCtx' } as ContractIndexingContext<string, string>;
    });

    const describeMember = describeMemberFactory<typeof wrappedIndexer>();

    describeMember('name', () => {
        it(`should be read from 'name' property`, () => {
            expect(wrappedIndexer.name).toBe('MyExplicitName');
        });
    });

    describeMember('shouldIndex', () => {
        let contract: DeepWritable<LazyContract>;

        const act = async () => wrappedIndexer.shouldIndex!(contract, 'myDb');

        beforeEach(() => {
            contract = {
                address: 'KT1MFfBe4EtzDofUcfeBmNShJPesY4HhfjMC',
                config: { name: 'MyContract' },
            } as LazyContract;
        });

        it('should delegate to decorated method correctly', async () => {
            const data = await act();

            expect(data).toBe('contractData[KT1MFfBe4EtzDofUcfeBmNShJPesY4HhfjMC, myDb]');
        });

        it('should not delegate if class filter not passed', async () => {
            contract.config.name = 'WtfContract';

            const data = await act();

            expect(data).toBeFalse();
        });
    });

    describeMember('indexBigMapDiff', () => {
        let diff: Writable<BigMapDiff>;

        const act = async () => wrappedIndexer.indexBigMapDiff!(diff, 'myDb', context);

        beforeEach(() => {
            diff = {
                sourceBigMap: mockBigMap(['data', 'items']),
                destinationBigMap: mockBigMap(['tzip', 'ledger']),
                uid: 'diffUid',
            } as typeof diff;
        });

        it('should delegate to decorated method correctly', async () => {
            await runExpectingExecuted(act, ['indexBigMapDiff(diffUid, myDb, myCtx)']);
        });

        it('should not delegate if method filter not passed', async () => {
            diff.destinationBigMap = mockBigMap(['tzip', 'ledgerX']);

            await runExpectingExecuted(act, []);
        });
    });

    describeMember('indexBigMapUpdate', () => {
        let update: Writable<BigMapUpdate>;

        const act = async () => wrappedIndexer.indexBigMapUpdate!(update, 'myDb', context);

        beforeEach(() => {
            update = {
                bigMap: mockBigMap(['assets', 'owners']),
                key: { convert: () => 'convKey' },
                value: { convert: () => 'convValue' },
            } as typeof update;
        });

        it('should delegate to decorated method correctly', async () => {
            await runExpectingExecuted(act, ['indexBigMapUpdate(convKey, convValue, myDb, myCtx)']);
        });

        it('should not delegate if method filter not passed', async () => {
            update.bigMap = mockBigMap(['assets2', 'owners']);

            await runExpectingExecuted(act, []);
        });
    });

    describeMember('indexEvent', () => {
        let event: Writable<ContractEvent>;

        const act = async () => wrappedIndexer.indexEvent!(event, 'myDb', context);

        beforeEach(() => {
            event = {
                tag: 'on_transferred',
                payload: { convert: () => 'convPayload' },
            } as typeof event;
        });

        it('should delegate to decorated method correctly', async () => {
            await runExpectingExecuted(act, ['indexEvent(convPayload, myDb, myCtx)']);
        });

        it('should not delegate if method filter not passed', async () => {
            event.tag = 'on_deposited';

            await runExpectingExecuted(act, []);
        });
    });

    describeMember('indexOrigination', () => {
        it('should delegate to decorated method correctly', async () => {
            const origination = {
                result: {
                    getInitialStorage: async () => Promise.resolve({
                        convert: () => 'convStorage',
                    }),
                },
            } as ContractOrigination;

            await runExpectingExecuted(
                async () => wrappedIndexer.indexOrigination!(origination, 'myDb', context),
                ['indexOrigination(convStorage, myDb, myCtx)'],
            );
        });
    });

    describeMember('indexStorageChange', () => {
        it('should delegate to decorated method correctly', async () => {
            const storageChange = {
                newValue: { convert: () => 'convStorage' },
            } as StorageChange;

            await runExpectingExecuted(
                async () => wrappedIndexer.indexStorageChange!(storageChange, 'myDb', context),
                ['indexStorageChange(convStorage, myDb, myCtx)'],
            );
        });
    });

    describeMember('indexEvent', () => {
        let transaction: Writable<TransactionParameter>;

        const act = async () => wrappedIndexer.indexTransaction!(transaction, 'myDb', context);

        beforeEach(() => {
            transaction = {
                entrypoint: 'transfer',
                value: { convert: () => 'convValue' },
            } as typeof transaction;
        });

        it('should delegate to decorated method correctly', async () => {
            await runExpectingExecuted(act, [
                'indexTransaction(transfer, convValue, myDb, myCtx)',
                'indexEntrypoint(convValue, myDb, myCtx)',
            ]);
        });

        it('should not delegate if method filter not passed', async () => {
            transaction.entrypoint = 'deposit';

            await runExpectingExecuted(act, ['indexTransaction(deposit, convValue, myDb, myCtx)']);
        });
    });

    function mockBigMap(path: readonly string[]) {
        return { path, name: path[path.length - 1]! } as BigMapInfo;
    }

    async function runExpectingExecuted(act: () => Promise<void>, expected: string[]) {
        await act();

        expect(executed).toEqual(expected);
    }
});
