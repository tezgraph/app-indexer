/* eslint-disable no-param-reassign */
import { AsyncIterableProvider, injectExplicit, injectLogger, Logger } from '@tezos-dappetizer/utils';
import { injectable } from 'tsyringe';

import { BoostedLevels, ContractIndexingBoost } from './contract-indexing-boost';
import { ContractIndexingBoostFactory } from './contract-indexing-boost-factory';

@injectable()
export class ContractBoostedBlockProvider implements AsyncIterableProvider<number> {
    constructor(
        @injectExplicit() private readonly nextProvider: AsyncIterableProvider<number>,
        private readonly boostFactory: ContractIndexingBoostFactory,
        @injectLogger(ContractBoostedBlockProvider) private readonly logger: Logger,
    ) {}

    iterate(): AsyncIterableIterator<number> {
        const boost = this.boostFactory.create();
        return boost
            ? this.iterateBoosted(boost)
            : this.nextProvider.iterate();
    }

    private async *iterateBoosted(boost: ContractIndexingBoost): AsyncIterableIterator<number> {
        this.logger.logInformation('Using configured {boost} for contract indexing.', { boost: boost.name });
        let levels: BoostedLevels | 'onHead' = { hasMore: true, lastLoadedLevel: -1, levelsWithContracts: new Set() };

        for await (const level of this.nextProvider.iterate()) {
            const commonLogData = { boost: boost.name, level };

            while (levels !== 'onHead' && level > levels.lastLoadedLevel && levels.hasMore) {
                this.logger.logDebug('Loading more data for contract indexing {boost}'
                    + ' to evaluate block {level} vs. {lastLoadedBoostLevel}. It may take longer, depending on level difference.', {
                    ...commonLogData,
                    lastLoadedBoostLevel: levels.lastLoadedLevel,
                });

                try {
                    levels = await boost.getLevelsWithContracts({ fromBlockLevel: level });
                } catch (error) {
                    this.logger.logWarning('Failed {boost} of contract indexing. So indexing all blocks from {level}. {error}', {
                        ...commonLogData,
                        error,
                    });
                    levels = 'onHead';
                }
            }

            if (levels !== 'onHead' && level > levels.lastLoadedLevel) {
                this.logger.logInformation('Indexing all blocks from {level} because {boost} with {lastBoostLevel} does not help anymore.', {
                    ...commonLogData,
                    lastBoostLevel: levels.lastLoadedLevel,
                });
                levels = 'onHead';
            }

            if (levels === 'onHead') {
                yield level;
            } else if (levels.levelsWithContracts.has(level)) {
                this.logger.logDebug('Indexing will jump to block {level} according to configured {boost} for contracts.', commonLogData);
                yield level;
            }
        }
    }
}
