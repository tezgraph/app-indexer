import { isWhiteSpace } from '@tezos-dappetizer/utils';
import { last } from 'lodash';
import { EOL } from 'os';

export class CodeStringBuilder {
    private readonly lineList: string[] = [];
    private indentTabs = 0;

    get lines(): readonly string[] {
        return this.lineList;
    }

    static join(...lines: readonly (readonly string[])[]): string {
        return lines.flatMap(l => l).join(EOL);
    }

    getFinalCode(): string {
        return CodeStringBuilder.join(this.lineList);
    }

    addLine(codeLine: string): this {
        if (isWhiteSpace(codeLine)) {
            this.lineList.push('');
        } else {
            const indentPrefix = ' '.repeat(this.indentTabs * 4);
            this.lineList.push(`${indentPrefix}${codeLine}`);
        }
        return this;
    }

    addLines(codeLines: readonly string[]): this {
        codeLines.forEach(l => this.addLine(l));
        return this;
    }

    addEmptyLine(): this {
        return this.addLine('');
    }

    indent(): this {
        this.indentTabs++;
        return this;
    }

    unindent(): this {
        this.indentTabs = Math.max(0, this.indentTabs - 1);
        return this;
    }

    trimEnd(): this {
        while (last(this.lineList) === '') {
            this.lineList.pop();
        }
        return this;
    }
}
