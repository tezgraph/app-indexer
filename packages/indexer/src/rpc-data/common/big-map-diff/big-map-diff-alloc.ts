import { BaseBigMapDiff, BigMapDiffAction } from './base-big-map-diff';
import { BigMapInfo } from '../../../contracts/data/big-map-info/big-map-info';
import { MichelsonSchema } from '../../../michelson/michelson-schema';

/**
 * Corresponds to RPC schema (`updates` correspond to {@link BigMapUpdate}-s):
 * ```
 * { // big_map
 *      "kind": "big_map",
 *      "id": $008-PtEdo2Zk.big_map_id,
 *      "diff": { // update
 *          "action": "update",
 *          "updates":
 *              [ { "key_hash": $script_expr,
 *                  "key": $micheline.008-PtEdo2Zk.michelson_v1.expression,
 *                  "value"?: $micheline.008-PtEdo2Zk.michelson_v1.expression } ... ],
 *          "key_type": $micheline.008-PtEdo2Zk.michelson_v1.expression,
 *          "value_type": $micheline.008-PtEdo2Zk.michelson_v1.expression } }
 * ```
 *
 * and until *Delphi* protocol (including) to RPC schema:
 * ```
 * { // alloc
 *      "action": "alloc",
 *      "big_map": $bignum,
 *      "key_type": $micheline.007-PsDELPH1.michelson_v1.expression,
 *      "value_type": $micheline.007-PsDELPH1.michelson_v1.expression }
 * ```
 * @category RPC Block Data
 */
export interface BigMapAlloc extends BaseBigMapDiff {
    // Narrowing properties:

    /** Usable for narrowing the type of this object from {@link BigMapDiff}. */
    readonly action: BigMapDiffAction.Alloc;

    // Specific properties:

    /** It is `null` if the corresponding ID is negative - the big map existed only within the block as temporary. */
    readonly bigMap: BigMapInfo | null;

    readonly keyType: MichelsonSchema;

    readonly valueType: MichelsonSchema;

    // Null properties:

    /** Always `null` for this type so that the property is directly available on {@link BigMapDiff} union. */
    readonly destinationBigMap: null;

    /** Always `null` for this type so that the property is directly available on {@link BigMapDiff} union. */
    readonly key: null;

    /** Always `null` for this type so that the property is directly available on {@link BigMapDiff} union. */
    readonly keyHash: null;

    /** Always `null` for this type so that the property is directly available on {@link BigMapDiff} union. */
    readonly sourceBigMap: null;

    /** Always `null` for this type so that the property is directly available on {@link BigMapDiff} union. */
    readonly value: null;
}
