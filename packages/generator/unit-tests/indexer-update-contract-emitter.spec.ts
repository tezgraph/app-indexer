import { deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { IndexerClassInterfacesGenerator } from '../src/file-generators/contract-indexer/indexer-class-interfaces-generator';
import { FileEmitter } from '../src/file-generators/file-emitter';
import { DappetizerNetworkConfigGenerator } from '../src/file-generators/structural/dappetizer-network-config-generator';
import { ContractDetails, ContractDetailsResolver } from '../src/helpers/contract-details-resolver';
import { UpdateContractIndexerClassOptions } from '../src/indexer-emitter';
import { IndexerUpdateContractEmitter } from '../src/indexer-update-contract-emitter';

describe(IndexerUpdateContractEmitter.name, () => {
    let target: IndexerUpdateContractEmitter;
    let fileEmitter: FileEmitter;
    let contractDetailsResolver: ContractDetailsResolver;
    let indexerClassInterfacesGenerator: IndexerClassInterfacesGenerator;
    let dappetizeContractsConfigGenerator: DappetizerNetworkConfigGenerator;

    let contractDetails: ContractDetails;

    beforeEach(() => {
        [fileEmitter, contractDetailsResolver] = [mock(), mock()];
        indexerClassInterfacesGenerator = 'mockedIndexerClassInterfacesGenerator' as any;
        dappetizeContractsConfigGenerator = 'mockedDappetizerContractsConfigGenerator' as any;
        target = new IndexerUpdateContractEmitter(
            instance(fileEmitter),
            instance(contractDetailsResolver),
            indexerClassInterfacesGenerator,
            dappetizeContractsConfigGenerator,
        );

        contractDetails = 'mockedContractDetails' as any;
        when(contractDetailsResolver.resolve('KT1Ezht4PDKZri7aVppVGT4Jkw39sesaFnww', 'Foo Contract')).thenResolve(contractDetails);
    });

    it('should generate indexer class for a contract', async () => {
        const options = {
            outDirPath: './out/dir',
            contractAddress: 'KT1Ezht4PDKZri7aVppVGT4Jkw39sesaFnww',
            contractName: 'Foo Contract',
        } as UpdateContractIndexerClassOptions;

        await target.updateContractIndexerClass(options);

        verify(fileEmitter.emitFiles(
            './out/dir',
            deepEqual({ ...options, contractDetails }),
            deepEqual([indexerClassInterfacesGenerator, dappetizeContractsConfigGenerator]),
        )).once();
    });
});
