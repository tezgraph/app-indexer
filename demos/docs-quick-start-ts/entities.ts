import { commonDbColumns } from '@tezos-dappetizer/database';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Registration {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    domain!: string;

    @Column()
    owner!: string;

    @Column()
    duration!: number;

    @Column(commonDbColumns.blockHash)
    block!: string;
}
