import { generatorDefaults } from '@tezos-dappetizer/generator';
import { BlockRange, DappetizerConfig, ExplicitBlocksToIndex } from '@tezos-dappetizer/indexer';
import {
    addressValidator,
    candidateConfigFileNames,
    CLI_SCRIPT,
    cliCommands,
    cliOptions,
    dappetizerAssert,
    dumpToString,
    errorToString,
    fullCliOption,
    hasLength,
    isNullish,
    isWhiteSpace,
    keyof,
    keysOf,
    MAINNET,
    NonNullish,
} from '@tezos-dappetizer/utils';
import { NonEmptyArray } from 'ts-essentials';
import { URL } from 'url';
import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';

import { CommonGenerateArgs, ConsoleArgs, ConsoleCommand } from './console-args';

export function parseConsoleArgs(processArgv: readonly string[]): ConsoleArgs {
    const parsedArgv = parser.parse(hideBin([...processArgv]));
    const consoleArgs = getConsoleArgs(parsedArgv);

    if (isNullish(consoleArgs)) {
        throw new Error(`Failed to parse a supported command from: ${processArgv.join(' ')}`);
    }
    return consoleArgs;
}

const parser = yargs
    .scriptName(CLI_SCRIPT)
    .command(
        `${cliCommands.INIT} [${cliOptions.CONTRACT_ADDRESS}]`,
        'Generates a new Dappetizer app using TypeScript, optionally with code for indexing a given contract.',
        builder => addCommonGeneratorOptions(builder)
            .positional(cliOptions.CONTRACT_ADDRESS, {
                description: 'The address of the contract for which indexer code should be generated.',
                type: 'string',
            })
            .check(argv => checkOption(argv, cliOptions.CONTRACT_ADDRESS, validateContractAddress))
            .option(cliOptions.CONTRACT_NAME, {
                description: 'The name of the contract used for naming generated code.'
                    + ` If not specified, then the name from TZIP-16 metadata is used, then the '${generatorDefaults.CONTRACT_NAME}' constant.`,
                type: 'string',
                implies: cliOptions.CONTRACT_ADDRESS,
            })
            .check(argv => checkOption(argv, cliOptions.CONTRACT_NAME, validateNotWhiteSpace))
            .option(cliOptions.NPM_INSTALL, {
                description: 'Instructs the generator to (not) install dependent Node.js modules.',
                type: 'boolean',
                default: true,
            })
            .option(cliOptions.INDEXER_MODULE, {
                description: 'The name of the indexer module to generate.'
                    + ` If not specified, then the 'name' from 'package.json' is used, then the '${generatorDefaults.INDEXER_MODULE_NAME}' constant.`,
                type: 'string',
            })
            .check(argv => checkOption(argv, cliOptions.INDEXER_MODULE, validateNotWhiteSpace))
            .option(cliOptions.USAGE_STATISTICS, {
                description: 'Help improve Dappetizer by sharing anonymous usage statistics?'
                    + ` Check ${generatorDefaults.USAGE_STATISTICS_DOCS_URL} for information about later opt-out.`,
                type: 'boolean',
                default: true,
            }),
        argv => setConsoleArgs(argv, {
            command: ConsoleCommand.GenerateInitialApp,
            contractAddress: argv[cliOptions.CONTRACT_ADDRESS] ?? null,
            contractName: argv[cliOptions.CONTRACT_NAME] ?? null,
            enableUsageStatistics: argv[cliOptions.USAGE_STATISTICS],
            installNpmDependencies: argv[cliOptions.NPM_INSTALL],
            indexerModuleName: argv[cliOptions.INDEXER_MODULE] ?? null,
            outDirPath: argv[cliOptions.OUT_DIR],
            ...determineTezosArgs(argv),
        }),
    )
    .command(
        `${cliCommands.START} [${cliOptions.CONFIG_FILE}]`,
        'Starts indexing according to the config.',
        builder => builder
            .positional(cliOptions.CONFIG_FILE, {
                description: `The path to TypeScript, JavaScript or JSON configuration file.`
                    + ` If not specified then first existing of these files is used: ${dumpToString(candidateConfigFileNames)}`,
                type: 'string',
            })
            .check(argv => checkOption(argv, cliOptions.CONFIG_FILE, validateNotWhiteSpace))
            .option(cliOptions.NETWORK, {
                description: `The Tezos network that should be indexed.`
                    + ` It is used to select respective part of '${keyof<DappetizerConfig>('networks')}' Dappetizer configuration.`,
                type: 'string',
                default: MAINNET,
            })
            .check(argv => checkOption(argv, cliOptions.NETWORK, validateNotWhiteSpace))
            .option(cliOptions.BLOCKS, {
                description: `The list of block levels or level ranges to be re-indexed explicitly`
                    + ` instead of regular indexing based on Dappetizer configuration.`
                    + ` It is useful to repair some bug from the past.`
                    + ` Example: 5,7-13,18`,
                type: 'string',
            })
            .check(argv => checkOption(argv, cliOptions.BLOCKS, validateExplicitBlocks)),
        argv => setConsoleArgs(argv, {
            command: ConsoleCommand.StartIndexing,
            configFilePath: argv[cliOptions.CONFIG_FILE] ?? null,
            tezosNetwork: argv[cliOptions.NETWORK],
            blocks: argv.blocks ? determineExplicitBlocks(argv.blocks) : null,
        }),
    )
    .command(
        `${cliCommands.UPDATE} <${cliOptions.CONTRACT_NAME}> <${cliOptions.CONTRACT_ADDRESS}>`,
        'Updates (re-generates) already generated contract indexer class corresponding to the specified name'
        + ' to be according to the schema of the specified contract address.',
        builder => addCommonGeneratorOptions(builder)
            .positional(cliOptions.CONTRACT_NAME, {
                description: 'The name of the contract used for naming already generated indexer code.'
                    + ` For example 'FooBar' will update the file 'src/foo-bar-indexer-base.generated.ts' within '${fullCliOption('OUT_DIR')}'.`,
                type: 'string',
                demandOption: true,
            })
            .check(argv => checkOption(argv, cliOptions.CONTRACT_NAME, validateNotWhiteSpace))
            .positional(cliOptions.CONTRACT_ADDRESS, {
                description: 'The address of the contract for which indexer code should be generated.',
                type: 'string',
                demandOption: true,
            })
            .check(argv => checkOption(argv, cliOptions.CONTRACT_ADDRESS, validateContractAddress)),
        argv => setConsoleArgs(argv, {
            command: ConsoleCommand.UpdateContractIndexerClass,
            contractAddress: argv[cliOptions.CONTRACT_ADDRESS],
            contractName: argv[cliOptions.CONTRACT_NAME], // Yargs sets aliased options to the same value.
            outDirPath: argv[cliOptions.OUT_DIR],
            ...determineTezosArgs(argv),
        }),
    )
    .strict()
    .demandCommand()
    .help();

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
function addCommonGeneratorOptions<T>(builder: yargs.Argv<T>) {
    return builder
        .option(cliOptions.NETWORK, {
            description: `The Tezos network where the contract resides.`
                + ` It is used to determine '${fullCliOption('TEZOS_ENDPOINT')}' if that is not specified.`
                + ` The default is '${MAINNET}'.`,
            // The 'default' property replaces empty string e.g. --network='  ' -> confusing -> handle the default fallback manually.
            type: 'string',
        })
        .check(argv => checkOption(argv, cliOptions.NETWORK, validateNotWhiteSpace))
        .option(cliOptions.TEZOS_ENDPOINT, {
            description: 'The URL of the Tezos Node to be used to fetch contract details.'
                + ` If not specified, then it is determined from '${fullCliOption('NETWORK')}' option like this:`
                + ` ${Object.entries(generatorDefaults.tezosNodeUrlsByNetwork)
                    .map(([network, url]) => `'${url}' for '${fullCliOption('NETWORK', network)}'`).join(', ')}`,
            type: 'string',
        })
        .check(argv => checkOption(argv, cliOptions.TEZOS_ENDPOINT, validateAbsUrl))
        .option(cliOptions.OUT_DIR, {
            description: 'The relative or absolute path of the directory where the indexer code should be generated.',
            type: 'string',
            default: generatorDefaults.OUT_DIR,
        })
        .check(argv => checkOption(argv, cliOptions.OUT_DIR, validateNotWhiteSpace))
        .check(validateTezosArgs);
}

const tezosNodeUrls: Record<string, string> = generatorDefaults.tezosNodeUrlsByNetwork;

interface TezosArgv {
    [cliOptions.TEZOS_ENDPOINT]?: string;
    [cliOptions.NETWORK]?: string;
}

// Needs to be inside check() so that yargs calls process.exit().
function validateTezosArgs(argv: TezosArgv): boolean {
    if (!argv[cliOptions.TEZOS_ENDPOINT] && argv.network && !(argv.network in tezosNodeUrls)) {
        throw new Error(`There is no built-in Tezos Node URL for the specified '${fullCliOption('NETWORK', argv.network)}'.`
            + ` Either specify '${fullCliOption('TEZOS_ENDPOINT')}' explicitly,`
            + ` or pick one of built-in networks: ${keysOf(tezosNodeUrls).map(n => `'${n}'`).join(', ')}.`);
    }
    return true;
}

function determineTezosArgs(argv: TezosArgv): Pick<CommonGenerateArgs, 'tezosNetwork' | 'tezosNodeUrl' | 'isTezosNodeUrlExplicit'> {
    const tezosNetwork = argv[cliOptions.NETWORK] ?? MAINNET;
    let tezosNodeUrl = argv[cliOptions.TEZOS_ENDPOINT];
    let isTezosNodeUrlExplicit = true;

    if (!tezosNodeUrl) {
        tezosNodeUrl = tezosNodeUrls[tezosNetwork];
        isTezosNodeUrlExplicit = false;
        dappetizerAssert(!isNullish(tezosNodeUrl), `Should be handled by ${validateTezosArgs.name}().`, { argv, tezosNodeUrls });
    }
    return { tezosNodeUrl, isTezosNodeUrlExplicit, tezosNetwork };
}

function checkOption<TOptionName extends string, TOptionValue extends NonNullish>(
    argv: { [P in TOptionName]: TOptionValue | undefined },
    optionName: TOptionName,
    validate: (value: TOptionValue) => string | null,
): boolean {
    const optionValue: TOptionValue | undefined = argv[optionName];
    if (isNullish(optionValue)) {
        return true; // Option isn't specified.
    }

    const invalidReason = validate(optionValue);
    if (invalidReason) {
        throw new Error(`Specified value ${JSON.stringify(optionValue)} for '--${optionName}' ${invalidReason}.`);
    }
    return true;
}

function validateNotWhiteSpace(value: string): string | null {
    return isWhiteSpace(value) ? 'cannot be empty nor white-space' : null;
}

function validateContractAddress(value: string): string | null {
    try {
        addressValidator.validate(value, ['KT1']);
        return null;
    } catch {
        return 'must be a valid KT1 contract address';
    }
}

function validateAbsUrl(value: string): string | null {
    try {
        new URL(value); // eslint-disable-line no-new
        return null;
    } catch {
        return 'must be an absolute URL';
    }
}

function determineExplicitBlocks(value: string): ExplicitBlocksToIndex {
    return value.split(',').map<BlockRange>(rangeStr => {
        const levelStrs = rangeStr.split('-');

        if (hasLength(levelStrs, 1)) {
            const level = parseBlockLevel(levelStrs[0]);
            return { fromLevel: level, toLevel: level };
        }
        if (hasLength(levelStrs, 2)) {
            return {
                fromLevel: parseBlockLevel(levelStrs[0]),
                toLevel: parseBlockLevel(levelStrs[1]),
            };
        }
        throw new Error(`contains invalid block level or level range '${rangeStr}'.`);
    }) as NonEmptyArray<BlockRange>;
}

function parseBlockLevel(str: string): number {
    const level = Number(str);
    if (isWhiteSpace(str) || isNaN(level)) {
        throw new Error(`contains invalid block level '${str}'.`);
    }
    return level;
}

function validateExplicitBlocks(value: string): string | null {
    try {
        determineExplicitBlocks(value);
        return null;
    } catch (error) {
        return errorToString(error, { onlyMessage: true });
    }
}

function setConsoleArgs(argv: yargs.Arguments<unknown>, args: ConsoleArgs): void {
    argv.consoleArgs = args;
}

function getConsoleArgs(argv: yargs.Arguments<unknown>): ConsoleArgs | undefined {
    return argv.consoleArgs as ConsoleArgs;
}
