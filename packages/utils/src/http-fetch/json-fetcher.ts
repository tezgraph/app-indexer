import { InjectionToken } from 'tsyringe';

import { RequestOptions } from './http-fetcher';

export interface JsonFetchCommand<TResponse> {
    readonly description: string;
    readonly url: string;
    readonly requestOptions?: RequestOptions;

    /** It can transform the response JSON and/or strongly validate it. */
    transformResponse(rawResponse: unknown): TResponse;
}

/**
 * The helper for HTTP requests to fetch a JSON with these features:
 * - If the HTTP response is not 200 OK, then the execution fails with an `Error` including all response details.
 * - The response JSON can be transformed and/or strongly validated.
 * - The execution is logged and its duration measured.
 * - The request is aborted on app shutdown.
 */
export interface JsonFetcher {
    execute<TResponse>(command: JsonFetchCommand<TResponse>): Promise<TResponse>;
}

export const JSON_FETCHER_DI_TOKEN: InjectionToken<JsonFetcher> = 'Dappetizer:Indexer:JsonFetcher';
