import { BigNumber } from 'bignumber.js';
import { when } from 'ts-mockito';

import {
    CommonMainOperationWithResult,
    CommonMainOperationWithResultConverter,
    TaquitoRpcMainOperationWithResult,
} from '../../../src/rpc-data/operations/common/main-operation/common-main-operation-with-result-converter';
import {
    CommonSmartRollupOperation,
    CommonSmartRollupOperationConverter,
    TaquitoRpcCommonSmartRollupOperation,
} from '../../../src/rpc-data/operations/smart-rollup/common/converters/common-smart-rollup-operation-converter';
import { RpcConvertOptions } from '../../../src/rpc-data/rpc-convert-options';

export function setupCommonOpWithResultProperties(
    commonOperationConverter: CommonMainOperationWithResultConverter,
    rpcData: TaquitoRpcMainOperationWithResult,
    operationUid: string,
    options: RpcConvertOptions,
): CommonMainOperationWithResult {
    const commonProperties = getProperties();

    when(commonOperationConverter.convert(rpcData, operationUid, options)).thenReturn(commonProperties);
    return commonProperties;
}

export function setupCommonSmartRollupOpProperties(
    commonOperationConverter: CommonSmartRollupOperationConverter,
    rpcData: TaquitoRpcCommonSmartRollupOperation,
    operationUid: string,
    options: RpcConvertOptions,
): CommonSmartRollupOperation {
    const commonProperties: CommonSmartRollupOperation = {
        ...getProperties(),
        rollup: 'mockedRollup' as any,
    };

    when(commonOperationConverter.convert(rpcData, operationUid, options)).thenReturn(commonProperties);
    return commonProperties;
}

function getProperties(): CommonMainOperationWithResult {
    return {
        balanceUpdates: 'mockedBalanceUpdates' as any,
        counter: new BigNumber(701),
        fee: new BigNumber(702),
        gasLimit: new BigNumber(703),
        internalOperations: 'mockedInternalOperations' as any,
        isInternal: false,
        sourceAddress: 'mockedSourceAddress',
        storageLimit: new BigNumber(704),
        uid: 'mockedUid',
    };
}
