import { Counter } from 'prom-client';
import { Writable } from 'ts-essentials';
import { instance, mock, when } from 'ts-mockito';

import { describeMember, TestClock, verifyCalled } from '../../../../../test-utilities/mocks';
import { LoggerImpl } from '../../../src/logging/impl/logger-impl';
import { LoggingMetrics } from '../../../src/logging/impl/logging-metrics';
import { LogData, LogLevel } from '../../../src/logging/log-entry';
import { LoggerScope } from '../../../src/logging/logger-scope';
import { LoggingConfig } from '../../../src/logging/logging-config';
import { LoggedPackagesProvider } from '../../../src/logging/package-info/logged-packages-provider';
import { RootLogger } from '../../../src/logging/root-logger';

describe(LoggerImpl.name, () => {
    let target: LoggerImpl;
    let rootLogger: RootLogger;
    let clock: TestClock;
    let logCounter: LoggingMetrics['logCount'];
    let config: Writable<LoggingConfig>;
    let packagesProvider: LoggedPackagesProvider;

    beforeEach(() => {
        logCounter = mock(Counter);
        const metrics = { logCount: instance(logCounter) } as LoggingMetrics;

        rootLogger = mock();
        clock = new TestClock();
        config = {} as LoggingConfig;
        packagesProvider = { packages: 'mockedVersions' as any };
        target = new LoggerImpl('FooCategory', instance(rootLogger), clock, metrics, config, {} as LoggerScope, packagesProvider);
    });

    describeMember<typeof target>('isEnabled', () => {
        it.each([
            [LogLevel.Debug, true],
            [LogLevel.Error, false],
        ])('should be delegate to root logger e.g. %s %s', (level, enabled) => {
            when(rootLogger.isEnabled(level)).thenReturn(enabled);

            const actual = target.isEnabled(level);

            expect(actual).toBe(enabled);
        });
    });

    describeMember<typeof target>('writeToLog', () => {
        it.each([LogLevel.Debug, LogLevel.Error])('should build log entry for all levels e.g. %s', level => {
            const data: LogData = { name: 'Batman' };
            config.globalData = 'globalTest';

            target.writeToLog(level, 'test msg', data);

            verifyCalled(rootLogger.log).onceWith({
                timestamp: clock.nowDate,
                category: 'FooCategory',
                level,
                message: 'test msg',
                data,
                globalData: 'globalTest',
                packages: packagesProvider.packages,
            });
            verifyCalled(logCounter.inc).onceWith({ level });
        });
    });
});
