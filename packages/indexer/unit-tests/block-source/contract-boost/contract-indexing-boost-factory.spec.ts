import { Writable } from 'ts-essentials';
import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { asReadonly } from '../../../../utils/src/public-api';
import { ContractIndexingBoostFactory } from '../../../src/block-source/contract-boost/contract-indexing-boost-factory';
import { TzktIndexingBoost } from '../../../src/block-source/contract-boost/tzkt/tzkt-indexing-boost';
import { TzktIndexingBoostFactory } from '../../../src/block-source/contract-boost/tzkt/tzkt-indexing-boost-factory';
import { ContractsBoostType, IndexingConfig, TzktContractsBoostConfig } from '../../../src/config/indexing/indexing-config';

describe(ContractIndexingBoostFactory.name, () => {
    let target: ContractIndexingBoostFactory;
    let indexingConfig: Writable<IndexingConfig>;
    let tzktBoostFactory: TzktIndexingBoostFactory;

    beforeEach(() => {
        indexingConfig = {
            contracts: asReadonly([
                { name: 'Contract1', addresses: asReadonly(['KT1UcszCkuL5eMpErhWxpRmuniAecD227Dwp', 'KT1Cp18EbxDk2WcbC1YVUyGuwuvtzyujwu4U']) },
                { name: 'Contract2', addresses: asReadonly(['KT1WvzYHCNBvDSdwafTHv7nJ1dWmZ8GCYuuC']) },
            ]),
        } as typeof indexingConfig;

        tzktBoostFactory = mock();
        target = new ContractIndexingBoostFactory(indexingConfig, instance(tzktBoostFactory));
    });

    it('should create TzKT boost correctly', () => {
        const boostConfig = { type: ContractsBoostType.Tzkt } as TzktContractsBoostConfig;
        const tzktBoost = {} as TzktIndexingBoost;
        const addresses = ['KT1UcszCkuL5eMpErhWxpRmuniAecD227Dwp', 'KT1Cp18EbxDk2WcbC1YVUyGuwuvtzyujwu4U', 'KT1WvzYHCNBvDSdwafTHv7nJ1dWmZ8GCYuuC'];
        indexingConfig.contractBoost = boostConfig;
        when(tzktBoostFactory.create(boostConfig, deepEqual(addresses))).thenReturn(tzktBoost);

        const boost = target.create();

        expect(boost).toBe(tzktBoost);
    });

    it('should return null if boost disabled', () => {
        const boost = target.create();

        expect(boost).toBeNull();
        verify(tzktBoostFactory.create(anything(), anything())).never();
    });
});
