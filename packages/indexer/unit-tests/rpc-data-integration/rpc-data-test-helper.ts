import { container as globalContainer, DependencyContainer } from 'tsyringe';

import { registerExplicitConfigFilePath } from '../../../utils/src/public-api';
import { MainOperation, registerDappetizerIndexer } from '../../src/public-api';
import { MainOperationConverter } from '../../src/rpc-data/operations/main-operation-converter';

export function shouldConvertRpcOperation(dataDesc: string, testCase: { input: any; expected: MainOperation }) {
    shouldConvertRpc(dataDesc, diContainer => {
        const operationConverter = diContainer.resolve(MainOperationConverter);

        const operations = operationConverter.convertArray([testCase.input], 'ops', {
            blockHash: 'haha',
            predecessorBlockHash: 'pred-haha',
            lastStorageWithinBlock: new Map(),
        });

        expect(operations).toMatchObject([testCase.expected]);
    });
}

export function shouldConvertRpc(dataDesc: string, runTest: (c: DependencyContainer) => void) {
    describe('full RPC conversion', () => {
        it(`should convert ${dataDesc} correctly`, () => {
            const diContainer = globalContainer.createChildContainer();
            registerDappetizerIndexer(diContainer);
            registerExplicitConfigFilePath(diContainer, { useValue: '../../dappetizer.config.ts' });

            runTest(diContainer);
        });
    });
}
