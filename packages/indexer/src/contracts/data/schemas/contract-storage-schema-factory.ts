import { errorToString } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { MichelsonSchema } from '../../../michelson/michelson-schema';
import { getUnderlyingMichelson } from '../../../michelson/michelson-utils';
import { ContractAbstraction } from '../../contract';

@singleton()
export class ContractStorageSchemaFactory {
    create(contractAbstraction: ContractAbstraction): MichelsonSchema {
        try {
            return new MichelsonSchema(getUnderlyingMichelson(contractAbstraction.schema));
        } catch (error) {
            throw new Error(`Failed to create storage schema for the contract. ${errorToString(error)}`);
        }
    }
}
