import { describeMember } from '../../../../test-utilities/mocks';
import { ConfigElement, ConfigObjectElement } from '../../src/configuration/config-element';

describe(ConfigObjectElement.name, () => {
    let target: ConfigObjectElement;
    let obj: Record<string, unknown>;

    beforeEach(() => {
        obj = {};
        target = new ConfigObjectElement(obj, 'C:/config.json', '$.parentProp');
    });

    describeMember<ConfigObjectElement>('get', () => {
        shouldGetConfigElement(prop => target.get(prop));
    });

    describeMember<ConfigObjectElement>('getOptional', () => {
        shouldGetConfigElement(prop => target.getOptional(prop));

        it.each([undefined, null])('should return null if %s', value => {
            obj.testProp = value;

            const elm = target.getOptional('testProp');

            expect(elm).toBeNull();
        });
    });

    function shouldGetConfigElement(act: (prop: string) => ConfigElement | null) {
        it('should create child element wrapping given value', () => {
            obj.testProp = 'abc';

            const elm = act('testProp')!;

            expect(elm).toBeInstanceOf(ConfigElement);
            expect(elm.value).toBe('abc');
            expect(elm.filePath).toBe('C:/config.json');
            expect(elm.propertyPath).toBe('$.parentProp.testProp');
        });
    }
});
