import { BigNumber } from 'bignumber.js';
import { instance, mock, when } from 'ts-mockito';

import {
    BalanceUpdateConverter,
} from '../../../../../src/rpc-data/common/balance-update/converters/balance-update-converter';
import {
    RegisterGlobalConstantResultConverter,
    RegisterGlobalConstantResultParts,
} from '../../../../../src/rpc-data/operations/register-global-constant/converters/register-global-constant-result-converter';
import { BalanceUpdate, TaquitoRpcRegisterGlobalConstantResult } from '../../../../../src/rpc-data/rpc-data-interfaces';
import { convertAppliedResultParts } from '../../../rpc-test-helpers';

describe(RegisterGlobalConstantResultConverter, () => {
    let target: RegisterGlobalConstantResultConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;

    let rpcData: TaquitoRpcRegisterGlobalConstantResult;
    let balanceUpdates: BalanceUpdate[];

    const act = () => convertAppliedResultParts(target, rpcData, 'res/id');

    beforeEach(() => {
        balanceUpdateConverter = mock();
        target = new RegisterGlobalConstantResultConverter(null!, instance(balanceUpdateConverter));

        rpcData = {
            global_address: 'exprvSXZYcKPxYqkizbkqRbjWJT4dKMzxcmcjg1ZfUcXzjgRs3rnRt',
            storage_size: '587',
        } as typeof rpcData;
        balanceUpdates = 'mockedBalanceUpdates' as any;

        when(balanceUpdateConverter.convertArrayProperty(rpcData, 'balance_updates', 'res/id')).thenReturn(balanceUpdates);
    });

    it('should convert correctly', () => {
        const result = act();

        expect(result).toEqual<RegisterGlobalConstantResultParts>({
            balanceUpdates,
            globalAddress: 'exprvSXZYcKPxYqkizbkqRbjWJT4dKMzxcmcjg1ZfUcXzjgRs3rnRt',
            storageSize: new BigNumber(587),
        });
    });
});
