import { dumpType, freezeResult, isNullish, isReadOnlyArray, Logger, Nullish, withIndex } from '@tezos-dappetizer/utils';

import { RpcConversionError } from './rpc-conversion-error';
import { joinUid } from './rpc-converter';

export class RpcSkippableConversionError extends RpcConversionError {}

export abstract class RpcSkippableArrayConverter<TRpcItem extends object, TTargetItem, TOptions = void> {
    constructor(
        private readonly logger: Logger,
    ) {}

    @freezeResult()
    convertArray(rpcData: Nullish<readonly TRpcItem[]>, uid: string, options: TOptions): readonly TTargetItem[] {
        if (isNullish(rpcData)) {
            return [];
        }
        if (!isReadOnlyArray(rpcData)) {
            throw new RpcConversionError(uid, `It must be an array but its type is ${dumpType(rpcData)}'.`);
        }

        const resultItems: TTargetItem[] = [];
        for (const [rpcItemData, index] of withIndex(rpcData)) {
            const itemUid = joinUid(uid, index);

            try {
                if (typeof rpcItemData !== 'object' || isNullish(rpcItemData)) {
                    throw new Error(`It must be an object but its type is ${dumpType(rpcItemData)}.`);
                }

                const item = this.convertObject(rpcItemData, itemUid, options);
                resultItems.push(item);
            } catch (error) {
                const wrappedError = RpcConversionError.wrap(error, itemUid);

                if (wrappedError instanceof RpcSkippableConversionError) {
                    this.logger.logDebug(`Skipping {itemUid} from RPC data because it is not supported by current Dappetizer version.`, { itemUid });
                    continue;
                }

                throw wrappedError;
            }
        }
        return resultItems;
    }

    convertArrayProperty<TProperty extends string>(
        rpcParentData: { [P in TProperty]?: Nullish<readonly TRpcItem[]> },
        propertyName: TProperty,
        parentUid: string,
        options: TOptions,
    ): readonly TTargetItem[] {
        return this.convertArray(rpcParentData[propertyName], joinUid(parentUid, propertyName), options);
    }

    protected abstract convertObject(rpcData: TRpcItem, uid: string, options: TOptions): TTargetItem;
}
