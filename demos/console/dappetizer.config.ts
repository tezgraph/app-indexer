import config from '../../dappetizer.config';

const { database, ...configRest } = config;

export default configRest;
