export enum BigMapTyping {
    InitialValues = 'InitialValues',
    RawID = 'RawID',
    TaquitoAbstraction = 'TaquitoAbstraction',
    Unexpected = 'Unexpected',
}
