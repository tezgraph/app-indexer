import { Block, IndexedBlock, IndexerDatabaseHandler } from '@tezos-dappetizer/indexer';

export class RegIndexerDatabaseHandler implements IndexerDatabaseHandler<object> {
    startTransaction(): object {
        return {};
    }

    commitTransaction(_dbContext: {}): void {
        /* We just check that it passes. */
    }

    rollBackTransaction(_dbContext: {}): void {
        /* We just check that it passes. */
    }

    insertBlock(block: Block, _dbContext: {}): void {
        if (typeof block.level !== 'number') {
            throw new Error('Invalid block.');
        }
    }

    deleteBlock(_block: IndexedBlock, _dbContext: {}): void {
        throw new Error('Method not implemented.');
    }

    getIndexedChainId(): null {
        return null;
    }

    getLastIndexedBlock(): null {
        return null;
    }

    getIndexedBlock(_level: number): null {
        return null;
    }
}
