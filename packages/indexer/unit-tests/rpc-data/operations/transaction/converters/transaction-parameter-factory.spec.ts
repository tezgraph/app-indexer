import * as taquitoRpc from '@taquito/rpc';
import { last } from 'lodash';
import { NonEmptyArray } from 'ts-essentials';
import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { expectToThrow } from '../../../../../../../test-utilities/mocks';
import { Contract } from '../../../../../src/contracts/contract';
import { MichelsonSchema } from '../../../../../src/michelson/michelson-schema';
import { LazyMichelsonValueFactory } from '../../../../../src/rpc-data/common/michelson/lazy-michelson-value-factory';
import {
    DEFAULT,
    TransactionParameterFactory,
} from '../../../../../src/rpc-data/operations/transaction/converters/transaction-parameter-factory';
import {
    LazyMichelsonValue,
    TaquitoRpcTransactionParameter,
    TransactionParameter,
} from '../../../../../src/rpc-data/rpc-data-interfaces';

describe(TransactionParameterFactory.name, () => {
    let target: TransactionParameterFactory;
    let lazyValueFactory: LazyMichelsonValueFactory;

    beforeEach(() => {
        lazyValueFactory = mock();
        target = new TransactionParameterFactory(instance(lazyValueFactory));
    });

    describe('should create transaction parameter correctly', () => {
        it.each([
            ['named', 'mint'],
            ['named explicitly as "default"', DEFAULT],
        ])('if %s entrypoint with raw parameter', (_desc, entrypoint) => {
            const schema = mockSchema({ prim: 'nat' });

            runTest({
                entrypointSchemas: { [entrypoint]: schema },
                inputRpcData: {
                    entrypoint,
                    value: { int: '11' },
                },
                expectedEntrypointPath: [entrypoint],
                expectedValueMichelson: { int: '11' },
                expectedSchema: schema,
            });
        });

        it('if named entrypoint with complex parameter', () => {
            const schema = mockSchema({
                prim: 'pair',
                args: [
                    { prim: 'address', annots: [':from'] },
                    { prim: 'nat', annots: [':value'] },
                ],
            });
            const valueMichelson: taquitoRpc.MichelsonV1Expression = {
                prim: 'Pair',
                args: [
                    { string: 'tz1NGQWcfuY9WfZmPjwjn2Vy3kbAcNLtFqhQ' },
                    { int: '20000000000' },
                ],
            };

            runTest({
                entrypointSchemas: { burn: schema },
                inputRpcData: {
                    entrypoint: 'burn',
                    value: valueMichelson,
                },
                expectedEntrypointPath: ['burn'],
                expectedValueMichelson: valueMichelson,
                expectedSchema: schema,
            });
        });

        it('if named entrypoint with nested another named entrypoint', () => {
            const mintSchema = mockSchema();

            runTest({
                entrypointSchemas: {
                    safeEntrypoints: mockSchema({
                        prim: 'or',
                        args: [
                            { prim: 'string', annots: ['%burn'] },
                            { prim: 'nat', annots: [':mint'] },
                        ],
                    }),
                    mint: mintSchema,
                    burn: mockSchema(),
                },
                inputRpcData: {
                    entrypoint: 'safeEntrypoints',
                    value: {
                        prim: 'Right',
                        args: [{ int: '11' }],
                    },
                },
                expectedEntrypointPath: ['safeEntrypoints', 'mint'],
                expectedValueMichelson: { int: '11' },
                expectedSchema: mintSchema,
            });
        });

        it('if default entrypoint', () => {
            const schema = mockSchema({ prim: 'nat' });

            runTest({
                defaultSchema: schema,
                inputRpcData: {
                    entrypoint: DEFAULT,
                    value: { int: '11' },
                },
                expectedEntrypointPath: [DEFAULT],
                expectedValueMichelson: { int: '11' },
                expectedSchema: schema,
            });
        });

        it('if default entrypoint with nested named entrypoint', () => {
            const mintSchema = mockSchema();

            runTest({
                defaultSchema: mockSchema({
                    prim: 'or',
                    args: [
                        { prim: 'string', annots: ['%burn'] },
                        { prim: 'nat', annots: [':mint'] },
                    ],
                }),
                entrypointSchemas: {
                    mint: mintSchema,
                    burn: mockSchema(),
                },
                inputRpcData: {
                    entrypoint: DEFAULT,
                    value: {
                        prim: 'Right',
                        args: [{ int: '11' }],
                    },
                },
                expectedEntrypointPath: [DEFAULT, 'mint'],
                expectedValueMichelson: { int: '11' },
                expectedSchema: mintSchema,
            });
        });

        it('if default entrypoint with deeply nested named entrypoint', () => {
            const mintSchema = mockSchema();

            runTest({
                defaultSchema: mockSchema({
                    prim: 'or',
                    args: [
                        {
                            prim: 'or',
                            args: [
                                { prim: 'string', annots: ['%burn'] },
                                {
                                    prim: 'or',
                                    args: [
                                        { prim: 'string', annots: ['%foo'] },
                                        { prim: 'nat', annots: [':mint'] },
                                    ],
                                },
                            ],
                            annots: ['%safeEntrypoints'],
                        },
                        { prim: 'bytes', annots: ['%transfer'] },
                    ],
                }),
                entrypointSchemas: {
                    mint: mintSchema,
                    safeEntrypoints: mockSchema(),
                    burn: mockSchema(),
                    transfer: mockSchema(),
                    foo: mockSchema(),
                },
                inputRpcData: {
                    entrypoint: DEFAULT,
                    value: {
                        prim: 'Left',
                        args: [{
                            prim: 'Right',
                            args: [{
                                prim: 'Right',
                                args: [{ int: '11' }],
                            }],
                        }],
                    },
                },
                expectedEntrypointPath: [DEFAULT, 'safeEntrypoints', 'mint'],
                expectedValueMichelson: { int: '11' },
                expectedSchema: mintSchema,
            });
        });

        it('ignoring annots if it does not correspond to any entrypoint', () => {
            const safeEntrypointsSchema = mockSchema({
                prim: 'or',
                args: [
                    { prim: 'string', annots: ['%burn'] },
                    { prim: 'nat', annots: [':mint'] },
                ],
            });
            const valueMichelson: taquitoRpc.MichelsonV1Expression = {
                prim: 'Right',
                args: [{ int: '11' }],
            };

            runTest({
                entrypointSchemas: { safeEntrypoints: safeEntrypointsSchema },
                inputRpcData: {
                    entrypoint: 'safeEntrypoints',
                    value: valueMichelson,
                },
                expectedEntrypointPath: ['safeEntrypoints'],
                expectedValueMichelson: valueMichelson,
                expectedSchema: safeEntrypointsSchema,
            });
        });
    });

    it('should wrap errors', () => {
        const rpcData: TaquitoRpcTransactionParameter = {
            entrypoint: 'omg',
            value: { string: 'wtf' },
        };
        const contract = {
            address: 'KT191reDVKrLxU9rjTSxg53wRqj6zh8pnHgr',
            parameterSchemas: {
                entrypoints: {
                    ['lol' as string]: mockSchema(),
                    ['troll' as string]: mockSchema(),
                },
            },
        } as Contract;

        const error = expectToThrow(() => target.create(rpcData, 'param/id', contract));

        expect(error.message).toIncludeMultiple([
            `'KT191reDVKrLxU9rjTSxg53wRqj6zh8pnHgr'`,
            `["lol","troll"]`,
            JSON.stringify(rpcData),
        ]);
    });

    function runTest(testCase: {
        inputRpcData: TaquitoRpcTransactionParameter;
        defaultSchema?: MichelsonSchema;
        entrypointSchemas?: Record<string, MichelsonSchema>;
        expectedEntrypointPath: NonEmptyArray<string>;
        expectedValueMichelson: taquitoRpc.MichelsonV1Expression;
        expectedSchema: MichelsonSchema;
    }) {
        const lazyValue = {} as LazyMichelsonValue;
        const contract = {
            parameterSchemas: {
                default: testCase.defaultSchema ?? {},
                entrypoints: testCase.entrypointSchemas ?? {},
            },
        } as Contract;
        when(lazyValueFactory.create(anything(), anything())).thenReturn(lazyValue);

        const parameter = target.create(testCase.inputRpcData, 'param/id', contract);

        expect(parameter).toEqual<TransactionParameter>({
            rpcData: testCase.inputRpcData,
            entrypoint: last(testCase.expectedEntrypointPath)!,
            entrypointPath: testCase.expectedEntrypointPath,
            uid: 'param/id',
            value: lazyValue,
        });
        expect(parameter).toBeFrozen();
        expect(parameter.entrypointPath).toBeFrozen();
        verify(lazyValueFactory.create(deepEqual(testCase.expectedValueMichelson), testCase.expectedSchema)).once();
    }

    function mockSchema(michelson?: taquitoRpc.MichelsonV1Expression) {
        return { michelson } as MichelsonSchema;
    }
});
