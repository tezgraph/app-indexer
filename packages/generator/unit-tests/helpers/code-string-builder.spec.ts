import { EOL } from 'os';

import { CodeStringBuilder } from '../../src/helpers/code-string-builder';

describe(CodeStringBuilder.name, () => {
    let target: CodeStringBuilder;

    beforeEach(() => {
        target = new CodeStringBuilder();
    });

    it('should be empty by default', () => {
        expect(target.lines).toBeEmpty();
        expect(target.getFinalCode()).toBe('');
    });

    it('should concat lines', () => {
        target.addLine('a')
            .addLine('b')
            .addLine('c');

        expect(target.lines).toEqual(['a', 'b', 'c']);
        expect(target.getFinalCode()).toBe(`a${EOL}b${EOL}c`);
    });

    it('should indent lines', () => {
        target.indent()
            .addLine('a')
            .addLine('b')
            .indent()
            .addLine('c')
            .unindent()
            .addLine('d')
            .unindent()
            .addLine('e');

        expect(target.lines).toEqual([
            '    a',
            '    b',
            '        c',
            '    d',
            'e',
        ]);
    });

    it('should add multiple lines', () => {
        target.addLines(['a'])
            .indent()
            .addLines(['b', 'c']);

        expect(target.lines).toEqual([
            'a',
            '    b',
            '    c',
        ]);
    });

    it('should add an empty new line even if indented', () => {
        target.addEmptyLine()
            .indent()
            .addLine('a')
            .addEmptyLine()
            .addLine('b');

        expect(target.lines).toEqual([
            '',
            '    a',
            '',
            '    b',
        ]);
    });

    it.each(['', '\t  '])(`should add '%s' as an empty new line`, str => {
        target.addLine(str);

        expect(target.lines).toEqual(['']);
    });

    it('should silently pass if no indentation to unindent', () => {
        target.unindent().addLine('a').unindent().indent().addLine('b');

        expect(target.lines).toEqual(['a', '    b']);
    });

    it('should trim new lines at the end', () => {
        target.addLine('a')
            .addEmptyLine()
            .addEmptyLine()
            .trimEnd();

        expect(target.lines).toEqual(['a']);
    });
});
