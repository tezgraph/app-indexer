import { Column, Entity, Index, PrimaryColumn } from 'typeorm';

import { commonDbColumns } from '../helpers/common-db-columns';

@Entity()
export class DbBlock {
    @PrimaryColumn(commonDbColumns.blockHash)
        hash!: string;

    @Column(commonDbColumns.blockHash)
        predecessor!: string;

    @Index('IDX_block_level', { unique: true })
    @Column({ type: 'integer' })
        level!: number;

    @Index('IDX_block_timestamp')
    @Column()
        timestamp!: Date;
}
