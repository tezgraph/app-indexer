import { asyncToArray, asyncWrap } from 'iter-tools';
import { instance, mock, when } from 'ts-mockito';

import { AsyncIterableProvider } from '../../../utils/src/public-api';
import { BlockLevelOffsetProvider } from '../../src/block-source/block-level-offset-provider';
import { IndexingConfig } from '../../src/config/indexing/indexing-config';

describe(BlockLevelOffsetProvider.name, () => {
    let target: AsyncIterableProvider<number>;
    let nextProvider: AsyncIterableProvider<number>;

    const act = async () => asyncToArray(target.iterate());

    beforeEach(() => {
        nextProvider = mock();
        const config = { headLevelOffset: 3 } as IndexingConfig;
        target = new BlockLevelOffsetProvider(instance(nextProvider), config);
    });

    it('should offset levels', async () => {
        when(nextProvider.iterate()).thenReturn(asyncWrap([1, 2, 3, 4, 5, 6, 7, 8]));

        const levels = await act();

        expect(levels).toEqual<number[]>([1, 2, 3, 4, 5]);
    });
});
