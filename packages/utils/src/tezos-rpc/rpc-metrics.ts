import { Counter, Gauge, Histogram } from 'prom-client';
import { singleton } from 'tsyringe';

import { asRequiredLabels } from '../metrics/public-api';

const retryCountLabels = ['name', 'host', 'delay', 'index'] as const;

@singleton()
export class RpcMetrics {
    readonly rpcClientCallDuration = asRequiredLabels(new Histogram({
        name: 'ai_rpc_client_call_duration',
        help: 'How long it took to execute RPC client call (ms).',
        labelNames: ['name', 'host'] as const,
        buckets: [50, 100, 250, 500, 750, 1000, 2000, 5000],
    }));

    readonly rpcClientRetryCount = asRequiredLabels(new Counter({
        name: 'ai_rpc_client_retries_total',
        help: 'Number of RPC client call retries.',
        labelNames: retryCountLabels,
    }));

    readonly monitorReconnectCount = new Counter({
        name: 'ai_rpc_monitor_reconnects_total',
        help: 'Number of tezos monitor reconnects.',
    });

    readonly monitorInfiniteLoopCount = new Counter({
        name: 'ai_rpc_monitor_infinite_loops_total',
        help: 'Number of tezos monitor enters to infinite retry loop.',
    });

    readonly monitorRetryCount = asRequiredLabels(new Counter({
        name: 'ai_rpc_monitor_retries_total',
        help: 'Number of tezos monitor call retries.',
        labelNames: retryCountLabels,
    }));

    readonly monitorBlockLevel = new Gauge({
        name: 'ai_rpc_monitor_block_level',
        help: 'Last block level processed by tezos monitor.',
    });
}
