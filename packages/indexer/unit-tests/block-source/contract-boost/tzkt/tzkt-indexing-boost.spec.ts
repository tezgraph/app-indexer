import { deepEqual, instance, mock, when } from 'ts-mockito';

import { describeMemberFactory } from '../../../../../../test-utilities/mocks';
import { JsonFetcher } from '../../../../../utils/src/public-api';
import {
    BoostLevelsOptions,
    ContractIndexingBoost,
} from '../../../../src/block-source/contract-boost/contract-indexing-boost';
import { LastIndexedLevelQuery } from '../../../../src/block-source/contract-boost/tzkt/queries/last-indexed-level-query';
import {
    OriginationBlockLevelsQuery,
} from '../../../../src/block-source/contract-boost/tzkt/queries/origination-block-levels-query';
import {
    TransactionBlockLevelsQuery,
} from '../../../../src/block-source/contract-boost/tzkt/queries/transaction-block-levels-query';
import { TzktIndexingBoost } from '../../../../src/block-source/contract-boost/tzkt/tzkt-indexing-boost';
import { TzktContractsBoostConfig } from '../../../../src/config/indexing/indexing-config';

describe(TzktIndexingBoost.name, () => {
    let target: ContractIndexingBoost;
    let fetcher: JsonFetcher;

    const API_URL = 'http://tzkt/api';
    const CONTRACT_ADDRESSES = Object.freeze(['KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton', 'KT1WvzYHCNBvDSdwafTHv7nJ1dWmZ8GCYuuC']);

    beforeEach(() => {
        fetcher = mock();
        const config = { apiUrl: API_URL } as TzktContractsBoostConfig;
        target = new TzktIndexingBoost(instance(fetcher), config, CONTRACT_ADDRESSES);
    });

    const describeMember = describeMemberFactory<typeof target>();

    describeMember('name', () => {
        it('should be correct', () => {
            expect(target.name).toBe('TzKT');
        });
    });

    describeMember('getLevelsWithContracts', () => {
        it('should load block levels from originations and transactions if more available', async () => {
            await runTest({
                input: {
                    originationLevels: [11, 20],
                    transactionLevels: [12, 16, 15], // Test not-sorted.
                    hasMoreTransactions: true,
                    lastIndexedLevel: 30,
                },
                expected: {
                    levelsWithContracts: [11, 12, 15, 16, 20],
                    lastLoadedLevel: 16, // From transactions, ignoring originations b/c there could be more transactions in-between.
                    hasMore: true,
                },
            });
        });

        it('should load correctly if transactions have no more items', async () => {
            await runTest({
                input: {
                    originationLevels: [11, 20],
                    transactionLevels: [12, 13, 15],
                    hasMoreTransactions: false,
                    lastIndexedLevel: 30,
                },
                expected: {
                    levelsWithContracts: [11, 12, 13, 15, 20],
                    lastLoadedLevel: 30, // Last indexed.
                    hasMore: false,
                },
            });
        });

        it('should load correctly if no transactions', async () => {
            await runTest({
                input: {
                    originationLevels: [11, 20],
                    transactionLevels: [],
                    hasMoreTransactions: false,
                    lastIndexedLevel: 30,
                },
                expected: {
                    levelsWithContracts: [11, 20],
                    lastLoadedLevel: 30, // Last indexed.
                    hasMore: false,
                },
            });
        });

        async function runTest(testCase: {
            input: { originationLevels: number[]; transactionLevels: number[]; hasMoreTransactions: boolean; lastIndexedLevel: number };
            expected: { levelsWithContracts: number[]; lastLoadedLevel: number; hasMore: boolean };
        }) {
            const options: BoostLevelsOptions = { fromBlockLevel: 666 };
            when(fetcher.execute(deepEqual(new LastIndexedLevelQuery(API_URL))))
                .thenResolve({ level: testCase.input.lastIndexedLevel, chainId: 'cc' });
            when(fetcher.execute(deepEqual(new OriginationBlockLevelsQuery(API_URL, CONTRACT_ADDRESSES, options))))
                .thenResolve(testCase.input.originationLevels);
            when(fetcher.execute(deepEqual(new TransactionBlockLevelsQuery(API_URL, CONTRACT_ADDRESSES, options))))
                .thenResolve({ levels: testCase.input.transactionLevels, hasMore: testCase.input.hasMoreTransactions });

            const levels = await target.getLevelsWithContracts(options);

            expect(levels.levelsWithContracts).toStrictEqual(new Set(testCase.expected.levelsWithContracts));
            expect(levels.lastLoadedLevel).toBe(testCase.expected.lastLoadedLevel);
            expect(levels.hasMore).toBe(testCase.expected.hasMore);
        }
    });

    describeMember('getChainId', () => {
        it('should get correctly', async () => {
            when(fetcher.execute(deepEqual(new LastIndexedLevelQuery(API_URL))))
                .thenResolve({ level: 666, chainId: 'cc' });

            const chainId = await target.getChainId();

            expect(chainId).toBe('cc');
        });
    });
});
