import { StrictExclude } from 'ts-essentials';
import { singleton } from 'tsyringe';

import { SmartRollupConverter } from './smart-rollup-converter';
import {
    SmartRollupAddMessagesOperation,
    SmartRollupOperation,
    SmartRollupOriginateOperation,
} from '../../../../../rpc-data/rpc-data-interfaces';
import { RpcConvertOptions } from '../../../../rpc-convert-options';
import { RpcObjectConverter } from '../../../../rpc-converter';
import {
    CommonMainOperationWithResult,
    CommonMainOperationWithResultConverter,
} from '../../../common/main-operation/common-main-operation-with-result-converter';

type SmartRollupOperationWithRollup = StrictExclude<SmartRollupOperation, SmartRollupAddMessagesOperation | SmartRollupOriginateOperation>;

export type TaquitoRpcCommonSmartRollupOperation = SmartRollupOperationWithRollup['rpcData'];

export type CommonSmartRollupOperation = CommonMainOperationWithResult & Pick<SmartRollupOperationWithRollup, 'rollup'>;

@singleton()
export class CommonSmartRollupOperationConverter
    extends RpcObjectConverter<TaquitoRpcCommonSmartRollupOperation, CommonSmartRollupOperation, RpcConvertOptions> {
    constructor(
        private readonly commonOperationConverter: CommonMainOperationWithResultConverter,
        private readonly rollupConverter: SmartRollupConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcCommonSmartRollupOperation,
        uid: string,
        options: RpcConvertOptions,
    ): CommonSmartRollupOperation {
        const commonProperties = this.commonOperationConverter.convert(rpcData, uid, options);
        const rollup = this.rollupConverter.convertProperty(rpcData, 'rollup', uid);

        return {
            ...commonProperties,
            rollup,
        };
    }
}
