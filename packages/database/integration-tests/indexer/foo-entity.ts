import { DbBlock } from '@tezos-dappetizer/database';
import { Entity, ManyToOne, PrimaryColumn } from 'typeorm';

@Entity()
export class FooEntity {
    @PrimaryColumn({ type: 'varchar' })
        value!: string;

    @ManyToOne(() => DbBlock, { onDelete: 'CASCADE' })
        block!: DbBlock;
}
