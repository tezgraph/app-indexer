import { ContractDetails } from '../../helpers/contract-details-resolver';
import { FileGenerator } from '../file-generator';

export interface ContractFileOptions {
    readonly contractDetails: ContractDetails;
    readonly tezosNodeUrl: string;
    readonly isTezosNodeUrlExplicit: boolean;
    readonly tezosNetwork: string;
}

export type ContractFileGenerator = FileGenerator<ContractFileOptions>;
