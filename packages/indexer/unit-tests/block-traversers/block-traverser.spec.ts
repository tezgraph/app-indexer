import { anything, capture, instance, mock, verify } from 'ts-mockito';

import { MockedIndexerModule, mockIndexerModule } from './traverser-test-utils';
import { ContextData, DbContext } from '../../../../test-utilities/mocks';
import { asReadonly } from '../../../utils/src/public-api';
import { BalanceUpdateTraverser } from '../../src/block-traversers/balance-update-traverser';
import { BlockTraverser } from '../../src/block-traversers/block-traverser';
import { OperationGroupTraverser } from '../../src/block-traversers/operation-group-traverser';
import {
    BlockIndexingContext,
    BlockMetadataBalanceUpdateIndexingContext,
    OperationGroupIndexingContext,
} from '../../src/client-indexers/block-data-indexer';
import { BalanceUpdate, Block, OperationGroup } from '../../src/rpc-data/rpc-data-interfaces';

describe(BlockTraverser.name, () => {
    let target: BlockTraverser<DbContext>;
    let operationGroupTraverser: OperationGroupTraverser<DbContext>;
    let balanceUpdateTraverser: BalanceUpdateTraverser<DbContext>;

    let indexerModule: MockedIndexerModule;
    let db: DbContext;
    let block: Block;
    let operationGroup1: OperationGroup;
    let operationGroup2: OperationGroup;
    let balanceUpdates: readonly BalanceUpdate[];
    let indexingContext: BlockIndexingContext<ContextData>;

    const act = async () => target.traverse(indexerModule, db, block, indexingContext);

    beforeEach(() => {
        [operationGroupTraverser, balanceUpdateTraverser] = [mock(), mock()];
        target = new BlockTraverser(instance(operationGroupTraverser), instance(balanceUpdateTraverser));

        indexerModule = mockIndexerModule();
        db = 'mockedDb' as any;
        operationGroup1 = 'mockedGroup1' as any;
        operationGroup2 = 'mockedGroup2' as any;
        balanceUpdates = 'mockedBalanceUpdates' as any;
        block = {
            metadataBalanceUpdates: balanceUpdates,
            operationGroups: asReadonly([operationGroup1, operationGroup2]),
        } as typeof block;
        indexingContext = { data: { ctxVal: 'abc' } };
    });

    it('should index block and traverse all operation groups and balance updates', async () => {
        await act();

        verify(indexerModule.mocks.blockDataIndexer.indexBlock(block, db, indexingContext)).once();

        const receivedOpGroupCtx = capture<any, any, any, any>(operationGroupTraverser.traverse).first()[3];
        expect(receivedOpGroupCtx).toEqual<OperationGroupIndexingContext<ContextData>>({
            data: indexingContext.data,
            block,
        });
        expect(receivedOpGroupCtx).toBeFrozen();
        verify(operationGroupTraverser.traverse(anything(), anything(), anything(), anything())).times(2);
        verify(operationGroupTraverser.traverse(indexerModule, db, operationGroup1, receivedOpGroupCtx)).once();
        verify(operationGroupTraverser.traverse(indexerModule, db, operationGroup2, receivedOpGroupCtx)).once();

        const receivedBalanceUpdateCtx = capture<any, any, any, any>(balanceUpdateTraverser.traverse).first()[3];
        expect(receivedBalanceUpdateCtx).toEqual<BlockMetadataBalanceUpdateIndexingContext<ContextData>>({
            data: indexingContext.data,
            block,
            balanceUpdateSource: 'BlockMetadata',
            operationGroup: null,
            mainOperation: null,
            internalOperation: null,
        });
        expect(receivedBalanceUpdateCtx).toBeFrozen();
        verify(balanceUpdateTraverser.traverse(anything(), anything(), anything(), anything())).once();
        verify(balanceUpdateTraverser.traverse(indexerModule, db, balanceUpdates, receivedBalanceUpdateCtx)).once();
    });
});
