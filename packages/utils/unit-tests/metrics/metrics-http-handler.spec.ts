import express from 'express';
import { Registry } from 'prom-client';
import { instance, mock, verify, when } from 'ts-mockito';

import { httpHeaders } from '../../src/http-fetch/public-api';
import { MetricsCollector } from '../../src/metrics/metrics-collector';
import { MetricsHttpHandler } from '../../src/metrics/metrics-http-handler';

describe(MetricsHttpHandler.name, () => {
    let target: MetricsHttpHandler;
    let prometheusRegistry: Registry;
    let response: express.Response;
    let metricsCollector: MetricsCollector;

    beforeEach(() => {
        [prometheusRegistry, metricsCollector] = [mock(), mock()];
        target = new MetricsHttpHandler(instance(prometheusRegistry), instance(metricsCollector));

        response = mock();
    });

    it('should send metrics to HTTP response', async () => {
        when(metricsCollector.collectMetrics()).thenResolve('lol omg');
        when(prometheusRegistry.contentType).thenReturn('funny/memes');

        await target.handle(null!, instance(response));

        verify(response.setHeader(httpHeaders.CONTENT_TYPE, 'funny/memes'));
        verify(response.send('lol omg'));
    });
});
