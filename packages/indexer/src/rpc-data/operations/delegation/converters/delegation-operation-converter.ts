import { singleton } from 'tsyringe';

import { DelegationResultConverter } from './delegation-result-converter';
import { primitives } from '../../../primitives';
import { RpcConvertOptions } from '../../../rpc-convert-options';
import { RpcObjectConverter } from '../../../rpc-converter';
import { DelegationOperation, OperationKind, TaquitoRpcDelegationOperation } from '../../../rpc-data-interfaces';
import {
    CommonMainOperationWithResultConverter,
} from '../../common/main-operation/common-main-operation-with-result-converter';

@singleton()
export class DelegationOperationConverter extends RpcObjectConverter<TaquitoRpcDelegationOperation, DelegationOperation, RpcConvertOptions> {
    constructor(
        private readonly commonOperationConverter: CommonMainOperationWithResultConverter,
        private readonly resultConverter: DelegationResultConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcDelegationOperation,
        uid: string,
        options: RpcConvertOptions,
    ): DelegationOperation {
        const commonProperties = this.commonOperationConverter.convert(rpcData, uid, options);
        const result = this.resultConverter.convertFromMetadataOperationResult(rpcData, uid);

        return Object.freeze<DelegationOperation>({
            ...commonProperties,
            delegateAddress: primitives.address.convertNullableProperty(rpcData, 'delegate', uid),
            kind: OperationKind.Delegation,
            result,
            rpcData,
        });
    }
}
