import * as taquito from '@taquito/taquito';
import * as taquitoTzip12 from '@taquito/tzip12';
import * as taquitoTzip16 from '@taquito/tzip16';
import {
    BackgroundWorker,
    registerBackgroundWorker,
    registerConfigWithDiagnostics,
    registerDappetizerUtils,
    registerHealthCheck,
    registerOnce,
    registerPublicApi,
    registerSingleton,
    resolveWithExplicitArgs,
    TAQUITO_HTTP_BACKEND_DI_TOKEN,
    TAQUITO_RPC_CLIENT_DI_TOKEN,
} from '@tezos-dappetizer/utils';
import { DependencyContainer } from 'tsyringe';

import {
    BlockIndexingBackgroundWorker,
    ShutDownAppOnCompletetionProcessor,
} from './block-processing/block-indexing-background-worker';
import { BlockIndexingProcessor } from './block-processing/block-indexing-processor';
import { BlockPreparationProcessor } from './block-processing/block-preparation-processor';
import { ErrorHandlingIndexingProcessor } from './block-processing/error-handling-indexing-processor';
import { BlockIndexingHealthCheck } from './block-processing/health/block-indexing-health-check';
import { ToBlockLevelProcessor } from './block-processing/to-block-level-processor';
import { BlockLevelForIndexingProvider } from './block-source/block-level-for-indexing-provider';
import { BlockLevelOffsetProvider } from './block-source/block-level-offset-provider';
import { ContractBoostedBlockProvider } from './block-source/contract-boost/contract-boosted-block-provider';
import {
    ForbidBlockDataIndexerIfContractBoostVerifier,
} from './block-source/contract-boost/forbid-block-data-indexer-if-contract-boost-verifier';
import { DownloadBlockByLevelProvider } from './block-source/download-block-by-level-provider';
import { ExplicitBlockLevelsProvider } from './block-source/explicit-blocks/explicit-block-levels-provider';
import { ReorgBlockProvider } from './block-source/reorg-block-provider';
import { ToBlockLevelProvider } from './block-source/to-block-level-provider';
import { VerifyBlockProvider } from './block-source/verify-block-provider';
import { registerPartialIndexerModuleVerifier } from './client-indexers/verification/partial-indexer-module-verifier';
import { IndexingConfig } from './config/indexing/indexing-config';
import { SELECTIVE_INDEXING_CONFIG_DI_TOKEN } from './config/indexing/selective-indexing-config';
import { CONTRACT_PROVIDER_DI_TOKEN } from './contracts/contract-provider';
import { ContractProviderImpl } from './contracts/contract-provider-impl';
import { INDEXER_PACKAGE_JSON_DI_TOKEN } from './helpers/indexer-package-json';
import { IndexerPackageJsonImpl } from './helpers/indexer-package-json-impl';
import { IpfsConfig } from './helpers/ipfs/ipfs-config';
import { IpfsGatewayRaceHttpBackend } from './helpers/ipfs/ipfs-gateway-race-http-backend';
import { TAQUITO_TEZOS_TOOLKIT_DI_TOKEN } from './helpers/tezos-toolkit-di-token';
import { SMART_ROLLUP_PROVIDER_DI_TOKEN } from './smart-rollups/smart-rollup-provider';
import { SmartRollupProviderImpl } from './smart-rollups/smart-rollup-provider-impl';
import { TezosBlockIndexer } from './tezos-block-indexer';

export function registerDappetizerIndexer(diContainer: DependencyContainer): void {
    registerOnce(diContainer, 'Dappetizer:Indexer', () => {
        // Dependencies:
        registerDappetizerUtils(diContainer);

        // Public API:
        registerSingleton(diContainer, TAQUITO_TEZOS_TOOLKIT_DI_TOKEN, { useFactory: createTezosToolkit });
        registerPublicApi(diContainer, CONTRACT_PROVIDER_DI_TOKEN, {
            useInternalToken: ContractProviderImpl,
            createProxy: internal => ({
                getContract: internal.getContract.bind(internal),
            }),
        });
        registerPublicApi(diContainer, INDEXER_PACKAGE_JSON_DI_TOKEN, {
            useInternalToken: IndexerPackageJsonImpl,
            createProxy: internal => ({
                dependencies: internal.dependencies,
                version: internal.version,
            }),
        });
        registerPublicApi(diContainer, SMART_ROLLUP_PROVIDER_DI_TOKEN, {
            useInternalToken: SmartRollupProviderImpl,
            createProxy: internal => ({
                getRollup: internal.getRollup.bind(internal),
            }),
        });
        registerPublicApi(diContainer, SELECTIVE_INDEXING_CONFIG_DI_TOKEN, {
            useInternalToken: IndexingConfig,
            createProxy: internal => ({
                configPropertyPath: internal.configPropertyPath,
                contracts: internal.contracts,
            }),
        });

        // Inject to Dappetizer features:
        registerBackgroundWorker(diContainer, { useFactory: createBlockIndexingBackgroundWorker });
        registerConfigWithDiagnostics(diContainer, { useToken: IndexingConfig });
        registerConfigWithDiagnostics(diContainer, { useToken: IpfsConfig });
        registerHealthCheck(diContainer, { useToken: BlockIndexingHealthCheck });
        registerPartialIndexerModuleVerifier(diContainer, { useToken: ForbidBlockDataIndexerIfContractBoostVerifier });
    });
}

function createBlockIndexingBackgroundWorker(diContainer: DependencyContainer): BackgroundWorker {
    return new BlockIndexingBackgroundWorker(
        resolveWithExplicitArgs(diContainer, ShutDownAppOnCompletetionProcessor, [
            resolveWithExplicitArgs(diContainer, ErrorHandlingIndexingProcessor, [
                resolveWithExplicitArgs(diContainer, BlockIndexingProcessor, [
                    resolveWithExplicitArgs(diContainer, ReorgBlockProvider, [
                        resolveWithExplicitArgs(diContainer, VerifyBlockProvider, [
                            resolveWithExplicitArgs(diContainer, DownloadBlockByLevelProvider, [
                                resolveWithExplicitArgs(diContainer, ExplicitBlockLevelsProvider, [
                                    resolveWithExplicitArgs(diContainer, ToBlockLevelProvider, [
                                        resolveWithExplicitArgs(diContainer, BlockLevelOffsetProvider, [
                                            resolveWithExplicitArgs(diContainer, ContractBoostedBlockProvider, [
                                                diContainer.resolve(BlockLevelForIndexingProvider),
                                            ]),
                                        ]),
                                    ]),
                                ]),
                            ]),
                        ]),
                    ]),
                    resolveWithExplicitArgs(diContainer, BlockPreparationProcessor, [
                        resolveWithExplicitArgs(diContainer, ToBlockLevelProcessor, [
                            diContainer.resolve(TezosBlockIndexer),
                        ]),
                    ]),
                ]),
            ]),
        ]),
    );
}

function createTezosToolkit(diContainer: DependencyContainer): taquito.TezosToolkit {
    const rpcClient = diContainer.resolve(TAQUITO_RPC_CLIENT_DI_TOKEN);
    const toolkit = new taquito.TezosToolkit(rpcClient);
    const metadataProvider = createMetadataProvider(diContainer);

    toolkit.setPackerProvider(new taquito.MichelCodecPacker());
    toolkit.addExtension(new taquitoTzip12.Tzip12Module(metadataProvider));
    toolkit.addExtension(new taquitoTzip16.Tzip16Module(metadataProvider));
    return toolkit;
}

function createMetadataProvider(diContainer: DependencyContainer): taquitoTzip16.MetadataProvider {
    const handlers = new Map(taquitoTzip16.DEFAULT_HANDLERS.entries());
    const httpBackend = diContainer.resolve(TAQUITO_HTTP_BACKEND_DI_TOKEN);

    const ipfsHandler = new taquitoTzip16.IpfsHttpHandler();
    ipfsHandler.httpBackend = resolveWithExplicitArgs(diContainer, IpfsGatewayRaceHttpBackend, [httpBackend]);
    handlers.set('ipfs', ipfsHandler);

    const httpHandler = new taquitoTzip16.HttpHandler();
    httpHandler.httpBackend = httpBackend;
    handlers.set('http', httpHandler);
    handlers.set('https', httpHandler);

    return new taquitoTzip16.MetadataProvider(handlers);
}
