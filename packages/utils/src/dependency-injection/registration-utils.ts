import {
    DependencyContainer,
    InjectionToken,
    instanceCachingFactory,
    isClassProvider,
    isFactoryProvider,
    isTokenProvider,
    isValueProvider,
    Lifecycle,
    Provider,
} from 'tsyringe';

import { ArgError, argGuard } from '../basics/public-api';

export function registerSingleton<T>(
    diContainer: DependencyContainer,
    token: InjectionToken<T>,
    provider: Provider<T>,
): void {
    argGuard.object(diContainer, 'diContainer');
    argGuard.object(provider, 'provider');

    if (isValueProvider(provider)) {
        diContainer.registerInstance(token, provider.useValue);
    } else if (isFactoryProvider(provider)) {
        diContainer.register(token, { useFactory: instanceCachingFactory(provider.useFactory) });
    } else if (isTokenProvider(provider)) {
        diContainer.register(token, provider, { lifecycle: Lifecycle.Singleton });
    } else if (isClassProvider(provider)) {
        diContainer.register(token, provider, { lifecycle: Lifecycle.Singleton });
    } else {
        throw new ArgError({
            argName: 'provider',
            specifiedValue: provider,
            details: 'The value must be one of supported tsyring providers.',
        });
    }
}

export function registerOnce(diContainer: DependencyContainer, name: string, registerFunc: () => void): void {
    argGuard.object(diContainer, 'diContainer');
    argGuard.nonWhiteSpaceString(name, 'name');
    argGuard.function(registerFunc, 'registerFunc');

    const mutex: InjectionToken<string> = `mutex:${name}`;
    if (!diContainer.isRegistered(mutex)) {
        diContainer.registerInstance(mutex, 'dummy');
        registerFunc();
    }
}

/** @internal Intended only for Dappetizer itself. */
export function registerPublicApi<T>(
    diContainer: DependencyContainer,
    publicToken: InjectionToken<T>,
    options: {
        useInternalToken: InjectionToken<T>;
        createProxy: (internalValue: T) => T;
    },
): void {
    registerSingleton(diContainer, publicToken, {
        useFactory: c => {
            const internalValue = c.resolve(options.useInternalToken);
            const publicValue = options.createProxy(internalValue);
            return Object.freeze(publicValue);
        },
    });
}
