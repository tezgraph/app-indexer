import { injectLogger, Logger, typed } from '@tezos-dappetizer/utils';
import { inject, registry, singleton } from 'tsyringe';

import { ContractFileGenerator } from './file-generators/contract-indexer/contract-file-generator';
import { IndexerClassGenerator } from './file-generators/contract-indexer/indexer-class-generator';
import { IndexerClassInterfacesGenerator } from './file-generators/contract-indexer/indexer-class-interfaces-generator';
import { FileEmitter } from './file-generators/file-emitter';
import { DappetizerConfigGenerator } from './file-generators/structural/dappetizer-config-generator';
import { DappetizerNetworkConfigGenerator } from './file-generators/structural/dappetizer-network-config-generator';
import { DockerfileGenerator } from './file-generators/structural/dockerfile-generator';
import { IndexerModuleGenerator } from './file-generators/structural/indexer-module-generator';
import { PackageJsonGenerator } from './file-generators/structural/package-json-generator';
import { StructuralFileGenerator } from './file-generators/structural/structural-file-generator';
import { TsconfigGenerator } from './file-generators/structural/tsconfig-generator';
import { ContractDetailsResolver } from './helpers/contract-details-resolver';
import { generatorDefaults } from './helpers/generator-defaults';
import { IndexerModuleNameResolver } from './helpers/indexer-module-name-resolver';
import { NpmDappetizerInstaller } from './helpers/npm-dappetizer-installer';
import { OutDirInitializer } from './helpers/out-dir-initializer';
import { EmitInitialAppOptions, IndexerEmitter } from './indexer-emitter';

const STRUCTURAL_GENERATORS = Symbol('StructuralGenerators');
const CONTRACT_GENERATORS = Symbol('ContractGenerators');

@singleton()
@registry([
    {
        token: STRUCTURAL_GENERATORS,
        useFactory: c => typed<StructuralFileGenerator[]>([
            c.resolve(DappetizerConfigGenerator),
            c.resolve(DappetizerNetworkConfigGenerator),
            c.resolve(DockerfileGenerator),
            c.resolve(IndexerModuleGenerator),
            c.resolve(PackageJsonGenerator),
            c.resolve(TsconfigGenerator),
        ]),
    },
    {
        token: CONTRACT_GENERATORS,
        useFactory: c => typed<ContractFileGenerator[]>([
            c.resolve(IndexerClassGenerator),
            c.resolve(IndexerClassInterfacesGenerator),
        ]),
    },
])
export class IndexerInitialAppEmitter implements Pick<IndexerEmitter, 'emitInitialApp'> {
    constructor( // eslint-disable-line max-params
        private readonly npmDappetizerInstaller: NpmDappetizerInstaller,
        private readonly fileEmitter: FileEmitter,
        private readonly outDirInitializer: OutDirInitializer,
        private readonly contractDetailsResolver: ContractDetailsResolver,
        private readonly indexerModuleNameResolver: IndexerModuleNameResolver,
        @inject(STRUCTURAL_GENERATORS) private readonly structuralGenerators: readonly StructuralFileGenerator[],
        @inject(CONTRACT_GENERATORS) private readonly contractGenerators: readonly ContractFileGenerator[],
        @injectLogger(IndexerInitialAppEmitter) private readonly logger: Logger,
    ) {}

    async emitInitialApp(options: EmitInitialAppOptions): Promise<void> {
        if (options.enableUsageStatistics) {
            this.logger.logInformation(`Dappetizer collects anonymous usage statistics.`
                + ` Check ${generatorDefaults.USAGE_STATISTICS_DOCS_URL} for information about opt-out.`);
        }

        const [indexerModuleName, contractDetails] = await Promise.all([
            this.indexerModuleNameResolver.resolve(options.indexerModuleName, options.outDirPath),
            options.contractAddress
                ? await this.contractDetailsResolver.resolve(options.contractAddress, options.contractName)
                : null,
        ]);

        await this.outDirInitializer.init(options.outDirPath, 'initial app');

        if (options.installNpmDependencies) {
            await this.npmDappetizerInstaller.installPackages(options.outDirPath);
        }

        await Promise.all([
            this.fileEmitter.emitFiles(
                options.outDirPath,
                { ...options, indexerModuleName, contractDetails },
                this.structuralGenerators,
            ),
            contractDetails
                ? this.fileEmitter.emitFiles(
                    options.outDirPath,
                    { ...options, contractDetails },
                    this.contractGenerators,
                )
                : null,
        ]);
    }
}
