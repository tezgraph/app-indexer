import { Applied, isApplied } from './applied';
import { isMainOperationWithResult } from './main-operation-with-result';
import { IncreasePaidStorageOperation } from '../increase-paid-storage/increase-paid-storage-operation';
import { MainOperation } from '../main-operation';
import { OperationKind } from '../operation-kind';
import { OriginationOperation } from '../origination/origination-operation';
import { RegisterGlobalConstantOperation } from '../register-global-constant/register-global-constant-operation';
import {
    SmartRollupExecuteOutboxMessageOperation,
} from '../smart-rollup/execute-outbox-message/smart-rollup-execute-outbox-message-operation';
import { SmartRollupOriginateOperation } from '../smart-rollup/originate/smart-rollup-originate-operation';
import { SmartRollupPublishOperation } from '../smart-rollup/publish/smart-rollup-publish-operation';
import { SmartRollupRecoverBondOperation } from '../smart-rollup/recover-bond/smart-rollup-recover-bond-operation';
import { SmartRollupRefuteOperation } from '../smart-rollup/refute/smart-rollup-refute-operation';
import { SmartRollupTimeoutOperation } from '../smart-rollup/timeout/smart-rollup-timeout-operation';
import { TransactionOperation } from '../transaction/transaction-operation';
import { TransferTicketOperation } from '../transfer-ticket/transfer-ticket-operation';

/**
 * A main (top-level, directly in operation groups) operation with balance updates on its `result`.
 * @category RPC Block Data
 */
export type MainOperationWithBalanceUpdatesInResult =
    Applied<
    | IncreasePaidStorageOperation
    | OriginationOperation
    | RegisterGlobalConstantOperation
    | SmartRollupExecuteOutboxMessageOperation
    | SmartRollupOriginateOperation
    | SmartRollupPublishOperation
    | SmartRollupRecoverBondOperation
    | SmartRollupRefuteOperation
    | SmartRollupTimeoutOperation
    | TransactionOperation
    | TransferTicketOperation>;

const narrowedKinds = new Set([
    OperationKind.IncreasePaidStorage,
    OperationKind.Origination,
    OperationKind.RegisterGlobalConstant,
    OperationKind.SmartRollupExecuteOutboxMessage,
    OperationKind.SmartRollupOriginate,
    OperationKind.SmartRollupPublish,
    OperationKind.SmartRollupRecoverBond,
    OperationKind.SmartRollupRefute,
    OperationKind.SmartRollupTimeout,
    OperationKind.Transaction,
    OperationKind.TransferTicket,
]);

export function isMainOperationWithBalanceUpdatesInResult(operation: MainOperation): operation is MainOperationWithBalanceUpdatesInResult {
    return isMainOperationWithResult(operation)
        && isApplied(operation)
        && narrowedKinds.has(operation.kind);
}
