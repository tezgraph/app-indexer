import { anything, instance, mock, verify, when } from 'ts-mockito';

import { DbContext, IndexingContext } from '../../../../../test-utilities/mocks';
import { BigMapCopy, BigMapDiff, BigMapDiffAction, BigMapUpdate } from '../../../../indexer/src/public-api';
import { asReadonly } from '../../../../utils/src/public-api';
import { DecoratedMethod } from '../../../src/helpers/decorated-method-discovery';
import {
    DecoratorOptions,
    IndexBigMapDiff,
    IndexBigMapDiffWrapper,
} from '../../../src/indexer-wrappers/contract/index-big-map-diff-wrapper';
import { executeDecorated } from '../method-wrapper-base-helpers';

describe(IndexBigMapDiffWrapper.name, () => {
    let target: IndexBigMapDiffWrapper;
    let decoratedMethod: DecoratedMethod<IndexBigMapDiff<DbContext, IndexingContext>, DecoratorOptions>;
    let db: DbContext;
    let indexingContext: IndexingContext;
    let diff: BigMapDiff;

    const act = async () => executeDecorated(target, instance(decoratedMethod), diff, db, indexingContext);

    beforeEach(() => {
        target = new IndexBigMapDiffWrapper();
        decoratedMethod = mock();
        db = 'mockedDb' as any;
        indexingContext = 'mockedIndexingContext' as any;
        diff = {
            action: BigMapDiffAction.Update,
            bigMap: {
                path: asReadonly(['p1', 'p2']),
                name: 'nnn',
            },
        } as BigMapUpdate;
    });

    it.each<[string, DecoratorOptions]>([
        ['all', undefined],
        ['by name', { name: 'nnn' }],
        ['by path', { path: ['p1', 'p2'] }],
    ])('should index big map diff if matched %s', async (_desc, decoratorOptions) => {
        when(decoratedMethod.decoratorOptions).thenReturn(decoratorOptions);

        await act();

        verify(decoratedMethod.execute(diff, db, indexingContext)).once();
    });

    it.each<[string, DecoratorOptions]>([
        ['name', { name: 'wtf' }],
        ['path (same prefix)', { path: ['p1', 'wtf'] }],
        ['path (same suffix)', { path: ['wtf', 'p2'] }],
        ['path (same as name)', { path: ['nnn'] }],
    ])('should NOT index big map diff if NOT matched by %s', async (_desc, decoratorOptions) => {
        when(decoratedMethod.decoratorOptions).thenReturn(decoratorOptions);

        await act();

        verify(decoratedMethod.execute(anything(), anything(), anything())).never();
    });

    it.each([
        ['source', 'sss'],
        ['destination', 'sss'],
    ])('should check %s big map on diff', async (_desc, name) => {
        when(decoratedMethod.decoratorOptions).thenReturn({ name });
        diff = {
            action: BigMapDiffAction.Copy,
            sourceBigMap: { name: 'sss', path: asReadonly(['p1']) },
            destinationBigMap: { name: 'ddd', path: asReadonly(['p1']) },
        } as BigMapCopy;

        await act();

        verify(decoratedMethod.execute(diff, db, indexingContext)).once();
    });
});
