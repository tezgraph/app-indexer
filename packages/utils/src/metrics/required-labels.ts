import { Counter, Gauge, Histogram } from 'prom-client';

export interface CounterWithRequiredLabels<TLabels extends string> {
    inc(labels: RequiredLabelValues<TLabels>): void;
}

export interface GaugeWithRequiredLabels<TLabels extends string> {
    set(labels: RequiredLabelValues<TLabels>, value: number): void;
}

export interface HistogramWithRequiredLabels<TLabels extends string> {
    observe(labels: RequiredLabelValues<TLabels>, value: number): void;
}

export type RequiredLabelValues<T extends string> = Record<T, string | number>;

// Gauge before Counter b/c it has same interface.
export function asRequiredLabels<TLabels extends string>(counter: Gauge<TLabels>): GaugeWithRequiredLabels<TLabels>;
export function asRequiredLabels<TLabels extends string>(counter: Counter<TLabels>): CounterWithRequiredLabels<TLabels>;
export function asRequiredLabels<TLabels extends string>(counter: Histogram<TLabels>): HistogramWithRequiredLabels<TLabels>;
export function asRequiredLabels(counter: unknown): unknown {
    return counter;
}
