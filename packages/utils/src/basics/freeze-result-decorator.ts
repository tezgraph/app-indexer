import { getConstructorName, hasConstructor } from './reflection-utils';

export function freezeResult(): MethodDecorator {
    type Method = (...args: readonly unknown[]) => unknown;

    return createTransformMethodDecorator<Method>(originalMethod => {
        return function (...args) {
            const result = originalMethod(...args);

            return hasConstructor(result, Promise)
                ? result.then(Object.freeze)
                : Object.freeze(result);
        };
    });
}

function createTransformMethodDecorator<TMethod extends Function>(
    transform: (original: TMethod) => TMethod,
): MethodDecorator {
    return function (target: Object, propertyKey: string | symbol, descriptor: PropertyDescriptor): void {
        if (typeof descriptor.value !== 'function') {
            throw new Error(`Member '${propertyKey.toString()}' of ${getConstructorName(target)} is not a method.`);
        }

        const originalUnboundMethod = descriptor.value as TMethod;

        descriptor.value = function (this: object, ...args: readonly unknown[]) {
            const originalMethod = originalUnboundMethod.bind(this) as TMethod;
            const transformedMethod = transform(originalMethod);

            return transformedMethod(...args) as unknown;
        };
    };
}
