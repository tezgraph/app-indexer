import { BigNumber } from 'bignumber.js';

import * as samples from './common-samples';
import { OperationKind, OperationResultStatus, SmartRollupAddMessagesOperation } from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    counter: '63345',
    fee: '501',
    gas_limit: '1103',
    kind: 'smart_rollup_add_messages',
    message: ['0000000031010000000b48656c6c6f20776f726c6401bdb6f61e4f12c952f807ae7d3341af5367887dac000000000764656661756c74'],
    metadata: {
        balance_updates: samples.balanceUpdates.input,
        internal_operation_results: samples.internalOperations.input,
        operation_result: {
            consumed_milligas: '1002343',
            status: 'applied',
        },
    },
    source: 'tz2EmiqCnatjJDTVnZcVPs4DtdiMSsKFCdTk',
    storage_limit: '340',
};

const expected: SmartRollupAddMessagesOperation = {
    balanceUpdates: samples.balanceUpdates.getExpected('ops/0[smart_rollup_add_messages]/metadata'),
    counter: new BigNumber(63345),
    fee: new BigNumber(501),
    gasLimit: new BigNumber(1103),
    internalOperations: samples.internalOperations.getExpected('ops/0[smart_rollup_add_messages]/metadata'),
    isInternal: false,
    kind: OperationKind.SmartRollupAddMessages,
    message: ['0000000031010000000b48656c6c6f20776f726c6401bdb6f61e4f12c952f807ae7d3341af5367887dac000000000764656661756c74'],
    result: {
        consumedGas: new BigNumber(1003),
        consumedMilligas: new BigNumber(1002343),
        rpcData: input.metadata.operation_result as any,
        status: OperationResultStatus.Applied,
        uid: 'ops/0[smart_rollup_add_messages]/metadata/operation_result',
    },
    rpcData: input as any,
    sourceAddress: 'tz2EmiqCnatjJDTVnZcVPs4DtdiMSsKFCdTk',
    storageLimit: new BigNumber(340),
    uid: 'ops/0[smart_rollup_add_messages]',
};

shouldConvertRpcOperation(expected.kind, { input, expected });
