import { AddressPrefix, typed } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { LazyTransactionParameterConverter } from './lazy-transaction-parameter-converter';
import { TransactionResultConverter } from './transaction-result-converter';
import { LazyContractConverter } from '../../../common/lazy-contract/lazy-contract-converter';
import { primitives } from '../../../primitives';
import { RpcConvertOptions } from '../../../rpc-convert-options';
import { joinUid } from '../../../rpc-converter';
import {
    TaquitoRpcInternalOperation,
    TaquitoRpcTransactionOperation,
    TaquitoRpcTransactionResult,
    TransactionInternalOperation,
    TransactionOperation,
    UserTransactionDestination,
} from '../../../rpc-data-interfaces';
import { OperationKind } from '../../operation-kind';

export type CommonTransactionOperation =
    Pick<TransactionOperation | TransactionInternalOperation,
    'amount' | 'destinationAddress' | 'destinationContract' | 'kind' | 'transactionParameter' | 'result'>
    & Pick<TransactionOperation, 'destination'>;

interface CommonTransactionConvertOptions extends RpcConvertOptions {
    readonly rpcResult: TaquitoRpcTransactionResult;
    readonly rpcResultProperty: readonly string[];
}

@singleton()
export class CommonTransactionOperationConverter {
    constructor(
        private readonly resultConverter: TransactionResultConverter,
        private readonly lazyContractConverter: LazyContractConverter,
        private readonly lazyTransactionParameterConverter: LazyTransactionParameterConverter,
    ) {}

    convert(
        rpcData: TaquitoRpcTransactionOperation | TaquitoRpcInternalOperation,
        uid: string,
        options: CommonTransactionConvertOptions,
    ): CommonTransactionOperation {
        const destinationAddress = primitives.address.convertProperty(rpcData, 'destination', uid);
        const destinationContract = destinationAddress.startsWith(typed<AddressPrefix>('KT1'))
            ? this.lazyContractConverter.convertProperty(rpcData, 'destination', uid)
            : null;
        const result = this.resultConverter.convert(options.rpcResult, joinUid(uid, ...options.rpcResultProperty), {
            ...options,
            lazyContract: destinationContract,
        });
        const transactionParameter = this.lazyTransactionParameterConverter.convertNullableProperty(rpcData, 'parameters', uid, {
            lazyContract: destinationContract,
            transactionResult: result,
        });

        return {
            amount: primitives.bigInteger.convertProperty(rpcData, 'amount', uid, { min: 0 }),
            destination: destinationContract ?? Object.freeze<UserTransactionDestination>({ type: 'User', address: destinationAddress }),
            destinationAddress,
            destinationContract,
            kind: OperationKind.Transaction,
            result,
            transactionParameter,
        };
    }
}
