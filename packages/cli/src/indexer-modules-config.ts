import { ConfigElement, ConfigObjectElement, ConfigWithDiagnostics, isNullish, ROOT_CONFIG_ELEMENT_DI_TOKEN } from '@tezos-dappetizer/utils';
import path from 'path';
import { inject, singleton } from 'tsyringe';

export interface IndexerModuleConfig {
    readonly id: string;
    readonly config: ConfigElement;
}

@singleton()
export class IndexerModulesConfig implements ConfigWithDiagnostics {
    static readonly ROOT_NAME = 'modules';

    readonly modules: readonly IndexerModuleConfig[];

    constructor(@inject(ROOT_CONFIG_ELEMENT_DI_TOKEN) rootElement: ConfigObjectElement<keyof IndexerModulesConfig>) {
        const moduleElements = rootElement.get('modules').asArray('NotEmpty').map(e => e.asObject<keyof IndexerModuleConfig>());
        const configFileDir = path.dirname(rootElement.filePath);

        this.modules = moduleElements.map<IndexerModuleConfig>(moduleElement => {
            // Make module ID absolute b/c it's relative to config file, not the script that loads it.
            const relOrAbsId = moduleElement.get('id').asString('NotWhiteSpace');
            const absId = relOrAbsId.startsWith('.') ? path.resolve(configFileDir, relOrAbsId) : relOrAbsId;

            return {
                id: absId,
                config: moduleElement.get('config'),
            };
        });
    }

    getDiagnostics(): [string, unknown] {
        const modules = this.modules.map(module => ({
            id: module.id,
            ...!isNullish(module.config.value) ? { configElement: '{ ... }' } : null,
        }));
        return [IndexerModulesConfig.ROOT_NAME, modules];
    }
}
