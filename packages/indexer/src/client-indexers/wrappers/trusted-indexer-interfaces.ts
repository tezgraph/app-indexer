import { StrictOmit } from 'ts-essentials';

import { WithInfo } from './indexer-info';
import { BlockDataIndexer } from '../block-data-indexer';
import { ContractIndexer } from '../contract-indexer';
import { SmartRollupIndexer } from '../smart-rollup-indexer';

// Wrapped client indexers that underwent basic verification and sanitization for undefined methods.

export type TrustedBlockDataIndexer<TDbContext, TContextData> =
    WithoutName<Required<BlockDataIndexer<TDbContext, TContextData>>>;

export type TrustedContractIndexer<TDbContext, TContextData, TContractData> =
    WithInfo<WithoutName<Required<ContractIndexer<TDbContext, TContextData, TContractData>>>>;

export type TrustedSmartRollupIndexer<TDbContext, TContextData, TRollupData> =
    WithInfo<WithoutName<Required<SmartRollupIndexer<TDbContext, TContextData, TRollupData>>>>;

type WithoutName<T extends { readonly name: string }> = StrictOmit<T, 'name'>;
