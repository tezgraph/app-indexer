const { commonDbColumns } = require('@tezos-dappetizer/database');
const typeorm = require('typeorm');

module.exports = new typeorm.EntitySchema({
    name: 'Registration',
    columns: {
        id: {
            primary: true,
            type: 'int',
            generated: true,
        },
        domain: {
            type: 'varchar',
        },
        owner: {
            type: 'varchar',
        },
        duration: {
            type: 'int',
        },
        block: commonDbColumns.blockHash,
    },
});