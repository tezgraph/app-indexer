import { BigNumber } from 'bignumber.js';

import * as samples from './common-samples';
import { DelegationOperation, OperationKind, OperationResultStatus } from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    kind: 'delegation',
    source: 'tz1eNQL4PPNfTBTjoMmM1gQt8pHYqccMT3Ri',
    fee: '1500',
    counter: '40042679',
    gas_limit: '10600',
    storage_limit: '257',
    delegate: 'tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93',
    metadata: {
        balance_updates: samples.balanceUpdates.input,
        internal_operation_results: samples.internalOperations.input,
        operation_result: {
            status: 'applied',
            consumed_gas: '1000',
            consumed_milligas: '1000000',
        },
    },
};

const expected: DelegationOperation = {
    balanceUpdates: samples.balanceUpdates.getExpected('ops/0[delegation]/metadata'),
    counter: new BigNumber(40042679),
    delegateAddress: 'tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93',
    fee: new BigNumber(1500),
    gasLimit: new BigNumber(10600),
    internalOperations: samples.internalOperations.getExpected('ops/0[delegation]/metadata'),
    isInternal: false,
    kind: OperationKind.Delegation,
    result: {
        consumedGas: new BigNumber(1000),
        consumedMilligas: new BigNumber(1000000),
        rpcData: input.metadata.operation_result as any,
        status: OperationResultStatus.Applied,
        uid: 'ops/0[delegation]/metadata/operation_result',
    },
    rpcData: input as any,
    sourceAddress: 'tz1eNQL4PPNfTBTjoMmM1gQt8pHYqccMT3Ri',
    storageLimit: new BigNumber(257),
    uid: 'ops/0[delegation]',
};

shouldConvertRpcOperation(expected.kind, { input, expected });
