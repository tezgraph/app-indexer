import { deepEqual, instance, mock, when } from 'ts-mockito';

import { NumberValidator } from '../../../../utils/src/public-api';
import { IntegerConverter } from '../../../src/rpc-data/primitives/integer-converter';
import { convertPrimitive, getTypeDescription } from '../rpc-test-helpers';

describe(IntegerConverter.name, () => {
    let target: IntegerConverter;
    let validator: NumberValidator;

    beforeEach(() => {
        validator = mock();
        target = new IntegerConverter(instance(validator));
    });

    describe(getTypeDescription.name, () => {
        it('should be delegated to validator with integer limit', () => {
            when(validator.getExpectedValueDescription(deepEqual({ integer: true, min: 10 }))).thenReturn('foo bar');

            expect(getTypeDescription(target, { min: 10 })).toBe('foo bar');
        });
    });

    describe(convertPrimitive.name, () => {
        it('should be delegated to validator', () => {
            when(validator.validate('rpc', deepEqual({ integer: true, min: 10 }))).thenReturn(123);

            expect(convertPrimitive(target, 'rpc', { min: 10 })).toBe(123);
        });
    });
});
