import { indexPublish } from '../../../src/decorators/smart-rollup/index-publish-decorator';

describe(indexPublish.name, () => {
    it('should pass if correctly declared', () => {
        indexPublish();
    });
});
