# Story: Indexing of Contract Transactions

**As** an indexer developer using Dappetizer,  
**When** indexing a block  
**I want** Dappetizer to call my indexer code providing respective contract transactions,  
**So that** I can index them.

## Scenario: Indexing Block 5771696

**When** Dappetizer indexes block 5771696  
**Then** Dappetizer calls my index method with respective data one-by-one as follows:

| Entrypoint | Contract Address | Operation Group Hash |
| --- | --- | --- |
| default | KT1MNN3eEAu3nfKq8u814KfMGuwap7qZ7LPv | ongp9yC7eBYYQxScswxb3uxRg2EDvgtBDZMFUg6P15aM3dLfwzY |
| burn | KT1CeFqjJRJPNVvhvznQrWfHad2jCiDZ6Lyj | ootfQ18AU3K5XWyEC81AMCbxX2QFY3doJw5D3aD53gFPopHJqvW |
| offer | KT1GbyoDi7H1sfXmimXpptZJuCdHMh66WS9u | onm1EFR1dc4dPLTosv7im2GySVxS6YiMEGR5WwGN7iKp6JXgbfs |
