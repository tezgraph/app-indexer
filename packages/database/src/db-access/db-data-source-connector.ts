import {
    injectLogger,
    injectValue,
    Logger,
    LONG_EXECUTION_HELPER_DI_TOKEN,
    LongExecutionHelper,
} from '@tezos-dappetizer/utils';
import { inject, singleton } from 'tsyringe';
import { DataSource } from 'typeorm';

import { DbConfig } from './db-config';
import { DbEntitiesResolver } from './db-entities-resolver';
import { DbInitializerExecutor } from './db-initializer-executor';
import { dappetizerNamingStrategy } from '../helpers/naming-helper';
import { TypeormAbstraction, typeormAbstraction } from '../helpers/typeorm-abstraction';

@singleton()
export class DbDataSourceConnector {
    constructor(
        private readonly dbConfig: DbConfig,
        private readonly dbEntitiesResolver: DbEntitiesResolver,
        @inject(LONG_EXECUTION_HELPER_DI_TOKEN) private readonly longExecutionHelper: LongExecutionHelper,
        private readonly initializerExecutor: DbInitializerExecutor,
        @injectValue(typeormAbstraction) private readonly typeorm: TypeormAbstraction,
        @injectLogger(DbDataSourceConnector) private readonly logger: Logger,
    ) {}

    async connect(): Promise<DataSource> {
        const dataSource = this.typeorm.newDataSource({
            ...this.dbConfig.dataSourceOptions,
            entities: this.dbEntitiesResolver.resolve(),
            namingStrategy: dappetizerNamingStrategy,
            logging: false,
        });

        if (dataSource.options.synchronize === true) {
            this.logger.logInformation('Connecting to database and synchronizing schema, it may take long.');

            await this.beforeSynchronization(dataSource);
            await dataSource.initialize();
            await this.afterSynchronization(dataSource);

            this.logger.logInformation('Successfully connected and synchronized database.');
            return dataSource;
        }

        return this.longExecutionHelper.warn({
            description: 'connect to database',
            execute: async () => dataSource.initialize(),
        });
    }

    private async beforeSynchronization(appDataSource: DataSource): Promise<void> {
        const initDataSource = this.typeorm.newDataSource({
            ...appDataSource.options,
            synchronize: false,
        });

        await initDataSource.initialize();
        try {
            await this.initializerExecutor.beforeSynchronization(initDataSource);
        } finally {
            await this.destroySafely(initDataSource, 'before-synchronize');
        }
    }

    private async afterSynchronization(appDataSource: DataSource): Promise<void> {
        try {
            await this.initializerExecutor.afterSynchronization(appDataSource);
        } catch (error) {
            await this.destroySafely(appDataSource, 'app-wide');
            throw error;
        }
    }

    private async destroySafely(dataSource: DataSource, description: string): Promise<void> {
        try {
            await dataSource.destroy();
        } catch (error) {
            this.logger.logError(`Failed to destroy ${description} data source.`
                + ` The app fails anyway for the other error (logged after this). {error}`, { error });
        }
    }
}
