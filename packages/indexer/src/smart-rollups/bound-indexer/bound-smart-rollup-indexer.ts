import { StrictOmit } from 'ts-essentials';

import { SmartRollupIndexer } from '../../client-indexers/smart-rollup-indexer';

export const UNBOUND_ROLLUP_DATA = Symbol('UnboundRollupData');
export type UnboundRollupData = typeof UNBOUND_ROLLUP_DATA;

/**
 * Client's SmartRollupIndexer bound to particular rollup and its rollup data from shouldIndex().
 * Method indexAddMessages() is omitted b/c it can be called before binding -> must be called on raw indexers.
 */
export type BoundSmartRollupIndexer<TDbContext, TContextData> =
    StrictOmit<Required<SmartRollupIndexer<TDbContext, TContextData, UnboundRollupData>>, 'name' | 'shouldIndex' | 'indexAddMessages'>;
