import { AsyncIterableProvider, injectExplicit, isNullish } from '@tezos-dappetizer/utils';
import { asyncTakeWhile } from 'iter-tools';
import { injectable } from 'tsyringe';

import { IndexingConfig } from '../config/indexing/indexing-config';

/** Limits block levels provided for indexing according to configured `toBlockLevel`. */
@injectable()
export class ToBlockLevelProvider implements AsyncIterableProvider<number> {
    constructor(
        @injectExplicit() private readonly nextProvider: AsyncIterableProvider<number>,
        private readonly config: IndexingConfig,
    ) {}

    iterate(): AsyncIterableIterator<number> {
        const iterable = this.nextProvider.iterate();
        const toBlockLevel = this.config.toBlockLevel;

        return !isNullish(toBlockLevel)
            ? asyncTakeWhile(l => l <= toBlockLevel, iterable)
            : iterable;
    }
}
