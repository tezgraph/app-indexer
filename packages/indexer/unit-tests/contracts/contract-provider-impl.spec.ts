import { times } from 'lodash';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { sleep } from '../../../../test-utilities/mocks';
import { IndexingConfig } from '../../src/config/indexing/indexing-config';
import { Contract } from '../../src/contracts/contract';
import { ContractFactory } from '../../src/contracts/contract-factory';
import { ContractProvider } from '../../src/contracts/contract-provider';
import { ContractProviderImpl } from '../../src/contracts/contract-provider-impl';

describe(ContractProviderImpl.name, () => {
    let target: ContractProvider;
    let contractFactory: ContractFactory;

    const ADDRESS_X = 'KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9';
    const ADDRESS_Y = 'KT19kgnqC5VWoxktLRdRUERbyUPku9YioE8W';
    let contractX: Contract;
    let contractY: Contract;

    beforeEach(() => {
        contractFactory = mock();
        target = new ContractProviderImpl(
            instance(contractFactory),
            { contractCacheMillis: 100 } as IndexingConfig,
        );

        contractX = { address: ADDRESS_X } as Contract;
        contractY = { address: ADDRESS_Y } as Contract;
        when(contractFactory.create(ADDRESS_X)).thenResolve(contractX);
        when(contractFactory.create(ADDRESS_Y)).thenResolve(contractY);
    });

    it('should cache created contract', async () => {
        await Promise.all(times(5, async () => {
            expect(await target.getContract(ADDRESS_X)).toBe(contractX);
        }));
        verify(contractFactory.create(anything())).once();
    });

    it('should cache on promise level', () => {
        const promise1 = target.getContract(ADDRESS_X);
        const promise2 = target.getContract(ADDRESS_X);

        expect(promise1).toBe(promise2);
    });

    it('should cache contracts by address', async () => {
        expect(await target.getContract(ADDRESS_X)).toBe(contractX);
        expect(await target.getContract(ADDRESS_Y)).toBe(contractY);
        expect(await target.getContract(ADDRESS_X)).toBe(contractX);

        verify(contractFactory.create(anything())).times(2);
    });

    it('should expire after configured time', async () => {
        expect(await target.getContract(ADDRESS_X)).toBe(contractX);

        await sleep(150);

        expect(await target.getContract(ADDRESS_X)).toBe(contractX);
        verify(contractFactory.create(anything())).times(2);
    });

    it('should slide expiration', async () => {
        expect(await target.getContract(ADDRESS_X)).toBe(contractX);

        await sleep(70);
        expect(await target.getContract(ADDRESS_X)).toBe(contractX);
        await sleep(70);

        expect(await target.getContract(ADDRESS_X)).toBe(contractX);
        verify(contractFactory.create(anything())).once();
    });
});
