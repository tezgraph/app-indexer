import { Counter } from 'prom-client';
import { singleton } from 'tsyringe';

import { asRequiredLabels } from '../../metrics/public-api';

@singleton()
export class LoggingMetrics {
    readonly logCount = asRequiredLabels(new Counter({
        name: 'ai_logger_logs_total',
        help: 'Number of logs.',
        labelNames: ['level'] as const,
    }));
}
