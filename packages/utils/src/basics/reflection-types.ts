/**
 * Useful for generic constraints `T extends NonNullish`.
 * More explicit than writing `T extends {}` and it passes eslint there.
 */
export type NonNullish = {};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type Constructor<T extends NonNullish = NonNullish> = new(...args: any[]) => T;

export type DefaultConstructor<T extends NonNullish = NonNullish> = new() => T;

export type KeyOfType<TObject extends NonNullish, TProperty> = {
    [P in keyof TObject]: TObject[P] extends TProperty ? P : never
} [keyof TObject];

// eslint-disable-next-line @typescript-eslint/ban-types
export type StringKeyOfType<TObject extends NonNullish, TProperty> = KeyOfType<TObject, TProperty> & string;

export type Nullish<T> = T | null | undefined;

export type TypeOfName = 'bigint' | 'boolean' | 'function' | 'number' | 'object' | 'string' | 'symbol' | 'undefined';

/** See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify#tojson_behavior */
export interface ToJSON {
    toJSON(parentIndex: string): unknown;
}
