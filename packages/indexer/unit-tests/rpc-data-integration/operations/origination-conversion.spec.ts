import { BigNumber } from 'bignumber.js';

import * as samples from './common-samples';
import {
    BigMapDiffAction,
    LazyBigMapDiff,
    LazyContract,
    OperationKind,
    OperationResultStatus,
    OriginationOperation,
} from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    kind: 'origination',
    source: 'tz1P26stJ7vqhjNvYyRUMUo365Lae1d6TuwL',
    fee: '1979',
    delegate: 'tz1WnfXMPaNTBmH7DBPwqCWs9cPDJdkGBTZ8',
    counter: '25979746',
    gas_limit: '1898',
    storage_limit: '1856',
    balance: '8',
    script: {
        code: [
            { prim: 'parameter', args: [{ prim: 'string', annots: ['%name'] }] },
            { prim: 'code', args: [{ prim: 'DUP' }] },
            { prim: 'storage', args: [{ prim: 'string', annots: ['%label'] }] },
        ],
        storage: { string: 'abc' },
    },
    metadata: {
        balance_updates: samples.balanceUpdates.input,
        internal_operation_results: samples.internalOperations.input,
        operation_result: {
            status: 'applied',
            balance_updates: samples.balanceUpdates2.input,
            lazy_storage_diff: [{
                kind: 'big_map',
                id: '203375',
                diff: { action: 'remove' },
            }],
            originated_contracts: ['KT1SxUH1ibc72CPAJbLBsrJ2K1EjHL2RJLn9'],
            consumed_gas: '1798',
            consumed_milligas: '1797947',
            storage_size: '1599',
            paid_storage_size_diff: '1599',
        },
    },
};

const expected: OriginationOperation = {
    balance: new BigNumber(8),
    balanceUpdates: samples.balanceUpdates.getExpected('ops/0[origination]/metadata'),
    counter: new BigNumber(25979746),
    delegateAddress: 'tz1WnfXMPaNTBmH7DBPwqCWs9cPDJdkGBTZ8',
    fee: new BigNumber(1979),
    gasLimit: new BigNumber(1898),
    internalOperations: samples.internalOperations.getExpected('ops/0[origination]/metadata'),
    isInternal: false,
    kind: OperationKind.Origination,
    result: {
        balanceUpdates: samples.balanceUpdates2.getExpected('ops/0[origination]/metadata/operation_result'),
        bigMapDiffs: [{
            action: BigMapDiffAction.Remove,
            rpcData: input.metadata.operation_result.lazy_storage_diff[0] as any,
            uid: 'ops/0[origination]/metadata/operation_result/lazy_storage_diff/0[remove]',
        } as LazyBigMapDiff],
        consumedGas: new BigNumber(1798),
        consumedMilligas: new BigNumber(1797947),
        getInitialStorage: expect.any(Function),
        originatedContract: { address: 'KT1SxUH1ibc72CPAJbLBsrJ2K1EjHL2RJLn9' } as LazyContract,
        paidStorageSizeDiff: new BigNumber(1599),
        rpcData: input.metadata.operation_result as any,
        status: OperationResultStatus.Applied,
        storageSize: new BigNumber(1599),
        uid: 'ops/0[origination]/metadata/operation_result',
    },
    rpcData: input as any,
    sourceAddress: 'tz1P26stJ7vqhjNvYyRUMUo365Lae1d6TuwL',
    storageLimit: new BigNumber(1856),
    uid: 'ops/0[origination]',
};

shouldConvertRpcOperation(expected.kind, { input, expected });
