import { BigNumber } from 'bignumber.js';

import { dumpToString } from '../../src/basics/dump-to-string';

describe(dumpToString.name, () => {
    it.each([
        ['undefined', undefined, 'undefined'],
        ['null', null, 'null'],

        ['true boolean', true, 'true'],
        ['false boolean', false, 'false'],

        ['regular integer', 123, '123'],
        ['regular decimal', 1.23, '1.23'],
        ['very large', 1.23e+35, '1.23e+35'],
        ['not a number', NaN, 'NaN'],
        ['infinity', Infinity, 'Infinity'],

        ['regular string', 'abc', `'abc'`],
        ['empty string', '', `''`],
        ['white-space string', ' \t', `' \t'`],

        ['big number', new BigNumber(123), '123'],
        ['large big number', new BigNumber('12345678901234567890'), '12345678901234567890'],
        ['date', new Date('2021-09-13T09:45:49.000Z'), '2021-09-13T09:45:49.000Z'],

        ['regular array', [1, 'a'], `[1, 'a']`],
        ['empty array', [], '[]'],

        ['regular object', { level: 1, hash: 'a' }, `{ level: 1, hash: 'a' }`],
        ['empty object', {}, '{}'],
    ])('should dump %s', (_desc, input, expected) => {
        const dumped = dumpToString(input);

        expect(dumped).toBe(expected);
    });
});
