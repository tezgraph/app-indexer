import { injectOptional } from '@tezos-dappetizer/utils';
import { distinct, execPipe, flatMap, range, takeSorted } from 'iter-tools';
import { singleton } from 'tsyringe';

import { ExplicitBlocksToIndex } from './block-range';
import { EXPLICIT_BLOCKS_TO_INDEX_DI_TOKEN } from './explicit-blocks-to-index-di-token';

/** Resolves levels of blocks to be indexed if explicitly specified in DI container - usually comes from the console option. */
@singleton()
export class ExplicitBlockLevelsResolver {
    constructor(@injectOptional(EXPLICIT_BLOCKS_TO_INDEX_DI_TOKEN) private readonly explicitBlocksToIndex: ExplicitBlocksToIndex | undefined) {}

    resolveLevels(): readonly number[] | null {
        if (!this.explicitBlocksToIndex) {
            return null;
        }

        return Array.from(execPipe(
            this.explicitBlocksToIndex,
            flatMap(r => range(r.fromLevel, r.toLevel + 1)),
            distinct(l => l),
            takeSorted((x, y) => x - y),
        ));
    }
}
