import { IndexerVerifier } from './generic-indexer-verifier';
import { SmartRollupIndexer } from '../smart-rollup-indexer';
import { IndexerInfo } from '../wrappers/indexer-info';

export function verifySmartRollupIndexer(indexer: SmartRollupIndexer<unknown>, info: IndexerInfo): void {
    const verifier = new IndexerVerifier(indexer, info);

    verifier.verifyOptionalName();
    verifier.verifyOptionalFunctions([
        'shouldIndex',
    ]);
    verifier.verifyAtLeastOneIndexFunctionImplemented({
        indexAddMessages: '',
        indexCement: '',
        indexExecuteOutboxMessage: '',
        indexOriginate: '',
        indexPublish: '',
        indexRecoverBond: '',
        indexRefute: '',
        indexTimeout: '',
    });
}
