import { URL } from 'url';

import {
    dumpType,
    getConstructorName,
    getEnumValues,
    getVariableName,
    hasConstructor,
    hasToJSON,
    isNotNullish,
    isNullish,
    isObjectLiteral,
    keyof,
    listAllPropertyNames,
} from '../../src/basics/reflection-utils';

class TestData {
    value!: string;
}

enum Memes {
    Lol = 'lol',
    Omg = 'omg',
    Wtf = 'wtf',
}

describe('Reflection utils', () => {
    describe(keyof.name, () => {
        it('should return key of specified type', () => {
            const name = keyof<TestData>('value');

            expect(name).toBe('value');
        });
    });

    describe(isNullish.name, () => {
        it.each([
            [true, undefined],
            [true, null],
            [false, ''],
            [false, 'abc'],
            [false, 0],
            [false, 123],
        ])('should return %s for is %s and %s', (expected, value) => {
            const actual = isNullish(value);
            const actualNot = isNotNullish(value);

            expect(actual).toBe(expected);
            expect(actualNot).toBe(!expected);
        });
    });

    describe(getEnumValues.name, () => {
        it('should get all enum values', () => {
            const values = getEnumValues(Memes);

            expect(values).toEqual([Memes.Lol, Memes.Omg, Memes.Wtf]);
        });
    });

    describe(getVariableName.name, () => {
        it('should get variable name', () => {
            const lol = 'memes';

            const name = getVariableName({ lol });

            expect(name).toBe('lol');
        });
    });

    describe(isObjectLiteral.name, () => {
        it.each([
            [true, {}],
            [true, { hash: 'haha' }],
            [true, new Object()], // eslint-disable-line no-new-object
            [false, undefined],
            [false, null],
            [false, new URL('https://tezos.com/')],
            [false, new Date()],
            [false, 'omg'],
            [false, new String('omg')], // eslint-disable-line no-new-wrappers
            [false, 123],
        ])('should return %s for %s', (expected, value) => {
            const actual = isObjectLiteral(value);

            expect(actual).toBe(expected);
        });
    });

    class Bar {}
    class Foo extends Bar {}

    describe(getConstructorName.name, () => {
        it.each([
            ['abc', 'String'],
            [123, 'Number'],
            [Object.create(null), 'Object'], // No constructor.
            [new Foo(), 'Foo'],
            [new Bar(), 'Bar'],
        ])('%s should be %s', (value, expected) => {
            expect(getConstructorName(value)).toBe(expected);
        });
    });

    describe(hasConstructor.name, () => {
        it.each([
            ['undefined is Object', false, undefined, Object],
            ['null is Object', false, null, Object],
            ['string is String', true, 'abc', String],
            ['string is Object', false, 'abc', Object],
            ['string is Number', false, 'abc', Number],
            ['number is Number', true, 123, Number],
            ['object without constructor is Object', true, Object.create(null), Object],
            ['foo is Foo', true, new Foo(), Foo],
            ['foo is base class', false, new Foo(), Bar],
            ['foo is Object', false, new Foo(), Object],
        ])('%s should be %s', (_desc, expected, value, type) => {
            expect(hasConstructor(value, type)).toBe(expected);
        });
    });

    describe(hasToJSON.name, () => {
        it.each([
            [true, { toJSON: () => 'json' }],
            [false, { value: 123 }],
        ])('should have toJSON() %s', (expected, obj) => {
            expect(hasToJSON(obj)).toBe(expected);
        });
    });

    describe(dumpType.name, () => {
        it.each([
            [`'undefined'`, undefined],
            [`'object' (null)`, null],
            [`'string'`, 'abc'],
            [`'number'`, 123],
            [`'object' (constructor Date)`, new Date()],
        ])('should return %s', (expected, value) => {
            expect(dumpType(value)).toBe(expected);
        });
    });

    describe(listAllPropertyNames.name, () => {
        class FooPropsBase {
            fooPropBase = 'hello base';
            fooMethodBase(): string {
                return 'hello base';
            }
        }

        class FooProps extends FooPropsBase {
            fooProp = 'hello';
            fooMethod(): string {
                return 'hello';
            }
        }

        it('should collect all property names', () => {
            const props = listAllPropertyNames(new FooProps());

            const expectedProps: (keyof FooProps)[] = ['fooMethod', 'fooMethodBase', 'fooProp', 'fooPropBase'];
            expect(props).toEqual(expectedProps);
        });
    });
});
