import { BLOCK_COUNT, getConfig } from './dappetizer.base.config';

export default getConfig({
    protocol: 'jakarta',
    firstLevel: 2_736_129,
    lastLevel: 2_981_888,

    // Aliquot to 245_759 blocks for this protocol vs. other protocols.
    levelRangeSizeToGenerate: 2 * BLOCK_COUNT,
});
