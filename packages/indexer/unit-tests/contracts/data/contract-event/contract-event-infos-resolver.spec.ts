import { ContractAbstraction } from '../../../../src/contracts/contract';
import { ContractEventInfosResolver } from '../../../../src/contracts/data/contract-event/contract-event-infos-resolver';
import { MichelsonSchema } from '../../../../src/michelson/michelson-schema';

describe(ContractEventInfosResolver.name, () => {
    it('should convert all events correctly', () => {
        const target = new ContractEventInfosResolver();
        const contractAbstraction = {
            eventSchema: [
                { tag: '%shipped' },
                { type: { prim: 'nat', annots: ['%amount'] } },
                { tag: ':ordered', type: { prim: 'string', annots: ['%product'] } },
            ],
        } as ContractAbstraction;

        const events = target.resolve(contractAbstraction);

        expect(events).toEqual([ // Should be ordered by tag.
            { tag: 'ordered', schema: new MichelsonSchema({ prim: 'string', annots: ['%product'] }) },
            { tag: 'shipped', schema: null },
            { tag: null, schema: new MichelsonSchema({ prim: 'nat', annots: ['%amount'] }) },
        ]);
        expect(events).toBeFrozen();
        events.forEach(e => expect(e).toBeFrozen());
    });
});
