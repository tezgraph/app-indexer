import { expectToThrow } from '../../../../../test-utilities/mocks';
import { SupportedProtocolVerifier } from '../../../src/block-source/verifiers/supported-protocol-verifier';
import { TaquitoRpcBlock } from '../../../src/rpc-data/rpc-data-interfaces';

describe(SupportedProtocolVerifier.name, () => {
    const target = new SupportedProtocolVerifier();

    it('should pass if supported block', () => {
        const block = { protocol: 'ppp' } as TaquitoRpcBlock;

        target.verify(block, null);
    });

    it.each([
        'Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd',
        'PsYLVpVvgbLhAhoqAkMFUo6gudkJ9weNXhUYCiLDzcUpFpkk8Wt',
        'PsBabyM1eUXZseaJdmXFApDSBqj8YBfwELoxZHHW77EMcAbbwAS',
    ])('should throw if unsupported protocol e.g. %s', protocol => {
        const block = { protocol } as TaquitoRpcBlock;

        const error = expectToThrow(() => target.verify(block, null));

        expect(error.message).toIncludeMultiple([
            `'${protocol}'`,
            `not supported`,
            `'fromBlockLevel'`,
        ]);
    });
});
