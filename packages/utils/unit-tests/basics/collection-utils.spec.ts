import { anything, instance, mock, verify, when } from 'ts-mockito';

import {
    deduplicate,
    findNonNullish,
    hasLength,
    isNonEmpty,
    isReadOnlyArray,
    last,
    withIndex,
} from '../../src/basics/collection-utils';

describe('collection utils', () => {
    describe(isReadOnlyArray.name, () => {
        it.each([
            [false, 'not array', 123],
            [true, 'array', []],
        ])('should return %s for %s', (expected, _desc, input) => {
            expect(isReadOnlyArray(input)).toBe(expected);
        });
    });

    describe(isNonEmpty.name, () => {
        it.each([
            [false, 'undefined', undefined],
            [false, 'null', null],
            [false, 'empty array', []],
            [true, 'non-empty array', ['x']],
        ])('should return %s for %s', (expected, _desc, input) => {
            expect(isNonEmpty(input)).toBe(expected);
        });
    });

    describe(last.name, () => {
        it('should get last item', () => {
            expect(last([1, 2, 3])).toBe(3);
        });
    });

    describe(withIndex.name, () => {
        it('should iterate items with their index', () => {
            const results = Array.from(withIndex(['a', 'b', 'c']));

            expect(results).toEqual([
                ['a', 0],
                ['b', 1],
                ['c', 2],
            ]);
        });
    });

    describe(hasLength.name, () => {
        it.each([
            [false, 'undefined', 2, undefined],
            [false, 'null', 2, null],
            [true, 'one item', 1, ['x']],
            [false, 'one item', 2, ['x']],
            [false, 'two items', 1, ['x', 'y']],
            [true, 'two items', 2, ['x', 'y']],
            [false, 'two items', 3, ['x', 'y']],
        ])('should return %s for %s and checked length %s', (expected, _desc, lengthToCheck, input) => {
            expect(hasLength(input, lengthToCheck as 1)).toBe(expected);
        });
    });

    describe(deduplicate.name, () => {
        interface Item {
            name: string;
            id: number;
        }

        shouldDeduplicateIfCalled('directly', items => {
            return deduplicate<Item>(i => i.name, items);
        });

        shouldDeduplicateIfCalled('via pipe', items => {
            const funcForPipe = deduplicate<Item>(i => i.name);
            return funcForPipe(items);
        });

        function shouldDeduplicateIfCalled(desc: string, act: (i: Item[]) => Iterable<Item>) {
            it(`should return first items with given key if called ${desc}`, () => {
                const items: Item[] = [
                    { name: 'foo', id: 1 },
                    { name: 'foo', id: 2 },
                    { name: 'bar', id: 3 },
                    { name: 'foo', id: 4 },
                    { name: 'bar', id: 5 },
                ];

                const deduplicated = Array.from(act(items));

                expect(deduplicated).toEqual([items[0], items[2]]);
            });
        }
    });

    describe(findNonNullish.name, () => {
        interface Mapper {
            map(num: number): string | null | undefined;
        }

        it('should return first non-nullish mapped item', () => {
            const mapper = mock<Mapper>();
            when(mapper.map(1)).thenReturn(undefined);
            when(mapper.map(2)).thenReturn(null);
            when(mapper.map(3)).thenReturn('x');

            const result = findNonNullish([1, 2, 3, 4, 5], x => instance(mapper).map(x));

            expect(result).toBe('x');
            verify(mapper.map(anything())).times(3);
        });

        it('should return null if no mapped item is non-nullish', () => {
            const result = findNonNullish([1, 2, 3], () => undefined);

            expect(result).toBeNull();
        });
    });
});
