import { ToJSON } from '@tezos-dappetizer/utils';
import { BigNumber } from 'bignumber.js';
import { last } from 'lodash';

import { BigMapInfo } from './big-map-info';
import { MichelsonSchema } from '../../../michelson/michelson-schema';

/** Frozen but lastKnownId can be refreshed. */
export class BigMapInfoImpl implements BigMapInfo, ToJSON {
    readonly path: readonly string[];
    readonly pathStr: string;
    readonly name: string | null;
    #lastKnownId: BigNumber;

    constructor(
        path: readonly string[],
        lastKnownId: BigNumber.Value,
        readonly keySchema: MichelsonSchema,
        readonly valueSchema: MichelsonSchema,
    ) {
        this.path = Object.freeze(path.slice());
        this.pathStr = path.join('.');
        this.name = last(path) ?? null;
        this.#lastKnownId = new BigNumber(lastKnownId);

        Object.freeze(this);
    }

    get lastKnownId(): BigNumber {
        return this.#lastKnownId;
    }

    set lastKnownId(value: BigNumber) {
        this.#lastKnownId = value;
    }

    toJSON(): unknown {
        const { path, keySchema, valueSchema, lastKnownId } = this;
        return { path, keySchema, valueSchema, lastKnownId };
    }
}
