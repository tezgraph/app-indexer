export function shouldIndexBasedOnData<TData>(data: false | TData): data is TData {
    return data !== false;
}
