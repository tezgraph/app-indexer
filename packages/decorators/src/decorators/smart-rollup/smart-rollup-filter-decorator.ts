import { SmartRollupFilterOptions } from './smart-rollup-filter-options';
import { createDecorator } from '../../helpers/decorator-accessors';
import { validateSelectiveFilterOptions } from '../selective-filter-options-validator';

/**
 * The decorator for a smart rollup indexer class to select a smart rollup for indexing based on specified filter options.
 * @example Demonstrates the usage:
 * ```typescript
 * import { smartRollupFilter } from '@tezos-dappetizer/decorators';
 *
 * @smartRollupFilter({ name: 'TezosDomains' })
 * class YourSmartRollupIndexerByName {
 *     ...
 * }
 *
 * @smartRollupFilter({ anyOfAddresses: ['KT191reDVKrLxU9rjTSxg53wRqj6zh8pnHgr'] })
 * class YourSmartRollupIndexerByAddress {
 *     ...
 * }
 * ```
 * @remark This is based on {@link "@tezos-dappetizer/indexer".SmartRollupIndexer.shouldIndex | SmartRollupIndexer.shouldIndex(...)}.
 * @experimental
 */
export function smartRollupFilter(options: SmartRollupFilterOptions): ClassDecorator {
    const validatedOptions = validateSelectiveFilterOptions(options, smartRollupFilter, 'sr1');
    return createDecorator(smartRollupFilter, validatedOptions);
}
