import { MichelsonSchema } from '../../../michelson/michelson-schema';

export interface ContractParameterSchemas {
    readonly default: MichelsonSchema;
    readonly entrypoints: ContractEntrypointSchemas;
}

export type ContractEntrypointSchemas = Readonly<Record<string, MichelsonSchema>>;
