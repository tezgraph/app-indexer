import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { SmartRollupAddMessagesResult } from './smart-rollup-add-messages-result';
import { BaseMainOperationWithResult } from '../../common/main-operation/base-main-operation-with-result';
import { OperationKind } from '../../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcSmartRollupAddMessagesOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultSmartRollupAddMessages>;

/**
 * It is used to add messages to the inbox shared by all the smart rollups originated in the Tezos blockchain.
 * These messages are interpreted by the smart rollups according to their specific semantics.
 * @category RPC Block Data
 * @experimental
 */
export interface SmartRollupAddMessagesOperation extends BaseMainOperationWithResult<TaquitoRpcSmartRollupAddMessagesOperation> {
    readonly kind: OperationKind.SmartRollupAddMessages;

    // Specific properties:

    readonly message: readonly string[];
    readonly result: SmartRollupAddMessagesResult;
}
