import { BigNumber } from 'bignumber.js';

import { TicketUpdateConverter } from '../../../../../src/rpc-data/common/ticket/converters/ticket-update-converter';
import { TicketToken } from '../../../../../src/rpc-data/common/ticket/ticket-token';
import { TaquitoRpcTicketUpdate, TicketUpdate } from '../../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../../rpc-test-helpers';

describe(TicketUpdateConverter.name, () => {
    let target: TicketUpdateConverter;

    let rpcData: TaquitoRpcTicketUpdate;
    let token: TicketToken;

    const act = () => convertObject(target, rpcData, 'tck/id', token);

    beforeEach(() => {
        target = new TicketUpdateConverter();

        rpcData = {
            account: 'tz1MYod7hC8Rdqe6SdiigPgw8CU52CEUPUVM',
            amount: '123',
        };
        token = 'mockedToken' as any;
    });

    it('should convert correctly', () => {
        const update = act();

        expect(update).toEqual<TicketUpdate>({
            accountAddress: 'tz1MYod7hC8Rdqe6SdiigPgw8CU52CEUPUVM',
            amount: new BigNumber(123),
            rpcData,
            token,
            uid: 'tck/id',
        });
        expect(update).toBeFrozen();
    });
});
