import { DependencyContainer } from 'tsyringe';

import { CLOCK_DI_TOKEN } from './clock';
import { ClockImpl } from './clock-impl';
import { LONG_EXECUTION_HELPER_DI_TOKEN } from './long-execution-helper';
import { LongExecutionHelperImpl } from './long-execution-helper-impl';
import { SLEEP_HELPER_DI_TOKEN } from './sleep-helper';
import { SleepHelperImpl } from './sleep-helper-impl';
import { TimeConfig } from './time-config';
import { TIMER_HELPER_DI_TOKEN } from './timer-helper';
import { TimerHelperImpl } from './timer-helper-impl';
import { registerConfigWithDiagnostics } from '../configuration/public-api';
import { registerPublicApi } from '../dependency-injection/registration-utils';

export function registerTime(diContainer: DependencyContainer): void {
    // Public API:
    registerPublicApi(diContainer, CLOCK_DI_TOKEN, {
        useInternalToken: ClockImpl,
        createProxy: internal => ({
            getNowDate: internal.getNowDate.bind(internal),
            getNowTimestamp: internal.getNowTimestamp.bind(internal),
        }),
    });
    registerPublicApi(diContainer, LONG_EXECUTION_HELPER_DI_TOKEN, {
        useInternalToken: LongExecutionHelperImpl,
        createProxy: internal => ({
            warn: internal.warn.bind(internal),
        }),
    });
    registerPublicApi(diContainer, SLEEP_HELPER_DI_TOKEN, {
        useInternalToken: SleepHelperImpl,
        createProxy: internal => ({
            sleep: internal.sleep.bind(internal),
        }),
    });
    registerPublicApi(diContainer, TIMER_HELPER_DI_TOKEN, {
        useInternalToken: TimerHelperImpl,
        createProxy: internal => ({
            setTimeout: internal.setTimeout.bind(internal),
        }),
    });

    // Inject to Dappetizer features:
    registerConfigWithDiagnostics(diContainer, { useToken: TimeConfig });
}
