import { expectToThrow } from '../../../../../../test-utilities/mocks';
import { ContractAbstraction } from '../../../../src/contracts/contract';
import { ContractStorageSchemaFactory } from '../../../../src/contracts/data/schemas/contract-storage-schema-factory';

describe(ContractStorageSchemaFactory.name, () => {
    const target = new ContractStorageSchemaFactory();

    it('should create schema correctly', () => {
        const contract = { schema: { root: { val: { prim: 'int' } } } as any } as ContractAbstraction;

        const schema = target.create(contract);

        expect(schema.michelson).toEqual({ prim: 'int' });
    });

    it('should wrap errors', () => {
        const contract = { schema: { root: 'wtf' } as any } as ContractAbstraction;

        const error = expectToThrow(() => target.create(contract));

        expect(error.message).toIncludeMultiple([
            'create storage schema',
            `{"root":"wtf"}`,
        ]);
    });
});
