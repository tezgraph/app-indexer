import { singleton } from 'tsyringe';

import { RevealResultConverter } from './reveal-result-converter';
import { primitives } from '../../../primitives';
import { RpcConvertOptions } from '../../../rpc-convert-options';
import { RpcObjectConverter } from '../../../rpc-converter';
import { OperationKind, RevealOperation, TaquitoRpcRevealOperation } from '../../../rpc-data-interfaces';
import {
    CommonMainOperationWithResultConverter,
} from '../../common/main-operation/common-main-operation-with-result-converter';

@singleton()
export class RevealOperationConverter extends RpcObjectConverter<TaquitoRpcRevealOperation, RevealOperation, RpcConvertOptions> {
    constructor(
        private readonly commonOperationConverter: CommonMainOperationWithResultConverter,
        private readonly resultConverter: RevealResultConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcRevealOperation,
        uid: string,
        options: RpcConvertOptions,
    ): RevealOperation {
        const commonProperties = this.commonOperationConverter.convert(rpcData, uid, options);
        const result = this.resultConverter.convertFromMetadataOperationResult(rpcData, uid);

        return Object.freeze<RevealOperation>({
            ...commonProperties,
            kind: OperationKind.Reveal,
            publicKey: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'public_key', uid),
            result,
            rpcData,
        });
    }
}
