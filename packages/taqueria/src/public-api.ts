import 'reflect-metadata';

import { NonEmptyString, Option, Plugin, sendErr, Task, Template } from '@taqueria/node-sdk';
import { errorToString, registerDappetizerUtils } from '@tezos-dappetizer/utils';
import { container } from 'tsyringe';

import { optionFlag, quotedOption, taqueriaCommands, TaqueriaConsoleArgs } from './taqueria-console-args';
import { TaqueriaRunner } from './taqueria-runner';

const tezosNodeOptions = [
    Option.create({
        flag: optionFlag('network'),
        description: `The name of network to be used. If not specified, then ${quotedOption('sandbox')} is used.`,
    }),
    Option.create({
        flag: optionFlag('sandbox'),
        description: `The name of sandbox to be used. If not specified, then current Taqueria environment is used.`,
    }),
];
const contractOptions = [
    Option.create({
        flag: optionFlag('contractAddress'),
        description: `The address of the contract to be used.`
            + ` If specified, then ${quotedOption('contractName')} must be also specified.`
            + ` If not specified, then the recently originated contract is used.`,
    }),
    Option.create({
        flag: optionFlag('contractName'),
        description: `The name of the contract used for naming generated code.`
            + ` If specified, then ${quotedOption('contractAddress')} must be also specified.`
            + ` If not specified, then the recently originated contract is used.`,
    }),
];

void Plugin.create(
    () => ({
        name: '@tezos-dappetizer/taqueria',
        schema: '1.0',
        version: '0.1',
        alias: 'dappetizer',
        tasks: [
            Task.create({
                task: taqueriaCommands.BUILD_INDEXER,
                command: taqueriaCommands.BUILD_INDEXER,
                description: `Builds the generated Dappetizer indexer.`,
                handler: 'proxy',
                encoding: 'none',
            }),
            Task.create({
                task: taqueriaCommands.START_INDEXER,
                command: taqueriaCommands.START_INDEXER,
                description: `Starts the generated Dappetizer indexer. It must be built.`,
                options: tezosNodeOptions,
                handler: 'proxy',
            }),
            Task.create({
                task: taqueriaCommands.UPDATE_INDEXER,
                command: taqueriaCommands.UPDATE_INDEXER,
                description: `Updates (re-generates) already generated Dappetizer app - its contract indexer classes corresponding to`
                    + ` recently originated or explicitly specified contract.`,
                options: [...contractOptions, ...tezosNodeOptions],
                handler: 'proxy',
            }),
        ],
        templates: [
            Template.create({
                template: taqueriaCommands.CREATE_INDEXER,
                command: taqueriaCommands.CREATE_INDEXER,
                description: `Generates a new Dappetizer app using TypeScript for recently originated or explicitly specified contract.`,
                options: [...contractOptions, ...tezosNodeOptions],
                handler: async rawArgs => {
                    const args = rawArgs as TaqueriaConsoleArgs;
                    args.task = NonEmptyString.from(taqueriaCommands.CREATE_INDEXER); // Adapts Template API to Proxy API.
                    await runDappetizerTaqueria(args);
                },
            }),
        ],
        proxy: async a => runDappetizerTaqueria(a as unknown as TaqueriaConsoleArgs),
    }),
    process.argv,
);

async function runDappetizerTaqueria(args: TaqueriaConsoleArgs): Promise<void> {
    try {
        registerDappetizerUtils(container);
        const runner = container.resolve(TaqueriaRunner);
        await runner.run(args);
    } catch (error) {
        // Taqueria hides error details -> show them explicitly and rethrow to exit with non-zero code.
        sendErr(errorToString(error, { onlyMessage: true }));
        throw error;
    }
}
