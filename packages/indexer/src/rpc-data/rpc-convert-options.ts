import { dappetizerAssert, isNullish } from '@tezos-dappetizer/utils';

import { LazyContract } from './common/lazy-contract/lazy-contract';
import { Michelson } from './common/michelson/michelson';

export interface RpcConvertOptions {
    readonly blockHash: string;
    readonly predecessorBlockHash: string;
    readonly lastStorageWithinBlock: Map<string, Michelson>;
}

export interface RpcConvertOptionsWithLazyContract extends RpcConvertOptions {
    readonly lazyContract: LazyContract | null;
}

export function getRequiredLazyContract(options: RpcConvertOptionsWithLazyContract, dataToDump: unknown): LazyContract {
    dappetizerAssert(!isNullish(options.lazyContract), 'There must be a corresponding contract if particular RPC data exist.', dataToDump);
    return options.lazyContract;
}
