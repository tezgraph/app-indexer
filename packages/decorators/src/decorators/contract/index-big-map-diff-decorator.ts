import { IndexBigMapOptions } from './index-big-map-options';
import { validateBigMapOptions } from './index-big-map-options-validator';
import { createDecorator } from '../../helpers/decorator-accessors';

/** See {@link IndexBigMapOptions}. */
export type IndexBigMapDiffOptions = IndexBigMapOptions;

/**
 * The decorator for a method of a contract indexer to index diffs of the specified big map.
 * @example Demonstrates the method signature:
 * ```typescript
 * import { DbContext } from '@tezos-dappetizer/database';
 * import { indexBigMapDiff } from '@tezos-dappetizer/decorators';
 * import { BigMapDiff, BigMapDiffIndexingContext } from '@tezos-dappetizer/indexer';
 *
 * class YourContractIndexer {
 *     @indexBigMapDiff({ name: 'ledger' })
 *     indexLedgerDiff(
 *         bigMapDiff: BigMapDiff,
 *         dbContext: DbContext,
 *         indexingContext: BigMapDiffIndexingContext<TContextData, TContractData>,
 *     ): void | PromiseLike<void> {
 *         // Your indexing code.
 *     }
 * }
 * ```
 * @remark This is based on {@link "@tezos-dappetizer/indexer".ContractIndexer.indexBigMapDiff | ContractIndexer.indexBigMapDiff(...)}.
 */
export function indexBigMapDiff(options?: IndexBigMapDiffOptions): MethodDecorator {
    const validatedOptions = options !== undefined ? validateBigMapOptions(options, indexBigMapDiff) : undefined;
    return createDecorator(indexBigMapDiff, validatedOptions);
}
