import * as taquitoRpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { DeepReadonly } from 'ts-essentials';

import { BalanceUpdate } from '../../../common/balance-update/balance-update';
import { TicketUpdate } from '../../../common/ticket/ticket-update';
import { AppliedOperationResult, OperationResult } from '../../common/operation-result/operation-results';

/** @category RPC Block Data */
export type TaquitoRpcSmartRollupExecuteOutboxMessageResult = DeepReadonly<taquitoRpc.OperationResultSmartRollupExecuteOutboxMessage>;

/** @category RPC Block Data */
export type SmartRollupExecuteOutboxMessageResult = OperationResult<AppliedSmartRollupExecuteOutboxMessageResult>;

/**
 * @category RPC Block Data
 * @experimental
 */
export interface AppliedSmartRollupExecuteOutboxMessageResult extends AppliedOperationResult<TaquitoRpcSmartRollupExecuteOutboxMessageResult> {
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly paidStorageSizeDiff: BigNumber | null;
    readonly ticketUpdates: readonly TicketUpdate[];
}
