import { StrictOmit } from 'ts-essentials';

import { verifyPropertyNames } from '../../../../test-utilities/mocks';
import { ConfigObjectElement } from '../../../utils/src/public-api';
import { HasuraConfig, HasuraNamingStyle } from '../../src/hasura/config/hasura-config';
import { HasuraDappetizerConfig } from '../../src/hasura/config/hasura-dappetizer-config';
import { DappetizerConfigUsingDb } from '../../src/helpers/dappetizer-config-using-db';

type HasuraConfigProperties = StrictOmit<HasuraConfig, 'getDiagnostics'>;

describe(HasuraConfig.name, () => {
    function act(thisJson: DappetizerConfigUsingDb['hasura']) {
        const rootJson = { [HasuraConfig.ROOT_NAME]: thisJson };
        return new HasuraConfig(new ConfigObjectElement(rootJson, 'file', '$'));
    }

    it('should be created with all values specified', () => {
        const thisJson = {
            url: 'http://6.7.8.9/',
            dropExistingTracking: true,
            autotrackEntities: false,
            adminSecret: 'abc',
            schema: 'indexer',
            source: 'default',
            allowAggregations: false,
            selectLimit: 123,
            namingStyle: HasuraNamingStyle.SnakeCase,
            customMappings: [],
        };

        const config = act(thisJson);

        expect(config).toEqual<HasuraConfigProperties>({
            value: {
                url: 'http://6.7.8.9/',
                dropExistingTracking: true,
                autotrackEntities: false,
                adminSecret: 'abc',
                source: 'default',
                allowAggregations: false,
                selectLimit: 123,
                namingStyle: HasuraNamingStyle.SnakeCase,
                customMappings: [],
                autotrackedMappings: [],
            },
        });
    });

    it.each([
        ['no root element', undefined],
        ['empty root element', {}],
    ])('should be created with defaults if %s', (_desc, thisJson) => {
        const config = act(thisJson);

        expect(config).toEqual<HasuraConfigProperties>({
            value: null,
        });
    });

    it('should expose its properties in DappetizerConfig', () => {
        expect(HasuraConfig.ROOT_NAME).toBe<keyof DappetizerConfigUsingDb>('hasura');

        const config = act({ url: 'http://6.7.8.9/' });
        verifyPropertyNames<HasuraDappetizerConfig>(config.value!, [
            'url',
            'dropExistingTracking',
            'autotrackEntities',
            'adminSecret',
            'source',
            'selectLimit',
            'allowAggregations',
            'namingStyle',
            'customMappings',
            'autotrackedMappings',
        ]);
    });
});
