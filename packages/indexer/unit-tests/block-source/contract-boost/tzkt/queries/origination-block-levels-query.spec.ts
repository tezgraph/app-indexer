import { describeMemberFactory, expectToThrow } from '../../../../../../../test-utilities/mocks';
import { DappetizerBug } from '../../../../../../utils/src/public-api';
import {
    OriginationBlockLevelsQuery,
} from '../../../../../src/block-source/contract-boost/tzkt/queries/origination-block-levels-query';

describe(OriginationBlockLevelsQuery.name, () => {
    let target: OriginationBlockLevelsQuery;

    beforeEach(() => {
        target = new OriginationBlockLevelsQuery(
            'http://tzkt/api',
            ['KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton', 'KT1WvzYHCNBvDSdwafTHv7nJ1dWmZ8GCYuuC'],
            { fromBlockLevel: 123 },
            5,
        );
    });

    const describeMember = describeMemberFactory<typeof target>();

    describeMember('description', () => {
        it('should include details', () => {
            expect(target.description).toIncludeMultiple(['origination', '123']);
        });
    });

    describeMember('url', () => {
        it('should be correct', () => {
            expect(target.url).toBe('http://tzkt/api/v1/operations/originations?level.ge=123&select.values=level&limit=5'
                + '&originatedContract.in=KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton,KT1WvzYHCNBvDSdwafTHv7nJ1dWmZ8GCYuuC');
        });
    });

    describeMember('transformResponse', () => {
        it('should pass if valid levels', () => {
            const rawlevels = [0, 11, 22];

            const levels = target.transformResponse(rawlevels);

            expect(levels).toBe(rawlevels);
        });

        it.each([
            ['undefined', undefined],
            ['null', null],
            ['not array - primitive', 'wtf'],
            ['not array - object', {}],
            ['level is undefined', ['ccc']],
            ['level is null', [null]],
            ['level is not number', [undefined]],
            ['level is below zero', [-1]],
        ])('should throw if %s', (_desc, rawLevels: any) => {
            expectToThrow(() => target.transformResponse(rawLevels));
        });

        it('should throw if level limit reached', () => {
            expectToThrow(() => target.transformResponse([1, 2, 3, 4, 5]), DappetizerBug);
        });
    });
});
