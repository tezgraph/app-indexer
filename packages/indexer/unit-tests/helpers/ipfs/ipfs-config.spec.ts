import { StrictOmit } from 'ts-essentials';

import { verifyPropertyNames } from '../../../../../test-utilities/mocks';
import { ConfigObjectElement } from '../../../../utils/src/public-api';
import { DappetizerConfig } from '../../../src/config/dappetizer-config';
import { IpfsConfig } from '../../../src/helpers/ipfs/ipfs-config';
import { IpfsDappetizerConfig } from '../../../src/helpers/ipfs/ipfs-dappetizer-config';

type IpfsConfigProperties = StrictOmit<IpfsConfig, 'getDiagnostics'>;

describe(IpfsConfig.name, () => {
    function act(thisJson: DappetizerConfig['ipfs']) {
        const rootJson = { [IpfsConfig.ROOT_NAME]: thisJson };
        return new IpfsConfig(new ConfigObjectElement(rootJson, 'file', '$'));
    }

    it('should be created with all values specified', () => {
        const gateways: [string, string] = ['http://ipfs.io/', 'http://cloudflare.com/'];
        const thisJson = {
            gateways,
            timeoutMillis: 123,
        };

        const config = act(thisJson);

        expect(config).toEqual<IpfsConfigProperties>(thisJson);
    });

    it.each([
        ['no root element', undefined],
        ['empty root element', {}],
    ])('should be created with defaults if %s', (_desc, thisJson) => {
        const config = act(thisJson);

        expect(config).toEqual<IpfsConfigProperties>({
            gateways: ['https://ipfs.io/', 'https://cloudflare-ipfs.com/'],
            timeoutMillis: 100_000,
        });
    });

    it('should expose its properties in DappetizerConfig', () => {
        expect(IpfsConfig.ROOT_NAME).toBe<keyof DappetizerConfig>('ipfs');

        const config = act({});
        verifyPropertyNames<IpfsDappetizerConfig>(config, ['gateways', 'timeoutMillis']);
    });
});
