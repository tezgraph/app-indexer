import { TaquitoHttpRequest } from '../../../src/tezos-rpc/http-infrastructure/taquito-http-request';
import { getRequestDescription } from '../../../src/tezos-rpc/http-infrastructure/taquito-http-utils';

describe(getRequestDescription.name, () => {
    it('should calculate correctly', () => {
        const request = {
            method: 'POST',
            url: 'http://tezos.node/blocks',
        } as TaquitoHttpRequest;

        const description = getRequestDescription(request);

        expect(description).toBe('POST http://tezos.node/blocks');
    });
});
