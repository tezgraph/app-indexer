import { instance, mock, when } from 'ts-mockito';

import { expectToThrow } from '../../../../../test-utilities/mocks';
import { ConfigObjectElement } from '../../../src/configuration/config-element';
import { RootConfigProviderFactory } from '../../../src/configuration/root/root-config-element-factory';
import { RootConfigFileResolver } from '../../../src/configuration/root/root-config-file-resolver';
import { RootConfigValueLoader } from '../../../src/configuration/root/root-config-value-loader';

describe(RootConfigProviderFactory.name, () => {
    let fileResolver: RootConfigFileResolver;
    let valueLoader1: RootConfigValueLoader;
    let valueLoader2: RootConfigValueLoader;

    const getTarget = () => new RootConfigProviderFactory(instance(fileResolver), {
        '.lol': instance(valueLoader1),
        '.omg': instance(valueLoader2),
    });

    beforeEach(() => {
        [fileResolver, valueLoader1, valueLoader2] = [mock(), mock(), mock()];
    });

    it.each([
        ['dir/config.lol', () => valueLoader1],
        ['dir/config.LOL', () => valueLoader1], // Should be case-insensitive.
        ['dir/config.omg', () => valueLoader2],
    ])('should load value using respective loader and return it as config element for file %s', (filePath, getLoader) => {
        const loadedConfig = { prop: 'configVal' };
        when(fileResolver.resolveAbsPath()).thenReturn(filePath);
        when(getLoader().loadValue(filePath)).thenReturn(loadedConfig);

        const config = getTarget().create();

        expect(config).toBeInstanceOf(ConfigObjectElement);
        expect(config.value).toBe(loadedConfig);
        expect(config.filePath).toBe(filePath);
        expect(config.propertyPath).toBe('$');
    });

    it('should throw if unsupported extension', () => {
        when(fileResolver.resolveAbsPath()).thenReturn('dir/config.wtf');
        const target = getTarget();

        const error = expectToThrow(() => target.create());

        expect(error.message).toIncludeMultiple([
            `Invalid configuration`,
            `'dir/config.wtf'`,
            `extension is not supported`,
            `['.lol', '.omg']`,
        ]);
    });

    it('should throw if loaded value is not object', () => {
        when(fileResolver.resolveAbsPath()).thenReturn('dir/config.lol');
        when(valueLoader1.loadValue('dir/config.lol')).thenReturn('notObject');

        const target = getTarget();

        const error = expectToThrow(() => target.create());

        expect(error.message).toIncludeMultiple([
            `Invalid configuration`,
            `'dir/config.lol'`,
            `must be an object literal`,
            `it is 'string'`,
        ]);
    });
});
