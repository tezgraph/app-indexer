import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { BalanceUpdate } from '../../common/balance-update/balance-update';
import { EntityToIndex } from '../../entity-to-index';
import { OperationKind } from '../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcActivateAccountOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultActivateAccount>;

/**
 * It allows users which participated in the Tezos fundraiser to make their accounts operational.
 * @category RPC Block Data
 */
export interface ActivateAccountOperation extends EntityToIndex<TaquitoRpcActivateAccountOperation> {
    // Narrowing properties:
    readonly kind: OperationKind.ActivateAccount;

    // Specific properties:
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly pkh: string;
    readonly secret: string;
}
