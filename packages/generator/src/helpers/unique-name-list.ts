export class UniqueNameList {
    private readonly existingNames = new Set<string>();

    addAndGetUniqueFor(name: string): string {
        let uniqueName = name;
        let index = 2;

        while (this.existingNames.has(uniqueName)) {
            uniqueName = `${name}${index++}`;
        }

        this.existingNames.add(uniqueName);
        return uniqueName;
    }
}
