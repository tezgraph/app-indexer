import { expectToThrow } from '../../../../../test-utilities/mocks';
import { IsoDateConverter } from '../../../src/rpc-data/primitives/iso-date-converter';
import { convertPrimitive } from '../rpc-test-helpers';

describe(IsoDateConverter.name, () => {
    const target = new IsoDateConverter();

    const act = (d: unknown) => convertPrimitive(target, d);

    it.each([
        ['Date object', new Date(123), 123],
        ['ISO string', '2021-06-26T12:59:46Z', 1624712386000],
    ])('should convert %s', (_desc, input, expectedTimestamp) => {
        const date = act(input);

        expect(date.getTime()).toBe(expectedTimestamp);
    });

    it.each([
        ['NOT a timestamp', 123],
        ['Date is invalid', new Date(NaN)],
        ['NOT according to ISO 8601', '123'],
        ['NOT according to ISO 8601', '2021-13-32'],
        ['NOT according to ISO 8601', '2021-10-10 93546'],
    ])('should throw if %s with value %s', (expectedMsg, input) => {
        const error = expectToThrow(() => act(input));

        expect(error.message).toContain(expectedMsg);
    });
});
