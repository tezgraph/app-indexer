import { container as globalContainer, DependencyContainer, injectable, singleton } from 'tsyringe';

import { expectToThrow } from '../../../../test-utilities/mocks';
import { injectExplicit, resolveWithExplicitArgs } from '../../src/dependency-injection/explicit-args';

@singleton()
class BarDependency {}

class Explicit0 {}
class Explicit1 {}

@injectable()
class Foo {
    constructor(
        @injectExplicit(0) readonly explicit0: Explicit0,
        @injectExplicit(1) readonly explicit1: Explicit1,
        readonly regularlyInjected: BarDependency,
    ) {}
}

describe(resolveWithExplicitArgs.name, () => {
    let diContainer: DependencyContainer;

    beforeEach(() => {
        diContainer = globalContainer.createChildContainer();
    });

    it('should create with explicit args', () => {
        const dependency = diContainer.resolve(BarDependency);
        const explicitArg0 = new Explicit0();
        const explicitArg1 = new Explicit1();

        const foo = resolveWithExplicitArgs(diContainer, Foo, [explicitArg0, explicitArg1]);

        expect(foo).toBeInstanceOf(Foo);
        expect(foo.explicit0).toBe(explicitArg0);
        expect(foo.explicit1).toBe(explicitArg1);
        expect(foo.regularlyInjected).toBe(dependency);
        expectToThrow(() => diContainer.resolve(Foo)); // Should not pollute parent container.
    });

    it('should create a new instance each time', () => {
        const foo1 = resolveWithExplicitArgs(diContainer, Foo, [new Explicit0(), new Explicit1()]);
        const foo2 = resolveWithExplicitArgs(diContainer, Foo, [new Explicit0(), new Explicit1()]);

        expect(foo1).not.toBe(foo2);
    });

    it('should throw if resolved directly without explicit args', () => {
        expectToThrow(() => diContainer.resolve(Foo));
    });
});
