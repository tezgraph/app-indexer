import { DependencyContainer, inject, singleton } from 'tsyringe';

import { LoggerImpl } from './logger-impl';
import { argGuard } from '../../basics/public-api';
import { DI_CONTAINER_DI_TOKEN, resolveWithExplicitArgs } from '../../dependency-injection/public-api';
import { Logger } from '../logger';
import { LoggerCategory, LoggerFactory } from '../logger-factory';

/** Creates a Logger with given category. */
@singleton()
export class LoggerFactoryImpl implements LoggerFactory {
    constructor(
        @inject(DI_CONTAINER_DI_TOKEN) private readonly diContainer: DependencyContainer,
    ) {}

    getLogger(category: LoggerCategory): Logger {
        const categoryStr = typeof category === 'string'
            ? argGuard.nonWhiteSpaceString(category, 'category')
            : argGuard.function(category, 'category').name;

        return resolveWithExplicitArgs(this.diContainer, LoggerImpl, [categoryStr]);
    }
}
