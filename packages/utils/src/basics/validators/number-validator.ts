import { NumberValidatorImpl } from './number-validator-impl';
import { typeValidator } from './type-validator';
import { Validator } from './validator';
import { protectAsPublicApi } from './validator-utils';

export interface NumberLimits {
    readonly integer?: boolean;
    readonly min?: number;
    readonly max?: number;
}

/** Low-level validation of a `number`. */
export interface NumberValidator extends Validator<number, NumberLimits | void> {}

/** Static instance of {@link NumberValidator}. */
export const numberValidator: NumberValidator = protectAsPublicApi(new NumberValidatorImpl(typeValidator));
