import { describeMemberFactory, expectToThrow } from '../../../../test-utilities/mocks';
import { ArgError } from '../../../utils/src/basics/public-api';
import { FluentArgGuardImpl, fluentGuard } from '../../src/helpers/fluent-guard';

describe(fluentGuard.name, () => {
    let received: unknown[];
    const ARG_NAME = 'foo';

    beforeEach(() => {
        received = [];
    });

    const describeMember = describeMemberFactory<FluentArgGuardImpl>();

    describeMember('number', () => {
        it('should pass if correct value', () => {
            fluentGuard(123, ARG_NAME).number({ min: 20 });
        });

        it('should throw if invalid value', () => {
            const target = fluentGuard(12, ARG_NAME);

            runThrowTest(() => target.number({ min: 20 }), 12);
        });
    });

    describeMember('string', () => {
        it('should pass if correct value', () => {
            fluentGuard('abc', ARG_NAME).string('NotEmpty');
        });

        it('should throw if invalid value', () => {
            const target = fluentGuard('', ARG_NAME);

            runThrowTest(() => target.string('NotEmpty'), '');
        });
    });

    describeMember('array', () => {
        it('should pass if correct value', () => {
            fluentGuard(['x', 'y'], 'foos').array(g => received.push(g));

            expect(received).toHaveLength(2);
            expect(received[0]).toBeInstanceOf(FluentArgGuardImpl);
            expect(received[1]).toBeInstanceOf(FluentArgGuardImpl);
            expect(received[0]).toMatchObject<Partial<FluentArgGuardImpl>>({ argName: 'foos[0]', value: 'x' });
            expect(received[1]).toMatchObject<Partial<FluentArgGuardImpl>>({ argName: 'foos[1]', value: 'y' });
        });

        it('should throw if invalid value', () => {
            const target = fluentGuard('wtf' as any as unknown[], ARG_NAME);

            runThrowTest(() => target.array(g => received.push(g)), 'wtf');
        });
    });

    describeMember('object', () => {
        it('should pass if correct value', () => {
            const target = fluentGuard({ val: 123 }, ARG_NAME);

            target.object(g => received.push(g));

            expect(received).toHaveLength(1);
            expect(received[0]).toBe(target);
        });

        it('should throw if invalid value', () => {
            const target = fluentGuard('wtf' as any as object, ARG_NAME);

            runThrowTest(() => target.object(g => received.push(g)), 'wtf');
        });
    });

    describeMember('property', () => {
        it('should get guard for property', () => {
            const propertyGuard = fluentGuard({ val: 123 }, 'foo').property('val');

            expect(propertyGuard).toBeInstanceOf(FluentArgGuardImpl);
            expect(propertyGuard).toMatchObject<Partial<FluentArgGuardImpl>>({ argName: 'foo.val', value: 123 });
        });

        it('should throw if invalid parent obj', () => {
            const target = fluentGuard('wtf' as any as { val: number }, ARG_NAME);

            runThrowTest(() => target.property('val'), 'wtf');
        });
    });

    function runThrowTest(act: () => void, expectedValue: unknown) {
        const error = expectToThrow(act, ArgError);

        expect(error.argName).toBe(ARG_NAME);
        expect(error.specifiedValue).toBe(expectedValue);
        expect(received).toBeEmpty();
    }
});
