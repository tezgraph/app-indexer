import { BigMapDiffAction, TaquitoRpcBigMapDiff } from './base-big-map-diff';
import { BigMapAlloc } from './big-map-diff-alloc';
import { BigMapCopy } from './big-map-diff-copy';
import { BigMapRemove } from './big-map-diff-remove';
import { BigMapUpdate } from './big-map-diff-update';
import { EntityToIndex } from '../../entity-to-index';

/** @category RPC Block Data */
export interface LazyBigMapDiff extends EntityToIndex<TaquitoRpcBigMapDiff> {
    readonly action: BigMapDiffAction;

    getBigMapDiff(): Promise<BigMapDiff>;
}

/**
 * The diff of the big map. It is the discriminated union which can be narrowed by its `action`.
 * @category RPC Block Data
 */
export type BigMapDiff = BigMapAlloc | BigMapCopy | BigMapRemove | BigMapUpdate;
