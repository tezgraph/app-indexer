import { StrictExclude } from 'ts-essentials';

import { OperationResultStatus } from '../../../../src/rpc-data/operations/common/operation-result/operation-result-status';
import { MainOperation } from '../../../../src/rpc-data/operations/main-operation';
import {
    isMainOperationWithBalanceUpdatesInResult,
    MainOperationWithBalanceUpdatesInResult,
} from '../../../../src/rpc-data/operations/narrowed/main-operation-with-balance-updates-in-result';

describe(isMainOperationWithBalanceUpdatesInResult.name, () => {
    const positiveTestCases: Record<MainOperationWithBalanceUpdatesInResult['kind'], true> = {
        IncreasePaidStorage: true,
        Origination: true,
        RegisterGlobalConstant: true,
        SmartRollupExecuteOutboxMessage: true,
        SmartRollupOriginate: true,
        SmartRollupPublish: true,
        SmartRollupRecoverBond: true,
        SmartRollupRefute: true,
        SmartRollupTimeout: true,
        Transaction: true,
        TransferTicket: true,
    };
    const negativeTestCases: Record<StrictExclude<MainOperation['kind'], MainOperationWithBalanceUpdatesInResult['kind']>, false> = {
        ActivateAccount: false,
        Ballot: false,
        Delegation: false,
        DoubleBakingEvidence: false,
        DoubleEndorsementEvidence: false,
        DoublePreendorsementEvidence: false,
        DrainDelegate: false,
        Endorsement: false,
        Preendorsement: false,
        Proposals: false,
        Reveal: false,
        SeedNonceRevelation: false,
        SetDepositsLimit: false,
        SmartRollupAddMessages: false,
        SmartRollupCement: false,
        UpdateConsensusKey: false,
        VdfRevelation: false,
    };

    it.each(
        Object.entries({ ...positiveTestCases, ...negativeTestCases }),
    )('should narrow %s to %s', (kind, expectedNarrowing) => {
        const operation = { kind, result: { status: OperationResultStatus.Applied } } as MainOperation;

        const isNarrowed = isMainOperationWithBalanceUpdatesInResult(operation);

        expect(isNarrowed).toBe(expectedNarrowing);
    });
});
