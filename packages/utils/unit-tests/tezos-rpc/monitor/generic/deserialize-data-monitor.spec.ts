import { asyncToArray, asyncWrap } from 'iter-tools';
import { instance, mock, when } from 'ts-mockito';

import { TestLogger } from '../../../../../../test-utilities/mocks';
import { AsyncIterableProvider } from '../../../../src/basics/public-api';
import {
    DeserializeDataMonitor,
    ItemsDeserializer,
} from '../../../../src/tezos-rpc/monitor/generic/deserialize-data-monitor';

describe(DeserializeDataMonitor.name, () => {
    let target: AsyncIterableProvider<string>;
    let logger: TestLogger;
    let deserializer: ItemsDeserializer<string>;
    let nextProvider: AsyncIterableProvider<unknown>;

    beforeEach(() => {
        [deserializer, nextProvider] = [mock(), mock()];
        logger = new TestLogger();
        target = new DeserializeDataMonitor(logger, instance(deserializer), instance(nextProvider));
    });

    it('should iterate deserialized items', async () => {
        when(nextProvider.iterate()).thenReturn(asyncWrap([111, 222]));
        when(deserializer.deserialize(111)).thenReturn(['a1', 'a2']);
        when(deserializer.deserialize(222)).thenReturn(['b']);

        const items = await asyncToArray(target.iterate());

        expect(items).toEqual(['a1', 'a2', 'b']);
        logger.verifyNothingLogged();
    });

    it('should log error if item deserialization failed and continue', async () => {
        const error = new Error('Oups');
        when(nextProvider.iterate()).thenReturn(asyncWrap([111, 222]));
        when(deserializer.deserialize(111)).thenThrow(error);
        when(deserializer.deserialize(222)).thenReturn(['b']);

        const items = await asyncToArray(target.iterate());

        expect(items).toEqual(['b']);
        logger.loggedSingle().verify('Error', { error, data: 111 });
    });
});
