import { anyOfClass, deepEqual, instance, mock, when } from 'ts-mockito';

import { ContextData, DbContext, RollupData, TestLogger } from '../../../../../test-utilities/mocks';
import { keyof } from '../../../../utils/src/public-api';
import { ClientIndexerErrorHandler, ErrorHandlingOptions } from '../../../src/client-indexers/error-handling/client-indexer-error-handler';
import {
    createErrorHandlingSmartRollupIndexer,
    IndexAddMessagesOptions,
    IndexMethodOptions,
    ShouldIndexOptions,
} from '../../../src/client-indexers/error-handling/error-handling-smart-rollup-indexer';
import { SmartRollupIndexer } from '../../../src/client-indexers/smart-rollup-indexer';
import { IndexerInfo } from '../../../src/client-indexers/wrappers/indexer-info';
import {
    Applied,
    EntityToIndex,
    SmartRollupAddMessagesOperation,
    SmartRollupCementOperation,
} from '../../../src/rpc-data/rpc-data-interfaces';
import { SmartRollup } from '../../../src/smart-rollups/smart-rollup';

describe(createErrorHandlingSmartRollupIndexer.name, () => {
    let wrappedIndexer: SmartRollupIndexer<DbContext, ContextData, RollupData>;
    let info: IndexerInfo;
    let errorHandler: ClientIndexerErrorHandler;

    beforeEach(() => {
        wrappedIndexer = 'mockedWrappedIndexer' as any;
        errorHandler = mock();
        info = 'mockedInfo' as any;
    });

    it('should wrap indexer correctly', () => {
        setupErrorHandling('shouldIndex', anyOfClass(ShouldIndexOptions), 'mocked-shouldIndex');
        setupErrorHandling('indexAddMessages', anyOfClass(IndexAddMessagesOptions), 'mocked-indexAddMessages');
        setupIndexErrorHandling('indexCement', 'mocked-indexCement');
        setupIndexErrorHandling('indexExecuteOutboxMessage', 'mocked-indexExecuteOutboxMessage');
        setupIndexErrorHandling('indexOriginate', 'mocked-indexOriginate');
        setupIndexErrorHandling('indexPublish', 'mocked-indexPublish');
        setupIndexErrorHandling('indexRecoverBond', 'mocked-indexRecoverBond');
        setupIndexErrorHandling('indexRefute', 'mocked-indexRefute');
        setupIndexErrorHandling('indexTimeout', 'mocked-indexTimeout');

        const resultIndexer = createErrorHandlingSmartRollupIndexer(wrappedIndexer, info, instance(errorHandler));

        expect(resultIndexer).toEqual<typeof resultIndexer>({
            info,
            shouldIndex: 'mocked-shouldIndex' as any,
            indexAddMessages: 'mocked-indexAddMessages' as any,
            indexCement: 'mocked-indexCement' as any,
            indexExecuteOutboxMessage: 'mocked-indexExecuteOutboxMessage' as any,
            indexOriginate: 'mocked-indexOriginate' as any,
            indexPublish: 'mocked-indexPublish' as any,
            indexRecoverBond: 'mocked-indexRecoverBond' as any,
            indexRefute: 'mocked-indexRefute' as any,
            indexTimeout: 'mocked-indexTimeout' as any,
        });
    });

    function setupIndexErrorHandling(method: (keyof typeof wrappedIndexer) & `index${string}`, mockedResult: any) {
        setupErrorHandling(method, deepEqual(new IndexMethodOptions(method)), mockedResult);
    }

    function setupErrorHandling(method: keyof typeof wrappedIndexer, expectedOptions: ErrorHandlingOptions<any, any>, mockedResult: any) {
        when(errorHandler.wrap<any, any, any>(wrappedIndexer, method, info, expectedOptions)).thenReturn(mockedResult);
    }
});

describe(IndexMethodOptions.name, () => {
    let target: IndexMethodOptions<EntityToIndex, unknown, { rollup: SmartRollup }>;
    let operation: SmartRollupCementOperation;
    let indexingContext: { rollup: SmartRollup };

    beforeEach(() => {
        target = new IndexMethodOptions('indexFooBar');
        operation = { uid: 'uuu', sourceAddress: 'src' } as typeof operation;
        indexingContext = { rollup: { address: 'sr1R23ax3Gj8NDQFbQRfNnzuKEZhth5qvWVP' } } as typeof indexingContext;
    });

    it(`${keyof<typeof target>('getArgsToReport')} should get correct args`, () => {
        const args = target.getArgsToReport(operation, 'db', indexingContext);

        expect(args).toEqual({ uid: 'uuu', rollup: 'sr1R23ax3Gj8NDQFbQRfNnzuKEZhth5qvWVP' });
    });

    it(`${keyof<typeof target>('getArgsToTrace')} should get correct args`, () => {
        const args = target.getArgsToTrace(operation, 'db', indexingContext);

        expect(args).toEqual({ fooBarOperation: operation, indexingContext });
    });
});

describe(ShouldIndexOptions.name, () => {
    const target = new ShouldIndexOptions();

    it.each([
        ['false if false', false, false],
        ['true if valid', 'mockedData', true],
    ])(`${keyof<typeof target>('afterExecuted')} should log %s rollup data`, (_desc, rollupData, expectedWillIndex) => {
        const logger = new TestLogger();

        const logData = target.afterExecuted.logMessageArgs(rollupData);
        logger.logDebug(target.afterExecuted.logMessage, logData);

        logger.loggedSingle().verifyData({ willIndex: expectedWillIndex });
    });

    it(`${keyof<typeof target>('getArgsToReport')} should get correct args`, () => {
        const rollup = { address: 'sr1R23ax3Gj8NDQFbQRfNnzuKEZhth5qvWVP' } as SmartRollup;

        const args = target.getArgsToReport(rollup, 'db');

        expect(args).toEqual({ rollup: 'sr1R23ax3Gj8NDQFbQRfNnzuKEZhth5qvWVP' });
    });

    it(`${keyof<typeof target>('getArgsToTrace')} should get correct args`, () => {
        const args = target.getArgsToTrace('mockedRollup' as any, 'db');

        expect(args).toEqual({ rollup: 'mockedRollup' });
    });
});

describe(IndexAddMessagesOptions.name, () => {
    const target = new IndexAddMessagesOptions();

    it(`${keyof<typeof target>('getArgsToReport')} should get correct args`, () => {
        const operation = { uid: 'uuu', sourceAddress: 'src' } as Applied<SmartRollupAddMessagesOperation>;

        const args = target.getArgsToReport(operation, 'db', 'ctx' as any);

        expect(args).toEqual({ uid: 'uuu' });
    });

    it(`${keyof<typeof target>('getArgsToTrace')} should get correct args`, () => {
        const args = target.getArgsToTrace('mockedOperation' as any, 'db', 'ctx' as any);

        expect(args).toEqual({
            addMessagesOperation: 'mockedOperation',
            indexingContext: 'ctx',
        });
    });
});
