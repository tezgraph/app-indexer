import { Config } from '@jest/types';

export const config: Config.InitialOptions = {
    preset: 'ts-jest',
    verbose: true,
    testEnvironment: 'node',
    testMatch: ['<rootDir>/**/*.spec.ts'],
    setupFilesAfterEnv: ['jest-extended/all', 'reflect-metadata'],
    watchPlugins: [
        'jest-watch-typeahead/filename',
        'jest-watch-typeahead/testname',
    ],
    reporters: [
        'default', [
            'jest-junit', {
                outputDirectory: '<rootDir>/coverage',
                uniqueOutputName: 'true',
            },
        ],
    ],
    rootDir: '../',
    roots: ['<rootDir>/unit-tests'],
    collectCoverage: true,
    coverageReporters: ['json', 'text-summary', 'html', 'cobertura'],
    coverageDirectory: '<rootDir>/coverage',
    collectCoverageFrom: [
        '<rootDir>/src/**/*.ts',
        '!<rootDir>/unit-tests/*',
    ],
    maxWorkers: 10,
    forceExit: true,
    testTimeout: 30_000,
};
