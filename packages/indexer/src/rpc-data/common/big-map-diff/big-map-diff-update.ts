import { BaseBigMapDiff, BigMapDiffAction } from './base-big-map-diff';
import { BigMapInfo } from '../../../contracts/data/big-map-info/big-map-info';
import { LazyMichelsonValue } from '../michelson/lazy-michelson-value';

/**
 * Corresponds to each one of `updates` in the RPC schema:
 * ```
 * { // big_map
 *      "kind": "big_map",
 *      "id": $008-PtEdo2Zk.big_map_id,
 *      "diff":
 *          { // update
 *              "action": "update",
 *              "updates":
 *                  [ { "key_hash": $script_expr,
 *                      "key": $micheline.008-PtEdo2Zk.michelson_v1.expression,
 *                      "value"?: $micheline.008-PtEdo2Zk.michelson_v1.expression } ... ] }
 *          || { // copy
 *              "action": "copy",
 *              "source": $008-PtEdo2Zk.big_map_id,
 *              "updates":
 *                  [ { "key_hash": $script_expr,
 *                      "key": $micheline.008-PtEdo2Zk.michelson_v1.expression,
 *                      "value"?: $micheline.008-PtEdo2Zk.michelson_v1.expression } ... ] }
 *          || { // alloc
 *              "action": "alloc",
 *              "updates":
 *                  [ { "key_hash": $script_expr,
 *                      "key": $micheline.008-PtEdo2Zk.michelson_v1.expression,
 *                      "value"?: $micheline.008-PtEdo2Zk.michelson_v1.expression } ... ],
 *              "key_type": $micheline.008-PtEdo2Zk.michelson_v1.expression,
 *              "value_type": $micheline.008-PtEdo2Zk.michelson_v1.expression } }
 * ```
 *
 * and until *Delphi* protocol (including) to RPC schema:
 * ```
 * { // update
 *      "action": "update",
 *      "big_map": $bignum,
 *      "key_hash": $script_expr,
 *      "key": $micheline.007-PsDELPH1.michelson_v1.expression,
 *      "value"?: $micheline.007-PsDELPH1.michelson_v1.expression }
 * ```
 * @category RPC Block Data
 */
export interface BigMapUpdate extends BaseBigMapDiff {
    // Narrowing properties:

    /** Usable for narrowing the type of this object from {@link BigMapDiff}. */
    readonly action: BigMapDiffAction.Update;

    // Specific properties:

    /** It is `null` if the corresponding ID is negative - the big map existed only within the block as temporary. */
    readonly bigMap: BigMapInfo | null;

    readonly keyHash: string;

    /** It is `null` if {@link bigMap} is `null`. */
    readonly key: LazyMichelsonValue | null;

    /** It is `null` if the entry for {@link key} is removed from the {@link bigMap} or {@link bigMap} is `null`. */
    readonly value: LazyMichelsonValue | null;

    // Null properties:

    /** Always `null` for this type so that the property is directly available on {@link BigMapDiff} union. */
    readonly destinationBigMap: null;

    /** Always `null` for this type so that the property is directly available on {@link BigMapDiff} union. */
    readonly keyType: null;

    /** Always `null` for this type so that the property is directly available on {@link BigMapDiff} union. */
    readonly sourceBigMap: null;

    /** Always `null` for this type so that the property is directly available on {@link BigMapDiff} union. */
    readonly valueType: null;
}
