import { indexRefute } from '../../../src/decorators/smart-rollup/index-refute-decorator';

describe(indexRefute.name, () => {
    it('should pass if correctly declared', () => {
        indexRefute();
    });
});
