import { typed } from '@tezos-dappetizer/utils';

import { BaseConversion, createCategoryMapping } from './base-conversion';
import { primitives } from '../../../../primitives';
import {
    BalanceUpdate,
    BalanceUpdateBurned,
    BalanceUpdateBurnedLostRewards,
    BalanceUpdateCategory,
    BalanceUpdateKind,
} from '../../../../rpc-data-interfaces';
import * as RpcCategories from '../rpc-categories';

export class BurnedConversion extends BaseConversion {
    override convert(): BalanceUpdate {
        const category = this.convertCategory(categoryMapping);

        switch (category) {
            case BalanceUpdateCategory.LostAttestingRewards:
            case BalanceUpdateCategory.LostEndorsingRewards:
                return typed<BalanceUpdateBurnedLostRewards>({
                    ...this.getBlueprint(category),
                    ...this.convertOriginProperties(),
                    kind: BalanceUpdateKind.Burned,
                    delegateAddress: this.convertDelegateAddress(),
                    participation: primitives.boolean.convertProperty(this.rpcData, 'participation', this.uid),
                    revelation: primitives.boolean.convertProperty(this.rpcData, 'revelation', this.uid),
                });
            default:
                return typed<BalanceUpdateBurned>({
                    ...this.getBlueprint(category),
                    ...this.convertOriginProperties(),
                    kind: BalanceUpdateKind.Burned,
                });
        }
    }
}

const categoryMapping = createCategoryMapping<RpcCategories.Burned, BalanceUpdateKind.Burned>({
    burned: BalanceUpdateCategory.Burned,
    'lost attesting rewards': BalanceUpdateCategory.LostAttestingRewards,
    'lost endorsing rewards': BalanceUpdateCategory.LostEndorsingRewards,
    punishments: BalanceUpdateCategory.Punishments,
    smart_rollup_refutation_punishments: BalanceUpdateCategory.SmartRollupRefutationPunishments,
    'storage fees': BalanceUpdateCategory.StorageFees,
});
