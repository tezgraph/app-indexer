import { DappetizerConfigUsingDb } from '@tezos-dappetizer/database';

import { tezosNodeUrlForIntegrationTests } from '../../../../test-utilities/int-tezos-node';

const config: DappetizerConfigUsingDb = {
    database: {
        type: 'postgres',
        host: process.env.DB_HOST || 'localhost',
        port: 5432,
        username: 'postgres',
        password: 'postgrespassword',
        database: 'postgres',
        schema: 'hasura_integration_tests',
    },
    hasura: {
        url: `http://${process.env.HASURA_HOST || 'localhost'}:8080`,
        adminSecret: 'adminsecret',
        autotrackEntities: true,
        dropExistingTracking: true,
        customMappings: [{
            tableName: 'bar',
            graphQLTypeName: 'Baz',
            graphQLFieldName: 'baz',
            columns: [
                { columnName: 'key', graphQLName: 'keyyy' },
                { columnName: 'bar_value', graphQLName: 'barValueee' },
                { columnName: 'foo_key', hidden: true },
            ],
            relationships: [{
                type: 'manyToOne',
                name: 'foooo',
                referencedTableName: 'foo',
                columns: ['foo_key'],
                referencedColumns: ['key'],
            }]
        }],
    },
    modules: [
        { id: './indexer/dist' },
    ],
    networks: {
        mainnet: {
            indexing: { fromBlockLevel: 3_000_000 },
            tezosNode: { url: tezosNodeUrlForIntegrationTests },
        },
    },
    usageStatistics: {
        enabled: false,
    },
};

export default config;
