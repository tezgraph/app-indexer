export interface FileGenerator<TOptions> {
    getRelPath(options: TOptions): string;

    generateContents(options: TOptions, existingContents: string | null): string;
}
