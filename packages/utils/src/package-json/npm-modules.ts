export const npmModules = {
    BIGNUMBER: 'bignumber.js',
    dappetizer: {
        CLI: '@tezos-dappetizer/cli',
        DATABASE: '@tezos-dappetizer/database',
        DECORATORS: '@tezos-dappetizer/decorators',
        INDEXER: '@tezos-dappetizer/indexer',
        UTILS: '@tezos-dappetizer/utils',
    },
    taquito: {
        TAQUITO: '@taquito/taquito',
        MICHELSON_ENCODER: '@taquito/michelson-encoder',
    },
    TYPEORM: 'typeorm',
    TYPESCRIPT: 'typescript',
} as const;
