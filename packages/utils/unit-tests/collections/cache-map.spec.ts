/* eslint-disable deprecation/deprecation */
import { times } from 'lodash';
import { deepEqual, anything, instance, mock, verify, when } from 'ts-mockito';

import { CacheMap } from '../../src/collections/cache-map';

interface TestFactory {
    create(key: Key): Value;
}

interface Key {
    readonly str: string;
}

interface Value {
    readonly num: number;
}

describe(CacheMap.name, () => {
    let target: CacheMap<Key, Value>;
    let factory: TestFactory;

    beforeEach(() => {
        factory = mock();
        target = new CacheMap({
            stringifyKey: key => key.str,
            itemFactory: key => instance(factory).create(key),
        });
    });

    it('should be created correctly', () => {
        expect(target).toBeFrozen();
    });

    it('should create and cache the value', () => {
        const value: Value = { num: 123 };
        when(factory.create(anything())).thenReturn(value);

        times(3, () => {
            expect(target.get({ str: 'foo' })).toBe(value);
        });

        verify(factory.create(anything())).once();
        verify(factory.create(deepEqual({ str: 'foo' }))).once();
    });

    it('should cache values by key', () => {
        const fooValue: Value = { num: 111 };
        const barValue: Value = { num: 222 };
        when(factory.create(deepEqual({ str: 'foo' }))).thenReturn(fooValue);
        when(factory.create(deepEqual({ str: 'bar' }))).thenReturn(barValue);

        times(3, () => {
            expect(target.get({ str: 'foo' })).toBe(fooValue);
            expect(target.get({ str: 'bar' })).toBe(barValue);
        });

        verify(factory.create(anything())).times(2);
    });
});
