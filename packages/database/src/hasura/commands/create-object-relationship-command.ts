import { keysOf } from '@tezos-dappetizer/utils';

import { HasuraCommand } from './hasura-command';
import { HasuraRelationshipMapping } from '../hasura-mappings';

export class CreateObjectRelationshipCommand implements HasuraCommand {
    readonly description: string;

    constructor(readonly args: Readonly<{
        hasuraSource: string;
        dbSchema: string;
        tableName: string;
        relationship: HasuraRelationshipMapping;
    }>) {
        this.description = `create GraphQL object relationship '${this.args.relationship.graphQLName}' on table '${this.args.tableName}'`;
    }

    createPayload(): object {
        const hasCompoundKey = keysOf(this.args.relationship.sourceToForeignColumns).length > 1;
        const usingSection = hasCompoundKey || this.args.relationship.isCustom
            ? {
                manual_configuration: {
                    remote_table: {
                        schema: this.args.dbSchema,
                        name: this.args.relationship.foreignTableName,
                    },
                    column_mapping: this.args.relationship.sourceToForeignColumns,
                },
            }
            : {
                foreign_key_constraint_on: keysOf(this.args.relationship.sourceToForeignColumns),
            };

        return {
            type: 'pg_create_object_relationship',
            args: {
                source: this.args.hasuraSource,
                name: this.args.relationship.graphQLName,
                table: {
                    schema: this.args.dbSchema,
                    name: this.args.tableName,
                },
                using: usingSection,
            },
        };
    }
}
