import { anything, instance, mock, verify, when } from 'ts-mockito';
import { DataSource, DataSourceOptions } from 'typeorm';

import { expectToThrowAsync, TestLogger, verifyCalled } from '../../../../test-utilities/mocks';
import { DbSchemaInitializer } from '../../src/db-access/db-schema-initializer';
import { TypeormAbstraction } from '../../src/helpers/typeorm-abstraction';

describe(DbSchemaInitializer.name, () => {
    let target: DbSchemaInitializer;
    let typeorm: TypeormAbstraction;
    let logger: TestLogger;
    let createdDataSource: DataSource;

    beforeEach(() => {
        logger = new TestLogger();
        typeorm = mock();
        target = new DbSchemaInitializer(instance(typeorm), logger);

        createdDataSource = mock(DataSource);
        when(typeorm.newDataSource(anything())).thenReturn(instance(createdDataSource));
    });

    it.each([
        ['explicitly-specified', 'foo-schema', 'foo-schema'],
        ['default', '', 'public'],
    ])('should create %s schema', async (_desc, inputSchema, expectedSchema) => {
        const dataSource = { options: { type: 'postgres', database: 'ddd', schema: inputSchema } } as DataSource;

        await target.beforeSynchronization(dataSource);

        verifyCalled(typeorm.newDataSource).onceWith({ database: 'ddd', schema: undefined } as DataSourceOptions);
        verify(createdDataSource.query(`CREATE SCHEMA IF NOT EXISTS "${expectedSchema}"`)).once();
        verify(createdDataSource.destroy()).once();

        logger.verifyLoggedCount(2);
        logger.logged(0).verify('Information', { schema: expectedSchema }).verifyMessage('Creating');
        logger.logged(1).verify('Debug', { schema: expectedSchema }).verifyMessage('Created');
    });

    it('should destroy the data source even if schema init failed', async () => {
        const dataSource = { options: { type: 'postgres', schema: 'foo-schema' } } as DataSource;
        const queryError = new Error('oups');
        when(createdDataSource.query(anything())).thenReject(queryError);

        const error = await expectToThrowAsync(async () => target.beforeSynchronization(dataSource));

        expect(error).toBe(queryError);
        verify(createdDataSource.destroy()).once();
        logger.verifyLoggedCount(1);
    });

    it('should do nothing if schema not supported', async () => {
        const dataSource = { options: { type: 'sqlite' } } as DataSource;

        await target.beforeSynchronization(dataSource);

        verify(typeorm.newDataSource(anything())).never();
        logger.loggedSingle().verify('Information', { type: 'sqlite' }).verifyMessage('not support schema');
    });
});
