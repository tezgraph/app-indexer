import { anything, instance, mock, when } from 'ts-mockito';

import { verifyCalled } from '../../../../../test-utilities/mocks';
import { AbortControllerFactory } from '../../../src/app-control/abort-controller-factory';
import { AppShutdownManager } from '../../../src/app-control/app-shutdown-manager';
import { AppShutdownHttpBackend } from '../../../src/tezos-rpc/http-backends/app-shutdown-http-backend';
import { TaquitoHttpBackend } from '../../../src/tezos-rpc/http-infrastructure/taquito-http-backend';
import { TaquitoHttpRequest } from '../../../src/tezos-rpc/http-infrastructure/taquito-http-request';

describe(AppShutdownHttpBackend.name, () => {
    let target: AppShutdownHttpBackend;
    let nextBackend: TaquitoHttpBackend;
    let appShutdownManager: AppShutdownManager;
    let abortControllerFactory: AbortControllerFactory;

    let request: TaquitoHttpRequest;
    let appSignal: AbortSignal;

    const act = async () => target.execute(request);

    beforeEach(() => {
        [nextBackend, appShutdownManager, abortControllerFactory] = [mock(), mock(), mock()];
        target = new AppShutdownHttpBackend(instance(nextBackend), instance(appShutdownManager), instance(abortControllerFactory));

        request = { url: 'http://tezos/blocks' } as TaquitoHttpRequest;
        appSignal = 'mockedAppSignal' as any;

        when(appShutdownManager.signal).thenReturn(appSignal);
        when(nextBackend.execute(anything())).thenResolve('resultVal');
    });

    it('should set app signal if request has none', async () => {
        const result = await act();

        expect(result).toBe('resultVal');
        verifyCalled(nextBackend.execute).onceWith({
            signal: appSignal,
            url: 'http://tezos/blocks',
        } as TaquitoHttpRequest);
    });

    it('should link app signal with request signal', async () => {
        const requestSignal: AbortSignal = 'mockedRequestSignal' as any;
        const linkedSignal: AbortSignal = 'mockedLinkedSignal' as any;
        request.signal = requestSignal;
        when(abortControllerFactory.createLinked(requestSignal, appSignal)).thenReturn({ signal: linkedSignal } as AbortController);

        const result = await act();

        expect(result).toBe('resultVal');
        verifyCalled(nextBackend.execute).onceWith({
            signal: linkedSignal,
            url: 'http://tezos/blocks',
        } as TaquitoHttpRequest);
    });
});
