import { JsonFetcher } from '@tezos-dappetizer/utils';
import { concat } from 'iter-tools';

import { LastIndexedLevelQuery } from './queries/last-indexed-level-query';
import { OriginationBlockLevelsQuery } from './queries/origination-block-levels-query';
import { TransactionBlockLevelsQuery } from './queries/transaction-block-levels-query';
import { TzktContractsBoostConfig } from '../../../config/indexing/indexing-config';
import { BoostedLevels, BoostLevelsOptions, ContractIndexingBoost } from '../contract-indexing-boost';

export class TzktIndexingBoost implements ContractIndexingBoost {
    readonly name = 'TzKT';

    constructor(
        private readonly fetcher: JsonFetcher,
        private readonly config: TzktContractsBoostConfig,
        private readonly contractAddresses: readonly string[],
    ) {}

    async getChainId(): Promise<string> {
        const block = await this.fetcher.execute(new LastIndexedLevelQuery(this.config.apiUrl));
        return block.chainId;
    }

    async getLevelsWithContracts(options: BoostLevelsOptions): Promise<BoostedLevels> {
        const [lastIndexedBlock, originationLevels, transactionsResponse] = await Promise.all([
            this.fetcher.execute(new LastIndexedLevelQuery(this.config.apiUrl)),
            this.fetcher.execute(new OriginationBlockLevelsQuery(this.config.apiUrl, this.contractAddresses, options)),
            this.fetcher.execute(new TransactionBlockLevelsQuery(this.config.apiUrl, this.contractAddresses, options)),
        ]);

        return {
            levelsWithContracts: new Set<number>(concat(originationLevels, transactionsResponse.levels)),
            hasMore: transactionsResponse.hasMore,
            lastLoadedLevel: transactionsResponse.hasMore
                // Ignores origination levels b/c contract X can be originated after many transactions of contract Y which would be skipped.
                ? Math.max(...transactionsResponse.levels)
                : lastIndexedBlock.level,
        };
    }
}
