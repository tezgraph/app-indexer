import { InjectionToken } from 'tsyringe';

import { MetricsUpdater } from './metrics-updater';

export const METRICS_UPDATERS_DI_TOKEN: InjectionToken<MetricsUpdater> = 'Dappetizer:Utils:MetricsUpdater';
