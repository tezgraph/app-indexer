import { BLOCK_COUNT, getConfig } from './dappetizer.base.config';

export default getConfig({
    protocol: 'edo',
    firstLevel: 1_343_489,
    lastLevel: 1_466_367,

    // Aliquot according to 122_878 blocks of this protocol vs other protocols.
    levelRangeSizeToGenerate: 1 * BLOCK_COUNT,
});
