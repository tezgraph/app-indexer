import path from 'path';
import { inject, registry, singleton } from 'tsyringe';

import { NodeJSRequireConfigValueLoader } from './nodejs-require-config-value-loader';
import { RootConfigFileResolver } from './root-config-file-resolver';
import { RootConfigValueLoader } from './root-config-value-loader';
import { TypeScriptConfigValueLoader } from './typescript-config-value-loader';
import { dumpToString, errorToString, isObjectLiteral, typed } from '../../basics/public-api';
import { ConfigObjectElement } from '../config-element';

const LOADERS_DI_TOKEN = 'Dappetizer:Utils:RootConfigProviderFactory:ValueLoaders';

type ValueLoadersByExtension = Readonly<Record<string, RootConfigValueLoader>>;

@singleton()
@registry([
    {
        token: LOADERS_DI_TOKEN,
        useFactory: c => typed<ValueLoadersByExtension>({
            '.ts': c.resolve(TypeScriptConfigValueLoader),
            '.js': c.resolve(NodeJSRequireConfigValueLoader),
            '.json': c.resolve(NodeJSRequireConfigValueLoader),
        }),
    },
])
export class RootConfigProviderFactory {
    constructor(
        private readonly fileResolver: RootConfigFileResolver,
        @inject(LOADERS_DI_TOKEN) private readonly valueLoadersByExtension: ValueLoadersByExtension,
    ) {}

    create(): ConfigObjectElement {
        const filePath = this.fileResolver.resolveAbsPath();
        try {
            const valueloader = this.valueLoadersByExtension[path.extname(filePath).toLowerCase()];
            if (!valueloader) {
                throw new Error(`File type/extension is not supported.`
                    + ` Supported extensions: ${dumpToString(Object.keys(this.valueLoadersByExtension))}`);
            }

            const configValue = valueloader.loadValue(filePath);
            if (!isObjectLiteral(configValue)) {
                throw new Error(`It must be an object literal but it is '${typeof configValue}'.`);
            }

            return new ConfigObjectElement(configValue, filePath, '$');
        } catch (error) {
            throw new Error(`Invalid configuration in file '${filePath}'. ${errorToString(error, { onlyMessage: true })}`);
        }
    }
}
