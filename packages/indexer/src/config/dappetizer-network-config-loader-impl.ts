import { errorToString } from '@tezos-dappetizer/utils';
import nodeFS from 'fs';
import path from 'path';

import { DappetizerConfig } from './dappetizer-config';

export const PREFIX = 'dappetizer.';
export const SUFFIX = '.config.json';

export function loadNetworkConfigsImpl(
    dirPath: string,
    fs = nodeFS,
    nodeJS: { readonly require: NodeJS.Require } = { require },
): DappetizerConfig['networks'] {
    try {
        const dirItemNames = fs.readdirSync(dirPath);
        const configFileNames = dirItemNames.filter(f => f.startsWith(PREFIX) && f.endsWith(SUFFIX) && f.length > (PREFIX + SUFFIX).length);
        if (!configFileNames.length) {
            throw new Error(`The directory does not contain any file starting with '${PREFIX}' and ending with '${SUFFIX}'.`
                + ` Is it the correct directory?`);
        }

        const networkConfigs = configFileNames.map(fileName => {
            try {
                const network = fileName.slice(PREFIX.length, -1 * SUFFIX.length);
                const config = nodeJS.require(path.resolve(dirPath, fileName)) as DappetizerConfig['networks'][string];
                return [network, config] as const;
            } catch (error) {
                throw new Error(`Failed loading config from file '${fileName}'.`);
            }
        });
        return Object.fromEntries(networkConfigs);
    } catch (error) {
        throw new Error(`Failed loading network-specific parts of Dappetizer config from specified directory '${path.resolve(dirPath)}'.`
            + ` ${errorToString(error)}`);
    }
}
