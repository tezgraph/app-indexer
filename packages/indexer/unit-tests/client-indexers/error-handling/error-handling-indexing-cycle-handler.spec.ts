import { anyOfClass, deepEqual, instance, mock, when } from 'ts-mockito';

import { ContextData, DbContext } from '../../../../../test-utilities/mocks';
import { keyof } from '../../../../utils/src/public-api';
import { EmptyIndexingCycleHandler } from '../../../src/client-indexers/empty/empty-indexing-cycle-handler';
import {
    ClientIndexerErrorHandler,
    ErrorHandlingOptions,
} from '../../../src/client-indexers/error-handling/client-indexer-error-handler';
import {
    AfterBlockIndexedOptions,
    CreateContextOptions,
    createErrorHandlingIndexingCycleHandler,
    IndexersExecutionOptions,
    RollBackOptions,
} from '../../../src/client-indexers/error-handling/error-handling-indexing-cycle-handler';
import { IndexedBlock } from '../../../src/client-indexers/indexed-block';
import { IndexingCycleHandler } from '../../../src/client-indexers/indexing-cycle-handler';
import { IndexerInfo } from '../../../src/client-indexers/wrappers/indexer-info';
import { Block } from '../../../src/rpc-data/rpc-data-interfaces';

describe(createErrorHandlingIndexingCycleHandler.name, () => {
    let wrappedHandler: IndexingCycleHandler<DbContext, ContextData>;
    let info: IndexerInfo;
    let errorHandler: ClientIndexerErrorHandler;
    let emptyHandler: EmptyIndexingCycleHandler<DbContext, ContextData>;

    beforeEach(() => {
        wrappedHandler = 'mockedWrappedHandler' as any;
        errorHandler = mock();
        info = 'mockedInfo' as any;
        emptyHandler = { createContextData: () => 'mocked' as any } as typeof emptyHandler;
    });

    it('should wrap indexer correctly', () => {
        setupErrorHandling('createContextData', deepEqual(new CreateContextOptions(emptyHandler)), 'mocked-createContextData');
        setupErrorHandling('beforeIndexersExecute', anyOfClass(IndexersExecutionOptions), 'mocked-beforeIndexersExecute');
        setupErrorHandling('afterIndexersExecuted', anyOfClass(IndexersExecutionOptions), 'mocked-afterIndexersExecuted');
        setupErrorHandling('afterBlockIndexed', anyOfClass(AfterBlockIndexedOptions), 'mocked-afterBlockIndexed');
        setupErrorHandling('rollBackOnReorganization', anyOfClass(RollBackOptions), 'mocked-rollBackOnReorganization');

        const resultHandler = createErrorHandlingIndexingCycleHandler(wrappedHandler, info, instance(errorHandler), emptyHandler);

        expect(resultHandler).toEqual<typeof resultHandler>({
            createContextData: 'mocked-createContextData' as any,
            beforeIndexersExecute: 'mocked-beforeIndexersExecute' as any,
            afterIndexersExecuted: 'mocked-afterIndexersExecuted' as any,
            afterBlockIndexed: 'mocked-afterBlockIndexed' as any,
            rollBackOnReorganization: 'mocked-rollBackOnReorganization' as any,
        });
    });

    function setupErrorHandling(method: keyof typeof wrappedHandler, expectedOptions: ErrorHandlingOptions<any, any>, mockedResult: any) {
        when(errorHandler.wrap<any, any, any>(wrappedHandler, method, info, expectedOptions)).thenReturn(mockedResult);
    }
});

describe(CreateContextOptions.name, () => {
    let target: CreateContextOptions<unknown, unknown>;

    beforeEach(() => {
        const emptyHandler = mock<EmptyIndexingCycleHandler<unknown, unknown>>();
        when(emptyHandler.createContextData()).thenReturn('mockedEmptyData');
        target = new CreateContextOptions(instance(emptyHandler));
    });

    it(`${keyof<typeof target>('ifMethodNotDefined')} should be correct`, () => {
        expect(target.ifMethodNotDefined.returnValue).toBe('mockedEmptyData');
    });

    it(`${keyof<typeof target>('getArgsToReport')} should get correct args`, () => {
        const block = { level: 123, hash: 'haha', chainId: 'ccc' } as Block;

        const args = target.getArgsToReport(block, 'db');

        expect(args).toEqual({ block: { level: 123, hash: 'haha' } });
    });

    it(`${keyof<typeof target>('getArgsToTrace')} should get correct args`, () => {
        const args = target.getArgsToTrace('mockedBlock' as any, 'db');

        expect(args).toEqual({ block: 'mockedBlock' });
    });
});

describe(IndexersExecutionOptions.name, () => {
    const target = new IndexersExecutionOptions<unknown, unknown>();

    it(`${keyof<typeof target>('getArgsToReport')} should get correct args`, () => {
        const block = { level: 123, hash: 'haha', chainId: 'ccc' } as Block;

        const args = target.getArgsToReport(block, 'db', 'ctxData');

        expect(args).toEqual({ block: { level: 123, hash: 'haha' } });
    });

    it(`${keyof<typeof target>('getArgsToTrace')} should get correct args`, () => {
        const args = target.getArgsToTrace('mockedBlock' as any, 'db', 'ctxData');

        expect(args).toEqual({ block: 'mockedBlock', contextData: 'ctxData' });
    });
});

describe(AfterBlockIndexedOptions.name, () => {
    const target = new AfterBlockIndexedOptions<unknown>();

    it(`${keyof<typeof target>('getArgsToReport')} should get correct args`, () => {
        const block = { level: 123, hash: 'haha', chainId: 'ccc' } as Block;

        const args = target.getArgsToReport(block, 'ctxData');

        expect(args).toEqual({ block: { level: 123, hash: 'haha' } });
    });

    it(`${keyof<typeof target>('getArgsToTrace')} should get correct args`, () => {
        const args = target.getArgsToTrace('mockedBlock' as any, 'ctxData');

        expect(args).toEqual({ block: 'mockedBlock', contextData: 'ctxData' });
    });
});

describe(RollBackOptions.name, () => {
    const target = new RollBackOptions<unknown>();

    it(`${keyof<typeof target>('getArgsToReport')} should get correct args`, () => {
        const block: IndexedBlock = { level: 123, hash: 'haha' };

        const args = target.getArgsToReport(block, 'db');

        expect(args).toEqual({ block });
    });

    it(`${keyof<typeof target>('getArgsToTrace')} should get correct args`, () => {
        const args = target.getArgsToTrace('mockedBlock' as any, 'db');

        expect(args).toEqual({ block: 'mockedBlock' });
    });
});
