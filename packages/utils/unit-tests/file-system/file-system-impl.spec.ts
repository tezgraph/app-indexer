import { randomUUID } from 'crypto';
import fs from 'fs';
import { tmpdir } from 'os';
import path from 'path';

import { deleteFileOrDir, describeMember, expectToThrowAsync } from '../../../../test-utilities/mocks';
import { FileSystemImpl } from '../../src/file-system/file-system-impl';

describe(FileSystemImpl.name, () => {
    let target: FileSystemImpl;
    let tmpPath: string;

    beforeEach(() => {
        target = new FileSystemImpl();
        tmpPath = path.resolve(tmpdir(), randomUUID());
    });

    afterEach(() => {
        deleteFileOrDir(tmpPath);
    });

    describeMember<typeof target>('existsAsFile', () => {
        const act = async () => target.existsAsFile(tmpPath);

        it('should return true if file exists', async () => {
            fs.writeFileSync(tmpPath, 'testContent');

            const result = await act();

            expect(result).toBeTrue();
        });

        it('should return false if file does not exist', async () => {
            const result = await act();

            expect(result).toBeFalse();
        });

        it('should throw if exists but not a file', async () => {
            fs.mkdirSync(tmpPath);

            const error = await expectToThrowAsync(act);

            expect(error.message).toIncludeMultiple([tmpPath, 'existsAsFile(', 'different type']);
        });
    });

    describeMember<typeof target>('readFileTextIfExists', () => {
        const act = async () => target.readFileTextIfExists(tmpPath);

        it.each([
            ['file text', 'fooooo'],
            ['empty file', ''],
        ])('should read %s if file exists', async testContents => {
            fs.writeFileSync(tmpPath, testContents);

            const contents = await act();

            expect(contents).toBe(testContents);
        });

        it('should return null if file does not exist', async () => {
            const contents = await act();

            expect(contents).toBeNull();
        });

        it('should throw if some file system error', async () => {
            fs.mkdirSync(tmpPath);

            const error = await expectToThrowAsync(act);

            expect(error.message).toIncludeMultiple([tmpPath, 'readFileTextIfExists(', 'directory']);
        });
    });

    describeMember<typeof target>('writeFileText', () => {
        it('should write file', async () => runTest(tmpPath));

        it('should write file if parent directories do not exist', async () => {
            await runTest(path.resolve(tmpPath, 'does/not/exist/file.txt'));
        });

        it('should overwrite existing file', async () => {
            fs.writeFileSync(tmpPath, 'existingContent');

            await runTest(tmpPath);
        });

        it('should throw if some file system error', async () => {
            fs.writeFileSync(tmpPath, 'shouldBeDir');
            const filePath = path.resolve(tmpPath, 'file.txt');

            const error = await expectToThrowAsync(async () => target.writeFileText(filePath, 'testContent'));

            expect(error.message).toIncludeMultiple([filePath, 'writeFileText(', 'already exists']);
        });

        async function runTest(filePath: string) {
            await target.writeFileText(filePath, 'testContent');

            const contents = fs.readFileSync(filePath).toString();
            expect(contents).toBe('testContent');
        }
    });
});
