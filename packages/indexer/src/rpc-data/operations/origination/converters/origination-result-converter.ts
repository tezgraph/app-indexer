import { dumpToString, hasLength, keyof } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { BalanceUpdateConverter } from '../../../common/balance-update/converters/balance-update-converter';
import { BigMapDiffConverter } from '../../../common/big-map-diff/converters/big-map-diff-converter';
import { LazyContractConverter } from '../../../common/lazy-contract/lazy-contract-converter';
import { LazyMichelsonValueFactory } from '../../../common/michelson/lazy-michelson-value-factory';
import { OperationErrorConverter } from '../../../common/operation-error/operation-error-converter';
import { primitives } from '../../../primitives';
import { RpcConversionError } from '../../../rpc-conversion-error';
import { RpcConvertOptions } from '../../../rpc-convert-options';
import { joinUid } from '../../../rpc-converter';
import {
    AppliedOriginationResult,
    LazyContract,
    Michelson,
    TaquitoRpcOriginationResult,
} from '../../../rpc-data-interfaces';
import {
    BaseOperationResultConverter,
    SpecificResultParts,
} from '../../common/operation-result/base-operation-result-converter';

export type OriginationResultParts = SpecificResultParts<AppliedOriginationResult>;

export interface OriginationConvertOptions extends RpcConvertOptions {
    readonly storageMichelson: Michelson;
}

@singleton()
export class OriginationResultConverter
    extends BaseOperationResultConverter<TaquitoRpcOriginationResult, OriginationResultParts, OriginationConvertOptions> {
    constructor(
        operationErrorConverter: OperationErrorConverter,
        private readonly balanceUpdateConverter: BalanceUpdateConverter,
        private readonly bigMapDiffConverter: BigMapDiffConverter,
        private readonly contractConverter: LazyContractConverter,
        private readonly lazyValueFactory: LazyMichelsonValueFactory,
    ) {
        super(operationErrorConverter);
    }

    protected override convertAppliedResultParts(
        rpcData: TaquitoRpcOriginationResult,
        uid: string,
        options: OriginationConvertOptions,
    ): OriginationResultParts {
        const originatedContract = this.convertOriginatedContracts(rpcData, uid);
        const balanceUpdates = this.balanceUpdateConverter.convertArrayProperty(rpcData, 'balance_updates', uid);
        const bigMapDiffs = this.bigMapDiffConverter.convertArrayFrom(rpcData, uid, { ...options, lazyContract: originatedContract });

        options.lastStorageWithinBlock.set(originatedContract.address, options.storageMichelson);

        return {
            balanceUpdates,
            bigMapDiffs,
            originatedContract,
            paidStorageSizeDiff: primitives.bigInteger.convertNullableProperty(rpcData, 'paid_storage_size_diff', uid),
            storageSize: primitives.bigInteger.convertNullableProperty(rpcData, 'storage_size', uid),

            getInitialStorage: async () => {
                const contract = await originatedContract.getContract();
                return this.lazyValueFactory.create(options.storageMichelson, contract.storageSchema);
            },
        };
    }

    private convertOriginatedContracts(rpcData: TaquitoRpcOriginationResult, uid: string): LazyContract {
        const contracts = this.contractConverter.convertArrayProperty(rpcData, 'originated_contracts', uid);

        if (!hasLength(contracts, 1)) {
            throw new RpcConversionError(
                joinUid(uid, keyof<typeof rpcData>('originated_contracts')),
                `Origination result must have a single originated contract but there are: ${dumpToString(contracts.map(c => c.address))}`,
            );
        }
        return contracts[0];
    }
}
