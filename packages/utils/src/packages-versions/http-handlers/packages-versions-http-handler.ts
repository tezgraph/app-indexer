import express from 'express';
import { singleton } from 'tsyringe';

import { safeJsonStringify } from '../../basics/public-api';
import { contentTypes, httpHeaders } from '../../http-fetch/public-api';
import { HttpHandler } from '../../http-server/public-api';
import { PackagesVersionsProvider } from '../packages-versions';

@singleton()
export class PackagesVersionsHttpHandler implements HttpHandler {
    readonly urlPath = '/versions';

    constructor(
        private readonly packagesVersionsProvider: PackagesVersionsProvider,
    ) {}

    handle(_request: express.Request, response: express.Response): void {
        const versions = this.packagesVersionsProvider.versions;

        response.setHeader(httpHeaders.CONTENT_TYPE, contentTypes.JSON);
        response.send(safeJsonStringify(versions, { indent: 2 }));
    }
}
