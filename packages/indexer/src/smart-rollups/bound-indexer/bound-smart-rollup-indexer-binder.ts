import { isNotNullish } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { BoundSmartRollupIndexer } from './bound-smart-rollup-indexer';
import { BoundSmartRollupIndexerFactory } from './bound-smart-rollup-indexer-factory';
import { TrustedSmartRollupIndexer } from '../../client-indexers/wrappers/trusted-indexer-interfaces';
import { IndexerComposer } from '../../helpers/indexer-composer';
import { shouldIndexBasedOnData } from '../../helpers/should-index-evaluator';
import { SmartRollup } from '../smart-rollup';

@singleton()
export class BoundSmartRollupIndexerBinder<TDbContext> {
    constructor(
        private readonly boundIndexerFactory: BoundSmartRollupIndexerFactory<TDbContext>,
        private readonly indexerComposer: IndexerComposer,
    ) {}

    async bind<TContextData>(
        rollup: SmartRollup,
        indexers: readonly TrustedSmartRollupIndexer<TDbContext, TContextData, unknown>[],
        dbContext: TDbContext,
    ): Promise<BoundSmartRollupIndexer<TDbContext, TContextData> | null> {
        // Parallel execution using Promise.all() b/c shouldIndex() is unrelated to block indexing.
        const boundIndexers = await Promise.all(indexers.map(async i => this.bindSingleIndexer(i, rollup, dbContext)));

        return this.indexerComposer.createComposite(boundIndexers.filter(isNotNullish), {
            indexCement: '',
            indexExecuteOutboxMessage: '',
            indexOriginate: '',
            indexPublish: '',
            indexRecoverBond: '',
            indexRefute: '',
            indexTimeout: '',
        });
    }

    // Keep dedicated method to have strongly-typed TRollupData.
    private async bindSingleIndexer<TContextData, TRollupData>(
        indexer: TrustedSmartRollupIndexer<TDbContext, TContextData, TRollupData>,
        rollup: SmartRollup,
        dbContext: TDbContext,
    ): Promise<BoundSmartRollupIndexer<TDbContext, TContextData> | null> {
        const data = await indexer.shouldIndex(rollup, dbContext);

        return shouldIndexBasedOnData(data)
            ? this.boundIndexerFactory.create(indexer, data)
            : null;
    }
}
