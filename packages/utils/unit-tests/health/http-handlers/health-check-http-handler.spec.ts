import express from 'express';
import { Writable } from 'ts-essentials';
import { instance, mock, verify, when } from 'ts-mockito';

import { TestLogger } from '../../../../../test-utilities/mocks';
import { HealthCheckResult, HealthStatus } from '../../../src/health/health-check';
import { HealthProvider, HealthReport } from '../../../src/health/health-provider';
import { HealthCheckHttpHandler } from '../../../src/health/http-handlers/health-check-http-handler';
import { httpStatusCodes } from '../../../src/http-fetch/public-api';

describe(HealthCheckHttpHandler.name, () => {
    let target: HealthCheckHttpHandler;
    let healthProvider: HealthProvider;
    let logger: TestLogger;

    let response: express.Response;
    let report: Writable<HealthReport>;

    const act = async () => target.handle(null!, instance(response));

    beforeEach(() => {
        [healthProvider, response] = [mock(), mock()];
        logger = new TestLogger();
        target = new HealthCheckHttpHandler(instance(healthProvider), logger);

        report = {
            isHealthy: true,
            evaluatedOn: new Date(),
            results: { ['foo' as string]: { status: HealthStatus.Healthy } as HealthCheckResult },
        } as HealthReport;
        when(healthProvider.generateHealthReport()).thenResolve(report);
    });

    it('should respond with OK if healthy', async () => {
        await act();

        verify(response.send('OK')).once();
        logger.loggedSingle().verify('Information', { report });
    });

    it('should respond with 500 (internal server error) if unhealthy', async () => {
        report.isHealthy = false;
        report.results = {
            ['foo' as string]: { status: HealthStatus.Healthy } as HealthCheckResult,
            ['bar' as string]: { status: HealthStatus.Degraded } as HealthCheckResult,
            ['lol' as string]: { status: HealthStatus.Unhealthy } as HealthCheckResult,
        };

        await act();

        verify(response.sendStatus(httpStatusCodes.INTERNAL_SERVER_ERROR)).once();
        logger.loggedSingle().verify('Error', { report, unhealthyChecks: ['lol'] });
    });
});
