import { isNotNullish } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { BoundContractIndexer } from './bound-contract-indexer';
import { BoundContractIndexerFactory } from './bound-contract-indexer-factory';
import { TrustedContractIndexer } from '../../client-indexers/wrappers/trusted-indexer-interfaces';
import { IndexerComposer } from '../../helpers/indexer-composer';
import { shouldIndexBasedOnData } from '../../helpers/should-index-evaluator';
import { LazyContract } from '../../rpc-data/rpc-data-interfaces';

@singleton()
export class BoundContractIndexerBinder<TDbContext> {
    constructor(
        private readonly boundIndexerFactory: BoundContractIndexerFactory<TDbContext>,
        private readonly indexerComposer: IndexerComposer,
    ) {}

    async bind<TContextData>(
        lazyContract: LazyContract,
        indexers: readonly TrustedContractIndexer<TDbContext, TContextData, unknown>[],
        dbContext: TDbContext,
    ): Promise<BoundContractIndexer<TDbContext, TContextData> | null> {
        // Parallel execution using Promise.all() b/c shouldIndex() is unrelated to block indexing.
        const boundIndexers = await Promise.all(indexers.map(async i => this.bindSingleIndexer(i, lazyContract, dbContext)));

        return this.indexerComposer.createComposite(boundIndexers.filter(isNotNullish), {
            indexBigMapDiff: '',
            indexBigMapUpdate: '',
            indexEvent: '',
            indexOrigination: '',
            indexStorageChange: '',
            indexTransaction: '',
        });
    }

    // Keep dedicated method to have strongly-typed TContractData.
    private async bindSingleIndexer<TContextData, TContractData>(
        indexer: TrustedContractIndexer<TDbContext, TContextData, TContractData>,
        lazyContract: LazyContract,
        dbContext: TDbContext,
    ): Promise<BoundContractIndexer<TDbContext, TContextData> | null> {
        const data = await indexer.shouldIndex(lazyContract, dbContext);

        return shouldIndexBasedOnData(data)
            ? this.boundIndexerFactory.create(indexer, data)
            : null;
    }
}
