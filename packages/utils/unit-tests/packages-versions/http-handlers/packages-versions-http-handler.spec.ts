import express from 'express';
import { instance, mock, verify, when } from 'ts-mockito';

import { contentTypes, httpHeaders } from '../../../src/http-fetch/public-api';
import { PackagesVersionsHttpHandler } from '../../../src/packages-versions/http-handlers/packages-versions-http-handler';
import { PackagesVersionsProvider } from '../../../src/packages-versions/packages-versions';

describe(PackagesVersionsHttpHandler.name, () => {
    let target: PackagesVersionsHttpHandler;
    let packagesVersionsProvider: PackagesVersionsProvider;
    let response: express.Response;

    beforeEach(() => {
        [packagesVersionsProvider, response] = [mock(), mock()];
        target = new PackagesVersionsHttpHandler(instance(packagesVersionsProvider));
    });

    it('should respond with full health status', () => {
        const versions = {
            ['node_modules/@tezos-dappetizer/database/package.json']: {
                name: '@tezos-dappetizer/database',
            },
        };

        when(packagesVersionsProvider.versions).thenReturn(versions);

        target.handle(null!, instance(response));

        verify(response.setHeader(httpHeaders.CONTENT_TYPE, contentTypes.JSON)).once();
        verify(response.send(JSON.stringify(versions, null, 2))).once();
    });
});
