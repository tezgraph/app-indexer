import { keyof } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { CommonOriginationOperationConverter } from './common-origination-operation-converter';
import { RpcConvertOptions } from '../../../rpc-convert-options';
import { RpcObjectConverter } from '../../../rpc-converter';
import { OriginationOperation, TaquitoRpcOriginationOperation } from '../../../rpc-data-interfaces';
import {
    CommonMainOperationWithResultConverter,
} from '../../common/main-operation/common-main-operation-with-result-converter';

@singleton()
export class OriginationOperationConverter extends RpcObjectConverter<TaquitoRpcOriginationOperation, OriginationOperation, RpcConvertOptions> {
    constructor(
        private readonly commonOperationConverter: CommonMainOperationWithResultConverter,
        private readonly commonOriginationOperationConverter: CommonOriginationOperationConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcOriginationOperation,
        uid: string,
        options: RpcConvertOptions,
    ): OriginationOperation {
        const commonProperties = this.commonOperationConverter.convert(rpcData, uid, options);
        const specificProperties = this.commonOriginationOperationConverter.convert(rpcData, uid, {
            ...options,
            rpcResult: rpcData.metadata.operation_result,
            rpcResultProperty: [keyof<typeof rpcData>('metadata'), keyof<typeof rpcData.metadata>('operation_result')],
        });

        return Object.freeze<OriginationOperation>({
            ...commonProperties,
            ...specificProperties,
            rpcData,
        });
    }
}
