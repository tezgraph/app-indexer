import { singleton } from 'tsyringe';

import { SmartRollupExecuteOutboxMessageResultConverter } from './smart-rollup-execute-outbox-message-result-converter';
import { primitives } from '../../../../primitives';
import { RpcConvertOptions } from '../../../../rpc-convert-options';
import { RpcObjectConverter } from '../../../../rpc-converter';
import {
    OperationKind,
    SmartRollupExecuteOutboxMessageOperation,
    TaquitoRpcSmartRollupExecuteOutboxMessageOperation,
} from '../../../../rpc-data-interfaces';
import { CommonSmartRollupOperationConverter } from '../../common/converters/common-smart-rollup-operation-converter';

@singleton()
export class SmartRollupExecuteOutboxMessageOperationConverter
    extends RpcObjectConverter<TaquitoRpcSmartRollupExecuteOutboxMessageOperation, SmartRollupExecuteOutboxMessageOperation, RpcConvertOptions> {
    constructor(
        private readonly commonOperationConverter: CommonSmartRollupOperationConverter,
        private readonly resultConverter: SmartRollupExecuteOutboxMessageResultConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcSmartRollupExecuteOutboxMessageOperation,
        uid: string,
        options: RpcConvertOptions,
    ): SmartRollupExecuteOutboxMessageOperation {
        const commonProperties = this.commonOperationConverter.convert(rpcData, uid, options);
        const result = this.resultConverter.convertFromMetadataOperationResult(rpcData, uid);

        return Object.freeze<SmartRollupExecuteOutboxMessageOperation>({
            ...commonProperties,
            cementedCommitment: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'cemented_commitment', uid),
            kind: OperationKind.SmartRollupExecuteOutboxMessage,
            outputProof: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'output_proof', uid),
            result,
            rpcData,
        });
    }
}
