import express from 'express';
import { singleton } from 'tsyringe';

import { ConfigDiagnosticsAggregator } from './config-diagnostics-aggregator';
import { safeJsonStringify } from '../basics/safe-json-stringifier';
import { contentTypes, httpHeaders } from '../http-fetch/http-constants';
import { HttpHandler } from '../http-server/http-handler';

@singleton()
export class ConfigHttpHandler implements HttpHandler {
    readonly urlPath = '/config';

    constructor(
        private readonly configAggregator: ConfigDiagnosticsAggregator,
    ) {}

    handle(_request: express.Request, response: express.Response): void {
        const configDetails = this.configAggregator.aggregateConfigs();

        response.setHeader(httpHeaders.CONTENT_TYPE, contentTypes.JSON);
        response.send(safeJsonStringify(configDetails, { indent: 2 }));
    }
}
