import { singleton } from 'tsyringe';

import { ContractIndexingBoost } from './contract-indexing-boost';
import { TzktIndexingBoostFactory } from './tzkt/tzkt-indexing-boost-factory';
import { ContractsBoostType, IndexingConfig } from '../../config/indexing/indexing-config';

@singleton()
export class ContractIndexingBoostFactory {
    constructor(
        private readonly indexingConfig: IndexingConfig,
        private readonly tzktBoostFactory: TzktIndexingBoostFactory,
    ) {}

    create(): ContractIndexingBoost | null {
        const contractAddresses = (this.indexingConfig.contracts ?? []).flatMap(c => c.addresses);
        const boostConfig = this.indexingConfig.contractBoost;

        switch (boostConfig?.type) {
            case undefined:
                return null;
            case ContractsBoostType.Tzkt:
                return this.tzktBoostFactory.create(boostConfig, contractAddresses);
        }
    }
}
