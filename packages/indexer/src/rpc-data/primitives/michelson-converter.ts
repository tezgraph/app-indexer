import { dumpType, hasLength, isReadOnlyArray, keysOf, withIndex } from '@tezos-dappetizer/utils';

import { StringConverter } from './string-converter';
import { isPrim } from '../../michelson/michelson-utils';
import { RpcConversionError } from '../rpc-conversion-error';
import { joinUid, RpcObjectConverter } from '../rpc-converter';
import { Michelson } from '../rpc-data-interfaces';

export class MichelsonConverter extends RpcObjectConverter<Michelson, Michelson> {
    verifyProperty = this.convertProperty.bind(this);
    verify = this.convert.bind(this);

    private readonly strConverter = new StringConverter();

    protected override convertObject(rpcData: Michelson, uid: string): Michelson {
        if (isReadOnlyArray(rpcData)) {
            verifyArray(rpcData, uid, (a, u) => this.convert(a, u));
            return rpcData;
        }

        if (isPrim(rpcData)) {
            this.strConverter.convertProperty(rpcData, 'prim', uid);
            verifyArray(rpcData.annots ?? [], joinUid(uid, 'annots'), (a, u) => this.strConverter.convert(a, u));
            verifyArray(rpcData.args ?? [], joinUid(uid, 'args'), (a, u) => this.convert(a, u));
            return rpcData;
        }

        const keys = keysOf(rpcData);
        const key = hasLength(keys, 1) ? keys[0] : null;
        if (key === 'bytes' || key === 'int' || key === 'string') {
            this.strConverter.convertProperty(rpcData, key, uid);
            return rpcData;
        }

        throw new RpcConversionError(uid, `Invalid Michelson: ${JSON.stringify(rpcData)}.`);
    }
}

function verifyArray<T>(array: readonly T[], uid: string, verifyItem: (item: T, uid: string) => void): void {
    if (!isReadOnlyArray(array)) {
        throw new RpcConversionError(uid, `It must be an array but its type is ${dumpType(array)}'.`);
    }
    for (const [item, index] of withIndex(array)) {
        verifyItem(item, joinUid(uid, index));
    }
}
