import * as taquitoTzip16 from '@taquito/tzip16';
import { instance, mock, verify, when } from 'ts-mockito';

import { TestLogger } from '../../../../test-utilities/mocks';
import { Contract, ContractAbstraction, ContractProvider } from '../../../indexer/src/public-api';
import { ContractDetails, ContractDetailsResolver } from '../../src/helpers/contract-details-resolver';
import { generatorDefaults } from '../../src/helpers/generator-defaults';

describe(ContractDetailsResolver.name, () => {
    let target: ContractDetailsResolver;
    let contractProvider: ContractProvider;
    let logger: TestLogger;

    let contract: Contract;
    let contractTzip16: taquitoTzip16.Tzip16ContractAbstraction;

    const act = async (name: string | null = null) => target.resolve(contract.address, name);

    beforeEach(() => {
        contractProvider = mock();
        logger = new TestLogger();
        target = new ContractDetailsResolver(instance(contractProvider), logger);

        const contractAbstraction = mock<ContractAbstraction>();
        contractTzip16 = mock();
        contract = Object.freeze({
            address: 'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5',
            abstraction: instance(contractAbstraction),
        }) as Contract;

        when(contractProvider.getContract('KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5')).thenResolve(contract);
        when(contractAbstraction.tzip16()).thenReturn(instance(contractTzip16));
    });

    it('should resolve details with explictly specified name', async () => {
        const details = await act('FooBar');

        verifyDetails(details, 'FooBar');
        logger.loggedSingle().verify('Information', {
            contract: contract.address,
            name: 'FooBar',
        });
        verify(contractTzip16.getMetadata()).never();
    });

    it('should resolve details with name from TZIP-16 metadata', async () => {
        when(contractTzip16.getMetadata()).thenResolve({ metadata: { name: 'TZIP Foo' } } as taquitoTzip16.MetadataEnvelope);

        const details = await act();

        verifyDetails(details, 'TzipFoo');
        logger.loggedSingle()
            .verify('Information', {
                contract: contract.address,
                name: 'TzipFoo',
                cliOption: '--contract-name',
            })
            .verifyMessage('TZIP-16');
    });

    it.each([
        ['undefined', undefined],
        ['null', null],
        ['empty', ''],
        ['white-space', '  '],
    ])('should resolve details with default name if %s in TZIP-16 metadata', async (_desc, tzipName) => {
        when(contractTzip16.getMetadata()).thenResolve({ metadata: { name: tzipName } } as taquitoTzip16.MetadataEnvelope);

        const details = await act();

        verifyDetails(details, generatorDefaults.CONTRACT_NAME);
        logger.loggedSingle()
            .verify('Information', {
                contract: contract.address,
                defaultName: generatorDefaults.CONTRACT_NAME,
                cliOption: '--contract-name',
            })
            .verifyMessage('no name found', 'TZIP-16');
    });

    it('should resolve details with default name if TZIP-16 metadata failed', async () => {
        const tzipError = new Error('tzip');
        when(contractTzip16.getMetadata()).thenReject(tzipError);

        const details = await act();

        verifyDetails(details, generatorDefaults.CONTRACT_NAME);
        logger.loggedSingle()
            .verify('Warning', {
                contract: contract.address,
                defaultName: generatorDefaults.CONTRACT_NAME,
                error: tzipError,
                cliOption: '--contract-name',
            })
            .verifyMessage('TZIP-16');
    });

    function verifyDetails(details: ContractDetails, expectedName: string) {
        expect(details).toEqual<ContractDetails>({
            contract,
            name: expectedName,
        });
    }
});
