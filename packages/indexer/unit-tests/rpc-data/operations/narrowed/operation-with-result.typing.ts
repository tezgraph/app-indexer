/* eslint-disable @typescript-eslint/ban-types */
import { assert, IsExact } from 'conditional-type-checks';

import { InternalOperation } from '../../../../src/rpc-data/operations/internal-operation';
import { MainOperation } from '../../../../src/rpc-data/operations/main-operation';
import { OperationWithResult } from '../../../../src/rpc-data/operations/narrowed/operation-with-result';

assert<IsExact<OperationWithResult, Extract<MainOperation | InternalOperation, { result: unknown }>>>(true);
