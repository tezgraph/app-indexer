import { InjectionToken } from 'tsyringe';

import { RpcMonitorBlockHeader } from './rpc-monitor-block-header';
import { AsyncIterableProvider } from '../../../basics/public-api';

export type RpcBlockMonitor = AsyncIterableProvider<RpcMonitorBlockHeader>;

export const RPC_BLOCK_MONITOR_DI_TOKEN: InjectionToken<RpcBlockMonitor> = 'Dappetizer:Utils:RpcBlockMonitor';
