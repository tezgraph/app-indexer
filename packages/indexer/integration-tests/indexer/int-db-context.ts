import { IntContextData } from './int-context-data';

export interface IntDbContext {
    blockLevel?: number;
    contextData?: IntContextData;
    readonly executedMethods: ExecutedMethod[];

    readonly bdd: {
        blocks: { hash: string }[];
        operationGroups: { hash: string }[];
        operations: { kind: string; operationGroupHash: string }[];
        operationResults: { status: string; operationKind: string; operationGroupHash: string }[];
        balanceUpdates: { kind: string; contractOrDelegate: string | null; operationGroupHash: string }[];
        contractOriginations: { contractAddress: string; operationGroupHash: string }[];
        contractTransactions: { entrypoint: string; contractAddress: string; operationGroupHash: string }[];
        contractStorageChanges: { contractAddress: string; operationGroupHash: string }[];
        contratBigMapDiffs: { action: string; contractAddress: string; operationGroupHash: string }[];
    };
}

export interface ExecutedMethod {
    [key: string]: unknown;
    method: string;
}

export function verifyContextDataFlow(dbContext: IntDbContext, contextData: IntContextData): void {
    if (contextData !== dbContext.contextData) {
        throw new Error('Context data should flow.');
    }
}
