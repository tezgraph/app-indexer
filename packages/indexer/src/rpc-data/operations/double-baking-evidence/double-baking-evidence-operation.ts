import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { BlockHeader } from '../../block/block-header';
import { BalanceUpdate } from '../../common/balance-update/balance-update';
import { EntityToIndex } from '../../entity-to-index';
import { OperationKind } from '../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcDoubleBakingEvidenceOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultDoubleBaking>;

/**
 * It allows for accusing a delegate of having *double-baked* a block – i.e., of having signed two different blocks
 * at the same level and at same round. The bulk of the evidence consists of the block headers of each of the two offending blocks.
 * @category RPC Block Data
 */
export interface DoubleBakingEvidenceOperation extends EntityToIndex<TaquitoRpcDoubleBakingEvidenceOperation> {
    // Narrowing properties:
    readonly kind: OperationKind.DoubleBakingEvidence;

    // Specific properties:
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly blockHeader1: BlockHeader;
    readonly blockHeader2: BlockHeader;
}
