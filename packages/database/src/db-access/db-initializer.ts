import { registerSingleton } from '@tezos-dappetizer/utils';
import { AsyncOrSync } from 'ts-essentials';
import { DependencyContainer, Provider } from 'tsyringe';
import { DataSource } from 'typeorm';

import { DB_INITIALIZERS_DI_TOKEN } from './db-initializer-di-token';

/** Custom logic to initialize the database in addition to TypeORM synchronization. */
export interface DbInitializer {
    /** The name used to identify the initializer in logs. */
    readonly name: string;

    /** The execution order of initializers. This applies for both methods. The default is `0`. */
    readonly order?: number;

    /**
     * Executed before TypeORM does its sychronization.
     * @param dataSource It has TypeORM synchorinize disabled and it is already initialized so that you can use it directly.
     */
    beforeSynchronization?(dataSource: DataSource): AsyncOrSync<void>;

    /** Executed after TypeORM does its sychronization. */
    afterSynchronization?(dataSource: DataSource): AsyncOrSync<void>;
}

/**
 * Registers a database initialized to be executed when TypeORM synchronization is enabled (flag `synchronize = true` in Dappetizer config).
 * It is registered as **singleton**.
 */
export function registerDbInitializer(diContainer: DependencyContainer, dbInitializerProvider: Provider<DbInitializer>): void {
    registerSingleton(diContainer, DB_INITIALIZERS_DI_TOKEN, dbInitializerProvider);
}
