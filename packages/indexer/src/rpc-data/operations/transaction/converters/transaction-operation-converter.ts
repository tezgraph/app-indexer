import { keyof } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { CommonTransactionOperationConverter } from './common-transaction-operation-converter';
import { RpcConvertOptions } from '../../../rpc-convert-options';
import { RpcObjectConverter } from '../../../rpc-converter';
import { TaquitoRpcTransactionOperation, TransactionOperation } from '../../../rpc-data-interfaces';
import {
    CommonMainOperationWithResultConverter,
} from '../../common/main-operation/common-main-operation-with-result-converter';

@singleton()
export class TransactionOperationConverter extends RpcObjectConverter<TaquitoRpcTransactionOperation, TransactionOperation, RpcConvertOptions> {
    constructor(
        private readonly commonOperationConverter: CommonMainOperationWithResultConverter,
        private readonly commonTransactionOperationConverter: CommonTransactionOperationConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcTransactionOperation,
        uid: string,
        options: RpcConvertOptions,
    ): TransactionOperation {
        const commonProperties = this.commonOperationConverter.convert(rpcData, uid, options);
        const specificProperties = this.commonTransactionOperationConverter.convert(rpcData, uid, {
            ...options,
            rpcResult: rpcData.metadata.operation_result,
            rpcResultProperty: [keyof<typeof rpcData>('metadata'), keyof<typeof rpcData.metadata>('operation_result')],
        });

        return Object.freeze<TransactionOperation>({
            ...commonProperties,
            ...specificProperties,
            rpcData,
        });
    }
}
