/* eslint-disable @typescript-eslint/no-unnecessary-condition */
import { NonEmptyArray } from 'ts-essentials';

/**
 * Options for matching the contract to index. Used by {@link contractFilter} decorator.
 * @property `name` Matches contract by its `name` which comes from {@link "@tezos-dappetizer/indexer".IndexingDappetizerConfig.contracts}.
 * @property `anyOfAddresses` Matches contract if its {@link "@tezos-dappetizer/indexer".Contract.address} is one of these addresses.
 *      It must be a non-empty array.
 */
export type ContractFilterOptions =
    | {
        /** Matches contract by its `name` which comes from {@link "@tezos-dappetizer/indexer".IndexingDappetizerConfig.contracts}. */
        readonly name: string;
        anyOfAddresses?: undefined;
    }
    | {
        /** Matches contract if its {@link "@tezos-dappetizer/indexer".Contract.address} is one of these addresses. It must be a non-empty array. */
        readonly anyOfAddresses: Readonly<NonEmptyArray<string>>;
        name?: undefined;
    };
