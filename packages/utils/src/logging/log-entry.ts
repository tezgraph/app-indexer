export enum LogLevel {
    Critical = 'critical',
    Error = 'error',
    Warning = 'warning',
    Information = 'information',
    Debug = 'debug',
    Trace = 'trace',
}

export type LogData = {
    readonly [key: string]: unknown;
};

export interface LogEntry {
    readonly timestamp: Date;
    readonly category: string;
    readonly level: LogLevel;
    readonly message: string;
    readonly data: LogData;
    readonly globalData: unknown;
    readonly packages: unknown;
}

/** @internal Intended only for Dappetizer itself. */
export const ENABLE_TRACE = '(set minLevel to trace)';
