import { instance, mock, when } from 'ts-mockito';

import { ConfigWithDiagnostics } from '../../src/configuration/config-diagnostics';
import { ConfigDiagnosticsAggregator } from '../../src/configuration/config-diagnostics-aggregator';

describe(ConfigDiagnosticsAggregator.name, () => {
    let target: ConfigDiagnosticsAggregator;
    let config1: ConfigWithDiagnostics;
    let config2: ConfigWithDiagnostics;

    beforeEach(() => {
        [config1, config2] = [mock(), mock()];
        target = new ConfigDiagnosticsAggregator([instance(config1), instance(config2)]);
    });

    it('should aggregate config diagnostics', () => {
        when(config1.getDiagnostics()).thenReturn(['foo', { val: 11 }]);
        when(config2.getDiagnostics()).thenReturn(['bar', 22]);

        const aggregated = target.aggregateConfigs();

        expect(aggregated).toEqual({ foo: { val: 11 }, bar: 22 });
        expect(Object.keys(aggregated)).toEqual(['bar', 'foo']); // Should be ordered.
    });
});
