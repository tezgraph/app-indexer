import * as taquitoHttp from '@taquito/http-utils';
import axios, { RawAxiosRequestConfig, isAxiosError } from 'axios';

import { TaquitoHttpRequest } from './taquito-http-request';
import { getRequestDescription } from './taquito-http-utils';
import { errorToString } from '../../basics/public-api';
import { contentEncodings, contentTypes, httpHeaders } from '../../http-fetch/public-api';

/**
 * Adds these features to regular `HttpBackend` from Taquito:
 * - Easy to be used with decorator pattern - just delegate `execute()`.
 * - Support for `AbortSignal`.
 * - Full URL is available on `request`. It does not have to be built manually from `options`.
 */
export class TaquitoHttpBackend extends taquitoHttp.HttpBackend {
    // Private fields are JS private (using #) & instance is frozen b/c the class is exposed as public API.
    readonly #axios: typeof axios;

    constructor(axiosAbstraction: typeof axios = axios) {
        super();
        this.#axios = axiosAbstraction;
    }

    override async createRequest<T>(options: taquitoHttp.HttpRequestOptions, data?: object | string): Promise<T> {
        const request: TaquitoHttpRequest = {
            data,
            headers: options.headers ?? {},
            json: options.json ?? true,
            method: options.method ?? 'GET',
            signal: undefined,
            timeout: options.timeout,
            url: options.url + this.serialize(options.query),
        };
        return this.execute(request) as Promise<T>;
    }

    async execute(request: TaquitoHttpRequest): Promise<unknown> {
        try {
            const axiosConfig: RawAxiosRequestConfig = {
                data: request.data,
                headers: {
                    [httpHeaders.ACCEPT]: request.json ? contentTypes.JSON : contentTypes.TEXT,
                    [httpHeaders.ACCEPT_ENCODING]: [contentEncodings.GZIP, contentEncodings.DEFLATE].join(', '),
                    ...request.headers,
                },
                method: request.method,
                responseType: request.json ? 'json' : 'text',
                signal: request.signal,
                timeout: request.timeout ?? 30_000,
                url: request.url,
            };

            const response = await this.#axios.request(axiosConfig);
            return response.data as unknown;
        } catch (error) {
            if (isAxiosError(error) && error.response) {
                const errorData = typeof error.response.data === 'string'
                    ? error.response.data
                    : JSON.stringify(error.response.data);

                throw new taquitoHttp.HttpResponseError(
                    `Request ${getRequestDescription(request)} failed with response:`
                    + ` (${error.response.status} ${error.response.statusText}) ${errorData}`,
                    error.response.status as taquitoHttp.STATUS_CODE,
                    error.response.statusText,
                    errorData,
                    request.url,
                );
            }
            const errorObj = error instanceof Error ? error : new Error(errorToString(error));
            throw new taquitoHttp.HttpRequestFailed(request.method, request.url, errorObj);
        }
    }
}
