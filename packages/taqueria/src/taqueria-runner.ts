import * as generator from '@tezos-dappetizer/generator';
import { CLI_SCRIPT, cliCommands, cliOptions, dumpToString, entriesOf } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { ChildProcessHelper } from './child-process-helper';
import { CommandBuilder } from './command-builder';
import { IndexerDirResolver } from './indexer-dir-resolver';
import { OriginatedContractResolver } from './originated-contract-resolver';
import { taqueriaCommands, TaqueriaConsoleArgs } from './taqueria-console-args';
import { TezosNodeResolver } from './tezos-node-resolver';

@singleton()
export class TaqueriaRunner {
    constructor(
        private readonly tezosNodeUrlResolver: TezosNodeResolver,
        private readonly originatedContractResolver: OriginatedContractResolver,
        private readonly indexerDirResolver: IndexerDirResolver,
        private readonly childProcess: ChildProcessHelper,
    ) {}

    async run(args: TaqueriaConsoleArgs): Promise<void> {
        const indexerDirPath = await this.indexerDirResolver.resolvePath(args.projectDir);

        switch (args.task) {
            case taqueriaCommands.BUILD_INDEXER:
                return this.buildDappetizerCommand(indexerDirPath);
            case taqueriaCommands.CREATE_INDEXER:
                return this.initDappetizerCommand(args, indexerDirPath);
            case taqueriaCommands.START_INDEXER:
                return this.startDappetizerCommand(args, indexerDirPath);
            case taqueriaCommands.UPDATE_INDEXER:
                return this.updateDappetizerCommand(args, indexerDirPath);
            default:
                throw new Error(`Unsupported command: '${args.task}'. Supported commands: ${dumpToString(Object.values(taqueriaCommands))}.`);
        }
    }

    private async initDappetizerCommand(args: TaqueriaConsoleArgs, indexerDirPath: string): Promise<void> {
        const tezosNode = this.tezosNodeUrlResolver.resolve(args);
        const contract = this.originatedContractResolver.resolve(args);

        const command = createDappetizerCommand(['INIT', contract.address], {
            CONTRACT_NAME: contract.name,
            TEZOS_ENDPOINT: tezosNode.url,
            NETWORK: tezosNode.network,
            OUT_DIR: indexerDirPath,
        });

        await this.childProcess.exec(command);
    }

    private async buildDappetizerCommand(indexerDirPath: string): Promise<void> {
        const command = `npm run ${generator.generatedNpmScripts.BUILD}`;
        await this.childProcess.exec(command, { cwd: indexerDirPath });
    }

    private async startDappetizerCommand(args: TaqueriaConsoleArgs, indexerDirPath: string): Promise<void> {
        const tezosNode = this.tezosNodeUrlResolver.resolve(args);

        const command = createDappetizerCommand(['START'], {
            NETWORK: tezosNode.network,
        });

        await this.childProcess.exec(command, { cwd: indexerDirPath });
    }

    private async updateDappetizerCommand(args: TaqueriaConsoleArgs, indexerDirPath: string): Promise<void> {
        const tezosNode = this.tezosNodeUrlResolver.resolve(args);
        const contract = this.originatedContractResolver.resolve(args);

        const command = createDappetizerCommand(['UPDATE', contract.name, contract.address], {
            TEZOS_ENDPOINT: tezosNode.url,
            NETWORK: tezosNode.network,
        });

        await this.childProcess.exec(command, { cwd: indexerDirPath });
    }
}

function createDappetizerCommand(
    command: [keyof typeof cliCommands, ...string[]],
    options: Partial<Record<keyof typeof cliOptions, string>>,
): string {
    const builder = new CommandBuilder()
        .setCommand('npx', CLI_SCRIPT, cliCommands[command[0]], ...command.slice(1));

    for (const [name, value] of entriesOf(options)) {
        builder.setOption(cliOptions[name], value);
    }
    return builder.build();
}
