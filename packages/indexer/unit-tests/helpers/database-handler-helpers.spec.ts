import { instance, mock, verify, when } from 'ts-mockito';

import { DbContext, TestLogger } from '../../../../test-utilities/mocks';
import { IndexerDatabaseHandlerWrapper } from '../../src/client-indexers/wrappers/indexer-database-handler-wrapper';
import { tryRollBackTransaction } from '../../src/helpers/database-handler-helpers';

describe(tryRollBackTransaction.name, () => {
    let databaseHandler: IndexerDatabaseHandlerWrapper<DbContext>;
    let logger: TestLogger;
    let db: DbContext;

    const act = async () => tryRollBackTransaction(instance(databaseHandler), logger, db);

    beforeEach(() => {
        databaseHandler = mock();
        logger = new TestLogger();
        db = 'mockedDb' as any;
    });

    it('should rollback transaction', async () => {
        await act();

        verify(databaseHandler.rollBackTransaction(db)).once();
        logger.verifyNothingLogged();
    });

    it('should log if rollback failed', async () => {
        const rollbackError = new Error('oups');
        when(databaseHandler.rollBackTransaction(db)).thenReject(rollbackError);

        await act();

        logger.loggedSingle().verify('Error', { rollbackError });
    });
});
