import { Clock } from './clock';

/** Measures elapsed time. The measurement is started automatically on the creation. */
export class Stopwatch {
    readonly startTimestamp = this.clock.getNowTimestamp();

    constructor(private readonly clock: Clock) {}

    get elapsedMillis(): number {
        return this.clock.getNowTimestamp() - this.startTimestamp;
    }
}
