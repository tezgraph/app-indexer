import { singleton } from 'tsyringe';

import { primitives } from '../../primitives';
import { RpcConvertOptions } from '../../rpc-convert-options';
import { RpcObjectConverter } from '../../rpc-converter';
import { OperationKind, ProposalsOperation, TaquitoRpcProposalsOperation } from '../../rpc-data-interfaces';

@singleton()
export class ProposalsOperationConverter extends RpcObjectConverter<TaquitoRpcProposalsOperation, ProposalsOperation, RpcConvertOptions> {
    protected override convertObject(
        rpcData: TaquitoRpcProposalsOperation,
        uid: string,
        _options: RpcConvertOptions,
    ): ProposalsOperation {
        return Object.freeze<ProposalsOperation>({
            kind: OperationKind.Proposals,
            period: primitives.integer.convertProperty(rpcData, 'period', uid),
            proposals: primitives.nonWhiteSpaceString.convertArrayProperty(rpcData, 'proposals', uid),
            rpcData,
            sourceAddress: primitives.address.convertProperty(rpcData, 'source', uid),
            uid,
        });
    }
}
