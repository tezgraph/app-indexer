import { Validator } from './validator';

export function protectAsPublicApi<TValue, TLimits>(validator: Validator<TValue, TLimits>): Validator<TValue, TLimits> {
    return Object.freeze<Validator<TValue, TLimits>>({
        validate: validator.validate.bind(validator),
        getExpectedValueDescription: validator.getExpectedValueDescription.bind(validator),
    });
}
