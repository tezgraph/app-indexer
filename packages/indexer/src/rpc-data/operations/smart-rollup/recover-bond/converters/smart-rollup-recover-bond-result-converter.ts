import { singleton } from 'tsyringe';

import { BalanceUpdateConverter } from '../../../../common/balance-update/converters/balance-update-converter';
import { OperationErrorConverter } from '../../../../common/operation-error/operation-error-converter';
import { AppliedSmartRollupRecoverBondResult, TaquitoRpcSmartRollupRecoverBondResult } from '../../../../rpc-data-interfaces';
import {
    BaseOperationResultConverter,
    SpecificResultParts,
} from '../../../common/operation-result/base-operation-result-converter';

export type SmartRollupRecoverBondResultParts = SpecificResultParts<AppliedSmartRollupRecoverBondResult>;

@singleton()
export class SmartRollupRecoverBondResultConverter
    extends BaseOperationResultConverter<TaquitoRpcSmartRollupRecoverBondResult, SmartRollupRecoverBondResultParts> {
    constructor(
        operationErrorConverter: OperationErrorConverter,
        private readonly balanceUpdateConverter: BalanceUpdateConverter,
    ) {
        super(operationErrorConverter);
    }

    protected override convertAppliedResultParts(
        rpcData: TaquitoRpcSmartRollupRecoverBondResult,
        uid: string,
    ): SmartRollupRecoverBondResultParts {
        const balanceUpdates = this.balanceUpdateConverter.convertArrayProperty(rpcData, 'balance_updates', uid);

        return { balanceUpdates };
    }
}
