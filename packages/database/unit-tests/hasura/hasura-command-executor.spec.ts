import { Writable } from 'ts-essentials';
import { anything, capture, instance, mock, verify, when } from 'ts-mockito';

import { TestLogger } from '../../../../test-utilities/mocks';
import { JsonFetchCommand, JsonFetcher } from '../../../utils/src/http-fetch/public-api';
import { HasuraCommand } from '../../src/hasura/commands/hasura-command';
import { EnabledHasuraConfig } from '../../src/hasura/config/hasura-config';
import { ADMIN_SECRET_HTTP_HEADER, HasuraCommandExecutor } from '../../src/hasura/hasura-command-executor';

describe(HasuraCommandExecutor.name, () => {
    let target: HasuraCommandExecutor;
    let jsonFetcher: JsonFetcher;
    let logger: TestLogger;

    let config: Writable<EnabledHasuraConfig>;
    let command: HasuraCommand<string>;

    const act = async () => target.execute(config, instance(command));

    beforeEach(() => {
        jsonFetcher = mock();
        logger = new TestLogger();
        target = new HasuraCommandExecutor(instance(jsonFetcher), logger);

        config = {} as typeof config;
        command = mock();

        config.url = 'http://hasura/api/';
        config.adminSecret = 'sss';
        when(command.description).thenReturn('myDesc');
        when(command.createPayload()).thenReturn({ pay: 'load' });
        when(command.transformResponse!(anything())).thenCall((s: string) => `transformed-${s}`);
        when(jsonFetcher.execute(anything())).thenResolve('mockedResponse');
    });

    it('should execute command correctly', async () => {
        const response = await act();

        expect(response).toBe('mockedResponse');
        logger.verifyLoggedCount(2);
        logger.logged(0).verify('Debug', { command: 'myDesc' }).verifyMessage('Executing');
        logger.logged(1).verify('Debug', { command: 'myDesc' }).verifyMessage('executed');

        const receivedJsonCommand = capture<JsonFetchCommand<string>>(jsonFetcher.execute).first()[0];
        expect(receivedJsonCommand).toEqual<typeof receivedJsonCommand>({
            description: 'Hasura myDesc',
            url: 'http://hasura/api/v1/metadata',
            requestOptions: {
                method: 'post',
                body: '{"pay":"load"}',
                headers: { [ADMIN_SECRET_HTTP_HEADER]: 'sss' },
            },
            transformResponse: expect.any(Function),
        });
        verify(command.transformResponse!(anything())).never();
        expect(receivedJsonCommand.transformResponse('foo')).toBe('transformed-foo');
    });

    it('should return raw response if no transform func exists', async () => {
        when(command.transformResponse).thenReturn(undefined);

        await act();

        const receivedJsonCommand = capture<JsonFetchCommand<string>>(jsonFetcher.execute).first()[0];
        expect(receivedJsonCommand.transformResponse('foo')).toBe('foo');
    });
});
