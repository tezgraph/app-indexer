import { InjectionToken } from 'tsyringe';

import { HttpHandler } from './http-handler';

export const HTTP_HANDLERS_DI_TOKEN: InjectionToken<HttpHandler> = 'Dappetizer:Utils:HttpHandlers';
