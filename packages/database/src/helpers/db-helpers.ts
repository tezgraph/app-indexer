import { QueryRunner } from 'typeorm';

export async function startTransaction(runner: QueryRunner): Promise<void> {
    await runner.connect();
    await runner.startTransaction();
}

export async function commitTransaction(runner: QueryRunner): Promise<void> {
    await runner.commitTransaction();
    await runner.release();
}

export async function rollbackTransaction(runner: QueryRunner): Promise<void> {
    await runner.rollbackTransaction();
    await runner.release();
}
