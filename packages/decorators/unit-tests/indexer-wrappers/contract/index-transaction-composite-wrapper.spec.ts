import { instance, mock, verify, when } from 'ts-mockito';

import { DbContext } from '../../../../../test-utilities/mocks';
import { ContractIndexer, TransactionIndexingContext, TransactionParameter } from '../../../../indexer/src/public-api';
import { IndexEntrypointWrapper } from '../../../src/indexer-wrappers/contract/index-entrypoint-wrapper';
import {
    IndexTransactionCompositeWrapper,
} from '../../../src/indexer-wrappers/contract/index-transaction-composite-wrapper';
import { IndexTransactionsWrapper } from '../../../src/indexer-wrappers/contract/index-transactions-wrapper';

describe(IndexTransactionCompositeWrapper.name, () => {
    let target: IndexTransactionCompositeWrapper;
    let indexAllTransactionsWrapper: IndexTransactionsWrapper;
    let indexEntrypointWrapper: IndexEntrypointWrapper;
    let decoratedIndexer: object;

    const act = () => target.wrapMethod(decoratedIndexer);

    beforeEach(() => {
        target = new IndexTransactionCompositeWrapper(
            instance(indexAllTransactionsWrapper = mock()),
            instance(indexEntrypointWrapper = mock()),
        );
        decoratedIndexer = { whatever: 123 };
    });

    it('should combine index methods', async () => {
        const wrapper = mock<{
            indexAllTransactions: NonNullable<ContractIndexer<DbContext>['indexTransaction']>;
            indexEntrypoint: NonNullable<ContractIndexer<DbContext>['indexTransaction']>;
        }>();
        when(indexAllTransactionsWrapper.wrapMethod<DbContext, any>(decoratedIndexer))
            .thenReturn(instance(wrapper).indexAllTransactions.bind(instance(wrapper)));
        when(indexEntrypointWrapper.wrapMethod<DbContext, any>(decoratedIndexer))
            .thenReturn(instance(wrapper).indexEntrypoint.bind(instance(wrapper)));

        const transactionParameter = { entrypoint: 'eee' } as TransactionParameter;
        const db: DbContext = 'mockedDb' as any;
        const indexingContext = { block: { hash: 'hhh' } } as TransactionIndexingContext;

        const method = act();
        await method!(transactionParameter, db, indexingContext);

        verify(wrapper.indexAllTransactions(transactionParameter, db, indexingContext)).once();
        verify(wrapper.indexEntrypoint(transactionParameter, db, indexingContext)).once();
    });

    it('should return indexAllTransactions() directly if only wrapped method', () => {
        const indexAllTransactions: ContractIndexer<DbContext>['indexTransaction'] = 'mockedMethod' as any;
        when(indexAllTransactionsWrapper.wrapMethod<DbContext, any>(decoratedIndexer)).thenReturn(indexAllTransactions);

        const method = act();

        expect(method).toBe(indexAllTransactions);
    });

    it('should return indexEntrypoint() directly if only wrapped method', () => {
        const indexEntrypoint: ContractIndexer<DbContext>['indexTransaction'] = 'mockedMethod' as any;
        when(indexEntrypointWrapper.wrapMethod<DbContext, any>(decoratedIndexer)).thenReturn(indexEntrypoint);

        const method = act();

        expect(method).toBe(indexEntrypoint);
    });

    it('should return undefined if no wrapped method', () => {
        const method = act();

        expect(method).toBeUndefined();
    });
});
