/* eslint-disable no-console */
import {
    BigMapDiffAction,
    Block,
    IndexedBlock,
    IndexerDatabaseHandler,
    IndexerModuleFactory,
    registerIndexerDatabaseHandler,
} from '@tezos-dappetizer/indexer';

interface MyDbContext {
    actions: unknown[];
    block?: IndexedBlock;
}

const state: { lastIndexedBlock?: IndexedBlock } = {};

const databaseHandler: IndexerDatabaseHandler<MyDbContext> = {
    startTransaction(): MyDbContext {
        return { actions: [] };
    },
    commitTransaction(dbContext: MyDbContext): void {
        console.log('INDEXED BLOCK', dbContext.block?.level, dbContext.actions);
        state.lastIndexedBlock = dbContext.block;
    },
    rollBackTransaction(): void {
        /* Nothing to do. */
    },
    insertBlock(block: Block, dbContext: MyDbContext): void {
        dbContext.block = {
            hash: block.hash,
            level: block.level,
        };
    },
    deleteBlock(): void {
        /* Nothing to do. */
    },
    getIndexedChainId(): string | null {
        return null;
    },
    getLastIndexedBlock(): IndexedBlock | undefined {
        return state.lastIndexedBlock;
    },
    getIndexedBlock(_level: number): null {
        throw new Error('Should not be called during the test.');
    },
};

export const indexerModule: IndexerModuleFactory<MyDbContext> = options => {
    registerIndexerDatabaseHandler(options.diContainer, { useValue: databaseHandler });

    return {
        name: 'ConsoleDemo',
        contractIndexers: [{
            indexOrigination(origination, dbContext): void {
                dbContext.actions.push({
                    origination: origination.result.originatedContract.address,
                });
            },
            indexTransaction(transactionParameter, dbContext): void {
                dbContext.actions.push({
                    entrypoint: {
                        name: transactionParameter.entrypoint,
                        parameters: JSON.stringify(transactionParameter.value.convert()),
                    },
                });
            },
            indexStorageChange(storageChange, dbContext): void {
                dbContext.actions.push({
                    storage: JSON.stringify(storageChange.newValue.convert()),
                });
            },
            indexBigMapDiff(bigMapDiff, dbContext): void {
                dbContext.actions.push({
                    bigMapDiff: {
                        action: bigMapDiff.action,
                        id: (bigMapDiff.bigMap ?? bigMapDiff.sourceBigMap)?.lastKnownId,
                        ...bigMapDiff.action === BigMapDiffAction.Update
                            ? {
                                key: JSON.stringify(bigMapDiff.key?.convert()),
                                value: JSON.stringify(bigMapDiff.value?.convert()),
                            }
                            : {},
                    },
                });
            },
        }],
    };
};
