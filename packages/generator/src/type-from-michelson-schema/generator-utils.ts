import { CodeStringBuilder } from '../helpers/code-string-builder';

export function addTypeDocComment(builder: CodeStringBuilder, commentLines: readonly string[]): CodeStringBuilder {
    switch (commentLines.length) {
        case 0: return builder;
        case 1: return builder.addLine(`/** ${commentLines[0] ?? ''} */`);
        default: return builder.addLine(`/**`)
            .addLines(commentLines.map(c => ` * ${c}`))
            .addLine(` */`);
    }
}
