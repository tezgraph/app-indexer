/** @internal Intended only for Dappetizer itself. */
export const CLI_SCRIPT = 'dappetizer';

/** @internal Intended only for Dappetizer itself. */
export const cliCommands = Object.freeze({
    INIT: 'init',
    START: 'start',
    UPDATE: 'update',
});

/** @internal Intended only for Dappetizer itself. */
export const cliOptions = Object.freeze({
    BLOCKS: 'blocks',
    CONFIG_FILE: 'config-file',
    CONTRACT_ADDRESS: 'contract-address',
    CONTRACT_NAME: 'contract-name',
    INDEXER_MODULE: 'indexer-module',
    NETWORK: 'network',
    NPM_INSTALL: 'npm-install',
    OUT_DIR: 'out-dir',
    TEZOS_ENDPOINT: 'tezos-endpoint',
    USAGE_STATISTICS: 'usage-statistics',
});

/** @internal Intended only for Dappetizer itself. */
export function fullCliOption(name: keyof typeof cliOptions, value?: string): string {
    const valueSuffix = value !== undefined ? `=${value}` : '';
    return `--${cliOptions[name]}${valueSuffix}`;
}
