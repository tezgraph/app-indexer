import { FileGenerator } from '../../src/file-generators/file-generator';
import { JsonFileGenerator } from '../../src/file-generators/json-file-generator';

export type Exposed<TGenerator> = TGenerator extends JsonFileGenerator<infer TContents, infer TOptions>
    ? FileGenerator<TOptions> & {
        validateExistingContents(contents: TContents): void;
        generateContentsObject(options: TOptions, existingContents: TContents | null): TContents;
    }
    : never;
