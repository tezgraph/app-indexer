import { BigNumber } from 'bignumber.js';

import * as samples from './common-samples';
import { IncreasePaidStorageOperation, OperationKind, OperationResultStatus } from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    kind: 'increase_paid_storage',
    source: 'tz2Jd8F8Yqpehz73DzQ9mEAdKuFwNLpHwTfj',
    fee: '561',
    counter: '763424',
    gas_limit: '1100',
    storage_limit: '0',
    amount: '1',
    destination: 'KT1D4jdLqhtRXNkY2jfSaTJnHSFhsJQuFamG',
    metadata: {
        balance_updates: samples.balanceUpdates.input,
        internal_operation_results: samples.internalOperations.input,
        operation_result: {
            balance_updates: samples.balanceUpdates2.input,
            status: 'applied',
            consumed_milligas: '1000123',
        },
    },
};

const expected: IncreasePaidStorageOperation = {
    amount: new BigNumber(1),
    balanceUpdates: samples.balanceUpdates.getExpected('ops/0[increase_paid_storage]/metadata'),
    counter: new BigNumber(763424),
    destinationAddress: 'KT1D4jdLqhtRXNkY2jfSaTJnHSFhsJQuFamG',
    fee: new BigNumber(561),
    gasLimit: new BigNumber(1100),
    internalOperations: samples.internalOperations.getExpected('ops/0[increase_paid_storage]/metadata'),
    isInternal: false,
    kind: OperationKind.IncreasePaidStorage,
    result: {
        balanceUpdates: samples.balanceUpdates2.getExpected('ops/0[increase_paid_storage]/metadata/operation_result'),
        consumedGas: new BigNumber(1001),
        consumedMilligas: new BigNumber(1000123),
        rpcData: input.metadata.operation_result as any,
        status: OperationResultStatus.Applied,
        uid: 'ops/0[increase_paid_storage]/metadata/operation_result',
    },
    rpcData: input as any,
    sourceAddress: 'tz2Jd8F8Yqpehz73DzQ9mEAdKuFwNLpHwTfj',
    storageLimit: new BigNumber(0),
    uid: 'ops/0[increase_paid_storage]',
};

shouldConvertRpcOperation(expected.kind, { input, expected });
