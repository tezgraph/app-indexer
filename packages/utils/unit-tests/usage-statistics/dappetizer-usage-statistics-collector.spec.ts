import { Span, Tracer } from '@opentelemetry/api';
import { machineIdSync } from 'node-machine-id';
import { Writable } from 'ts-essentials';
import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { DappetizerUsageStatisticsCollector } from '../../src/usage-statistics/dappetizer-usage-statistics-collector';
import { UsageStatisticsController } from '../../src/usage-statistics/usage-statistics-controller';
import { versionInfo } from '../../src/version';

describe(DappetizerUsageStatisticsCollector.name, () => {
    let target: DappetizerUsageStatisticsCollector;
    let statisticsProvider: UsageStatisticsController;
    let process: Writable<NodeJS.Process>;
    let dappetizerVersion: Writable<typeof versionInfo>;

    let tracer: Tracer;
    let span: Span;

    beforeEach(() => {
        statisticsProvider = mock();
        process = {} as any;
        dappetizerVersion = {} as any;
        target = new DappetizerUsageStatisticsCollector(instance(statisticsProvider), process, dappetizerVersion);

        process.arch = 'RISC' as any;
        process.platform = 'Skynet' as any;
        process.version = '6.6.6';
        dappetizerVersion.VERSION = '1.2.3';

        tracer = mock();
        span = mock();
        when(statisticsProvider.tracer).thenReturn(instance(tracer));
        when(tracer.startSpan(anything())).thenReturn(instance(span));
        when(span.setAttributes(anything())).thenReturn(instance(span));
    });

    it('should collect dappetizer statistics', () => {
        target.collect('RunFoo', { foo: 123 });

        verify(tracer.startSpan('RunFoo'));
        verify(span.setAttributes(deepEqual({
            uuid: machineIdSync(false),
            architecture: 'RISC',
            platform: 'Skynet',
            nodeVersion: '6.6.6',
            dappetizerVersion: '1.2.3',
            foo: 123,
        }))).once();
        verify(span.end()).once();
    });

    it('should just pass if no tracer', () => {
        when(statisticsProvider.tracer).thenReturn(null);

        target.collect('RunFoo', {});
    });
});
