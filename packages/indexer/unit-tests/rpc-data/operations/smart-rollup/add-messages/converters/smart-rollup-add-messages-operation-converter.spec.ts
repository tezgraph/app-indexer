import { instance, mock, when } from 'ts-mockito';

import { asReadonly } from '../../../../../../../utils/src/basics/public-api';
import {
    CommonMainOperationWithResult,
    CommonMainOperationWithResultConverter,
} from '../../../../../../src/rpc-data/operations/common/main-operation/common-main-operation-with-result-converter';
import {
    SmartRollupAddMessagesOperationConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/add-messages/converters/smart-rollup-add-messages-operation-converter';
import {
    SmartRollupAddMessagesResultConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/add-messages/converters/smart-rollup-add-messages-result-converter';
import { RpcConvertOptions } from '../../../../../../src/rpc-data/rpc-convert-options';
import {
    OperationKind,
    SmartRollupAddMessagesOperation,
    SmartRollupAddMessagesResult,
    TaquitoRpcSmartRollupAddMessagesOperation,
} from '../../../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../../../rpc-test-helpers';
import { setupCommonOpWithResultProperties } from '../../../operation-rpc-test-helpers';

describe(SmartRollupAddMessagesOperationConverter.name, () => {
    let target: SmartRollupAddMessagesOperationConverter;
    let commonOperationConverter: CommonMainOperationWithResultConverter;
    let resultConverter: SmartRollupAddMessagesResultConverter;

    let rpcData: TaquitoRpcSmartRollupAddMessagesOperation;
    let options: RpcConvertOptions;
    let commonProperties: CommonMainOperationWithResult;
    let opResult: SmartRollupAddMessagesResult;

    const act = () => convertObject(target, rpcData, 'op/id', options);

    beforeEach(() => {
        [commonOperationConverter, resultConverter] = [mock(), mock()];
        target = new SmartRollupAddMessagesOperationConverter(instance(commonOperationConverter), instance(resultConverter));

        rpcData = { message: asReadonly(['mm', 'nn']) } as typeof rpcData;
        options = 'mockedOptions' as any;
        opResult = 'mockedResult' as any;

        commonProperties = setupCommonOpWithResultProperties(commonOperationConverter, rpcData, 'op/id', options);
        when(resultConverter.convertFromMetadataOperationResult(rpcData, 'op/id')).thenReturn(opResult);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<SmartRollupAddMessagesOperation>({
            balanceUpdates: commonProperties.balanceUpdates,
            counter: commonProperties.counter,
            fee: commonProperties.fee,
            gasLimit: commonProperties.gasLimit,
            internalOperations: commonProperties.internalOperations,
            isInternal: commonProperties.isInternal,
            kind: OperationKind.SmartRollupAddMessages,
            message: ['mm', 'nn'],
            result: opResult,
            rpcData,
            sourceAddress: commonProperties.sourceAddress,
            storageLimit: commonProperties.storageLimit,
            uid: commonProperties.uid,
        });
        expect(operation).toBeFrozen();
    });
});
