import * as taquitoMichelson from '@taquito/michelson-encoder';
import { Block, IndexedBlock, IndexerDatabaseHandler, LazyMichelsonValue, MichelsonSchema } from '@tezos-dappetizer/indexer';
import { hasConstructor, keyof } from '@tezos-dappetizer/utils';
import fs from 'fs';
import { startCase } from 'lodash';
import { EOL } from 'os';
import path from 'path';

import { IntDbContext } from './int-db-context';

export class IntIndexerDatabaseHandler implements IndexerDatabaseHandler<IntDbContext> {
    private lastIndexed?: {
        level: number;
        hash: string;
        chainId: string;
    };

    constructor(
        private readonly outDirPath: string,
    ) {}

    startTransaction(): IntDbContext {
        return {
            executedMethods: [{
                method: this.getMethodName('startTransaction'),
            }],
            bdd: {
                blocks: [],
                operationGroups: [],
                operations: [],
                operationResults: [],
                balanceUpdates: [],
                contractOriginations: [],
                contractTransactions: [],
                contractStorageChanges: [],
                contratBigMapDiffs: [],
            },
        };
    }

    async commitTransaction(dbContext: IntDbContext): Promise<void> {
        dbContext.executedMethods.push({
            method: this.getMethodName('commitTransaction'),
        });

        this.writeIndexedBlock(dbContext);
        return Promise.resolve(); // To test async.
    }

    rollBackTransaction(dbContext: IntDbContext): void {
        dbContext.executedMethods.push({
            method: this.getMethodName('rollBackTransaction'),
        });

        this.writeIndexedBlock(dbContext);
    }

    insertBlock(block: Block, dbContext: IntDbContext): void {
        dbContext.executedMethods.push({
            method: this.getMethodName('insertBlock'),
            blockHash: block.hash,
        });

        this.lastIndexed = {
            level: block.level,
            hash: block.hash,
            chainId: block.chainId,
        };
    }

    deleteBlock(_block: IndexedBlock, _dbContext: IntDbContext): void {
        throw new Error('Method not implemented.');
    }

    getIndexedChainId(): string | undefined {
        return this.lastIndexed?.chainId;
    }

    async getLastIndexedBlock(): Promise<IndexedBlock | undefined> {
        return Promise.resolve(this.lastIndexed); // To test async.
    }

    getIndexedBlock(_level: number): null {
        throw new Error('Should not be called during the test.');
    }

    private writeIndexedBlock(dbContext: IntDbContext): void {
        const blockLevel = dbContext.blockLevel;
        if (blockLevel === undefined) {
            throw new Error('Block was not indexed.');
        }

        const contents = stringifyPretty(dbContext.executedMethods);
        writeFileText(`${this.outDirPath}/block_${blockLevel}.json`, contents);

        this.writeBdd(dbContext.bdd, blockLevel);
    }

    private writeBdd(bdd: IntDbContext['bdd'], blockLevel: number): void {
        for (const [entityName, items] of Object.entries(bdd)) {
            if (items.length === 0) {
                continue;
            }

            const entityFilePath = `${this.outDirPath}/bdd/${entityName}.md`;
            const lineBreak = '  '; // Markdown.

            if (!fs.existsSync(entityFilePath)) {
                const preambleContents = [
                    `# Story: Indexing of ${startCase(entityName)}`,
                    ``,
                    `**As** an indexer developer using Dappetizer,${lineBreak}`,
                    `**When** indexing a block${lineBreak}`,
                    `**I want** Dappetizer to call my indexer code providing respective ${startCase(entityName).toLowerCase()},${lineBreak}`,
                    `**So that** I can index them.`,
                    ``,
                ].join(EOL);
                writeFileText(entityFilePath, preambleContents);
            }

            const itemProperties = Object.keys(items[0] ?? {});
            const blockScenarioContents = [
                ``,
                `## Scenario: Indexing Block ${blockLevel}`,
                ``,
                `**When** Dappetizer indexes block ${blockLevel}${lineBreak}`,
                `**Then** Dappetizer calls my index method with respective data one-by-one as follows:`,
                ``,
                `| ${itemProperties.map(startCase).join(' | ')} |`,
                `| ${itemProperties.map(_ => '---').join(' | ')} |`,
                ...items.map((item: Record<string, string | null>) => `| ${itemProperties.map(p => item[p]).join(' | ')} |`),
                ``,
            ].join(EOL);
            fs.appendFileSync(entityFilePath, blockScenarioContents);
        }
    }

    private getMethodName(methodName: keyof typeof this & string): string {
        return `${this.constructor.name}.${methodName}`;
    }
}

function stringifyPretty(data: unknown): string {
    return JSON.stringify(data, jsonReplacer, '\t'); // Tab char is used to minimize file size.
}

function jsonReplacer(key: string, value: unknown): unknown {
    if (typeof value === 'object' && value !== null) {
        if (key === 'rpcData') {
            return 'excluded';
        }
        if (value instanceof Promise) {
            throw new Error('Promises cannot be stringified.');
        }
        if (hasConstructor(value, taquitoMichelson.MichelsonMap)) {
            return Array.from(value.entries());
        }
        if (hasConstructor(value, MichelsonSchema)) {
            return value.generated;
        }
        if (isLazyMichelsonValue(value)) {
            return {
                converted: value.convert(),
                schema: value.schema,
            };
        }
        if (!Array.isArray(value)) {
            // Sort by property names.
            return Object.entries(value)
                .sort(([p1], [p2]) => p1.localeCompare(p2))
                .reduce((r, [k, v]) => ({ ...r, [k]: v }), {});
        }
    }
    return value;
}

function writeFileText(filePath: string, contents: string): void {
    ensureDirExists(path.dirname(filePath));
    fs.writeFileSync(filePath, contents);
}

function ensureDirExists(dirPath: string): void {
    if (!fs.existsSync(dirPath)) {
        fs.mkdirSync(dirPath, { recursive: true });
    }
}

function isLazyMichelsonValue(value: object): value is LazyMichelsonValue {
    return keyof<LazyMichelsonValue>('convert') in value
        && keyof<LazyMichelsonValue>('michelson') in value
        && keyof<LazyMichelsonValue>('schema') in value;
}
