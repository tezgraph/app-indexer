import { describeMember } from '../../../../../../../test-utilities/mocks';
import { TicketTokenImpl } from '../../../../../src/rpc-data/common/ticket/converters/ticket-token-impl';
import { LazyMichelsonValue } from '../../../../../src/rpc-data/rpc-data-interfaces';

describe(TicketTokenImpl.name, () => {
    let target: TicketTokenImpl;

    beforeEach(() => {
        target = new TicketTokenImpl('KT1BRADdqGk2eLmMqvyWzqVmPQ1RCBCbW5dY', {
            michelson: { string: 'abc' },
            schema: { michelson: { prim: 'string' } },
        } as LazyMichelsonValue);
    });

    it('should be frozen', () => {
        expect(target).toBeFrozen();
    });

    describeMember<typeof target>('equals', () => {
        it.each([
            [true, 'same values', 'KT1BRADdqGk2eLmMqvyWzqVmPQ1RCBCbW5dY', { string: 'abc' }, { prim: 'string' }],
            [false, 'different ticketer', 'KT1Aq4wWmVanpQhq4TTfjZXB5AjFpx15iQMM', { string: 'abc' }, { prim: 'string' }],
            [false, 'different content', 'KT1BRADdqGk2eLmMqvyWzqVmPQ1RCBCbW5dY', { string: 'xyz' }, { prim: 'string' }],
            [false, 'different content type', 'KT1BRADdqGk2eLmMqvyWzqVmPQ1RCBCbW5dY', { string: 'abc' }, { prim: 'bytes' }],
            [false, 'different values', 'KT1Aq4wWmVanpQhq4TTfjZXB5AjFpx15iQMM', { string: 'xyz' }, { prim: 'bytes' }],
        ])('should return %s if %s', (expected, _desc, otherTicketer, otherContent, otherContentType) => {
            const other = new TicketTokenImpl(otherTicketer, {
                michelson: otherContent,
                schema: { michelson: otherContentType },
            } as LazyMichelsonValue);

            const equal = target.equals(other);
            const equalViceVersa = other.equals(target);

            expect(equal).toBe(expected);
            expect(equalViceVersa).toBe(expected);
        });
    });

    describeMember<typeof target>('toJSON', () => {
        it('should return correct JSON', () => {
            const json = target.toJSON();

            expect(json).toEqual({
                ticketerAddress: 'KT1BRADdqGk2eLmMqvyWzqVmPQ1RCBCbW5dY',
                content: { string: 'abc' },
                contentType: { prim: 'string' },
            });
        });
    });
});
