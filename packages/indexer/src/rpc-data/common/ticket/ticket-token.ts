import { LazyMichelsonValue } from '../michelson/lazy-michelson-value';

/** @category RPC Block Data */
export interface TicketToken {
    readonly content: LazyMichelsonValue;

    /** Contract `KT1` address. */
    readonly ticketerAddress: string;

    equals(otherToken: TicketToken): boolean;
}
