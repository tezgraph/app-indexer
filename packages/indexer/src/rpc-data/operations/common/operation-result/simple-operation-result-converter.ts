import { BaseOperationResultConverter } from './base-operation-result-converter';
import { TaquitoRpcInternalOperation } from '../../../rpc-data-interfaces';

export class SimpleOperationResultConverter<TRpc extends TaquitoRpcInternalOperation['result']>
    extends BaseOperationResultConverter<TRpc, {}> {
    protected override convertAppliedResultParts(_rpcData: TRpc, _uid: string): {} {
        return {};
    }
}
