import { StringLimits, StringValidator } from './string-validator';
import { TypeValidator } from './type-validator';
import { ArgError } from '../arg-error';
import { dumpToString } from '../dump-to-string';

export class StringValidatorImpl implements StringValidator {
    constructor(private readonly typeValidator: TypeValidator) {}

    validate(value: unknown, limits?: StringLimits): string {
        const str = this.typeValidator.validate(value, 'string');

        switch (limits) {
            case undefined:
                break;
            case 'NotEmpty':
                if (str.length === 0) {
                    throw new Error('It cannot be an empty string.');
                }
                break;
            case 'NotWhiteSpace':
                if (str.length === 0) {
                    throw new Error('It cannot be an empty string.');
                }
                if (ONLY_WHITE_SPACES_REGEX.test(str)) {
                    throw new Error('It cannot contain only white-space chars.');
                }
                break;
            default:
                throwInvalidLimits(limits);
        }
        return str;
    }

    getExpectedValueDescription(limits?: StringLimits): string {
        switch (limits) {
            case undefined:
                return 'a string';
            case 'NotEmpty':
                return 'a non-empty string';
            case 'NotWhiteSpace':
                return 'a non-white-space string';
            default:
                throwInvalidLimits(limits);
        }
    }
}

export const ONLY_WHITE_SPACES_REGEX = /^\s*$/u;

function throwInvalidLimits(limits?: StringLimits): never {
    const supportedLimits: (typeof limits)[] = [undefined, 'NotEmpty', 'NotWhiteSpace'];
    throw new ArgError({
        argName: 'limits',
        specifiedValue: limits,
        details: `It must be one of values: ${dumpToString(supportedLimits)}.`,
    });
}
