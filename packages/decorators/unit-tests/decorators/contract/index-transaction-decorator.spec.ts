import { indexTransaction } from '../../../src/decorators/contract/index-transaction-decorator';

describe(indexTransaction.name, () => {
    it('should pass if correctly declared', () => {
        indexTransaction();
    });
});
