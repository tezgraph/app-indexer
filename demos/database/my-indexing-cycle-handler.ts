import { DbContext, IndexingCycleHandlerUsingDb } from '@tezos-dappetizer/database';
import { Block, IndexedBlock } from '@tezos-dappetizer/indexer';

import { DbEntrypoint } from './db-entrypoint';
import { MyContextData } from './my-context-data';

export class MyIndexingCycleHandler implements IndexingCycleHandlerUsingDb<MyContextData> {
    createContextData(_block: Block, _dbContext: DbContext): MyContextData {
        return { entrypoints: [] };
    }

    async afterIndexersExecuted(_block: Block, dbContext: DbContext, contextData: MyContextData): Promise<void> {
        await dbContext.transaction.insert(DbEntrypoint, contextData.entrypoints);
    }

    async rollBackOnReorganization(blockToRollBack: IndexedBlock, dbContext: DbContext): Promise<void> {
        await dbContext.transaction.createQueryBuilder()
            .delete()
            .from(DbEntrypoint)
            .where('block_hash = :hash', { hash: blockToRollBack.hash })
            .execute();
    }
}
