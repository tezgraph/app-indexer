import { instance, mock, when } from 'ts-mockito';

import {
    BalanceUpdateConverter,
} from '../../../../../../src/rpc-data/common/balance-update/converters/balance-update-converter';
import {
    SmartRollupGameStatusConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/common/converters/smart-rollup-game-status-converter';
import {
    SmartRollupTimeoutResultConverter,
    SmartRollupTimeoutResultParts,
} from '../../../../../../src/rpc-data/operations/smart-rollup/timeout/converters/smart-rollup-timeout-result-converter';
import {
    BalanceUpdate,
    SmartRollupGameStatus,
    TaquitoRpcSmartRollupTimeoutResult,
} from '../../../../../../src/rpc-data/rpc-data-interfaces';
import { convertAppliedResultParts } from '../../../../rpc-test-helpers';

describe(SmartRollupTimeoutResultConverter, () => {
    let target: SmartRollupTimeoutResultConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;
    let gameStatusConverter: SmartRollupGameStatusConverter;

    let rpcData: TaquitoRpcSmartRollupTimeoutResult;
    let balanceUpdates: BalanceUpdate[];
    let gameStatus: SmartRollupGameStatus;

    const act = () => convertAppliedResultParts(target, rpcData, 'res/id');

    beforeEach(() => {
        [balanceUpdateConverter, gameStatusConverter] = [mock(), mock()];
        target = new SmartRollupTimeoutResultConverter(null!, instance(balanceUpdateConverter), instance(gameStatusConverter));

        rpcData = 'mockedRpcData' as any;
        balanceUpdates = 'mockedBalanceUpdates' as any;
        gameStatus = 'mockedGameStatus' as any;

        when(balanceUpdateConverter.convertArrayProperty(rpcData, 'balance_updates', 'res/id')).thenReturn(balanceUpdates);
        when(gameStatusConverter.convertProperty(rpcData, 'game_status', 'res/id')).thenReturn(gameStatus);
    });

    it('should convert correctly', () => {
        const result = act();

        expect(result).toEqual<SmartRollupTimeoutResultParts>({ balanceUpdates, gameStatus });
    });
});
