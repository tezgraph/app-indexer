// Separate file from main operation b/c it refers internal operations -> circle.
import { SmartRollup } from '../../../../smart-rollups/smart-rollup';
import { LazyContract } from '../../../common/lazy-contract/lazy-contract';
import { EntityToIndex } from '../../../entity-to-index';
import { OperationKind } from '../../operation-kind';
import { TaquitoRpcInternalOperation } from '../operation-result/operation-results';

/** @category RPC Block Data */
export interface BaseInternalOperation extends EntityToIndex<TaquitoRpcInternalOperation> {
    // Narrowing properties:

    /** Usable for narrowing the type of this object from {@link InternalOperation}. */
    readonly kind: OperationKind;

    /** Usable for narrowing the type of this object from the union with main operations. */
    readonly isInternal: true;

    // Specific properties:

    /** It is an integer greater than or equal to `0`. */
    readonly nonce: number;

    /**
     * Contract `KT1` or smart rollup `sr1` address.
     * @deprecated Use `source.address` instead.
     */
    readonly sourceAddress: string;

    /** @deprecated Use `source` instead. */
    readonly sourceContract: LazyContract | null;

    readonly source: LazyContract | SmartRollup;
}
