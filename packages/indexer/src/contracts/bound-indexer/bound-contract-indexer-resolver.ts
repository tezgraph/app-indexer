import { getOrCreate } from '@tezos-dappetizer/utils';

import { BoundContractIndexer } from './bound-contract-indexer';
import { BoundContractIndexerBinder } from './bound-contract-indexer-binder';
import { TrustedContractIndexer } from '../../client-indexers/wrappers/trusted-indexer-interfaces';
import { LazyContract } from '../../rpc-data/rpc-data-interfaces';

export class BoundContractIndexerResolver<TDbContext, TContextData> {
    private readonly indexerCache = new Map<string, Promise<BoundContractIndexer<TDbContext, TContextData> | null>>();

    constructor(
        private readonly binder: BoundContractIndexerBinder<TDbContext>,
        private readonly indexers: readonly TrustedContractIndexer<TDbContext, TContextData, unknown>[],
    ) {}

    async resolve(
        lazyContract: LazyContract,
        dbContext: TDbContext,
    ): Promise<BoundContractIndexer<TDbContext, TContextData> | null> {
        return getOrCreate(this.indexerCache, lazyContract.address, async () => this.binder.bind(lazyContract, this.indexers, dbContext));
    }
}
