import { Gauge } from 'prom-client';
import { singleton } from 'tsyringe';

import { injectValue } from '../dependency-injection/public-api';
import { MetricsUpdater } from '../metrics/public-api';

@singleton()
export class AppUptimeMetrics {
    readonly uptime = new Gauge({
        name: 'ai_indexer_uptime',
        help: 'Indexer uptime in seconds.',
    });
}

@singleton()
export class AppUptimeMetricsUpdater implements MetricsUpdater {
    constructor(
        private readonly metrics: AppUptimeMetrics,
        @injectValue(global.process) private readonly process: NodeJS.Process,
    ) {}

    update(): void {
        const uptimeSeconds = Math.floor(this.process.uptime());
        this.metrics.uptime.set(uptimeSeconds);
    }
}
