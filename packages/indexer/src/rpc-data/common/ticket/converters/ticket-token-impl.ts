import { ToJSON } from '@tezos-dappetizer/utils';
import { isEqual } from 'lodash';

import { LazyMichelsonValue } from '../../michelson/lazy-michelson-value';
import { TicketToken } from '../ticket-token';

export class TicketTokenImpl implements TicketToken, ToJSON {
    constructor(
        readonly ticketerAddress: string,
        readonly content: LazyMichelsonValue,
    ) {
        Object.freeze(this);
    }

    equals(otherToken: TicketToken): boolean {
        return this.ticketerAddress === otherToken.ticketerAddress
            && isEqual(this.content.schema.michelson, otherToken.content.schema.michelson)
            && isEqual(this.content.michelson, otherToken.content.michelson);
    }

    toJSON(): unknown {
        return {
            ticketerAddress: this.ticketerAddress,
            content: this.content.michelson,
            contentType: this.content.schema.michelson,
        };
    }
}
