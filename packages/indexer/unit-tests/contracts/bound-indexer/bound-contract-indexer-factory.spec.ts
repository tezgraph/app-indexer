import { capture, deepEqual, instance, mock, verify } from 'ts-mockito';

import { ContextData, ContractData, DbContext } from '../../../../../test-utilities/mocks';
import { keysOf } from '../../../../utils/src/basics/public-api';
import { ContractIndexingContext } from '../../../src/client-indexers/contract-indexer';
import { TrustedContractIndexer } from '../../../src/client-indexers/wrappers/trusted-indexer-interfaces';
import { BoundContractIndexer } from '../../../src/contracts/bound-indexer/bound-contract-indexer';
import { BoundContractIndexerFactory } from '../../../src/contracts/bound-indexer/bound-contract-indexer-factory';

describe(BoundContractIndexerFactory.name, () => {
    let resultIndexer: BoundContractIndexer<DbContext, ContextData>;
    let wrappedIndexer: TrustedContractIndexer<DbContext, ContextData, ContractData>;
    let contractData: ContractData;

    beforeEach(() => {
        wrappedIndexer = mock();
        contractData = 'mockedContractData' as any;
        resultIndexer = new BoundContractIndexerFactory().create(instance(wrappedIndexer), contractData);
    });

    const methodsToTest: Record<keyof typeof resultIndexer, ''> = {
        indexBigMapDiff: '',
        indexBigMapUpdate: '',
        indexEvent: '',
        indexOrigination: '',
        indexStorageChange: '',
        indexTransaction: '',
    };
    describe.each(keysOf(methodsToTest))('%s', methodName => {
        it('should be delegated to wrapped indexer with contract data in indexing context', async () => {
            const entity: any = 'mockedEntity';
            const db: DbContext = 'mocedDb' as any;
            const context = {
                block: { hash: 'haha' },
                contractData: null,
            } as ContractIndexingContext<ContextData>;

            await resultIndexer[methodName](entity, db, context as any);

            verify(wrappedIndexer[methodName]!(entity, db, deepEqual({
                block: { hash: 'haha' },
                contractData,
            } as any))).once();
            const receivedContext = capture(wrappedIndexer[methodName] as any).first()[2];
            expect(receivedContext).toBeFrozen();
        });
    });
});
