import { createDecorator } from '../../helpers/decorator-accessors';

/**
 * The decorator for a method of a contract indexer to index its origination operation.
 * @example Demonstrates the method signature:
 * ```typescript
 * import { DbContext } from '@tezos-dappetizer/database';
 * import { indexOrigination } from '@tezos-dappetizer/decorators';
 * import { OriginationIndexingContext } from '@tezos-dappetizer/indexer';
 *
 * class YourContractIndexer {
 *     @indexOrigination()
 *     indexMyContractOrigination(
 *         initialStorage: StronglyTypedInitialStorage,
 *         dbContext: DbContext,
 *         indexingContext: OriginationIndexingContext<TContextData, TContractData>,
 *     ): void | Promise<void> {
 *         // Your indexing code.
 *     }
 * }
 * ```
 * @remark This is based on {@link "@tezos-dappetizer/indexer".ContractIndexer.indexOrigination | ContractIndexer.indexOrigination(...)}.
 */
export function indexOrigination(): MethodDecorator {
    return createDecorator(indexOrigination);
}
