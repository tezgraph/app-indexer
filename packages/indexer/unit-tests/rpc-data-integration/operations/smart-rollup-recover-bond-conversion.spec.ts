import { BigNumber } from 'bignumber.js';

import * as samples from './common-samples';
import { OperationKind, OperationResultStatus, SmartRollup, SmartRollupRecoverBondOperation } from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    counter: '25156',
    fee: '1000000',
    gas_limit: '4016',
    kind: 'smart_rollup_recover_bond',
    rollup: 'sr1EYxm4fQjr15TASs2Q7PgZ1LqS6unkZhHL',
    storage_limit: '578',
    source: 'tz1ZpuBypK6G754crXDZyoMPaVPoBmBsPda2',
    staker: 'tz1bTS4QDBnpQPmMPNM3rn7jN1hevkWDHSKw',
    metadata: {
        balance_updates: samples.balanceUpdates.input,
        internal_operation_results: samples.internalOperations.input,
        operation_result: {
            balance_updates: samples.balanceUpdates2.input,
            consumed_milligas: '3915240',
            status: 'applied',
        },
    },
};

const expected: SmartRollupRecoverBondOperation = {
    balanceUpdates: samples.balanceUpdates.getExpected('ops/0[smart_rollup_recover_bond]/metadata'),
    counter: new BigNumber(25156),
    fee: new BigNumber(1000000),
    gasLimit: new BigNumber(4016),
    internalOperations: samples.internalOperations.getExpected('ops/0[smart_rollup_recover_bond]/metadata'),
    isInternal: false,
    kind: OperationKind.SmartRollupRecoverBond,
    result: {
        balanceUpdates: samples.balanceUpdates2.getExpected('ops/0[smart_rollup_recover_bond]/metadata/operation_result'),
        consumedGas: new BigNumber(3916),
        consumedMilligas: new BigNumber(3915240),
        rpcData: input.metadata.operation_result as any,
        status: OperationResultStatus.Applied,
        uid: 'ops/0[smart_rollup_recover_bond]/metadata/operation_result',
    },
    rollup: { address: 'sr1EYxm4fQjr15TASs2Q7PgZ1LqS6unkZhHL' } as SmartRollup,
    rpcData: input as any,
    sourceAddress: 'tz1ZpuBypK6G754crXDZyoMPaVPoBmBsPda2',
    stakerAddress: 'tz1bTS4QDBnpQPmMPNM3rn7jN1hevkWDHSKw',
    storageLimit: new BigNumber(578),
    uid: 'ops/0[smart_rollup_recover_bond]',
};

shouldConvertRpcOperation(expected.kind, { input, expected });
