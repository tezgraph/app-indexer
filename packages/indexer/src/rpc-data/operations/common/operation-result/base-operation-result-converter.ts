import * as taquitoRpc from '@taquito/rpc';
import { freezeResult, isNullish, keyof } from '@tezos-dappetizer/utils';
import { BigNumber } from 'bignumber.js';
import { DeepReadonly } from 'ts-essentials';
import { inject } from 'tsyringe';

import { OperationErrorConverter } from '../../../common/operation-error/operation-error-converter';
import { primitives } from '../../../primitives';
import { joinUid, RpcObjectConverter } from '../../../rpc-converter';
import {
    AppliedOperationResult,
    BacktrackedOperationResult,
    FailedOperationResult,
    OperationResultStatus,
    SkippedOperationResult,
} from '../../../rpc-data-interfaces';

export type AppliedResult<TRpcData, TParts> = TParts & AppliedOperationResult<TRpcData>;

export type Result<TRpcData, TParts> =
    | AppliedResult<TRpcData, TParts>
    | BacktrackedOperationResult<TRpcData>
    | FailedOperationResult<TRpcData>
    | SkippedOperationResult<TRpcData>;

export type BaseRpcOperationResult = DeepReadonly<{
    consumed_gas?: string;
    consumed_milligas?: string;
    status: taquitoRpc.OperationResultStatusEnum;
    errors?: taquitoRpc.TezosGenericOperationError[];
}>;

// eslint-disable-next-line @typescript-eslint/ban-types
export type SpecificResultParts<TResult> = Omit<TResult, keyof AppliedOperationResult>;

export abstract class BaseOperationResultConverter<TRpc extends BaseRpcOperationResult, TResultParts, TOptions = void>
    extends RpcObjectConverter<TRpc, Result<TRpc, TResultParts>, TOptions> {
    constructor(
        @inject(OperationErrorConverter) private readonly operationErrorConverter: OperationErrorConverter,
    ) {
        super();
    }

    @freezeResult()
    protected override convertObject(rpcData: TRpc, uid: string, options: TOptions): Result<TRpc, TResultParts> {
        const status = primitives.enum.convertProperty(rpcData, 'status', uid, statusMapping);
        const commonProperties = { uid, rpcData };

        switch (status) {
            case OperationResultStatus.Applied:
                return {
                    status,
                    ...commonProperties,
                    ...this.convertGasProperties(rpcData, uid),
                    ...this.convertAppliedResultParts(rpcData, uid, options),
                };
            case OperationResultStatus.Backtracked:
                return {
                    status,
                    ...commonProperties,
                    ...this.convertGasProperties(rpcData, uid),
                    ...this.convertErrors(rpcData, uid),
                };
            case OperationResultStatus.Failed:
                return {
                    status,
                    ...commonProperties,
                    ...this.convertErrors(rpcData, uid),
                };
            case OperationResultStatus.Skipped:
                return {
                    status,
                    ...commonProperties,
                };
        }
    }

    convertFromMetadataOperationResult(
        rpcParentData: { readonly metadata: { readonly operation_result: TRpc } },
        parentUid: string,
        options: TOptions,
    ): Result<TRpc, TResultParts> {
        return this.convert(
            rpcParentData.metadata.operation_result,
            joinUid(parentUid, keyof<typeof rpcParentData>('metadata'), keyof<typeof rpcParentData.metadata>('operation_result')),
            options,
        );
    }

    private convertGasProperties(rpcData: TRpc, uid: string): Pick<AppliedOperationResult, 'consumedGas' | 'consumedMilligas'> {
        // 'consumed_milligas' was added in protocol 7 Delphi.
        // 'consumed_gas' was removed in protocol 14 Kathmandu.
        // If no 'consumed_gas' from RPC, fill it from 'consumed_milligas' by dividing & ceiling.
        const consumedMilligas = primitives.bigInteger.convertNullableProperty(rpcData, 'consumed_milligas', uid, { min: 0 });
        const consumedGas = !isNullish(rpcData.consumed_gas)
            ? primitives.bigInteger.convertProperty(rpcData, 'consumed_gas', uid, { min: 0 })
            : consumedMilligas?.dividedBy(new BigNumber(1_000)).integerValue(BigNumber.ROUND_CEIL) ?? null;

        return { consumedGas, consumedMilligas };
    }

    private convertErrors(rpcData: TRpc, uid: string): Pick<FailedOperationResult, 'errors'> {
        const errors = this.operationErrorConverter.convertArrayProperty(rpcData, 'errors', uid);
        return { errors };
    }

    protected abstract convertAppliedResultParts(rpcData: TRpc, uid: string, options: TOptions): TResultParts;
}

const statusMapping: Readonly<Record<taquitoRpc.OperationResultStatusEnum, OperationResultStatus>> = {
    applied: OperationResultStatus.Applied,
    backtracked: OperationResultStatus.Backtracked,
    failed: OperationResultStatus.Failed,
    skipped: OperationResultStatus.Skipped,
};
