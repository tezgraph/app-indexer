import * as taquitoHttp from '@taquito/http-utils';
import {
    Clock,
    CLOCK_DI_TOKEN,
    hasConstructor,
    injectExplicit,
    replaceAll,
    Stopwatch,
    TaquitoHttpBackend,
    TaquitoHttpRequest,
} from '@tezos-dappetizer/utils';
import { inject, injectable } from 'tsyringe';

import { IpfsMetrics } from './ipfs-metrics';

@injectable()
export class IpfsMetricsHttpBackend extends TaquitoHttpBackend {
    constructor(
        @injectExplicit() private readonly nextBackend: TaquitoHttpBackend,
        private readonly metrics: IpfsMetrics,
        @inject(CLOCK_DI_TOKEN) private readonly clock: Clock,
    ) {
        super();
    }

    override async execute(request: TaquitoHttpRequest): Promise<unknown> {
        this.metrics.requestCount.inc();
        const stopwatch = new Stopwatch(this.clock);

        try {
            const result = await this.nextBackend.execute(request);

            this.reportCompleted(stopwatch, getStatus(taquitoHttp.STATUS_CODE.OK, 'OK'));
            return result;
        } catch (error) {
            const status = hasConstructor(error, taquitoHttp.HttpResponseError)
                ? getStatus(error.status, error.statusText)
                : 'Error';

            this.reportCompleted(stopwatch, status);
            throw error;
        }
    }

    private reportCompleted(stopwatch: Stopwatch, status: string): void {
        this.metrics.executionDuration.observe(stopwatch.elapsedMillis);
        this.metrics.responseCount.inc({ status });
    }
}

function getStatus(statusCode: taquitoHttp.STATUS_CODE, statusText: string): string {
    return `${statusCode}_${replaceAll(statusText, ' ', '')}`;
}
