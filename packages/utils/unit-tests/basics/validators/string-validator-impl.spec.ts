import { instance, mock, when } from 'ts-mockito';

import { describeMember, expectToThrow } from '../../../../../test-utilities/mocks';
import { ArgError } from '../../../src/basics/arg-error';
import { StringLimits, StringValidator } from '../../../src/basics/validators/string-validator';
import { StringValidatorImpl } from '../../../src/basics/validators/string-validator-impl';
import { TypeValidator } from '../../../src/basics/validators/type-validator';

describe(StringValidatorImpl.name, () => {
    let target: StringValidator;
    let typeValidator: TypeValidator;

    beforeEach(() => {
        typeValidator = mock();
        target = new StringValidatorImpl(instance(typeValidator));
    });

    describeMember<typeof target>('validate', () => {
        const act = (l?: StringLimits) => target.validate('raw', l);

        it.each<[string, StringLimits | undefined, string]>([
            ['regular', undefined, 'abc'],
            ['regular', 'NotEmpty', 'abc'],
            ['regular', 'NotWhiteSpace', 'abc'],
            ['empty', undefined, ''],
            ['white-space', 'NotEmpty', '  \t'],
        ])('should pass %s string with limits %s', (_desc, limits, value) => {
            when(typeValidator.validate('raw', 'string')).thenReturn(value);

            const result = act(limits);

            expect(result).toBe(value);
        });

        it.each<[string, string, StringLimits | undefined]>([
            ['empty string', '', 'NotEmpty'],
            ['empty string', '', 'NotWhiteSpace'],
            ['white-space chars', '  \t', 'NotWhiteSpace'],
        ])(`should throw error %s if value is '%s' with limits %s`, (expectedError, value, limits) => {
            when(typeValidator.validate('raw', 'string')).thenReturn(value);

            const error = expectToThrow(() => act(limits));

            expect(error.message).toContain(expectedError);
        });

        shouldThrowIfInvalidLimits(act);
    });

    describeMember<typeof target>('getExpectedValueDescription', () => {
        const act = (l?: StringLimits) => target.getExpectedValueDescription(l);

        it.each<[StringLimits | undefined, string]>([
            [undefined, 'a string'],
            ['NotEmpty', 'a non-empty string'],
            ['NotWhiteSpace', 'a non-white-space string'],
        ])('should construct correct value description for %s', (limits, expected) => {
            const desc = act(limits);

            expect(desc).toBe(expected);
        });

        shouldThrowIfInvalidLimits(act);
    });

    function shouldThrowIfInvalidLimits(act: (l?: StringLimits) => unknown) {
        it.each([
            ['null', null],
            ['not string', 123],
            ['unsupported string', 'wtf'],
        ])('should throw if limits are %s', (_desc, limits: any) => {
            const error = expectToThrow(() => act(limits), ArgError);

            expect(error.argName).toBe('limits');
            expect(error.specifiedValue).toBe(limits);
            expect(error.details).toInclude(`[undefined, 'NotEmpty', 'NotWhiteSpace']`);
        });
    }
});
