import { DbEntrypoint } from './db-entrypoint';

export interface MyContextData {
    readonly entrypoints: Partial<DbEntrypoint>[];
}
