# Tezos Dappetizer Utils package

`@tezos-dappetizer/utils` provides common utilities for [Dappetizer](https://dappetizer.dev). 

See the [top-level](https://gitlab.com/tezos-dappetizer/dappetizer) README for details on reporting issues, contributing, and versioning.

## API Documentation

[TypeDoc documentation](https://typedoc.dappetizer.dev/modules/_tezos_dappetizer_utils.html) is available with more details.

## Disclaimer

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
