import * as taquitoRpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { DeepReadonly } from 'ts-essentials';

import { LazyTransactionParameter } from './transaction-parameter';
import { TransactionResult } from './transaction-result';
import { UserTransactionDestination } from './user-transaction-destination';
import { LazyContract } from '../../common/lazy-contract/lazy-contract';
import { BaseMainOperationWithResult } from '../common/main-operation/base-main-operation-with-result';
import { OperationKind } from '../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcTransactionOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultTransaction>;

/**
 * It allows users to either transfer tez between accounts and/or to invoke a smart contract.
 * @category RPC Block Data
 */
export interface TransactionOperation extends BaseMainOperationWithResult<TaquitoRpcTransactionOperation> {
    readonly kind: OperationKind.Transaction;

    // Common transaction operation properties:

    /** It is mutez - greater than or equal to `0`. */
    readonly amount: BigNumber;

    /** @deprecated Use `destination.address` instead. */
    readonly destinationAddress: string;

    /** @deprecated Use `destination` instead. */
    readonly destinationContract: LazyContract | null;

    readonly destination: UserTransactionDestination | LazyContract;
    readonly transactionParameter: LazyTransactionParameter | null;
    readonly result: TransactionResult;
}
