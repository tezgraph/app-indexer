import * as taquitoRpc from '@taquito/rpc';
import { DependencyContainer, InjectionToken } from 'tsyringe';

import { TAQUITO_HTTP_BACKEND_DI_TOKEN, TAQUITO_RPC_CLIENT_DI_TOKEN } from './di-tokens';
import { AppShutdownHttpBackend } from './http-backends/app-shutdown-http-backend';
import { LoggingHttpBackend } from './http-backends/logging-http-backend';
import { MetricsHttpBackend } from './http-backends/metrics-http-backend';
import { RetryHttpBackend } from './http-backends/retry-http-backend';
import { TimeoutHttpBackend } from './http-backends/timeout-http-backend';
import { TaquitoHttpBackend } from './http-infrastructure/taquito-http-backend';
import { RpcBlockMetricsMonitor } from './monitor/block/rpc-block-metrics-monitor';
import { RPC_BLOCK_MONITOR_DI_TOKEN, RpcBlockMonitor } from './monitor/block/rpc-block-monitor';
import { RpcBlockMonitorDeserializer } from './monitor/block/rpc-block-monitor-deserializer';
import { DeserializeDataMonitor } from './monitor/generic/deserialize-data-monitor';
import { DownloadDataMonitor } from './monitor/generic/download-data-monitor';
import { ReconnectIfStuckMonitor } from './monitor/generic/reconnect-if-stuck-monitor';
import { ReconnectOnErrorMonitor } from './monitor/generic/reconnect-on-error-monitor';
import { RPC_MEMPOOL_MONITOR_DI_TOKEN, RpcMempoolMonitor } from './monitor/mempool/rpc-mempool-monitor';
import { RpcMempoolMonitorDeserializer } from './monitor/mempool/rpc-mempool-monitor-deserializer';
import { RpcHealthWatcher } from './rpc-health-watcher';
import { TezosNodeConfig } from './tezos-node-config';
import { DeduplicateItemsProvider } from '../basics/async-iterables/deduplicate-items-provider';
import { typed } from '../basics/public-api';
import { registerConfigWithDiagnostics } from '../configuration/public-api';
import { registerSingleton, resolveWithExplicitArgs } from '../dependency-injection/public-api';
import { registerCachedHealthCheck } from '../health/checks/cached-health-check-registration';
import { ComponentHealthState } from '../health/checks/component-health-state';
import { registerHealthCheck } from '../health/public-api';
import { resolveLogger } from '../logging/public-api';

export function registerTezosRpc(diContainer: DependencyContainer): void {
    // Public API:
    registerTaquitoHttpBackend(diContainer);
    registerTaquitoRpcClient(diContainer);
    registerRpcBlockMonitor(diContainer);
    registerRpcMempoolMonitor(diContainer);

    // Inject to Dappetizer features:
    registerRpcHealthWatcher(diContainer);
    registerConfigWithDiagnostics(diContainer, { useToken: TezosNodeConfig });
}

function registerTaquitoHttpBackend(diContainer: DependencyContainer): void {
    registerSingleton(diContainer, TAQUITO_HTTP_BACKEND_DI_TOKEN, {
        useFactory: c => {
            const backend = resolveWithExplicitArgs(c, LoggingHttpBackend, [
                resolveWithExplicitArgs(c, AppShutdownHttpBackend, [
                    new TaquitoHttpBackend(),
                ]),
            ]);
            return Object.freeze({
                createRequest: backend.createRequest.bind(backend),
                execute: backend.execute.bind(backend),
            });
        },
    });
}

function registerTaquitoRpcClient(diContainer: DependencyContainer): void {
    registerSingleton(diContainer, TAQUITO_RPC_CLIENT_DI_TOKEN, {
        useFactory: c => new taquitoRpc.RpcClient(
            diContainer.resolve(TezosNodeConfig).url,
            undefined,
            new TimeoutHttpBackend(
                diContainer.resolve(TezosNodeConfig).timeoutMillis,
                resolveWithExplicitArgs(c, RetryHttpBackend, [
                    resolveWithExplicitArgs(c, MetricsHttpBackend, [
                        c.resolve(TAQUITO_HTTP_BACKEND_DI_TOKEN),
                    ]),
                ]),
            ),
        ),
    });
}

function registerRpcHealthWatcher(diContainer: DependencyContainer): void {
    registerCachedHealthCheck(diContainer, c => ({
        cacheTimeMillis: c.resolve(TezosNodeConfig).health.resultCacheMillis,
        healthCheckToCache: resolveWithExplicitArgs(c, RpcHealthWatcher, [
            new taquitoRpc.RpcClient(
                c.resolve(TezosNodeConfig).url,
                undefined,
                new TimeoutHttpBackend(
                    c.resolve(TezosNodeConfig).health.timeoutMillis,
                    resolveWithExplicitArgs(c, AppShutdownHttpBackend, [
                        new TaquitoHttpBackend(),
                    ]),
                ),
            ),
        ]),
    }));
}

function registerRpcBlockMonitor(diContainer: DependencyContainer): void {
    const blockMonitorName = 'BlockTezosMonitor';

    const blockHealthDIToken = typed<InjectionToken<ComponentHealthState>>('Dappetizer:Utils:RpcBlockMonitorHealth');
    registerHealthCheck(diContainer, { useToken: blockHealthDIToken });
    registerSingleton(diContainer, blockHealthDIToken, {
        useFactory: c => resolveWithExplicitArgs(c, ComponentHealthState, [blockMonitorName]),
    });

    registerSingleton(diContainer, RPC_BLOCK_MONITOR_DI_TOKEN, {
        useFactory: c => {
            const monitor = resolveWithExplicitArgs(c, RpcBlockMetricsMonitor, [
                new DeduplicateItemsProvider(
                    { getItemId: block => block.hash, numberOfLastItemsToDeduplicate: 100 },
                    resolveWithExplicitArgs(c, ReconnectOnErrorMonitor, [
                        c.resolve(blockHealthDIToken),
                        resolveLogger(diContainer, `${blockMonitorName}-${ReconnectOnErrorMonitor.name}`),
                        new DeserializeDataMonitor(
                            resolveLogger(c, `${blockMonitorName}-${DeserializeDataMonitor.name}`),
                            c.resolve(RpcBlockMonitorDeserializer),
                            resolveWithExplicitArgs(c, ReconnectIfStuckMonitor, [
                                resolveLogger(c, `${blockMonitorName}-${ReconnectIfStuckMonitor.name}`),
                                resolveWithExplicitArgs(c, DownloadDataMonitor, [
                                    resolveLogger(c, `${blockMonitorName}-${DownloadDataMonitor.name}`),
                                    '/monitor/heads/main',
                                ]),
                            ]),
                        ),
                    ]),
                ),
            ]);
            return Object.freeze<RpcBlockMonitor>({
                iterate: monitor.iterate.bind(monitor),
            });
        },
    });
}

function registerRpcMempoolMonitor(diContainer: DependencyContainer): void {
    const mempoolMonitorName = 'MempoolTezosMonitor';

    const mempoolHealthDIToken = typed<InjectionToken<ComponentHealthState>>('Dappetizer:Utils:RpcMempoolMonitorHealth');
    registerHealthCheck(diContainer, { useToken: mempoolHealthDIToken });
    registerSingleton(diContainer, mempoolHealthDIToken, {
        useFactory: c => resolveWithExplicitArgs(c, ComponentHealthState, [mempoolMonitorName]),
    });

    registerSingleton(diContainer, RPC_MEMPOOL_MONITOR_DI_TOKEN, {
        useFactory: c => {
            const monitor = new DeduplicateItemsProvider(
                { getItemId: group => group.signature, numberOfLastItemsToDeduplicate: 3_000 },
                resolveWithExplicitArgs(c, ReconnectOnErrorMonitor, [
                    c.resolve(mempoolHealthDIToken),
                    resolveLogger(c, `${mempoolMonitorName}-${ReconnectOnErrorMonitor.name}`),
                    new DeserializeDataMonitor(
                        resolveLogger(c, `${mempoolMonitorName}-${DeserializeDataMonitor.name}`),
                        c.resolve(RpcMempoolMonitorDeserializer),
                        resolveWithExplicitArgs(c, ReconnectIfStuckMonitor, [
                            resolveLogger(c, `${mempoolMonitorName}-${ReconnectIfStuckMonitor.name}`),
                            resolveWithExplicitArgs(c, DownloadDataMonitor, [
                                resolveLogger(c, `${mempoolMonitorName}-${DownloadDataMonitor.name}`),
                                '/chains/main/mempool/monitor_operations',
                            ]),
                        ]),
                    ),
                ]),
            );
            return Object.freeze<RpcMempoolMonitor>({
                iterate: monitor.iterate.bind(monitor),
            });
        },
    });
}
