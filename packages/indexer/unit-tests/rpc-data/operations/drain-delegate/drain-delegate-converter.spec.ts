import { Writable } from 'ts-essentials';
import { instance, mock, when } from 'ts-mockito';

import { BalanceUpdateConverter } from '../../../../src/rpc-data/common/balance-update/converters/balance-update-converter';
import {
    DrainDelegateOperationConverter,
} from '../../../../src/rpc-data/operations/drain-delegate/drain-delegate-operation-converter';
import {
    BalanceUpdate,
    DrainDelegateOperation,
    OperationKind,
    TaquitoRpcDrainDelegateOperation,
} from '../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../rpc-test-helpers';

describe(DrainDelegateOperationConverter.name, () => {
    let target: DrainDelegateOperationConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;

    let rpcData: Writable<TaquitoRpcDrainDelegateOperation>;
    let balanceUpdates: BalanceUpdate[];

    const act = () => convertObject(target, rpcData, 'op/id');

    beforeEach(() => {
        balanceUpdateConverter = mock();
        target = new DrainDelegateOperationConverter(instance(balanceUpdateConverter));

        rpcData = {
            delegate: 'tz1dShXbbgJ4i1L6KMWAuuNdBPk5xCM1NRrU',
            destination: 'tz1PMWrbSda1RvsRi8Xq7GRd3P7739jtyaM8',
            consensus_key: 'ccc',
            metadata: {},
        } as typeof rpcData;
        balanceUpdates = 'mockedBalanceUpdates' as any;

        when(balanceUpdateConverter.convertFromMetadataProperty(rpcData, 'op/id')).thenReturn(balanceUpdates);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<DrainDelegateOperation>({
            allocatedDestinationContract: false,
            balanceUpdates,
            consensusKey: 'ccc',
            delegateAddress: 'tz1dShXbbgJ4i1L6KMWAuuNdBPk5xCM1NRrU',
            destinationAddress: 'tz1PMWrbSda1RvsRi8Xq7GRd3P7739jtyaM8',
            kind: OperationKind.DrainDelegate,
            rpcData,
            uid: 'op/id',
        });
        expect(operation).toBeFrozen();
    });

    it.each([
        [true, true],
        [false, false],
        [undefined, false],
    ])('should convert allocated_destination_contract %s to %s', (input, expected) => {
        rpcData.metadata = { allocated_destination_contract: input };

        const result = act();

        expect(result.allocatedDestinationContract).toBe(expected);
    });
});
