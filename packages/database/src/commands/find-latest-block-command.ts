import { EntityManager } from 'typeorm';

import { DbCommand } from './db-command';
import { DbBlock } from '../entities/db-block';

export class FindLatestBlockCommand implements DbCommand<DbBlock | null> {
    readonly description = 'find latest block';

    constructor() {
        Object.freeze(this);
    }

    async execute(dbManager: EntityManager): Promise<DbBlock | null> {
        return dbManager.getRepository(DbBlock)
            .findOne({ where: {}, order: { level: 'DESC' } });
    }
}
