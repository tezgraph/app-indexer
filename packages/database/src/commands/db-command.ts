import { EntityManager } from 'typeorm';

export interface DbCommand<TResult = void> {
    readonly description: string;

    execute(dbManager: EntityManager): Promise<TResult>;
}
