import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { AppliedOperationResult, OperationResult } from '../common/operation-result/operation-results';

/** @category RPC Block Data */
export type TaquitoRpcDelegationResult = DeepReadonly<taquitoRpc.OperationResultDelegation>;

/** @category RPC Block Data */
export type DelegationResult = OperationResult<AppliedDelegationResult>;

/** @category RPC Block Data */
export interface AppliedDelegationResult extends AppliedOperationResult<TaquitoRpcDelegationResult> {}
