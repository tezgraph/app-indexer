import { TestLogger } from '../../../../../test-utilities/mocks';
import {
    DEFAULT_GROUP_NAME,
    EndpointGroup,
    metricGroups,
    MetricsRequestGrouping,
} from '../../../src/tezos-rpc/http-backends/metrics-request-grouping';

describe(MetricsRequestGrouping.name, () => {
    let target: MetricsRequestGrouping;
    let logger: TestLogger;

    beforeEach(() => {
        logger = new TestLogger();
        target = new MetricsRequestGrouping(logger);
    });

    describe('should get correct group for path', () => {
        it.each(metricGroups)('%s', path => {
            const group = target.getGroup(`http://tezos.node${path}`);

            expect(group).toEqual<EndpointGroup>({
                name: path,
                host: 'tezos.node',
            });
            logger.verifyNothingLogged();
        });
    });

    it('should handle dynamic url', () => {
        const group = target.getGroup(`http://tezos.org/chains/main/blocks/BKzdG96cGf85MMVaXxKgsrshBnC5pjwBBLcUfp3m9DvNbfUrsZk`);

        expect(group).toEqual<EndpointGroup>({
            name: '/chains/main/blocks/*',
            host: 'tezos.org',
        });
        logger.verifyNothingLogged();
    });

    it('should return default group if unknown path', () => {
        const url = 'http://tezos.org/wtf/lol';

        const group = target.getGroup(url);

        expect(group).toEqual<EndpointGroup>({
            name: DEFAULT_GROUP_NAME,
            host: 'tezos.org',
        });
        logger.loggedSingle().verify('Warning', { url, defaultGroupName: DEFAULT_GROUP_NAME });
    });
});
